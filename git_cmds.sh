git config --global alias.loga "log --all --graph --decorate --oneline"
git config --global alias.lga "log --all --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --date=relative"
