// vim: syntax=cpp

	V3 { f32 x,y,z; }
	V3 point_A, point_B;
	point_A.{x,z} = point_B.{z,x};
	point_B.{x,z} = 0.0;

	f32 a,b,c,d;

	{b,c} = {a,b} + {c,d};
	// this is different from:
	b = a + c;
	c = b + d;
	// in fact it's the same as:
	f32 old_b = b;
	b = a + c;
	c = old_b + d;
	// so you can perform an exchange lua style:
	{a,b} = {b,a};

	Some_Structure
	{
		Another_Structure
		{
			f32 a;
			u32 b;
		}
		Another_Structure m,n;
	}
	Some_Structure q,r,s,t;
	{q,r}.{m,n}.a = {s,t}.{n,m}.a;


