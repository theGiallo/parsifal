#ifndef __UTF8_H__
#define __UTF8_H__ 1

#include "stdio.h"
#include "basic_types.h"

typedef
union
{
	u32 all;
	u8  subchars[4];
} UTF8_Fullchar;


// NOTE(theGiallo): a subchar is a u8

// NOTE(theGiallo):  IMPORTANT: n is repeated into the macro!
#define UTF8_COMPARE_LEFT_BYTES(a,b,n) ( (a) >> (n) == (b) >> (n) )

// NOTE(theGiallo): the mask determines the length of the full character
const u8 C_utf8_key_mask_1st_byte[4] = { (u8)0x0,
                                         (u8)0xC0,
                                         (u8)0xE0,
                                         (u8)0xF0 };
const u8 C_utf8_key_mask_1st_byte_left_bits_count[4] = { 7, 5, 4, 3 };
const u8 C_utf8_bits_mask_1st_byte[4] = { (u8)0x7F,
                                          (u8)0x1F,
                                          (u8)0xF,
                                          (u8)0x7 };
const u8 C_utf8_bits_mask_following_bytes = 0x3F;
// NOTE(theGiallo): bytes after the 1st have this mask on
const u8 C_utf8_key_mask_following_bytes = 0x80;
const u8 C_utf8_key_mask_following_left_bits_count = 6;

/*
   NOTE(theGiallo):
     returns the pointer into the given UTF-8 string to the character next to
     the given one.
     IMPORTANT: assumes the given one is a valid UTF-8 char position.
     IMPORTANT: does not check if inside_a_string is really inside.

     Returns NULL on not valid inside_a_string (not a valid 1st byte) or if
     inside_a_string character ends after buffer end.
     Returns base_string + bytes_size if inside_a_string character was the last
     one.
 */
u8 *
utf8_next_char_ptr( const u8 * const inside_a_string,
                    const u8 * const base_string, u64 bytes_size );

/*
   NOTE(theGiallo):
   returns the pointer into the given UTF-8 string to the character next to the
   given one.
   Do not assumes the given one is a valid UTF-8 char position.
   inside_a_string can be inside a full character.
   IMPORTANT: does not check if inside_a_string is really inside.
   IMPORTANT: does not check if the returned pointed subchar is a valid 1st.
 */
u8 *
utf8_first_next_char_ptr( const u8 * inside_a_string,
                          const u8 * base_string, u64 bytes_size );

/*
   NOTE(theGiallo):
   size is the byte size of the string buffer.
   Returns the number of full characters of the string.
 */
s32
#if __cplusplus
utf8_char_count( const u8 * base_string, u64 bytes_size, const bool null_terminated = false );
#else
utf8_char_count( const u8 * base_string, u64 bytes_size, const bool null_terminated );
#endif

UTF8_Fullchar
utf8_full_char( const u8 * first );

void
#if __cplusplus
utf8_print_char( const UTF8_Fullchar fullchar, FILE * out = stdout );
#else
utf8_print_char( const UTF8_Fullchar fullchar, FILE * out );
#endif

bool
utf8_is_1st_subchar( u8 subchar );

bool
utf8_is_mb_subchar( u8 subchar );

u32
utf8_fullchar_to_ucs4( UTF8_Fullchar fullchar );

#endif /* ifndef __UTF8_H__ */
