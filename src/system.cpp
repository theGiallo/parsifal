#include "system.h"
#include "macro_tools.h"

#include <stdio.h>

s64
get_file_size( char * file_path )
{
#if 0
	s64 ret = 0;

	FILE * fp = fopen( file_path, "rb" );
	if ( !fp )
	{
		return -SYS_ERR_FILE_OPEN_FAILED;
	}

	fseek( fp, 0, SEEK_END ); // NOTE(theGiallo): SEEK_END is non std, so ~non-portable
	ret = ftell ( fp );

	fclose( fp );

	return ret;
#else

	s64 ret = 0;

	struct stat buf;

	if ( stat( file_path, &buf ) < 0 )
	{
		ret = SYS_ERRNO_TO_SYS_ERROR( errno );
		return ret;
	}

	ret = buf.st_size;

	return ret;
#endif
}
s64
read_entire_file( char * file_path, void * mem, s64 mem_size )
{
	s64 ret = 0;

	FILE * fp = fopen( file_path, "rb" );
	if ( !fp )
	{
		return -SYS_ERR_FILE_OPEN_FAILED;
	}

	fseek( fp, 0, SEEK_END ); // NOTE(theGiallo): SEEK_END is non std, so ~non-portable
	s64 size  = ftell ( fp );
	rewind( fp );
	ret = fread( mem, 1, MIN( size, mem_size ), fp );
	if ( size > mem_size )
	{
		// TODO(theGiallo, 2016-03-08): sth
		log_info( "warning: reading file '%s' of size %ld into a smaller buffer of %ld", file_path, size, mem_size );
	}
	if ( ret!=MIN( size, mem_size ) )
	{
		// TODO(theGiallo, 2016-03-08): sth
	}

	fclose ( fp );

	return ret;
}
#if LINUX
struct
Statm
{
	u64 size,
	    resident,
	    share,
	    text,
	    lib,
	    data,
	    dt;
};

internal
bool
linux_read_statm( Statm * result )
{
	bool ret = false;
	const char * statm_path = "/proc/self/statm";

	FILE *f = fopen( statm_path,"r" );
	if ( !f )
	{
		perror( statm_path );
	}  else
	{

		if ( 7 != fscanf( f, "%ld %ld %ld %ld %ld %ld %ld",
		                  &result->size,
		                  &result->resident,
		                  &result->share,
		                  &result->text,
		                  &result->lib,
		                  &result->data,
		                  &result->dt ) )
		{
			perror( statm_path );
		} else
		{
			ret = true;
		}

		fclose( f );
	}

	return ret;
}

Proc_Mem_Info
sys_get_proc_mem_info()
{
	Proc_Mem_Info ret = {};

	Statm statm = {};
	bool res_b = linux_read_statm( &statm );
	if ( res_b )
	{
		ret.bytes_virtual  = statm.size;
		ret.bytes_physical = statm.resident;
	}

	return ret;
}
#elif WINDOWS
#include <Psapi.h>
Proc_Mem_Info
sys_get_proc_mem_info()
{
	Proc_Mem_Info ret = {};

	HANDLE                  Process;
	PROCESS_MEMORY_COUNTERS_EX psmemCounters = {};

	DWORD processID = GetCurrentProcessId();
	Process = OpenProcess(  PROCESS_QUERY_INFORMATION |
	                        PROCESS_VM_READ,
	                        FALSE,
	                        processID );
	bool res_b =
	   GetProcessMemoryInfo( Process,
	                         &psmemCounters, sizeof( psmemCounters ) );
	if ( res_b )
	{
		ret.bytes_virtual  = psmemCounters.WorkingSetSize;
		ret.bytes_physical = psmemCounters.PrivateUsage;
	}

	CloseHandle( Process );

	return ret;
}
#endif
