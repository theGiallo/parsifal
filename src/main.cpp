#include <stdlib.h> // NOTE(theGiallo): malloc

#include "basic_types.h"
#include "macro_tools.h"
#include "tgmath.h"
#include "utility.h"
#include "system.h"
#include "parser.h"
#include "pool_allocator_generic.h"
#include "freelist_allocator.h"

s32
main( s32 argc, char ** argv )
{
	char * filename = NULL;
	for ( s32 i = 0; i != argc; ++i )
	{
		char * a = argv[i];
		log_info( "argument[%02d]: '%s'", i, a );
		filename = a;
	}

	s64 file_size = get_file_size( filename );
	if ( SYS_ERR_IS_ERRNO( file_size ) )
	{
		log_err( "failed to read file size of '%s'", filename );
		return 1;
	}
	file_size += 1;
	void * file_mem = malloc( file_size );
	if ( !file_mem )
	{
		log_err( "failed to allocate %luB for file '%s'", file_size, filename );
		return 1;
	}
	((u8*)file_mem)[file_size-1] = 0;
	s64 read = read_entire_file( filename, file_mem, file_size );
	if ( SYS_ERR_IS_ERRNO( read ) )
	{
		log_err( "failed to read file '%s'", filename );
		return 1;
	}

	printf( "read file:\n" );
	printf( "%s", file_mem );
	printf( "--------------------------------------------------------------------------------\n" );

	u64 start_ns = ns_time();

	Parser parser = {};
	parser_init( &parser );
	u64 after_init_ns = ns_time();
	parser_parse_file( &parser, file_mem, file_size );

	u64 end_ns = ns_time();

	parser_print_debug_parsed_elements( &parser );
	printf( "--------------------------------------------------------------------------------\n" );
	printf( "AST:\n\n" );
	parser_print_debug_ast( &parser );

	printf( "took %luns (or %fs) to initialize the parser\n%luns (or %fs) to parse the file\nfor a total of %luns (or %fs)\n",
	          after_init_ns - start_ns,
	        ( after_init_ns - start_ns ) * 1e-9,
	          end_ns - after_init_ns,
	        ( end_ns - after_init_ns ) * 1e-9,
	          end_ns - start_ns,
	        ( end_ns - start_ns ) * 1e-9 );

	return 0;
}

#include "tgmath.cpp"
#include "utility.cpp"
#include "system.cpp"
#include "parser.cpp"
#include "freelist_allocator.cpp"
#include "pool_allocator_generic.cpp"
#include "pool_allocator_generic_vm.cpp"
#include "utf8.cpp"
