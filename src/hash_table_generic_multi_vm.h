#ifndef _HASH_TABLE_GENERIC_MULTI_VM_H_
#define _HASH_TABLE_GENERIC_MULTI_VM_H_ 1

#include "basic_types.h"
#include "macro_tools.h"
#include "system.h"
#include "pool_allocator_generic_vm.h"

// NOTE(theGiallo): this hash table stores the k/v by pointer. It's the only
// way to be able to keep it non typed. If you need/want to k/v elements by
// value use the macro typed one. (when and if it's implemented)

// TODO(theGiallo, 2018-04-10): implement a mixed hash table in which each key has it's own size
// TODO(theGiallo, 2018-04-10): implement resident list ( that has first k/v in struct List )
// TODO(theGiallo, 2018-04-14): implement a set ds that has only value and no key

struct
Hash_Table_Generic_Multi_VM_Element
{
	u64    hash;
	void * key;
	void * value;
};

#define LIST_VM_TYPE Hash_Table_Generic_Multi_VM_Element
#define LIST_VM_TYPE_SHORT HTGMVM_Element
#include "list_vm.inc.h"

struct
Hash_Table_Generic_Multi_VM_Multi_Element
{
	u64    hash;
	void * key;
	HTGMVM_Element_List_VM list;
};

#define LIST_VM_TYPE Hash_Table_Generic_Multi_VM_Multi_Element
#define LIST_VM_TYPE_SHORT HTGMVM_M_Element
#include "list_vm.inc.h"

struct
Hash_Table_Generic_Multi_VM
{
	Pool_Allocator_Generic_VM * element_list_nodes_pool_p;
	Pool_Allocator_Generic_VM   element_list_nodes_pool;
	Pool_Allocator_Generic_VM * m_element_list_nodes_pool_p;
	Pool_Allocator_Generic_VM   m_element_list_nodes_pool;
	u64                         occupancy;
	// NOTE(theGiallo): element_size is used to compute the hash value.
	// Hash is computed using FNV1a on the whole memory of the element, thus the
	// padding inserted by the compiler could make two identical struct have a
	// different hash. IMPORTANT initialize every struct to {} so this won't
	// happen!
	// NOTE(theGiallo): if you want to use a string as key you can set key_size
	// to -1 and the hashing will be performed assuming a null terminated
	// string. Remember to use UTF-8 encoding (or ASCII).
	s64                         key_size;
	HTGMVM_M_Element_List_VM *  table;
	u32                         table_length;
	bool                        table_is_extern;
};

bool
hash_table_generic_multi_vm_init( Hash_Table_Generic_Multi_VM * ht,
                                  s64 key_size,
                                  u64 max_capacity = 512,
                                  u32 table_length = 1023,
                                  HTGMVM_M_Element_List_VM * table = 0,
                                  Pool_Allocator_Generic_VM * element_list_nodes_pool_p   = 0,
                                  Pool_Allocator_Generic_VM * m_element_list_nodes_pool_p = 0 );

void
hash_table_generic_multi_vm_destroy( Hash_Table_Generic_Multi_VM * ht );

bool
hash_table_generic_multi_vm_insert( Hash_Table_Generic_Multi_VM * ht, void * key, void * value );

bool
hash_table_generic_multi_vm_insert_unique_or_get_all( Hash_Table_Generic_Multi_VM * ht, void * key, void * value, HTGMVM_Element_List_VM ** out_values_list );

bool
hash_table_generic_multi_vm_insert_unique( Hash_Table_Generic_Multi_VM * ht, void * key, void * value );

bool
hash_table_generic_multi_vm_get_all( Hash_Table_Generic_Multi_VM * ht, void * key, HTGMVM_Element_List_VM ** out_values_list );

bool
hash_table_generic_multi_vm_remove_and_get_all( Hash_Table_Generic_Multi_VM * ht, void * key, HTGMVM_Element_List_VM * out_values_list );

bool
hash_table_generic_multi_vm_remove_all( Hash_Table_Generic_Multi_VM * ht, void * key );

#endif /* ifndef _HASH_TABLE_GENERIC_MULTI_VM_H_ */
