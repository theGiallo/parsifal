#ifndef TGMATH_H
#define TGMATH_H 1

#include "basic_types.h"
#include "stdio.h"
#include <cmath>

#ifdef LPR_HEADER
#define mx_abort() LPR_ILLEGAL_PATH()
#else
#ifdef __MACRO_TOOLS_H__
#define mx_abort() ILLEGAL_PATH()
#else
#include <stdlib.h>
#define mx_abort() abort()
#endif
#endif
#include "mx_template.h"
#include <string.h>

#define E_F 2.7182818284590452353602874713526624977572470936999595749f
#define TAU_F 6.283185307179586476925286766559f
#define PI_F 3.141592653589793238462643383279f
#define H_PI_F ( 3.141592653589793238462643383279f / 2.0f )
#define SQRT2_F 1.41421356237309504880168872420969807856967187537694807317667973799f
#define SQRT3_F 1.732050807568877293527446341505872366942805253810380628055806f
#define _1_OVER_SQRT3_F 0.577350269189625764509148780501957455647601751270126876018602f

inline
f32
mod1( f32 f )
{
	f32 ret, i;
	ret = modff( f, &i );
	return ret;
}
inline
f32
modz( f32 f, f32 z )
{
	f32 ret, i;
	ret = modff( f / z, &i ) * z;
	return ret;
}

inline
u32
squareu( u32 x )
{
	u32 ret = x * x;
	return ret;
}
inline
u32
cubeu( u32 x )
{
	u32 ret = x * x * x;
	return ret;
}

inline
s64
squares64( s64 x )
{
	s64 ret = x * x;
	return ret;
}
inline
u64
squareu64( u64 x )
{
	u64 ret = x * x;
	return ret;
}
inline
f32
square( f32 x )
{
	f32 ret = x * x;
	return ret;
}
inline
f32
cube( f32 x )
{
	f32 ret = x * x * x;
	return ret;
}

inline
f32
deg2rad( f32 a )
{
	f32 ret = a * PI_F / 180.0f;
	return ret;
}

inline
f32
rad2deg( f32 a )
{
	f32 ret = a * 180.0f / PI_F ;
	return ret;
}

union
V2
{
	struct
	{
		f32 x,y;
	};
	struct
	{
		f32 r,g;
	};
	struct
	{
		f32 w,h;
	};
	struct
	{
		f32 u,v;
	};
	struct
	{
		f32 arr[2];
	};
	Mx<f32,1,2> row_vector;
	Mx<f32,2,1> col_vector;

	#ifdef LPR_HEADER
	inline
	operator Lpr_V2() const
	{
		return Lpr_V2{x,y};
	}
	#endif
};

#ifdef LPR_HEADER
inline
V2
make_V2( Lpr_V2 lv )
{
	V2 ret = {lv.x, lv.y };
	return ret;
}
inline
V2
make_V2( Lpr_V2u32 lv )
{
	V2 ret = V2{(f32)lv.x, (f32)lv.y };
	return ret;
}
#endif
inline
V2
make_V2( f32 f )
{
	V2 ret = {f,f};
	return ret;
}


union
V2u32
{
	struct
	{
		u32 x,y;
	};
	struct
	{
		u32 r,g;
	};
	struct
	{
		u32 w,h;
	};
	struct
	{
		u32 u,v;
	};
	struct
	{
		u32 arr[2];
	};

	inline
	explicit operator V2 () const
	{
		V2 ret = {(f32)x,(f32)y};
		return ret;
	}
	#ifdef LPR_HEADER
	inline
	operator Lpr_V2u32() const
	{
		return Lpr_V2u32{x,y};
	}
	#endif
};
#ifdef LPR_HEADER
inline
V2u32
make_V2u32( Lpr_V2u32 lv )
{
	V2u32 ret = {lv.x, lv.y };
	return ret;
}
#endif

inline
V2u32
make_V2u32( V2 v )
{
	V2u32 ret = {(u32)v.x,(u32)v.y};
	return ret;
}

union
V2s32
{
	struct
	{
		s32 x,y;
	};
	struct
	{
		s32 r,g;
	};
	struct
	{
		s32 w,h;
	};
	struct
	{
		s32 u,v;
	};
	struct
	{
		s32 arr[2];
	};

	inline
	explicit operator V2 () const
	{
		V2 ret = {(f32)x,(f32)y};
		return ret;
	}
};

inline
V2s32
make_V2s32( V2 v )
{
	V2s32 ret = {(s32)v.x,(s32)v.y};
	return ret;
}
inline
V2s32
make_V2s32( V2u32 v )
{
	V2s32 ret = {(s32)v.x,(s32)v.y};
	return ret;
}
inline
V2u32
make_V2u32( V2s32 v )
{
	V2u32 ret = {(u32)v.x,(u32)v.y};
	return ret;
}


union
V3
{
	struct
	{
		f32 x,y,z;
	};
	struct
	{
		f32 r,g,b;
	};
	struct
	{
		f32 hue,sat,val;
	};
	struct
	{
		f32 w,h,d;
	};
	struct
	{
		f32 xyz[3];
	};
	struct
	{
		f32 rgb[3];
	};
	struct
	{
		f32 hsv[3];
	};
	struct
	{
		f32 arr[3];
	};
	struct
	{
		V2 xy;
		f32 _z;
	};
	struct
	{
		f32 _x;
		V2 yz;
	};
	struct
	{
		V2 rg;
		f32 _b;
	};
	struct
	{
		f32 _r;
		V2 gb;
	};
	struct
	{
		V2 hs;
		f32 _v;
	};
	struct
	{
		f32 _h;
		V2 sv;
	};
	Mx<f32,1,3> row_vector;
	Mx<f32,3,1> col_vector;
};

union
V3s32
{
	struct
	{
		s32 x,y,z;
	};
	struct
	{
		s32 r,g,b;
	};
	struct
	{
		s32 hue,sat,val;
	};
	struct
	{
		s32 w,h,d;
	};
	struct
	{
		s32 xyz[3];
	};
	struct
	{
		s32 rgb[3];
	};
	#ifdef LPR_HEADER
	struct
	{
		Lpr_Rgb_f32 lpr_rgb;
	};
	#endif
	struct
	{
		s32 hsv[3];
	};
	struct
	{
		s32 arr[3];
	};
	struct
	{
		V2s32 xy;
		s32 _z;
	};
	struct
	{
		s32 _x;
		V2s32 yz;
	};
	struct
	{
		V2s32 rg;
		s32 _b;
	};
	struct
	{
		s32 _r;
		V2s32 gb;
	};
	struct
	{
		V2s32 hs;
		s32 _v;
	};
	struct
	{
		s32 _h;
		V2s32 sv;
	};
	inline
	explicit operator V3 () const
	{
		V3 ret = {(f32)x,(f32)y,(f32)z};
		return ret;
	}
};
inline
V3s32
make_V3s32( V3 v )
{
	V3s32 ret = {(s32)v.x,(s32)v.y,(s32)v.z};
	return ret;
}


union
V4
{
	struct
	{
		f32 x,y,z,w;
	};
	struct
	{
		f32 r,g,b,a;
	};
	struct
	{
		f32 hue,sat,val,_a;
	};
	struct
	{
		f32 xyzw[4];
	};
	struct
	{
		f32 rgba[4];
	};
	#ifdef LPR_HEADER
	struct
	{
		Lpr_Rgba_f32 lpr_rgba;
	};
	#endif
	struct
	{
		f32 hsva[4];
	};
	struct
	{
		f32 arr[4];
	};
	struct
	{
		V3 xyz;
		f32 _w;
	};
	struct
	{
		f32 _x;
		V3 yzw;
	};
	struct
	{
		V2 xy;
		V2 zw;
	};
	struct
	{
		f32 __x;
		V2 yz;
		f32 __w;
	};
	struct
	{
		V2 rg;
		V2 ba;
	};
	struct
	{
		f32 _r;
		V2 gb;
		f32 __a;
	};
	struct
	{
		V2 hs;
		V2 va;
	};
	struct
	{
		f32 _hue;
		V2 sv;
		f32 ___a;
	};
	Mx<f32,1,4> row_vector;
	Mx<f32,4,1> col_vector;
};

union
V4s32
{
	struct
	{
		s32 x,y,z,w;
	};
	struct
	{
		s32 r,g,b,a;
	};
	struct
	{
		s32 hue,sat,val,_a;
	};
	struct
	{
		s32 xyzw[4];
	};
	struct
	{
		s32 rgba[4];
	};
	struct
	{
		s32 hsva[4];
	};
	struct
	{
		s32 arr[4];
	};
	struct
	{
		V2s32 xy;
		V2s32 zw;
	};
	struct
	{
		s32 _x;
		V2s32 yz;
		s32 _w;
	};
	struct
	{
		V2s32 rg;
		V2s32 ba;
	};
	struct
	{
		s32 _r;
		V2s32 gb;
		s32 __a;
	};
	struct
	{
		V2s32 hs;
		V2s32 va;
	};
	struct
	{
		s32 _hue;
		V2s32 sv;
		s32 ___a;
	};
	inline
	explicit operator V4 () const
	{
		V4 ret = {(f32)x,(f32)y,(f32)z,(f32)w};
		return ret;
	}
};
inline
V4s32
make_V4s32( V4 v )
{
	V4s32 ret = {(s32)v.x,(s32)v.y,(s32)v.z,(s32)v.w};
	return ret;
}

struct
Quat
{
	f32 w, x, y, z;
};

inline
Quat
make_Quat_no_rotation()
{
	Quat ret = {
	   .w = 1.0f,
	   .x = 0.0f,
	   .y = 0.0f,
	   .z = 0.0f
	};
	return ret;
}

inline
Quat
make_Quat_from_axis_angle_rad( V3 axis, f32 angle_rad )
{
	Quat ret;

	f32 h_angle_rad = angle_rad * 0.5f;
	f32 s = sinf( h_angle_rad );
	ret = {
	   .w = cosf( h_angle_rad ),
	   .x = axis.x * s,
	   .y = axis.y * s,
	   .z = axis.z * s,
	};

	return ret;
}

inline
Quat
make_Quat_from_axis_angle_deg( V3 axis, f32 angle_deg )
{
	Quat ret = make_Quat_from_axis_angle_rad( axis, deg2rad( angle_deg ) );
	return ret;
}

inline
Quat
operator * ( Quat q1, Quat q2 )
{
	Quat ret = {
	   .w = q1.w * q2.w - q1.x * q2.x - q1.y * q2.y - q1.z * q2.z,
	   .x = q1.w * q2.w + q1.x * q2.x + q1.y * q2.y - q1.z * q2.z,
	   .y = q1.w * q2.y - q1.x * q2.z + q1.y * q2.w + q1.z * q2.x,
	   .z = q1.w * q2.z + q1.x * q2.y - q1.y * q2.x + q1.z * q2.w,
	};

	return ret;
}

inline
f32
length( Quat q )
{
	f32 ret =
	   sqrtf(
	      square( q.w ) +
	      square( q.x ) +
	      square( q.y ) +
	      square( q.z ) );
	return ret;
}

inline
Quat
normalize( Quat q )
{
	Quat ret = q;

	f32 l = length( q );
	ret.w /= l;
	ret.x /= l;
	ret.y /= l;
	ret.z /= l;

	return ret;
}


typedef Mx<f32,4,4> Mx4;
typedef Mx<f32,3,3> Mx3;
typedef Mx<f32,2,2> Mx2;
typedef Mx<f32,2,3> Mx23;
typedef Mx<f32,3,2> Mx32;
typedef Mx<f32,3,4> Mx34;
typedef Mx<f32,4,3> Mx43;

inline
Mx4
mx4_identity()
{
	Mx4 ret = Mx_identity<float,4,4>();

	return ret;
}

inline
Mx3
mx3_identity()
{
	Mx3 ret = Mx_identity<float,3,3>();

	return ret;
}

inline
bool
operator == ( V2u32 v0, V2u32 v1 )
{
	bool ret = v0.x == v1.x && v0.y == v1.y;
	return ret;
}
inline
bool
operator == ( V2s32 v0, V2s32 v1 )
{
	bool ret = v0.x == v1.x && v0.y == v1.y;
	return ret;
}
inline
bool
operator == ( V2 v0, V2 v1 )
{
	bool ret = v0.x == v1.x && v0.y == v1.y;
	return ret;
}
inline
bool
operator == ( V3 v0, V3 v1 )
{
	bool ret = v0.x == v1.x && v0.y == v1.y && v0.z == v1.z;
	return ret;
}
inline
bool
operator == ( V3s32 v0, V3s32 v1 )
{
	bool ret = v0.x == v1.x && v0.y == v1.y && v0.z == v1.z;
	return ret;
}
inline
bool
operator == ( V4 v0, V4 v1 )
{
	bool ret = v0.x == v1.x && v0.y == v1.y && v0.z == v1.z && v0.w == v1.w;
	return ret;
}
inline
bool
operator == ( V4s32 v0, V4s32 v1 )
{
	bool ret = v0.x == v1.x && v0.y == v1.y && v0.z == v1.z && v0.w == v1.w;
	return ret;
}
inline
bool
operator != ( V2u32 v0, V2u32 v1 )
{
	bool ret = v0.x != v1.x || v0.y != v1.y;
	return ret;
}
inline
bool
operator != ( V2s32 v0, V2s32 v1 )
{
	bool ret = v0.x != v1.x || v0.y != v1.y;
	return ret;
}
inline
bool
operator != ( V2 v0, V2 v1 )
{
	bool ret = v0.x != v1.x || v0.y != v1.y;
	return ret;
}
inline
bool
operator != ( V3 v0, V3 v1 )
{
	bool ret = v0.x != v1.x || v0.y != v1.y || v0.z != v1.z;
	return ret;
}
inline
bool
operator != ( V3s32 v0, V3s32 v1 )
{
	bool ret = v0.x != v1.x || v0.y != v1.y || v0.z != v1.z;
	return ret;
}
inline
bool
operator != ( V4 v0, V4 v1 )
{
	bool ret = v0.x != v1.x || v0.y != v1.y || v0.z != v1.z || v0.w != v1.w;
	return ret;
}
inline
bool
operator != ( V4s32 v0, V4s32 v1 )
{
	bool ret = v0.x != v1.x || v0.y != v1.y || v0.z != v1.z || v0.w != v1.w;
	return ret;
}

inline
V2u32
operator + ( V2u32 v, u32 u )
{
	V2u32 ret = {v.x + u, v.y + u};
	return ret;
}
inline
V2s32
operator + ( V2s32 v, s32 s )
{
	V2s32 ret = {v.x + s, v.y + s};
	return ret;
}
inline
V2
operator + ( V2 v, f32 f )
{
	V2 ret = {v.x + f, v.y + f};
	return ret;
}
inline
V3
operator + ( V3 v, f32 f )
{
	V3 ret = {v.x + f, v.y + f, v.z + f};
	return ret;
}
inline
V3s32
operator + ( V3s32 v, s32 s )
{
	V3s32 ret = {v.x + s, v.y + s, v.z + s};
	return ret;
}
inline
V4
operator + ( V4 v, f32 f )
{
	V4 ret = {v.x + f, v.y + f, v.z + f, v.w + f};
	return ret;
}
inline
V4s32
operator + ( V4s32 v, s32 s )
{
	V4s32 ret = {v.x + s, v.y + s, v.z + s, v.w + s};
	return ret;
}
inline
V2u32
operator + ( V2u32 v0, V2u32 v1 )
{
	V2u32 ret = {v0.x + v1.x, v0.y + v1.y};
	return ret;
}
inline
V2s32
operator + ( V2s32 v0, V2s32 v1 )
{
	V2s32 ret = {v0.x + v1.x, v0.y + v1.y};
	return ret;
}
inline
V2
operator + ( V2 v0, V2 v1 )
{
	V2 ret = {v0.x + v1.x, v0.y + v1.y};
	return ret;
}
inline
V3
operator + ( V3 v0, V3 v1 )
{
	V3 ret = {v0.x + v1.x, v0.y + v1.y, v0.z + v1.z};
	return ret;
}
inline
V3s32
operator + ( V3s32 v0, V3s32 v1 )
{
	V3s32 ret = {v0.x + v1.x, v0.y + v1.y, v0.z + v1.z};
	return ret;
}
inline
V4
operator + ( V4 v0, V4 v1 )
{
	V4 ret = {v0.x + v1.x, v0.y + v1.y, v0.z + v1.z, v0.w + v1.w};
	return ret;
}
inline
V4s32
operator + ( V4s32 v0, V4s32 v1 )
{
	V4s32 ret = {v0.x + v1.x, v0.y + v1.y, v0.z + v1.z, v0.w + v1.w};
	return ret;
}
inline
V2u32 &
operator += ( V2u32 & v0, const V2u32 & v1 )
{
	V2u32 & ret = v0;
	ret = {v0.x + v1.x, v0.y + v1.y};
	return ret;
}
inline
V2s32 &
operator += ( V2s32 & v0, const V2s32 & v1 )
{
	V2s32 & ret = v0;
	ret = {v0.x + v1.x, v0.y + v1.y};
	return ret;
}
inline
V2 &
operator += ( V2 & v0, const V2 & v1 )
{
	V2 & ret = v0;
	ret = {v0.x + v1.x, v0.y + v1.y};
	return ret;
}
inline
V3 &
operator += ( V3 & v0, const V3 & v1 )
{
	V3 & ret = v0;
	ret = {v0.x + v1.x, v0.y + v1.y, v0.z + v1.z};
	return ret;
}
inline
V3s32 &
operator += ( V3s32 & v0, const V3s32 & v1 )
{
	V3s32 & ret = v0;
	ret = {v0.x + v1.x, v0.y + v1.y, v0.z + v1.z};
	return ret;
}
inline
V4 &
operator += ( V4 & v0, const V4 & v1 )
{
	V4 & ret = v0;
	ret = {v0.x + v1.x, v0.y + v1.y, v0.z + v1.z, v0.w + v1.w};
	return ret;
}
inline
V4s32 &
operator += ( V4s32 & v0, const V4s32 & v1 )
{
	V4s32 & ret = v0;
	ret = {v0.x + v1.x, v0.y + v1.y, v0.z + v1.z, v0.w + v1.w};
	return ret;
}
inline
V2u32 &
operator += ( V2u32 & v, u32 u )
{
	V2u32 & ret = v;
	ret = {v.x + u, v.y + u};
	return ret;
}
inline
V2s32 &
operator += ( V2s32 & v, s32 s )
{
	V2s32 & ret = v;
	ret = {v.x + s, v.y + s};
	return ret;
}
inline
V2 &
operator += ( V2 & v, f32 f )
{
	V2 & ret = v;
	ret = {v.x + f, v.y + f};
	return ret;
}
inline
V3 &
operator += ( V3 & v, f32 f )
{
	V3 & ret = v;
	ret = {v.x + f, v.y + f, v.z + f};
	return ret;
}
inline
V3s32 &
operator += ( V3s32 & v, s32 s )
{
	V3s32 & ret = v;
	ret = {v.x + s, v.y + s, v.z + s};
	return ret;
}
inline
V4 &
operator += ( V4 & v, f32 f )
{
	V4 & ret = v;
	ret = {v.x + f, v.y + f, v.z + f, v.w + f};
	return ret;
}
inline
V4s32 &
operator += ( V4s32 & v, s32 s )
{
	V4s32 & ret = v;
	ret = {v.x + s, v.y + s, v.z + s, v.w + s};
	return ret;
}
inline
V2u32
operator - ( V2u32 v )
{
	V2u32 ret;
	ret.x = -v.x;
	ret.y = -v.y;
	return ret;
}
inline
V2s32
operator - ( V2s32 v )
{
	V2s32 ret;
	ret.x = -v.x;
	ret.y = -v.y;
	return ret;
}
inline
V2
operator - ( V2 v )
{
	V2 ret;
	ret.x = -v.x;
	ret.y = -v.y;
	return ret;
}
inline
V3
operator - ( V3 v )
{
	V3 ret;
	ret.x = -v.x;
	ret.y = -v.y;
	ret.z = -v.z;
	return ret;
}
inline
V3s32
operator - ( V3s32 v )
{
	V3s32 ret;
	ret.x = -v.x;
	ret.y = -v.y;
	ret.z = -v.z;
	return ret;
}
inline
V4
operator - ( V4 v )
{
	V4 ret;
	ret.x = -v.x;
	ret.y = -v.y;
	ret.z = -v.z;
	ret.w = -v.w;
	return ret;
}
inline
V4s32
operator - ( V4s32 v )
{
	V4s32 ret;
	ret.x = -v.x;
	ret.y = -v.y;
	ret.z = -v.z;
	ret.w = -v.w;
	return ret;
}
inline
V2u32
operator - ( V2u32 v, u32 u )
{
	V2u32 ret = {v.x - u, v.y - u};
	return ret;
}
inline
V2s32
operator - ( V2s32 v, s32 s )
{
	V2s32 ret = {v.x - s, v.y - s};
	return ret;
}
inline
V2
operator - ( V2 v, f32 f )
{
	V2 ret = {v.x - f, v.y - f};
	return ret;
}
inline
V3
operator - ( V3 v, f32 f )
{
	V3 ret = {v.x - f, v.y - f, v.z - f};
	return ret;
}
inline
V3s32
operator - ( V3s32 v, s32 s )
{
	V3s32 ret = {v.x - s, v.y - s, v.z - s};
	return ret;
}
inline
V4
operator - ( V4 v, f32 f )
{
	V4 ret = {v.x - f, v.y - f, v.z - f, v.w - f};
	return ret;
}
inline
V4s32
operator - ( V4s32 v, s32 s )
{
	V4s32 ret = {v.x - s, v.y - s, v.z - s, v.w - s};
	return ret;
}
inline
V2u32
operator - ( V2u32 v0, V2u32 v1 )
{
	V2u32 ret = {v0.x - v1.x, v0.y - v1.y};
	return ret;
}
inline
V2s32
operator - ( V2s32 v0, V2s32 v1 )
{
	V2s32 ret = {v0.x - v1.x, v0.y - v1.y};
	return ret;
}
inline
V2
operator - ( V2 v0, V2 v1 )
{
	V2 ret = {v0.x - v1.x, v0.y - v1.y};
	return ret;
}
inline
V3
operator - ( V3 v0, V3 v1 )
{
	V3 ret = {v0.x - v1.x, v0.y - v1.y, v0.z - v1.z};
	return ret;
}
inline
V3s32
operator - ( V3s32 v0, V3s32 v1 )
{
	V3s32 ret = {v0.x - v1.x, v0.y - v1.y, v0.z - v1.z};
	return ret;
}
inline
V4
operator - ( V4 v0, V4 v1 )
{
	V4 ret = {v0.x - v1.x, v0.y - v1.y, v0.z - v1.z, v0.w - v1.w};
	return ret;
}
inline
V4s32
operator - ( V4s32 v0, V4s32 v1 )
{
	V4s32 ret = {v0.x - v1.x, v0.y - v1.y, v0.z - v1.z, v0.w - v1.w};
	return ret;
}
inline
V2u32 &
operator -= ( V2u32 & v, u32 u )
{
	V2u32 & ret = v;
	ret = {v.x - u, v.y - u};
	return ret;
}
inline
V2s32 &
operator -= ( V2s32 & v, s32 s )
{
	V2s32 & ret = v;
	ret = {v.x - s, v.y - s};
	return ret;
}
inline
V2 &
operator -= ( V2 & v, f32 f )
{
	V2 & ret = v;
	ret = {v.x - f, v.y - f};
	return ret;
}
inline
V3 &
operator -= ( V3 & v, f32 f )
{
	V3 & ret = v;
	ret = {v.x - f, v.y - f, v.z - f};
	return ret;
}
inline
V3s32 &
operator -= ( V3s32 & v, s32 s )
{
	V3s32 & ret = v;
	ret = {v.x - s, v.y - s, v.z - s};
	return ret;
}
inline
V4 &
operator -= ( V4 & v, f32 f )
{
	V4 & ret = v;
	ret = {v.x - f, v.y - f, v.z - f, v.w - f};
	return ret;
}
inline
V4s32 &
operator -= ( V4s32 & v, s32 s )
{
	V4s32 & ret = v;
	ret = {v.x - s, v.y - s, v.z - s, v.w - s};
	return ret;
}
inline
V2u32 &
operator -= ( V2u32 & v0, const V2u32 & v1 )
{
	V2u32 & ret = v0;
	ret = {v0.x - v1.x, v0.y - v1.y};
	return ret;
}
inline
V2s32 &
operator -= ( V2s32 & v0, const V2s32 & v1 )
{
	V2s32 & ret = v0;
	ret = {v0.x - v1.x, v0.y - v1.y};
	return ret;
}
inline
V2 &
operator -= ( V2 & v0, const V2 & v1 )
{
	V2 & ret = v0;
	ret = {v0.x - v1.x, v0.y - v1.y};
	return ret;
}
inline
V3 &
operator -= ( V3 & v0, const V3 & v1 )
{
	V3 & ret = v0;
	ret = {v0.x - v1.x, v0.y - v1.y, v0.z - v1.z};
	return ret;
}
inline
V3s32 &
operator -= ( V3s32 & v0, const V3s32 & v1 )
{
	V3s32 & ret = v0;
	ret = {v0.x - v1.x, v0.y - v1.y, v0.z - v1.z};
	return ret;
}
inline
V4 &
operator -= ( V4 & v0, const V4 & v1 )
{
	V4 & ret = v0;
	ret = {v0.x - v1.x, v0.y - v1.y, v0.z - v1.z, v0.w - v1.w};
	return ret;
}
inline
V4s32 &
operator -= ( V4s32 & v0, const V4s32 & v1 )
{
	V4s32 & ret = v0;
	ret = {v0.x - v1.x, v0.y - v1.y, v0.z - v1.z, v0.w - v1.w};
	return ret;
}
inline
V2u32
operator * ( V2u32 v0, u32 u )
{
	V2u32 ret = {v0.x * u, v0.y * u};
	return ret;
}
inline
V2s32
operator * ( V2s32 v0, s32 s )
{
	V2s32 ret = {v0.x * s, v0.y * s};
	return ret;
}
inline
V2u32
operator * ( u32 u, V2u32 v0 )
{
	V2u32 ret = {v0.x * u, v0.y * u};
	return ret;
}
inline
V2s32
operator * ( s32 s, V2s32 v0 )
{
	V2s32 ret = {v0.x * s, v0.y * s};
	return ret;
}
inline
V2
operator * ( V2 v0, f32 f )
{
	V2 ret = {v0.x * f, v0.y * f};
	return ret;
}
inline
V2
operator * ( f32 f, V2 v0 )
{
	V2 ret = {v0.x * f, v0.y * f};
	return ret;
}
inline
V3
operator * ( const V3 & v0, f32 f )
{
	V3 ret = {v0.x * f, v0.y * f, v0.z * f};
	return ret;
}
inline
V3s32
operator * ( const V3s32 & v0, s32 s )
{
	V3s32 ret = {v0.x * s, v0.y * s, v0.z * s};
	return ret;
}
inline
V4
operator * ( const V4 & v0, f32 f )
{
	V4 ret = {v0.x * f, v0.y * f, v0.z * f, v0.w * f};
	return ret;
}
inline
V4s32
operator * ( const V4s32 & v0, s32 s )
{
	V4s32 ret = {v0.x * s, v0.y * s, v0.z * s, v0.w * s};
	return ret;
}
inline
V3
operator * ( f32 f, const V3 & v0 )
{
	V3 ret = {v0.x * f, v0.y * f, v0.z * f};
	return ret;
}
inline
V3s32
operator * ( s32 s, const V3s32 & v0 )
{
	V3s32 ret = {v0.x * s, v0.y * s, v0.z * s};
	return ret;
}
inline
V4
operator * ( f32 f, const V4 & v0 )
{
	V4 ret = {v0.x * f, v0.y * f, v0.z * f, v0.w * f};
	return ret;
}
inline
V4s32
operator * ( s32 s, const V4s32 & v0 )
{
	V4s32 ret = {v0.x * s, v0.y * s, v0.z * s, v0.w * s};
	return ret;
}
inline
V2u32 &
operator *= ( V2u32 & v, u32 u )
{
	V2u32 & ret = v;
	ret = {v.x * u, v.y * u};
	return ret;
}
inline
V2s32 &
operator *= ( V2s32 & v, s32 s )
{
	V2s32 & ret = v;
	ret = {v.x * s, v.y * s};
	return ret;
}
inline
V2 &
operator *= ( V2 & v, f32 f )
{
	V2 & ret = v;
	ret = {v.x * f, v.y * f};
	return ret;
}
inline
V3 &
operator *= ( V3 & v, f32 f )
{
	V3 & ret = v;
	ret = {v.x * f, v.y * f, v.z * f};
	return ret;
}
inline
V3s32 &
operator *= ( V3s32 & v, s32 s )
{
	V3s32 & ret = v;
	ret = {v.x * s, v.y * s, v.z * s};
	return ret;
}
inline
V4 &
operator *= ( V4 & v, f32 f )
{
	V4 & ret = v;
	ret = {v.x * f, v.y * f, v.z * f, v.w * f};
	return ret;
}
inline
V4s32 &
operator *= ( V4s32 & v, s32 s )
{
	V4s32 & ret = v;
	ret = {v.x * s, v.y * s, v.z * s, v.w * s};
	return ret;
}
inline
V2u32
operator / ( V2u32 v0, u32 u )
{
	V2u32 ret = {v0.x / u, v0.y / u};
	return ret;
}
inline
V2s32
operator / ( V2s32 v0, s32 s )
{
	V2s32 ret = {v0.x / s, v0.y / s};
	return ret;
}
inline
V2
operator / ( V2 v0, f32 f )
{
	V2 ret = {v0.x / f, v0.y / f};
	return ret;
}
inline
V3
operator / ( const V3 & v0, f32 f )
{
	V3 ret = {v0.x / f, v0.y / f, v0.z / f};
	return ret;
}
inline
V3s32
operator / ( const V3s32 & v0, s32 s )
{
	V3s32 ret = {v0.x / s, v0.y / s, v0.z / s};
	return ret;
}
inline
V4
operator / ( const V4 & v0, f32 f )
{
	V4 ret = {v0.x / f, v0.y / f, v0.z / f, v0.w / f};
	return ret;
}
inline
V4s32
operator / ( const V4s32 & v0, s32 s )
{
	V4s32 ret = {v0.x / s, v0.y / s, v0.z / s, v0.w / s};
	return ret;
}
inline
V2u32 &
operator /= ( V2u32 & v, u32 u )
{
	V2u32 & ret = v;
	ret = {v.x / u, v.y / u};
	return ret;
}
inline
V2s32 &
operator /= ( V2s32 & v, s32 s )
{
	V2s32 & ret = v;
	ret = {v.x / s, v.y / s};
	return ret;
}
inline
V2 &
operator /= ( V2 & v, f32 f )
{
	V2 & ret = v;
	ret = {v.x / f, v.y / f};
	return ret;
}
inline
V3 &
operator /= ( V3 & v, f32 f )
{
	V3 & ret = v;
	ret = {v.x / f, v.y / f, v.z / f};
	return ret;
}
inline
V3s32 &
operator /= ( V3s32 & v, s32 s )
{
	V3s32 & ret = v;
	ret = {v.x / s, v.y / s, v.z / s};
	return ret;
}
inline
V4 &
operator /= ( V4 & v, f32 f )
{
	V4 & ret = v;
	ret = {v.x / f, v.y / f, v.z / f, v.w / f};
	return ret;
}
inline
V4s32 &
operator /= ( V4s32 & v, s32 s )
{
	V4s32 & ret = v;
	ret = {v.x / s, v.y / s, v.z / s, v.w / s};
	return ret;
}
inline
f32
operator * ( V2 v0, V2 v1 )
{
	f32 ret = v0.x * v1.x +
	          v0.y * v1.y;
	return ret;
}
inline
f32
dot( V2 v0, V2 v1 )
{
	f32 ret = v0 * v1;
	return ret;
}
inline
f32
operator * ( const V3 & v0, const V3 & v1 )
{
	f32 ret = v0.x * v1.x +
	          v0.y * v1.y +
	          v0.z * v1.z;
	return ret;
}
inline
f32
dot( V3 v0, V3 v1 )
{
	f32 ret = v0 * v1;
	return ret;
}
inline
f32
operator ^ ( V2 v0, V2 v1 )
{
	f32 ret = v0.x * v1.y - v0.y * v1.x;
	return ret;
}
inline
f32
cross( V2 v0, V2 v1 )
{
	f32 ret = v0 ^ v1;
	return ret;
}
inline
V3
operator ^ ( const V3 & v0, const V3 & v1 )
{
	V3 ret = {v0.y * v1.z - v0.z * v1.y,
	          v0.z * v1.x - v0.x * v1.z,
	          v0.x * v1.y - v0.y * v1.x };
	return ret;
}
inline
V3
cross( V3 v0, V3 v1 )
{
	V3 ret = v0 ^ v1;
	return ret;
}

inline
V3
operator * ( V3 v, Mx4 m )
{
	V3 ret;

	Mx<f32,1,4> v4 = {v.x,v.y,v.z,1.0f};
	v4 = v4*m;
	ret.x = v4[0][0];
	ret.y = v4[1][0];
	ret.z = v4[2][0];
	ret  /= v4[3][0];

	return ret;
}
inline
V4
operator * ( V4 v, Mx4 m )
{
	V4 ret;

	ret.row_vector = v.row_vector * m;

	return ret;
}

inline
V3
operator * ( Mx4 m, V3 v )
{
	V3 ret;

	Mx<f32,4,1> v4 = {v.x,v.y,v.z,1.0f};
	v4 = m*v4;
	ret.x = v4[0][0];
	ret.y = v4[0][1];
	ret.z = v4[0][2];
	ret /= v4[0][3];

	return ret;
}

inline
V4
operator * ( Mx4 m, V4 v )
{
	V4 ret;

	ret.col_vector = m * v.col_vector;

	return ret;
}

inline
V3
operator * ( V3 v, Mx3 m )
{
	V3 ret;

	Mx<f32,1,3> v3 = {v.x,v.y,v.z};
	v3 = v3*m;
	ret.x = v3[0][0];
	ret.y = v3[1][0];
	ret.z = v3[2][0];

	return ret;
}

inline
V3
operator * ( Mx3 m, V3 v )
{
	V3 ret;

	Mx<f32,3,1> v3 = {v.x,v.y,v.z};
	v3 = m*v3;
	ret.x = v3[0][0];
	ret.y = v3[0][1];
	ret.z = v3[0][2];

	return ret;
}

inline
V2
operator * ( V2 v, Mx3 m )
{
	V2 ret;

	Mx<f32,1,3> v3 = {v.x,v.y,1.0f};
	v3 = v3*m;
	ret.x = v3[0][0];
	ret.y = v3[1][0];
	ret  /= v3[2][0];

	return ret;
}

inline
V2
operator * ( Mx3 m, V2 v )
{
	V2 ret;

	Mx<f32,3,1> v3 = {v.x,v.y,1.0f};
	v3 = m*v3;
	ret.x = v3[0][0];
	ret.y = v3[0][1];
	ret /= v3[0][2];

	return ret;
}


inline
V2u32
hadamard( V2u32 v0, V2u32 v1 )
{
	V2u32 ret = {v0.x * v1.x, v0.y * v1.y};
	return ret;
}
inline
V2s32
hadamard( V2s32 v0, V2s32 v1 )
{
	V2s32 ret = {v0.x * v1.x, v0.y * v1.y};
	return ret;
}
inline
V2
hadamard( V2 v0, V2 v1 )
{
	V2 ret = {v0.x * v1.x, v0.y * v1.y};
	return ret;
}
inline
V3
hadamard( V3 v0, V3 v1 )
{
	V3 ret = {v0.x * v1.x, v0.y * v1.y, v0.z * v1.z};
	return ret;
}
inline
V3s32
hadamard( V3s32 v0, V3s32 v1 )
{
	V3s32 ret = {v0.x * v1.x, v0.y * v1.y, v0.z * v1.z};
	return ret;
}
inline
V4
hadamard( V4 v0, V4 v1 )
{
	V4 ret = {v0.x * v1.x, v0.y * v1.y, v0.z * v1.z, v0.w * v1.w};
	return ret;
}
inline
V4s32
hadamard( V4s32 v0, V4s32 v1 )
{
	V4s32 ret = {v0.x * v1.x, v0.y * v1.y, v0.z * v1.z, v0.w * v1.w};
	return ret;
}


inline
f32
sum( V2u32 v0 )
{
	f32 ret = v0.x + v0.y;
	return ret;
}
inline
f32
sum( V2s32 v0 )
{
	f32 ret = v0.x + v0.y;
	return ret;
}
inline
f32
sum( V2 v0 )
{
	f32 ret = v0.x + v0.y;
	return ret;
}
inline
f32
sum( V3 v0 )
{
	f32 ret = v0.x + v0.y + v0.z;
	return ret;
}
inline
f32
sum( V3s32 v0 )
{
	f32 ret = v0.x + v0.y + v0.z;
	return ret;
}
inline
f32
sum( V4 v0 )
{
	f32 ret = v0.x + v0.y + v0.z;
	return ret;
}
inline
f32
sum( V4s32 v0 )
{
	f32 ret = v0.x + v0.y + v0.z + v0.w;
	return ret;
}

inline
V2
rotate_deg( V2 v, f32 ad )
{
	V2 ret;
	f32 ar = deg2rad( ad );
	f32 s = sinf( ar );
	f32 c = cosf( ar );
	ret.x = v.x * c - v.y *s;
	ret.y = v.x * s + v.y *c;
	return ret;
}
inline
V2
rotate_rad( V2 v, f32 ar )
{
	V2 ret;

	f32 s = sinf( ar );
	f32 c = cosf( ar );
	ret.x = v.x * c - v.y *s;
	ret.y = v.x * s + v.y *c;

	return ret;
}

inline
V2
ortho_left( V2 v )
{
	V2 ret = {-v.y, v.x};
	return ret;
}
inline
V2
ortho_right( V2 v )
{
	V2 ret = {v.y, -v.x};
	return ret;
}

inline
f32
sq_length( V2 v )
{
	f32 ret = v.x * v.x + v.y * v.y;
	return ret;
}
inline
f32
sq_length( V3 v )
{
	f32 ret = v.x * v.x + v.y * v.y + v.z * v.z;
	return ret;
}
inline
f32
sq_length( V4 v )
{
	f32 ret = v.x * v.x + v.y * v.y + v.z * v.z + v.w * v.w;
	return ret;
}
inline
f32
length( V2 v )
{
	f32 ret = sqrtf( v.x * v.x + v.y * v.y );
	return ret;
}
inline
f32
length( V3 v )
{
	f32 ret = sqrtf( v.x * v.x + v.y * v.y + v.z * v.z );
	return ret;
}
inline
f32
length( V4 v )
{
	f32 ret = sqrtf( v.x * v.x + v.y * v.y + v.z * v.z + v.w * v.w );
	return ret;
}
inline
V2
normalize( V2 v )
{
	V2 ret = v / length( v );
	return ret;
}
inline
V3
normalize( V3 v )
{
	V3 ret = v / length( v );
	return ret;
}
inline
V4
normalize( V4 v )
{
	V4 ret = v / length( v );
	return ret;
}
inline
V2
normalize_or_0( V2 v )
{
	f32 l = length( v );
	V2 ret = l ? v / l : V2{};
	return ret;
}
inline
V3
normalize_or_0( V3 v )
{
	f32 l = length( v ); // TODO(theGiallo, 2017-04-07): can't we use length_sq?
	V3 ret = l ? v / l : V3{};
	return ret;
}
inline
V4
normalize_or_0( V4 v )
{
	f32 l = length( v );
	V4 ret = l ? v / l : V4{};
	return ret;
}

inline
s32
sign( s32 v )
{
	s32 ret = 0;
	if ( v > 0 )
	{
		ret = 1;
	} else
	if ( v < 0 )
	{
		ret = -1;
	}
	return ret;
}
inline
f32
sign( f32 v )
{
	f32 ret = 0.0f;
	if ( v > 0.0f )
	{
		ret = 1.0f;
	} else
	if ( v < 0.0f )
	{
		ret = -1.0f;
	}
	return ret;
}

inline
f32
mix( f32 l, f32 r, f32 a )
{
	f32 ret = l * ( 1.0f - a ) + r * a;

	return ret;
}
inline
V2
mix( V2 l, V2 r, f32 a )
{
	V2 ret = l * ( 1.0f - a ) + r * a;

	return ret;
}
inline
V3
mix( V3 l, V3 r, f32 a )
{
	V3 ret = l * ( 1.0f - a ) + r * a;

	return ret;
}
inline
V4
mix( V4 l, V4 r, f32 a )
{
	V4 ret = l * ( 1.0f - a ) + r * a;

	return ret;
}

inline
u8
min( u8 a, u8 b )
{
	u8 ret = a < b ? a : b;

	return ret;
}

inline
u8
max( u8 a, u8 b )
{
	u8 ret = a > b ? a : b;

	return ret;
}

inline
s8
min( s8 a, s8 b )
{
	s8 ret = a < b ? a : b;

	return ret;
}

inline
s8
max( s8 a, s8 b )
{
	s8 ret = a > b ? a : b;

	return ret;
}

inline
u16
min( u16 a, u16 b )
{
	u16 ret = a < b ? a : b;

	return ret;
}

inline
u16
max( u16 a, u16 b )
{
	u16 ret = a > b ? a : b;

	return ret;
}

inline
s16
min( s16 a, s16 b )
{
	s16 ret = a < b ? a : b;

	return ret;
}

inline
s16
max( s16 a, s16 b )
{
	s16 ret = a > b ? a : b;

	return ret;
}

inline
u32
min( u32 a, u32 b )
{
	u32 ret = a < b ? a : b;

	return ret;
}

inline
u32
max( u32 a, u32 b )
{
	u32 ret = a > b ? a : b;

	return ret;
}

inline
s32
min( s32 a, s32 b )
{
	s32 ret = a < b ? a : b;

	return ret;
}

inline
s32
max( s32 a, s32 b )
{
	s32 ret = a > b ? a : b;

	return ret;
}

inline
s64
min( s64 a, s64 b )
{
	s64 ret = a < b ? a : b;

	return ret;
}

inline
s64
max( s64 a, s64 b )
{
	s64 ret = a > b ? a : b;

	return ret;
}

inline
u64
min( u64 a, u64 b )
{
	u64 ret = a < b ? a : b;

	return ret;
}

inline
u64
max( u64 a, u64 b )
{
	u64 ret = a > b ? a : b;

	return ret;
}

inline
f32
min( f32 a, f32 b )
{
	f32 ret = a < b ? a : b;

	return ret;
}

inline
f32
max( f32 a, f32 b )
{
	f32 ret = a > b ? a : b;

	return ret;
}

inline
V2
min( V2 a, V2 b )
{
	V2 ret;
	ret.x = a.x < b.x ? a.x : b.x;
	ret.y = a.y < b.y ? a.y : b.y;

	return ret;
}
inline
V2
max( V2 a, V2 b )
{
	V2 ret;
	ret.x = a.x > b.x ? a.x : b.x;
	ret.y = a.y > b.y ? a.y : b.y;

	return ret;
}

inline
V3
min( V3 a, V3 b )
{
	V3 ret;
	ret.x = a.x < b.x ? a.x : b.x;
	ret.y = a.y < b.y ? a.y : b.y;
	ret.z = a.z < b.z ? a.z : b.z;

	return ret;
}
inline
V4
min( V4 a, V4 b )
{
	V4 ret;
	ret.x = a.x < b.x ? a.x : b.x;
	ret.y = a.y < b.y ? a.y : b.y;
	ret.z = a.z < b.z ? a.z : b.z;
	ret.z = a.w < b.w ? a.w : b.w;

	return ret;
}

inline
V3
max( V3 a, V3 b )
{
	V3 ret;
	ret.x = a.x > b.x ? a.x : b.x;
	ret.y = a.y > b.y ? a.y : b.y;
	ret.z = a.z > b.z ? a.z : b.z;

	return ret;
}

inline
V4
max( V4 a, V4 b )
{
	V4 ret;
	ret.x = a.x > b.x ? a.x : b.x;
	ret.y = a.y > b.y ? a.y : b.y;
	ret.z = a.z > b.z ? a.z : b.z;
	ret.w = a.w > b.w ? a.w : b.w;

	return ret;
}

inline
V2
min( V2 a, f32 b )
{
	V2 ret;
	ret.x = a.x < b ? a.x : b;
	ret.y = a.y < b ? a.y : b;

	return ret;
}
inline
V2
max( V2 a, f32 b )
{
	V2 ret;
	ret.x = a.x > b ? a.x : b;
	ret.y = a.y > b ? a.y : b;

	return ret;
}

inline
V3
min( V3 a, f32 b )
{
	V3 ret;
	ret.x = a.x < b ? a.x : b;
	ret.y = a.y < b ? a.y : b;
	ret.z = a.z < b ? a.z : b;

	return ret;
}
inline
V4
min( V4 a, f32 b )
{
	V4 ret;
	ret.x = a.x < b ? a.x : b;
	ret.y = a.y < b ? a.y : b;
	ret.z = a.z < b ? a.z : b;
	ret.z = a.w < b ? a.w : b;

	return ret;
}

inline
V3
max( V3 a, f32 b )
{
	V3 ret;
	ret.x = a.x > b ? a.x : b;
	ret.y = a.y > b ? a.y : b;
	ret.z = a.z > b ? a.z : b;

	return ret;
}

inline
V4
max( V4 a, f32 b )
{
	V4 ret;
	ret.x = a.x > b ? a.x : b;
	ret.y = a.y > b ? a.y : b;
	ret.z = a.z > b ? a.z : b;
	ret.w = a.w > b ? a.w : b;

	return ret;
}

inline
V2
floor( V2 v )
{
	V2 ret = {floorf( v.x ), floorf( v.y ) };
	return ret;
}
inline
V3
floor( V3 v )
{
	V3 ret = {floorf( v.x ), floorf( v.y ), floorf( v.z ) };
	return ret;
}
inline
V4
floor( V4 v )
{
	V4 ret = {floorf( v.x ), floorf( v.y ), floorf( v.z ), floorf( v.w ) };
	return ret;
}
inline
V2
ceil( V2 v )
{
	V2 ret = {ceilf( v.x ), ceilf( v.y ) };
	return ret;
}
inline
V3
ceil( V3 v )
{
	V3 ret = {ceilf( v.x ), ceilf( v.y ), ceilf( v.z ) };
	return ret;
}
inline
V4
ceil( V4 v )
{
	V4 ret = {ceilf( v.x ), ceilf( v.y ), ceilf( v.z ), ceilf( v.w ) };
	return ret;
}

inline
f32
clamp( f32 v, f32 min_v, f32 max_v )
{
	f32 ret = min( max( min_v, v ), max_v );

	return ret;
}
inline
V2
clamp( V2 v, V2 min_v, V2 max_v )
{
	V2 ret = min( max( min_v, v ), max_v );

	return ret;
}
inline
V3
clamp( V3 v, V3 min_v, V3 max_v )
{
	V3 ret = min( max( min_v, v ), max_v );

	return ret;
}
inline
V4
clamp( V4 v, V4 min_v, V4 max_v )
{
	V4 ret = min( max( min_v, v ), max_v );

	return ret;
}
inline
V2
clamp( V2 v, f32 min_v, f32 max_v )
{
	V2 ret = min( max( v, min_v ), max_v );

	return ret;
}
inline
V3
clamp( V3 v, f32 min_v, f32 max_v )
{
	V3 ret = min( max( v, min_v ), max_v );

	return ret;
}
inline
V4
clamp( V4 v, f32 min_v, f32 max_v )
{
	V4 ret = min( max( v, min_v ), max_v );

	return ret;
}

inline
f32
smoothstep( f32 x )
{
	f32 ret = x * x * (3 - 2 * x );

	return ret;
}
// NOTE(theGiallo): l <= v <= r
// return in [0,1]
inline
f32
smoothstep( f32 l, f32 r, f32 v )
{
	v = clamp( (v-l)/(r-l), 0.0f, 1.0f );
	f32 ret = smoothstep( v );

	return ret;
}

inline
f32
smootherstep( f32 x )
{
	f32 ret = x * x * x * ( x * ( x * 6 - 15 ) + 10 );

	return ret;
}

// NOTE(theGiallo): l <= v <= r
// return in [0,1]
inline
f32
smootherstep( f32 l, f32 r, f32 v )
{
	v = clamp( (v-l)/(r-l), 0.0f, 1.0f );
	f32 ret = smootherstep( v );

	return ret;
}

// NOTE(theGiallo): left-right and top-bottom
inline
f32
bilinear( f32 left_top, f32 right_top, f32 left_bottom, f32 right_bottom,
          f32 h_a, f32 v_a )
{
	f32 ret = mix( mix( left_top, right_top, h_a ), mix( left_bottom, right_bottom, h_a ), v_a );

	return ret;
}

inline
V2
square( V2 v )
{
	V2 ret;
	ret.x = square( v.x );
	ret.y = square( v.y );
	return ret;
}
inline
V3
square( V3 v )
{
	V3 ret;
	ret.x = square( v.x );
	ret.y = square( v.y );
	ret.z = square( v.z );
	return ret;
}
inline
V4
square( V4 v )
{
	V4 ret;
	ret.x = square( v.x );
	ret.y = square( v.y );
	ret.z = square( v.z );
	ret.w = square( v.w );
	return ret;
}

inline
V2
sign( V2 v )
{
	V2 ret;
	ret.x = sign( v.x );
	ret.y = sign( v.y );

	return ret;
}
inline
V3
sign( V3 v )
{
	V3 ret;
	ret.x = sign( v.x );
	ret.y = sign( v.y );
	ret.z = sign( v.z );

	return ret;
}
inline
V4
sign( V4 v )
{
	V4 ret;
	ret.x = sign( v.x );
	ret.y = sign( v.y );
	ret.z = sign( v.z );
	ret.w = sign( v.w );

	return ret;
}
inline
V2
sqrt( V2 v )
{
	V2 ret;
	ret.x = sqrtf( v.x );
	ret.y = sqrtf( v.y );

	return ret;
}
inline
V3
sqrt( V3 v )
{
	V3 ret;
	ret.x = sqrtf( v.x );
	ret.y = sqrtf( v.y );
	ret.z = sqrtf( v.z );

	return ret;
}
inline
V4
sqrt( V4 v )
{
	V4 ret;
	ret.x = sqrtf( v.x );
	ret.y = sqrtf( v.y );
	ret.z = sqrtf( v.z );
	ret.w = sqrtf( v.w );

	return ret;
}


inline
f32
angle_rad_between_unitv( V2 v0, V2 v1 )
{
	f32 ret;
	f32 s = v0 ^ v1;
	f32 c = v0 * v1;
	if ( c > 0 )
	{
		ret = asinf( s );
	} else
	if ( s > 0 )
	{
		ret = acosf( c );
	} else
	{
		ret = acosf( s ) + H_PI_F;
	}
	return ret;
}
inline
f32
angle_rad_between( V2 v0, V2 v1 )
{
	f32 ret = angle_rad_between_unitv( normalize( v0 ), normalize( v1 ) );
	return ret;
}
inline
f32
angle_deg_between_unitv( V2 v0, V2 v1 )
{
	f32 ret = rad2deg( angle_rad_between_unitv( v0, v1 ) );
	return ret;
}
inline
f32
angle_deg_between( V2 v0, V2 v1 )
{
	f32 ret = rad2deg( angle_rad_between( v0, v1 ) );
	return ret;
}
inline
f32
angle_rad_unitv( V2 v )
{
	f32 ret = angle_rad_between_unitv( {1.0f,0.0f}, v );
	return ret;
}
inline
f32
angle_rad( V2 v )
{
	f32 ret = angle_rad_between( {1.0f,0.0f}, v );
	return ret;
}
inline
f32
angle_deg_unitv( V2 v )
{
	f32 ret = rad2deg( angle_rad_unitv( v ) );
	return ret;
}
inline
f32
angle_deg( V2 v )
{
	f32 ret = rad2deg( angle_rad( v ) );
	return ret;
}
inline
f32
angle_rad_between_unitv( V3 v0, V3 v1 )
{
	f32 ret;
	f32 c = v0 * v1;
	c = clamp( c, -1.0f, 1.0f );
	ret = acosf( c );

	return ret;
}
inline
f32
angle_rad_between( V3 v0, V3 v1 )
{
	f32 ret = angle_rad_between_unitv( normalize( v0 ), normalize( v1 ) );
	return ret;
}
inline
f32
angle_deg_between_unitv( V3 v0, V3 v1 )
{
	f32 ret = rad2deg( angle_rad_between_unitv( v0, v1 ) );
	return ret;
}
inline
f32
angle_deg_between( V3 v0, V3 v1 )
{
	f32 ret = rad2deg( angle_rad_between( v0, v1 ) );
	return ret;
}
inline
f32
sin( V2 v0, V2 v1 )
{
	f32 ret = normalize( v0 ) ^ normalize( v1 );
	return ret;
}
inline
f32
cos( V2 v0, V2 v1 )
{
	f32 ret = normalize( v0 ) * normalize( v1 );
	return ret;
}

// NOTE(theGiallo): the up vector (0,1) is considered the zero rotation
// so x and y are swapped with respect to a normal rotation
inline
void
matrix_rot_from_up_vec( V2 up, f32 * matrix_4x4 )
{
	matrix_4x4[0] = up.y;
	matrix_4x4[1] = up.x;
	matrix_4x4[2] = 0;
	matrix_4x4[3] = 0;

	matrix_4x4[4] = -up.x;
	matrix_4x4[5] = up.y;
	matrix_4x4[6] = 0;
	matrix_4x4[7] = 0;

	matrix_4x4[8] = 0;
	matrix_4x4[9] = 0;
	matrix_4x4[10] = 1;
	matrix_4x4[11] = 0;

	matrix_4x4[12] = 0;
	matrix_4x4[13] = 0;
	matrix_4x4[14] = 0;
	matrix_4x4[15] = 1;
}

inline
void
translation_mx4( V3 translation, Mx4 * out_res )
{
	f32 arr[16] = {
	   1.0f, 0.0f, 0.0f, translation.x,
	   0.0f, 1.0f, 0.0f, translation.y,
	   0.0f, 0.0f, 1.0f, translation.z,
	   0.0f, 0.0f, 0.0f, 1.0f };
	memcpy( out_res->arr, arr, sizeof(arr) );
}
inline
Mx4
translation_mx4( V3 translation )
{
	Mx4 ret;
	translation_mx4( translation, &ret );
	return ret;
}
inline
void
rotation_mx4( V3 rotation, Mx4 * out_res )
{
	V3 r = {deg2rad( rotation.x ),
	        deg2rad( rotation.y ),
	        deg2rad( rotation.z )};
	Mx4 Rx = {
	   1.0f, 0.0f,      0.0f,     0.0f,
	   0.0f, cosf(r.x), -sinf(r.x), 0.0f,
	   0.0f, sinf(r.x),  cosf(r.x), 0.0f,
	   0.0f, 0.0f,      0.0f,     1.0f };
	Mx4 Ry = {
	    cosf(r.y), 0.0f, sinf(r.y), 0.0f,
	    0.0f,     1.0f, 0.0f,     0.0f,
	   -sinf(r.y), 0.0f, cosf(r.y), 0.0f,
	    0.0f,     0.0f, 0.0f,     1.0f };
	Mx4 Rz = {
	   cosf(r.z), -sinf(r.z), 0.0f, 0.0f,
	   sinf(r.z),  cosf(r.z), 0.0f, 0.0f,
	   0.0f,      0.0f,     1.0f, 0.0f,
	   0.0f,      0.0f,     0.0f, 1.0f };
	*out_res = Rz * Ry * Rx;
}
inline
Mx4
rotation_mx4( V3 rotation )
{
	Mx4 ret;
	rotation_mx4( rotation, &ret );
	return ret;
}
inline
void
rotation_mx4( V3 axis, f32 angle_deg, Mx4 * m )
{
	f32 rad = deg2rad( angle_deg );
	f32 c = cosf( rad );
	f32 s = sinf( rad );
	f32 omc = 1.0f - c;
	V3 u = axis;
	Mx4 rot = {
	   c + square(u.x)*omc,   u.x*u.y*omc - u.z*s,     u.x*u.z*omc + u.y*s, 0.0f,
	   u.x*u.y*omc + u.z*s,   c + square(u.y)*omc,     u.y*u.z*omc - u.x*s, 0.0f,
	   u.x*u.z*omc - u.y*s,   u.y*u.z*omc + u.x*s,     c + square(u.z)*omc, 0.0f,
	   0.0f,                  0.0f,                    0.0f,                1.0f
	};
	*m = rot;
}
inline
Mx4
rotation_mx4( V3 axis, f32 angle_deg )
{
	Mx4 ret;
	rotation_mx4( axis, angle_deg, &ret );
	return ret;
}
inline
void
scale_mx4( V3 scale, Mx4 * out_res )
{
	f32 arr[16] = {
	   scale.x, 0.0f,    0.0f,    0.0f,
	   0.0f,    scale.y, 0.0f,    0.0f,
	   0.0f,    0.0f,    scale.z, 0.0f,
	   0.0f,    0.0f,    0.0f,    1.0f };
	memcpy( out_res->arr, arr, sizeof(arr) );
}
inline
Mx4
scale_mx4( V3 scale )
{
	Mx4 ret;
	scale_mx4( scale, &ret );
	return ret;
}
inline
void
transform_mx4( V3 translation, V3 rotation, V3 scale, Mx4 * out_res )
{
	Mx4 S,R,T;
	scale_mx4( scale, &S);
	rotation_mx4( rotation, &R);
	translation_mx4( translation, &T);
	*out_res = T * R * S;
}
inline
Mx4
transform_mx4( V3 translation, V3 rotation, V3 scale )
{
	Mx4 ret;
	transform_mx4( translation, rotation, scale, &ret );
	return ret;
}
inline
void
perspective_mx4( f32 near, f32 far, V2 vanishing_point, V2 size, Mx4 * out_res )
{
	f32 left   =     - vanishing_point.x   * size.w;
	f32 right  = ( 1 - vanishing_point.x ) * size.w;
	f32 bottom =     - vanishing_point.y   * size.h;
	f32 top    = ( 1 - vanishing_point.y ) * size.h;

	/**
	 * gml style or http://www.songho.ca/opengl/gl_projectionmatrix.html
	 */
	(*out_res)[0][0] = (2 * near) / (right - left);
	(*out_res)[1][1] = (2 * near) / (top - bottom);
	(*out_res)[2][2] = -(far + near) / (far - near);
	(*out_res)[3][2] = -1;
	(*out_res)[2][3] = -(2 * far * near) / (far - near);
	(*out_res)[0][1] = 0.0f;
	(*out_res)[0][2] = 0.0f;
	(*out_res)[0][3] = 0.0f;
	(*out_res)[1][0] = 0.0f;
	(*out_res)[1][2] = 0.0f;
	(*out_res)[1][3] = 0.0f;
	(*out_res)[2][0] = 0.0f;
	(*out_res)[2][1] = 0.0f;
	(*out_res)[3][0] = 0.0f;
	(*out_res)[3][1] = 0.0f;
	(*out_res)[3][3] = 0.0f;
}
inline
Mx4
perspective_mx4( f32 near, f32 far, V2 vanishing_point, V2 size )
{
	Mx4 ret;
	perspective_mx4( near, far, vanishing_point, size, &ret );
	return ret;
}
inline
void
perspective_infinite_mx4( f32 near, V2 vanishing_point, V2 size, Mx4 * out_res )
{
	f32 left   =     - vanishing_point.x   * size.w;
	f32 right  = ( 1 - vanishing_point.x ) * size.w;
	f32 bottom =     - vanishing_point.y   * size.h;
	f32 top    = ( 1 - vanishing_point.y ) * size.h;

	f32 e = 0.000000238f; // NOTE(theGiallo): ~ 2^-22

	memset( out_res->arr, 0, sizeof(out_res->arr[0])*16 );
	/**
	 * Tightening the Precision of Perspective Rendering
	 * by Paul Upchurch and Mathieu Desbrun
	 * http://www.geometry.caltech.edu/pubs/UD12.pdf
	 */
	(*out_res)[0][0] = (2 * near) / (right - left);
	(*out_res)[1][1] = (2 * near) / (top - bottom);
	(*out_res)[2][2] = e -1.0f;
	(*out_res)[3][2] = -1.0f;
	(*out_res)[2][3] = (e -2) * near;
	(*out_res)[0][2] = (left+right)/(right-left);
	(*out_res)[1][2] = (top+bottom)/(top-bottom);
}
inline
Mx4
perspective_infinite_mx4( f32 near, V2 vanishing_point, V2 size )
{
	Mx4 ret;
	perspective_infinite_mx4( near, vanishing_point, size, &ret );
	return ret;
}
inline
V2
near_plane_size_from_fov( f32 near, f32 fov, f32 inverse_aspect_ratio )
{
	V2 ret;
	ret.w = near * tanf( deg2rad( fov * 0.5f ) ) * 2.0f;
	ret.h = ret.w * inverse_aspect_ratio;
	return ret;
}
inline
f32
fov_from_near_plane_size( f32 near, f32 width )
{
	f32 ret;
	V2 v = {width * 0.5f, near};
	ret = 2.0f * angle_deg_between_unitv( V2{0.0f,1.0f}, normalize( v ) );
	return ret;
}
inline
void
perspective_infinite_mx4( f32 near, V2 vanishing_point,
                          f32 fov, f32 inverse_aspect_ratio,
                          Mx4 * out_res, V2 * out_size = NULL )
{
	V2 size = near_plane_size_from_fov( near, fov, inverse_aspect_ratio );
	if ( out_size )
	{
		*out_size = size;
	}
	perspective_infinite_mx4( near, vanishing_point, size, out_res );
}
inline
Mx4
perspective_infinite_mx4( f32 near, V2 vanishing_point,
                          f32 fov, f32 inverse_aspect_ratio,
                          V2 * out_size = NULL )
{
	Mx4 ret;
	perspective_infinite_mx4( near, vanishing_point, fov, inverse_aspect_ratio,
	                          &ret, out_size );
	return ret;
}
inline
void
perspective_mx4( f32 near, f32 far, V2 vanishing_point,
                 f32 fov, f32 inverse_aspect_ratio,
                 Mx4 * out_res, V2 * out_size = NULL )
{
	V2 size = near_plane_size_from_fov( near, fov, inverse_aspect_ratio );
	if ( out_size )
	{
		*out_size = size;
	}
	perspective_mx4( near, far, vanishing_point, size, out_res );
}
inline
Mx4
perspective_mx4( f32 near, f32 far, V2 vanishing_point,
                 f32 fov, f32 inverse_aspect_ratio,
                 V2 * out_size = NULL )
{
	Mx4 ret;
	perspective_mx4( near, far, vanishing_point, fov, inverse_aspect_ratio,
	                 &ret, out_size );
	return ret;
}
inline
void
ortho_mx4( f32 near, f32 far, V2 vanishing_point, V2 size, Mx4 * out_res )
{
	f32 left   =     - vanishing_point.x   * size.w;
	f32 right  = ( 1 - vanishing_point.x ) * size.w;
	f32 bottom =     - vanishing_point.y   * size.h;
	f32 top    = ( 1 - vanishing_point.y ) * size.h;

	f32 l = left,
	    r = right,
	    t = top,
	    b = bottom,
	    f = far,
	    n = near;

	/**
	 * http://www.songho.ca/opengl/gl_projectionmatrix.html
	 **/
	f32 arr[16] = {
	   2.0f/(r-l), 0.0f,       0.0f,        -(r+l)/(r-l),
	   0.0f,       2.0f/(t-b), 0.0f,        -(t+b)/(t-b),
	   0.0f,       0.0f,       -2.0f/(f-n), -(f+n)/(f-n),
	   0.0f,       0.0f,       0.0f,        1.0f          };

	memcpy( out_res->arr, arr, sizeof(arr) );
}
inline
Mx4
ortho_mx4( f32 near, f32 far, V2 vanishing_point, V2 size )
{
	Mx4 ret;
	ortho_mx4( near, far, vanishing_point, size, &ret );
	return ret;
}
inline
void
view_mx4( V3 look, V3 up, V3 pos, Mx4 * out_res )
{
	V3 mz = normalize( look );
	V3 x  = normalize( mz ^ up );
	V3 y  = normalize( x ^ mz );

	// NOTE(theGiallo): this is a change of axes
	(*out_res)[0][0] = x.x  ;
	(*out_res)[0][1] = x.y  ;
	(*out_res)[0][2] = x.z  ;

	(*out_res)[1][0] = y.x   ;
	(*out_res)[1][1] = y.y   ;
	(*out_res)[1][2] = y.z   ;

	(*out_res)[2][0] = -mz.x  ;
	(*out_res)[2][1] = -mz.y  ;
	(*out_res)[2][2] = -mz.z  ;

	(*out_res)[0][3] = -(x  * pos);
	(*out_res)[1][3] = -(y  * pos);
	(*out_res)[2][3] =  (mz * pos);

	(*out_res)[3][0] = 0.0f  ;
	(*out_res)[3][1] = 0.0f  ;
	(*out_res)[3][2] = 0.0f  ;
	(*out_res)[3][3] = 1.0f  ;
}
inline
Mx4
view_mx4( V3 look, V3 up, V3 pos )
{
	Mx4 ret;
	view_mx4( look, up, pos, &ret );
	return ret;
}

inline
Mx4
rotation_matrix_from_unit_quaternion( Quat q )
{
	Mx4 ret = {
	   1.0f - 2.0f * square( q.y ) - 2.0f * square( q.z ),  2.0f * q.x * q.y - 2.0f * q.w * q.z,                 2.0f * q.x * q.z + 2.0f * q.w * q.y,                 0.0f,
	   2.0f * q.x * q.y + 2.0f * q.w * q.z,                 1.0f - 2.0f * square( q.x ) - 2.0f * square( q.z ),  2.0f * q.y * q.z + 2.0f * q.w * q.x,                 0.0f,
	   2.0f * q.x * q.z - 2.0f * q.w * q.y,                 2.0f * q.y * q.z - 2.0f * q.w * q.x,                 1.0f - 2.0f * square( q.x ) - 2.0f * square( q.y ),  0.0f,
	   0.0f,                                                0.0f,                                                0.0f,                                                1.0f
	};

	return ret;
}

inline
V3
rotate_V3_by_unit_quaternion( V3 v, Quat q )
{
	V3 ret;

	tg_assert( abs( length( q ) - 1.0f ) < 1e-6f );

	Mx4 rot_mx = rotation_matrix_from_unit_quaternion( q );
	ret = rot_mx * v;

	return ret;
}

inline
bool
point_in_aabb( V2 p, V2 min, V2 max )
{
	bool ret =
	   p.x <= max.x &&
	   p.x >= min.x &&
	   p.y <= max.y &&
	   p.y >= min.y;
	return ret;
}

inline
V2
complex_product( V2 c0, V2 c1 )
{
	V2 ret = {c0.x * c1.x - c0.y * c1.y, c0.x * c1.y + c0.y * c1.x};
	return ret;
}

inline
V2
complex_division( V2 c0, V2 c1 )
{
	V2 ret = V2{ c0.x * c1.x + c0.y * c1.y, c0.y * c1.x - c0.x * c1.y} / ( c1 * c1 );
	return ret;
}
inline
V2
complex_conjugate( V2 c )
{
	V2 ret = {c.x, -c.y};
	return ret;
}
inline
f32
complex_conjugate_product( V2 c )
{
	f32 ret = c.x * c.x - ( c.y * ( -c.y ) );
	return ret;
}

inline
f32
sphere_area( f32 radius )
{
	f32 ret = 4.0f * PI_F * square( radius );
	return ret;
}

inline
bool
eps_cmp( f32 v0, f32 v1, f32 eps )
{
	bool ret = abs( v0 - v1 ) <= eps;
	return ret;
}


u64
xorshift128plus( u64 seeds[2] );

#endif /* ifndef TGMATH_H */
