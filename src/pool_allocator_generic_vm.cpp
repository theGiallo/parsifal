#include "pool_allocator_generic_vm.h"
#include "macro_tools.h"
#include "system.h"

void
pool_allocator_generic_vm_init( Pool_Allocator_Generic_VM * pa, u64 element_size, s64 max_capacity, s64 initial_capacity, s64  capacity_increment )
{
	tg_assert( pa );
	tg_assert( element_size >= sizeof(u64) );
	tg_assert( max_capacity > 0 );
	tg_assert( initial_capacity <= max_capacity );
	tg_assert( capacity_increment >= 0 );

	u64 page_size = sys_page_size();

	u64 size_to_reserve = ( element_size *     max_capacity + page_size - 1 ) / page_size * page_size;
	max_capacity = size_to_reserve / element_size;

	pa->id_of_first_free = -1;
	pa->id_of_last_free  = -1;
	pa->occupancy = 0;
	pa->element_size = element_size;

	if ( capacity_increment == 0 )
	{
		capacity_increment = page_size / element_size;
	}
	if ( capacity_increment == 0 )
	{
		capacity_increment = 8;
	}
	pa->capacity_increment = capacity_increment;

	pa->vm_start = sys_mem_reserve( size_to_reserve );
	if ( pa->vm_start )
	{
		pa->reserved_size = size_to_reserve;
		pa->max_capacity  = max_capacity;
		if ( initial_capacity == -1 )
		{
			initial_capacity = max_capacity;
		}

		// NOTE(theGiallo): specialized version of pool_allocator_generic_vm_up_capacity_to
		s64 total_size_to_commit = ( pa->element_size * initial_capacity + page_size - 1 ) / page_size * page_size;
		initial_capacity = total_size_to_commit / element_size;
		void * new_touched_end = (void*)((s64)pa->vm_start + total_size_to_commit);
		pa->touched_end = new_touched_end;
		if ( total_size_to_commit )
		{
			tg_assert( total_size_to_commit % page_size == 0 );

			bool did_commit = sys_mem_commit( pa->vm_start, total_size_to_commit );
			if ( !did_commit )
			{
				log_err( "failed to commit memory 0x%016lx %ld", (u64)pa->vm_start, total_size_to_commit );
				sys_mem_free( pa->vm_start, pa->reserved_size );
				pa->vm_start = NULL;
			}

			pa->id_of_first_free = 0;

			u8 * m = (u8*)pa->vm_start;
			for ( s64 i = 0; i != initial_capacity; ++i )
			{
				*(s64*)m = i == initial_capacity - 1 ? -1 : i + 1;
				m += element_size;
			}
			pa->id_of_last_free = initial_capacity - 1;
		}
		pa->current_capacity = initial_capacity;
	}
}

void *
pool_allocator_generic_vm_allocate( Pool_Allocator_Generic_VM * pa )
{
	tg_assert( pa );

	void * ret = NULL;

	if ( pool_allocator_generic_vm_is_max_capacity_full( pa ) )
	{
		return ret;
	}
	if ( pool_allocator_generic_vm_is_current_capacity_full( pa ) )
	{
		if ( !pool_allocator_generic_vm_up_capacity_to( pa, pa->current_capacity + pa->capacity_increment ) )
		{
			return ret;
		}
	}

	tg_assert( pa->vm_start );
	tg_assert( pa->current_capacity > 0 );
	tg_assert( pa->max_capacity > 0 );
	tg_assert( pa->current_capacity <= pa->max_capacity );
	tg_assert( pa->occupancy < pa->current_capacity );
	tg_assert( pa->id_of_first_free != -1 );
	tg_assert( pa->id_of_last_free != -1 );

	u8 * m = (u8*)pa->vm_start;
	s64 * next = (s64*)( m + ( pa->element_size * pa->id_of_first_free ) );
	pa->id_of_first_free = *next;
	if ( pa->id_of_first_free == -1 )
	{
		pa->id_of_last_free = -1;
	}
	ret = next;
	++pa->occupancy;

	return ret;
}

void *
pool_allocator_generic_vm_allocate_clean( Pool_Allocator_Generic_VM * pa )
{
	tg_assert( pa );

	void * ret = pool_allocator_generic_vm_allocate( pa );

	if ( ret )
	{
		memset( ret, 0x0, pa->element_size );
	}

	return ret;
}

void
pool_allocator_generic_vm_deallocate( Pool_Allocator_Generic_VM * pa, void * p )
{
	tg_assert( pa );
	tg_assert( p >= pa->vm_start );
	tg_assert( p < ( (u8*)pa->vm_start ) + pa->current_capacity * pa->element_size );
	tg_assert( ( (s64)p - (s64)pa->vm_start ) % pa->element_size == 0 );

	*(s64*)p = pa->id_of_first_free;
	pa->id_of_first_free = ( (s64)p - (s64)pa->vm_start ) / pa->element_size;
	if ( pa->id_of_last_free == -1 )
	{
		pa->id_of_last_free = pa->id_of_first_free;
	}

	--pa->occupancy;
}

bool
pool_allocator_generic_vm_is_current_capacity_full( Pool_Allocator_Generic_VM * pa )
{
	tg_assert( pa );
	tg_assert( pa->occupancy >= 0 );
	tg_assert( pa->current_capacity >= 0 );
	tg_assert( pa->occupancy <= pa->current_capacity );
	tg_assert( pa->max_capacity >= 0 );
	tg_assert( pa->occupancy <= pa->max_capacity );

	bool ret = false;

	ret = pa->occupancy == pa->current_capacity;

	tg_assert( ( ret && pa->id_of_first_free < 0 ) || ( !ret && pa->id_of_first_free >= 0 ) );

	return ret;
}

bool
pool_allocator_generic_vm_is_max_capacity_full( Pool_Allocator_Generic_VM * pa )
{
	tg_assert( pa );
	tg_assert( pa->occupancy >= 0 );
	tg_assert( pa->current_capacity >= 0 );
	tg_assert( pa->occupancy <= pa->current_capacity );
	tg_assert( pa->max_capacity >= 0 );
	tg_assert( pa->occupancy <= pa->max_capacity );

	bool ret = false;

	ret = pa->occupancy == pa->max_capacity;

	tg_assert( (  ret && pa->id_of_first_free < 0 )
	        || ( !ret && pa->id_of_first_free >= 0 )
	        || ( !ret && pa->occupancy == pa->current_capacity ) );
	tg_assert( !ret || ( pa->current_capacity == pa->max_capacity ) );

	return ret;
}

bool
pool_allocator_generic_vm_release_unused_pm( Pool_Allocator_Generic_VM * pa )
{
	// TODO(theGiallo, 2018-04-08): implement
	ILLEGAL_PATH();
	return false;
}

bool
pool_allocator_generic_vm_release_unused_pm_to_capacity( Pool_Allocator_Generic_VM * pa, s64 target_capacity )
{
	// TODO(theGiallo, 2018-04-08): implement
	ILLEGAL_PATH();
	return false;
}

bool
pool_allocator_generic_vm_up_capacity_to( Pool_Allocator_Generic_VM * pa, s64 target_capacity )
{
	tg_assert( pa );
	tg_assert( target_capacity >= 0 );
	//tg_assert( pa->max_capacity >= target_capacity );

	bool ret = false;

	if ( target_capacity > pa->max_capacity )
	{
		ret = false;
	} else
	if ( pa->current_capacity >= target_capacity )
	{
		ret = true;
	} else
	{
		u64 page_size = sys_page_size();
		s64 total_size_to_commit  = ( pa->element_size * target_capacity + page_size - 1 ) / page_size * page_size;
		target_capacity = total_size_to_commit / pa->element_size;
		void * new_touched_end = (void*)((s64)pa->vm_start + total_size_to_commit);
		s64 size_to_commit = (s64)new_touched_end - (s64)pa->touched_end;

		tg_assert( size_to_commit % page_size == 0 );

		bool did_commit = sys_mem_commit( pa->touched_end, size_to_commit );
		if ( !did_commit )
		{
			log_err( "failed to commit memory 0x%016lx %ld", (u64)pa->vm_start, total_size_to_commit );
			ret = false;
			return ret;
		}

		pa->touched_end = new_touched_end;

		if ( pa->id_of_last_free != -1 )
		{
			s64 * last_free_el_next = (s64*)( (s64)pa->vm_start + pa->element_size * pa->id_of_last_free ) ;
			*last_free_el_next = pa->current_capacity;
		} else
		if ( pa->id_of_first_free == -1 )
		{
			tg_assert( pa->id_of_last_free == -1 );
			pa->id_of_first_free = pa->id_of_last_free = pa->current_capacity;
		}

		u8 * m = (u8*)pa->vm_start + pa->element_size * pa->current_capacity;
		for ( s64 i = pa->current_capacity; i != target_capacity; ++i )
		{
			*(s64*)m = i == target_capacity - 1 ? -1 : i + 1;
			m += pa->element_size;
		}
		pa->id_of_last_free = target_capacity - 1;
		pa->current_capacity = target_capacity;
		ret = true;
	}

	return ret;
}

void
pool_allocator_generic_vm_destroy( Pool_Allocator_Generic_VM * pa )
{
	tg_assert( pa );

	if ( pa->vm_start )
	{
		sys_mem_free( pa->vm_start, pa->reserved_size );
	}
	*pa = {};
	return;
}
