#ifndef __PARSER_H__

#define __PARSER_H__ 1
#include "basic_types.h"
#include "macro_tools.h"
#include "utility.h"
#include "pool_allocator_generic.h"
#include "pool_allocator_generic_vm.h"
#include "freelist_allocator.h"

#define PARSER_ERRORS_MAX_COUNT 65536
#define PARSER_TEXT_ELEMENTS_MAX_COUNT 1024*1024//64*1024*1024

struct
Text_Piece
{
	u8  * text;
	u32   length;
};

inline
bool
text_piece_equals( Text_Piece * tp0, Text_Piece * tp1 )
{
	tg_assert( tp0 );
	tg_assert( tp1 );

	bool ret = tp0->length == tp1->length && string_equal( tp0->text, tp1->text, tp0->length );

	return ret;
}

struct
File_Coordinate
{
	u32 line;
	u32 utf8_chars_in_line;
	u32 bytes_from_file_start;
	// TODO(theGiallo, 2018-02-26): do we need an invalid indicator?
};
struct
File_Coordinates_Range
{
	File_Coordinate first, last;
};

#define TEXT_ELEMENT_TYPE(ENTRY) \
	ENTRY(NONE     ) \
	ENTRY(WORD     ) \
	ENTRY(SYMBOL   ) \
	ENTRY(LINE_FEED) \
	ENTRY(BLANKS   ) \
	ENTRY(BLOB     ) \
	ENTRY(INVALID  ) \

#define TEXT_ELEMENT_TYPE_AS_ENUM(a) a,
#define TEXT_ELEMENT_TYPE_AS_STRINGS(a) TOSTRING(a),

enum class
Text_Element_Type : u32
{
	TEXT_ELEMENT_TYPE(TEXT_ELEMENT_TYPE_AS_ENUM)
	//---
	_COUNT_PLUS_1,
	_COUNT = _COUNT_PLUS_1 - 1
};

const char * text_element_type_strings[] = {
	TEXT_ELEMENT_TYPE(TEXT_ELEMENT_TYPE_AS_STRINGS)
};

struct
Text_Element
{
	Text_Piece        text;
	Text_Element_Type type;
	File_Coordinate   in_file_pos;
};

struct
Parser_Error
{
	Text_Piece text;
	u32        line;
};


#define LIST_VM_TYPE Text_Element
#include "list_vm.inc.h"

#define LIST_VM_TYPE Parser_Error
#include "list_vm.inc.h"

typedef
struct AST_Node * AST_Node_p;

#define LIST_VM_TYPE AST_Node_p
#include "list_vm.inc.h"

#define AST_NODE_TYPE(ENTRY) \
	ENTRY(UNKNOWN                      ) \
	ENTRY(ROOT                         ) \
	ENTRY(SEMICOLON                    ) \
	ENTRY(PLUS                         ) \
	ENTRY(MINUS                        ) \
	ENTRY(INCREMENT                    ) \
	ENTRY(DECREMENT                    ) \
	ENTRY(MULT                         ) \
	ENTRY(DIV                          ) \
	ENTRY(MODULE                       ) \
	ENTRY(LEFT_SHIFT                   ) \
	ENTRY(RIGHT_SHIFT                  ) \
	ENTRY(GREATER_THAN                 ) \
	ENTRY(LESS_THAN                    ) \
	ENTRY(GREATER_EQUAL_THAN           ) \
	ENTRY(LESS_EQUAL_THAN              ) \
	ENTRY(EQUAL                        ) \
	ENTRY(NOT_EQUAL                    ) \
	ENTRY(BITWISE_NOT                  ) \
	ENTRY(BITWISE_AND                  ) \
	ENTRY(BITWISE_XOR                  ) \
	ENTRY(BITWISE_OR                   ) \
	ENTRY(LOGICAL_NOT                  ) \
	ENTRY(LOGICAL_AND                  ) \
	ENTRY(LOGICAL_OR                   ) \
	ENTRY(CONCAT_OP                    ) \
	ENTRY(COMMA_OP                     ) \
	ENTRY(ASSIGNMENT_OP                ) \
	ENTRY(PLUS_ASSIGNMENT_OP           ) \
	ENTRY(MINUS_ASSIGNMENT_OP          ) \
	ENTRY(MULT_ASSIGNMENT_OP           ) \
	ENTRY(DIV_ASSIGNMENT_OP            ) \
	ENTRY(MODULE_ASSIGNMENT_OP         ) \
	ENTRY(LEFT_SHIFT_ASSIGNMENT_OP     ) \
	ENTRY(RIGHT_SHIFT_ASSIGNMENT_OP    ) \
	ENTRY(BIN_AND_ASSIGNMENT_OP        ) \
	ENTRY(BIN_OR_ASSIGNMENT_OP         ) \
	ENTRY(BIN_XOR_ASSIGNMENT_OP        ) \
	ENTRY(BINARY_FUN_OPERATOR          ) \
	ENTRY(STRUCT_MEMBER_ACCESS_OP      ) \
	ENTRY(DOT_PLUS                     ) \
	ENTRY(DOT_MINUS                    ) \
	ENTRY(DOT_MULT                     ) \
	ENTRY(DOT_DIV                      ) \
	ENTRY(DOT_INCREMENT                ) \
	ENTRY(DOT_DECREMENT                ) \
	ENTRY(DOT_MODULE                   ) \
	ENTRY(DOT_LEFT_SHIFT               ) \
	ENTRY(DOT_RIGHT_SHIFT              ) \
	ENTRY(DOT_GREATER_THAN             ) \
	ENTRY(DOT_LESS_THAN                ) \
	ENTRY(DOT_GREATER_EQUAL_THAN       ) \
	ENTRY(DOT_LESS_EQUAL_THAN          ) \
	ENTRY(DOT_EQUAL                    ) \
	ENTRY(DOT_NOT_EQUAL                ) \
	ENTRY(DOT_BITWISE_AND              ) \
	ENTRY(DOT_BITWISE_XOR              ) \
	ENTRY(DOT_BITWISE_OR               ) \
	ENTRY(DOT_LOGICAL_AND              ) \
	ENTRY(DOT_LOGICAL_OR               ) \
	ENTRY(DOT_CONCAT_OP                ) \
	ENTRY(DOT_COMMA_OP                 ) \
	ENTRY(DOT_ASSIGNMENT_OP            ) \
	ENTRY(DOT_PLUS_ASSIGNMENT_OP       ) \
	ENTRY(DOT_MINUS_ASSIGNMENT_OP      ) \
	ENTRY(DOT_MULT_ASSIGNMENT_OP       ) \
	ENTRY(DOT_DIV_ASSIGNMENT_OP        ) \
	ENTRY(DOT_MODULE_ASSIGNMENT_OP     ) \
	ENTRY(DOT_LEFT_SHIFT_ASSIGNMENT_OP ) \
	ENTRY(DOT_RIGHT_SHIFT_ASSIGNMENT_OP) \
	ENTRY(DOT_BIN_AND_ASSIGNMENT_OP    ) \
	ENTRY(DOT_BIN_OR_ASSIGNMENT_OP     ) \
	ENTRY(DOT_BIN_XOR_ASSIGNMENT_OP    ) \
	ENTRY(DOT_BINARY_FUN_OPERATOR      ) \
	ENTRY(ARRAY_ACCESS                 ) \
	ENTRY(ARRAY_EACH                   ) \
	ENTRY(TERNARY_OPERATOR_0           ) \
	ENTRY(TERNARY_OPERATOR_1           ) \
	ENTRY(THIS_OP                      ) \
	ENTRY(VARIABLE                     ) \
	ENTRY(UNARY_OPERATOR_POSTFIX       ) \
	ENTRY(UNARY_OPERATOR_PREFIX        ) \
	ENTRY(BINARY_OPERATOR_PREC_0       ) \
	ENTRY(BINARY_OPERATOR_PREC_1       ) \
	ENTRY(BINARY_OPERATOR_PREC_2       ) \
	ENTRY(BINARY_OPERATOR_PREC_3       ) \
	ENTRY(BINARY_OPERATOR_PREC_4       ) \
	ENTRY(BINARY_OPERATOR_PREC_5       ) \
	ENTRY(BINARY_OPERATOR_PREC_6       ) \
	ENTRY(BINARY_OPERATOR_PREC_7       ) \
	ENTRY(BINARY_OPERATOR_PREC_8       ) \
	ENTRY(BINARY_OPERATOR_PREC_9       ) \
	ENTRY(BINARY_OPERATOR_PREC_10      ) \
	ENTRY(BINARY_OPERATOR_PREC_11      ) \
	ENTRY(STUB_TERNARY_CONDITIONAL     ) \
	ENTRY(BINARY_OPERATOR_PREC_12      ) \
	ENTRY(BINARY_OPERATOR_PREC_13      ) \
	ENTRY(EXPR_TERMINAL                ) \
	ENTRY(EXPR_POSTFIX_OP              ) \
	ENTRY(EXPR_STRUCT_ACCESS           ) \
	ENTRY(EXPR_PREFIX_OP               ) \
	ENTRY(EXPR_BINARY_OP_PREC_0        ) \
	ENTRY(EXPR_BINARY_OP_PREC_1        ) \
	ENTRY(EXPR_BINARY_OP_PREC_2        ) \
	ENTRY(EXPR_BINARY_OP_PREC_3        ) \
	ENTRY(EXPR_BINARY_OP_PREC_4        ) \
	ENTRY(EXPR_BINARY_OP_PREC_5        ) \
	ENTRY(EXPR_BINARY_OP_PREC_6        ) \
	ENTRY(EXPR_BINARY_OP_PREC_7        ) \
	ENTRY(EXPR_BINARY_OP_PREC_8        ) \
	ENTRY(EXPR_BINARY_OP_PREC_9        ) \
	ENTRY(EXPR_BINARY_OP_PREC_10       ) \
	ENTRY(EXPR_BINARY_OP_PREC_11       ) \
	ENTRY(EXPR_TERNARY_CONDITIONAL     ) \
	ENTRY(EXPR_BINARY_OP_PREC_12       ) \
	ENTRY(EXPR_BINARY_OP_PREC_13       ) \
	ENTRY(RETURN                       ) \
	ENTRY(COMMENT                      ) \
	ENTRY(FUNCTION_DECLARATION         ) \
	ENTRY(CODE_BLOCK                   ) \
	ENTRY(IF                           ) \
	ENTRY(WHILE                        ) \
	ENTRY(ELSE                         ) \
	ENTRY(SWITCH                       ) \
	ENTRY(VAL                          ) \
	ENTRY(FOR                          ) \
	ENTRY(FOR_RANGES                   ) \
	ENTRY(FOR_INDEX                    ) \
	ENTRY(FOR_COUNTER                  ) \
	ENTRY(LOOP                         ) \
	ENTRY(LOOP_BOOT                    ) \
	ENTRY(LOOP_CHECK                   ) \
	ENTRY(LOOP_ADVANCE                 ) \
	ENTRY(LOOP_EPILOGUE                ) \
	ENTRY(LABEL                        ) \
	ENTRY(BREAK                        ) \
	ENTRY(CONTINUE                     ) \
	ENTRY(STRUCT_DECLARATION           ) \
	ENTRY(BITFIELD_DECLARATION         ) \
	ENTRY(BITFIELD_ANONYMOUS           ) \
	ENTRY(UNION_DECLARATION            ) \
	ENTRY(UNION_ANONYMOUS              ) \
	ENTRY(ENUM_DECLARATION             ) \
	ENTRY(MANY_BITS_DECLARATION        ) \
	ENTRY(RETURN_TYPE                  ) \
	ENTRY(ARGUMENT_STRUCT              ) \
	ENTRY(STRUCT_BODY                  ) \
	ENTRY(VARIABLE_DECLARATION         ) \
	ENTRY(CONSTANT_DECLARATION         ) \
	ENTRY(RANGE_LITERAL                ) \
	ENTRY(RANGE_EXTREME                ) \
	ENTRY(RANGE_EXTREME_EXCLUDE        ) \
	ENTRY(RANGE_STEP                   ) \
	ENTRY(RANGE_STEP_MULT              ) \
	ENTRY(RANGE_STEP_SIGN              ) \
	ENTRY(STRUCT_LITERAL               ) \
	ENTRY(STRUCT_LITERAL_BODY          ) \
	ENTRY(STRUCT_MEMBER_VALUE          ) \
	ENTRY(LITERAL_INTEGER              ) \
	ENTRY(LITERAL_REAL                 ) \
	ENTRY(LITERAL_TYPE                 ) \
	ENTRY(LITERAL_STRING               ) \
	ENTRY(LITERAL_UCS4                 ) \
	ENTRY(TYPE                         ) \
	ENTRY(TYPE_ACCESS                  ) \
	ENTRY(TYPEOF                       ) \
	ENTRY(TYPE_ARRAY                   ) \
	ENTRY(TYPE_REFERENCE               ) \
	ENTRY(FUNCTION_TYPE                ) \
	ENTRY(GET_REFERENCE_OP             ) \
	ENTRY(DEREFERENCE_OP               ) \
	ENTRY(NAME                         ) \
	ENTRY(EXPRESSION                   ) \
	ENTRY(FUNCTION_CALL                ) \
	ENTRY(USING                        ) \

	// NOTE(theGiallo): removed. Now it's all FUNCTION_CALL.
	//ENTRY(TYPE_CAST                )

#define AST_NODE_TYPE_AS_ENUM(a) a,
#define AST_NODE_TYPE_AS_STRINGS(a) TOSTRING(a),

enum class
AST_Node_Type : u32
{
	AST_NODE_TYPE(AST_NODE_TYPE_AS_ENUM)
};
const char * ast_node_type_strings[] = {
	AST_NODE_TYPE(AST_NODE_TYPE_AS_STRINGS)
};

#define LITERAL_TYPE(ENTRY) \
	ENTRY("u8" , U8 ) \
	ENTRY("u16", U16) \
	ENTRY("u32", U32) \
	ENTRY("u64", U64) \
	ENTRY("s8" , S8 ) \
	ENTRY("s16", S16) \
	ENTRY("s32", S32) \
	ENTRY("s64", S64) \
	ENTRY("x8" , X8 ) \
	ENTRY("x16", X16) \
	ENTRY("x32", X32) \
	ENTRY("x64", X64) \
	ENTRY("b8" , B8 ) \
	ENTRY("b16", B16) \
	ENTRY("b32", B32) \
	ENTRY("b64", B64) \
	ENTRY("o8" , O8 ) \
	ENTRY("o16", O16) \
	ENTRY("o32", O32) \
	ENTRY("o64", O64) \
	ENTRY("f32", F32) \
	ENTRY("f64", F64) \

#define LITERAL_TYPE_AS_ENUM(a,b) b,
#define LITERAL_TYPE_AS_STRING(a,b) a,
enum class
Literal_Type : u8
{
	LITERAL_TYPE(LITERAL_TYPE_AS_ENUM)
	//---
	_COUNT
};
const char * literal_types_strings[] = {
	LITERAL_TYPE(LITERAL_TYPE_AS_STRING)
};
inline
bool
literal_type_is_hexadecimal( Literal_Type lt )
{
	bool ret = lt < Literal_Type::_COUNT && literal_types_strings[u32(lt)][0] == 'x';
	return ret;
}
inline
bool
literal_type_is_octal( Literal_Type lt )
{
	bool ret = lt < Literal_Type::_COUNT && literal_types_strings[u32(lt)][0] == 'o';
	return ret;
}
inline
bool
literal_type_is_binary( Literal_Type lt )
{
	bool ret = lt < Literal_Type::_COUNT && literal_types_strings[u32(lt)][0] == 'b';
	return ret;
}
inline
bool
literal_type_is_integer( Literal_Type lt )
{
	bool ret = lt < Literal_Type::_COUNT
	        && literal_types_strings[u32(lt)][0] != 'f';
	return ret;
}
inline
bool
literal_type_is_real( Literal_Type lt )
{
	bool ret = lt < Literal_Type::_COUNT
	        && literal_types_strings[u32(lt)][0] == 'f';
	return ret;
}
inline
bool
is_octal( Text_Piece tp )
{
	bool ret = true;
	for ( u32 i = 0; i != tp.length && ret; ++i )
	{
		ret = ascii_is_octal_digit( tp.text[i] );
	}
	return ret;
}
inline
bool
is_binary( Text_Piece tp )
{
	bool ret = true;
	for ( u32 i = 0; i != tp.length && ret; ++i )
	{
		ret = ascii_is_binary_digit( tp.text[i] );
	}
	return ret;
}

struct
AST_Node
{
	AST_Node_Type type;
	Text_Element_List_VM   text_element_nodes;
	AST_Node_p_List_VM     children;
	File_Coordinates_Range file_range;
	// TODO(theGiallo, 2018-02-26): add file reference (probably an index of a table)
};

struct
AST
{
	AST_Node * root;

	Pool_Allocator_Generic_VM nodes_allocator;
	Pool_Allocator_Generic_VM nodes_p_allocator;
	Pool_Allocator_Generic_VM text_elements_allocator;
	Freelist_Allocator        text_allocator;
};
#define AST_MAX_NODES 1*1024*1024L
#define AST_MAX_TOTAL_CHILDS AST_MAX_NODES * 32L
#define AST_MAX_TEXT_ELEMENTS 2 * AST_MAX_NODES
#define AST_TEXT_TOTAL_MAX_SIZE 128*1024*1024L

struct
Parser
{
	Text_Element_List_VM text_elements;
	Parser_Error_List_VM errors;
	AST                  ast;
};

void
parser_init( Parser * parser );

bool
parser_parse_file( Parser * parser, void * file_mem, s64 file_size );

void
parser_print_debug_parsed_elements( Parser * parser );

void
parser_print_debug_ast( Parser * parser );

#endif /* ifndef __PARSER_H__ */
