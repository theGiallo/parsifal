#ifndef MX_TEMPLATE_H
#define MX_TEMPLATE_H 1

#define MX_MAX_SIZE_FOR_LAPLACE_DETERMINANT 11
#define MX_TO_STRING(m) #m
#define MX_MACRO_VALUE_TO_STRING(m) MX_TO_STRING(m)
#ifndef mx_abort
#define mx_abort()
#endif

template <typename T, unsigned M, unsigned N>
struct
Mx
{
	T arr[M*N];

	const T *
	operator [] ( unsigned long i ) const
	{
		return this->arr + N*i;
	}

	T *
	operator [] ( unsigned long i )
	{
		return this->arr + N*i;
	}

	void
	print( FILE * f ) const
	{
		for ( unsigned i=0; i!=M; ++i )
		{
			for ( unsigned j=0; j!=N; ++j )
			{
				fprintf( f, "%s%15.10f", j?" ":"", (double)(*this)[i][j] );
			}
			fprintf( f, "\n" );
		}
	}
};

template<typename T, unsigned M, unsigned N, unsigned O>
Mx<T,M,O>
operator * ( const Mx<T,M,N> & m0, const Mx<T,N,O> & m1 )
{
	Mx<T,M,O> ret = {};

	for ( unsigned i = 0; i != M; ++i )
	{
		for ( unsigned j = 0; j != N; ++j )
		{
			for ( unsigned j1 = 0; j1 != O; ++j1 )
			{
				ret[i][j1] += m0[i][j] * m1[j][j1];
			}
		}
	}

	return ret;
}

template<typename T, unsigned M, unsigned N>
Mx<T,M,N>
operator + ( const Mx<T,M,N> & m0, const Mx<T,M,N> & m1 )
{
	Mx<T,M,N> ret = {};

	for ( unsigned i = 0; i != M; ++i )
	{
		for ( unsigned j = 0; j != N; ++j )
		{
			ret[i][j] = m0[i][j] + m1[i][j];
		}
	}

	return ret;
}

template<typename T, unsigned M, unsigned N>
Mx<T,M,N>
operator - ( const Mx<T,M,N> & m0, const Mx<T,M,N> & m1 )
{
	Mx<T,M,N> ret = {};

	for ( unsigned i = 0; i != M; ++i )
	{
		for ( unsigned j = 0; j != N; ++j )
		{
			ret[i][j] = m0[i][j] - m1[i][j];
		}
	}

	return ret;
}

template<typename T, unsigned M, unsigned N, typename T1>
Mx<T,M,N>
operator * ( const Mx<T,M,N> & m, T1 n)
{
	Mx<T,M,N> ret = m;

	for ( unsigned i = 0; i != M*N; ++i )
	{
		ret.arr[i] *= n;
	}

	return ret;
}

template<typename T, unsigned M, unsigned N, typename T1>
Mx<T,M,N>
operator * ( T1 n, const Mx<T,M,N> & m)
{
	Mx<T,M,N> ret = m;

	for ( unsigned i = 0; i != M*N; ++i )
	{
		ret.arr[i] *= n;
	}

	return ret;
}

template<typename T, unsigned M, unsigned N, typename T1>
Mx<T,M,N>
operator / ( const Mx<T,M,N> & m, T1 n)
{
	Mx<T,M,N> ret = m;

	for ( unsigned i = 0; i != M*N; ++i )
	{
		ret.arr[i] /= n;
	}

	return ret;
}

template<typename T, unsigned M, unsigned N, typename T1>
Mx<T,M,N>
operator / ( T1 n, const Mx<T,M,N> & m)
{
	Mx<T,M,N> ret = m;

	for ( unsigned i = 0; i != M*N; ++i )
	{
		ret.arr[i] /= n;
	}

	return ret;
}

template<typename T, unsigned M, unsigned N, typename T1>
Mx<T,M,N>
operator + ( const Mx<T,M,N> & m, T1 n)
{
	Mx<T,M,N> ret = m;

	for ( unsigned i = 0; i != M*N; ++i )
	{
		ret.arr[i] += n;
	}

	return ret;
}

template<typename T, unsigned M, unsigned N, typename T1>
Mx<T,M,N>
operator + ( T1 n, const Mx<T,M,N> & m)
{
	Mx<T,M,N> ret = m;

	for ( unsigned i = 0; i != M*N; ++i )
	{
		ret.arr[i] += n;
	}

	return ret;
}



template<typename T, unsigned M, unsigned N, typename T1>
Mx<T,M,N>
operator - ( const Mx<T,M,N> & m, T1 n)
{
	Mx<T,M,N> ret = m;

	for ( unsigned i = 0; i != M*N; ++i )
	{
		ret.arr[i] -= n;
	}

	return ret;
}

template<typename T, unsigned M, unsigned N, typename T1>
Mx<T,M,N>
operator - ( T1 n, const Mx<T,M,N> & m)
{
	Mx<T,M,N> ret = m;

	for ( unsigned i = 0; i != M*N; ++i )
	{
		ret.arr[i] -= n;
	}

	return ret;
}




// NOTE(theGiallo): transposed
template<typename T, unsigned M, unsigned N>
Mx<T,N,M>
operator ~ ( const Mx<T,M,N> & m )
{
	Mx<T,N,M> ret;

	for ( unsigned i = 0; i < M; ++i )
	{
		for ( unsigned j = 0; j < N; ++j )
		{
			ret[j][i] = m[i][j];
		}
	}

	return ret;
}

template<typename T, unsigned M, unsigned N>
Mx<T,M,N>
Mx_identity()
{
	Mx<T,M,N> ret = {};

	for ( unsigned i = 0; i < M && i < N; ++i )
	{
		ret[i][i] = 1.0;
	}

	return ret;
}

// NOTE(theGiallo): inverse
template<typename T, unsigned M>
Mx<T,M,M>
operator ! ( const Mx<T,M,M> & m )
{
	Mx<T,M,M> ret = {};

	T det = Mx_determinant( m );
	if ( det != 0 )
	{
		float inv_det = 1.0f / det;
		ret = Mx_adjugate( m ) * inv_det;
	} else
	{
		fprintf( stderr, "Trying to compute the in verse of a matrix that has determinant = 0, thus it's not invertible!\nmatrix:\n" );
		m.print( stderr );
	}

	return ret;
}
Mx<double,1,1>
operator ! ( const Mx<double,1,1> & m )
{
	Mx<double,1,1> ret = {1.0 / m.arr[0]};

	return ret;
}
template<typename T>
Mx<T,1,1>
operator ! ( const Mx<T,1,1> & m )
{
	Mx<T,1,1> ret = {1.0f / m.arr[0]};

	return ret;
}
template<typename T>
Mx<T,2,2>
operator ! ( const Mx<T,2,2> & m )
{
	Mx<T,2,2> ret;

	float inv_det = 1.0f / ( m[0][0]*m[1][1] - m[0][1]*m[1][0] );

	ret[0][0] =  m[1][1];
	ret[0][1] = -m[0][1];
	ret[1][0] = -m[1][0];
	ret[1][1] =  m[0][0];

	ret[0][0] *= inv_det;
	ret[0][1] *= inv_det;
	ret[1][0] *= inv_det;
	ret[1][1] *= inv_det;

	return ret;
}
Mx<double,2,2>
operator ! ( const Mx<double,2,2> & m )
{
	Mx<double,2,2> ret;

	double inv_det = 1.0 / ( m[0][0]*m[1][1] - m[0][1]*m[1][0] );

	ret[0][0] =  m[1][1];
	ret[0][1] = -m[1][0];
	ret[1][0] = -m[0][1];
	ret[1][1] =  m[0][0];

	ret[0][0] *= inv_det;
	ret[0][1] *= inv_det;
	ret[1][0] *= inv_det;
	ret[1][1] *= inv_det;

	return ret;
}
template<typename T>
Mx<T,3,3>
operator ! ( const Mx<T,3,3> & m )
{
	Mx<T,3,3> ret = {
		m[1][1]*m[2][2] - m[1][2]*m[2][1], m[0][2]*m[2][1] - m[0][1]*m[2][2], m[0][1]*m[1][2] - m[0][2]*m[1][1],
		m[1][2]*m[2][0] - m[1][0]*m[2][2], m[0][0]*m[2][2] - m[0][2]*m[2][0], m[0][2]*m[1][0] - m[0][0]*m[1][2],
		m[0][1]*m[2][1] - m[1][1]*m[2][0], m[0][1]*m[0][2] - m[0][0]*m[2][1], m[0][0]*m[1][1] - m[0][1]*m[1][0]
	};

	float inv_det = 1.0f / ( m[0][0]*ret[0][0] + m[0][1]*ret[1][0] + m[0][2]*ret[2][0] );

	for ( int i = 0; i < 9; ++i )
	{
		ret.arr[i] *= inv_det;
	}

	return ret;
}
Mx<double,3,3>
operator ! ( const Mx<double,3,3> & m )
{
	Mx<double,3,3> ret = {
		m[1][1]*m[2][2] - m[1][2]*m[2][1], m[0][2]*m[2][1] - m[0][1]*m[2][2], m[0][1]*m[1][2] - m[0][2]*m[1][1],
		m[1][2]*m[2][0] - m[1][0]*m[2][2], m[0][0]*m[2][2] - m[0][2]*m[2][0], m[0][2]*m[1][0] - m[0][0]*m[1][2],
		m[0][1]*m[2][1] - m[1][1]*m[2][0], m[0][1]*m[0][2] - m[0][0]*m[2][1], m[0][0]*m[1][1] - m[0][1]*m[1][0]
	};

	double inv_det = 1.0f / ( m[0][0]*ret[0][0] + m[0][1]*ret[1][0] + m[0][2]*ret[2][0] );

	for ( int i = 0; i < 9; ++i )
	{
		ret.arr[i] *= inv_det;
	}

	return ret;
}
template<typename T>
Mx<T,4,4>
operator ! ( const Mx<T,4,4> & m )
{
	Mx<T,4,4> ret;

	Mx<T,4,4> m2 = m * m,
	          m3 = m2 * m,
	          I  = Mx_identity<T,4,4>();
	float tr_m  = Mx_trace( m  ),
	      tr_m2 = Mx_trace( m2 ),
	      tr_m3 = Mx_trace( m3 ),
	      tr2_m = tr_m * tr_m,
	      tr3_m = tr_m * tr_m * tr_m,
	      det = Mx_determinant( m ),
	      inv_det = 1.0f / det;

	ret = inv_det * ( I  * ( ( tr3_m - 3.0f * tr_m * tr_m2 + 2.0f * tr_m3 ) / 6.0f ) -
	                  m  * ( 0.5f * ( tr2_m - tr_m2 ) ) +
	                  m2 * tr_m -
	                  m3 );

	return ret;
}
template<typename T, unsigned M>
T
Mx_trace( const Mx<T,M,M> & m )
{
	T ret = {};

	for ( unsigned i = 0; i < M; ++i )
	{
		ret += m[i][i];
	}

	return ret;
}

template<typename T, unsigned M, unsigned N>
Mx<T,M-1,N-1>
Mx_minor( const Mx<T,M,N> & m, unsigned i, unsigned j )
{
	Mx<T,M-1,N-1> ret;

	for ( unsigned ii = 0; ii < i; ++ii )
	{
		for ( unsigned jj = 0; jj < j; ++jj )
		{
			ret[ii  ][jj  ] = m[ii][jj];
		}
		for ( unsigned jj = j+1; jj < N; ++jj )
		{
			ret[ii  ][jj-1] = m[ii][jj];
		}
	}

	for ( unsigned ii = i+1; ii < M; ++ii )
	{
		for ( unsigned jj = 0; jj < j; ++jj )
		{
			ret[ii-1][jj  ] = m[ii][jj];
		}
		for ( unsigned jj = j+1; jj < N; ++jj )
		{
			ret[ii-1][jj-1] = m[ii][jj];
		}
	}

	return ret;
}
template<typename T, unsigned M>
T
Mx_determinant( const Mx<T,M,M> & m )
{
	T ret = {};

	if ( M <= MX_MAX_SIZE_FOR_LAPLACE_DETERMINANT )
	{
		unsigned i = 0;
		for ( unsigned j = 0; j < M; ++j )
		{
			T det  = Mx_determinant( Mx_minor( m, i, j ) ),
		  	  sign = -2 * ( ( i + j ) % 2 ) + 1;
			ret += sign * m[i][j] * det;
		}
	} else
	{
		fprintf( stderr, "determinant function for matrices of size greater than " MX_MACRO_VALUE_TO_STRING( MX_MAX_SIZE_FOR_LAPLACE_DETERMINANT ) " has not yet been implemented!\n" );
		mx_abort();
	}

	return ret;
}
template<typename T>
T
Mx_determinant( const Mx<T,1,1> & m )
{
	T ret = m[0][0];
	return ret;
}
template<typename T, unsigned M>
Mx<T,M,M>
Mx_cofactor( const Mx<T,M,M> & m )
{
	Mx<T,M,M> ret;

	for ( unsigned i = 0; i < M; ++i )
	{
		for ( unsigned j = 0; j < M; ++j )
		{
			T det  = Mx_determinant( Mx_minor( m, i, j ) ),
			  sign = -2 * ( ( i + j ) % 2 ) + 1;
			ret[i][j] = sign * det;
		}
	}

	return ret;
}
template<typename T, unsigned M>
Mx<T,M,M>
Mx_adjugate( const Mx<T,M,M> & m )
{
	Mx<T,M,M> ret;

	ret = ~Mx_cofactor( m );

	return ret;
}

template<unsigned M>
double
length( const Mx<double,M,1> & v )
{
	double ret = {};

	for ( int i = 0; i < 1; ++i )
	{
		ret += v[i][0] * v[i][0];
	}

	ret = sqrt( ret );

	return ret;
}
template<typename T, unsigned M>
float
length( const Mx<T,M,1> & v )
{
	float ret = {};

	for ( int i = 0; i < 1; ++i )
	{
		ret += v[i][0] * v[i][0];
	}

	ret = sqrt( ret );

	return ret;
}
#endif /* ifndef MX_TEMPLATE_H */
