#include "utility.h"
#include "tgmath.h"

////////////////////////////////////////////////////////////////////////////////

// NOTE(theGiallo): the first time pass *internal_bit = 0
internal
u8
rand_bit( u64 seeds[2], u64 * internal_data,
          u8 * internal_bit_id )
{
	if ( !*internal_bit_id  || *internal_bit_id > 64 )
	{
		*internal_bit_id = 1;
		*internal_data = xorshift128plus( seeds );
	}

	u8 ret = ( *internal_data >> ( *internal_bit_id - 1 ) ) & 0x1;

	++*internal_bit_id;

	return ret;
}

internal
f32
randf( u64 seeds[2] )
{
	u64 rnd = xorshift128plus( seeds );
	f64 d = ( rnd / (f64)U64_MAX );
	f32 ret = (f64)FLT_MIN + d * ( (f64)FLT_MAX - (f64)FLT_MIN );
	return ret;
}

internal
f32
randd_01( u64 seeds[2] )
{
	u64 rnd = xorshift128plus( seeds );
	f32 ret = ( rnd / (f32)UINT64_MAX );
	return ret;
}

internal
f32
randd_between( u64 seeds[2], f32 left, f32 right )
{
	u64 rnd = xorshift128plus( seeds );
	f32 ret = ( rnd / (f32)U64_MAX ) * ( right - left ) + left;
	return ret;
}

internal
u32
randu_between( u64 seeds[2], u32 min, u32 max )
{
	u64 rnd = xorshift128plus( seeds );
	u32 * rnd2 = (u32*)&rnd;
	u32 ret = min + ( ( rnd2[0] ^ rnd2[1] ) % ( max + 1 - min ) );
	return ret;
}

// NOTE(theGiallo): random u32 in [0, count-1] interval
internal
u32
randu_with_distribution( u64 seeds[2], f32 * probability_distribution, u32 count )
{
	u32 ret = count - 1;
	f32 rnd = randd_01( seeds );

	f32 accum = 0.0f;
	for ( u64 i = 0; i!=count; ++i )
	{
		accum += probability_distribution[i];
		if ( rnd < accum )
		{
			ret = i;
			break;
		}
	}

	return ret;
}

internal
f64
randd_gauss_01( Marsaglia_Gauss_Data * g_data )
{
	f64 ret;

	if ( g_data->phase == 0 )
	{
		do
		{
			g_data->u1 = randd_01( g_data->seeds );
			g_data->u2 = randd_01( g_data->seeds );

			g_data->v1 = 2 * g_data->u1 - 1.0;
			g_data->v2 = 2 * g_data->u2 - 1.0;
			g_data->s = square( g_data->v1 ) + square( g_data->v2 );
		} while ( g_data->s >= 1 || g_data->s == 0 );

		ret = g_data->v1 * sqrt( -2.0 * log( g_data->s ) / g_data->s );
	} else
	{
		ret = g_data->v2 * sqrt( -2.0 * log( g_data->s ) / g_data->s );
	}

	g_data->phase = 1 - g_data->phase;

	return ret;
}

////////////////////////////////////////////////////////////////////////////////

// http://www.isthe.com/chongo/tech/comp/fnv/index.html

internal
u32
u32_FNV1a( const char * str )
{
	u32 ret;

	u32 FNV_prime    = 16777619,
	    offset_basis = 2166136261;

	ret = offset_basis;
	for ( u32 i = 0; str[i]; ++i )
	{
		ret = ret ^ (u32)str[i];
		ret = ret * FNV_prime;
	}

	return ret;
}

internal
u32
u32_FNV1a( const void * str, u32 bytes_size )
{
	u32 ret;

	u32 FNV_prime    = 16777619,
	    offset_basis = 2166136261;

	ret = offset_basis;
	for ( u32 i = 0; i < bytes_size; ++i )
	{
		ret = ret ^ (u32)((u8*)str)[i];
		ret = ret * FNV_prime;
	}

	return ret;
}

internal
u64
u64_FNV1a( const void * data, u64 bytes_size )
{
	u64 ret;

	u64 FNV_prime    = 1099511628211UL,
	    offset_basis = 14695981039346656037UL;

	ret = offset_basis;
	for ( u64 i = 0; i < bytes_size; ++i )
	{
		ret = ret ^ (u64)((u8*)data)[i];
		ret = ret * FNV_prime;
	}

	return ret;
}

internal
u64
u64_FNV1a( const char * str )
{
	u64 ret;

	u64 FNV_prime    = 1099511628211UL,
	    offset_basis = 14695981039346656037UL;

	ret = offset_basis;
	for ( u64 i = 0; str[i]; ++i )
	{
		ret = ret ^ (u64)str[i];
		ret = ret * FNV_prime;
	}

	return ret;
}



////////////////////////////////////////////////////////////////////////////////

void
reorder_16_s( u16 * ids, void * data, u32 count,
                  u32 elem_size )
{
	u8 v[elem_size];
	u8 moved[BITNSLOTS( 0xFFFF )] = {};
	for ( u32 i = 0; i < count; ++i )
	{
		if ( BITTEST( moved, i ) )
		{
			continue;
		}

		memcpy( v, (u8*)data + i * elem_size, elem_size );
		for ( u16 j = i; ; )
		{
			u16 next = ids[j];
			if ( next == i )
			{
				memcpy( (u8*)data + j * elem_size, v, elem_size );
				BITSET( moved, j );
				break;
			}
			memcpy( (u8*)data + j * elem_size, (u8*)data + next * elem_size, elem_size );
			BITSET( moved, j );
			j = next;
		}
	}
}
void
reorder_16_8( u16 * ids, u8 * data, u32 count )
{
	u8 v;
	u8 moved[BITNSLOTS( 0xFFFF )] = {};
	for ( u32 i = 0; i < count; ++i )
	{
		if ( BITTEST( moved, i ) )
		{
			continue;
		}

		v = data[i];
		for ( u16 j = i; ; )
		{
			u16 next = ids[j];
			if ( next == i )
			{
				data[j] = v;
				BITSET( moved, j );
				break;
			}
			data[j] = data[next];
			BITSET( moved, j );
			j = next;
		}
	}
}
void
reorder_16( u16 * ids, u16 * data, u32 count )
{
	u16 v;
	u8 moved[BITNSLOTS( 0xFFFF )] = {};
	for ( u32 i = 0; i < count; ++i )
	{
		if ( BITTEST( moved, i ) )
		{
			continue;
		}

		v = data[i];
		for ( u16 j = i; ; )
		{
			u16 next = ids[j];
			if ( next == i )
			{
				data[j] = v;
				BITSET( moved, j );
				break;
			}
			data[j] = data[next];
			BITSET( moved, j );
			j = next;
		}
	}
}

void
reorder_32( u32 * ids, u32 * data, u32 count )
{
	u32 v;
	u8 moved[BITNSLOTS( count )];
	// NOTE(theGiallo): it's dynamic, so we cant initialize it
	memset( moved, 0, BITNSLOTS( count ) * CHAR_BIT );
	for ( u32 i = 0; i < count; ++i )
	{
		if ( BITTEST( moved, i ) )
		{
			continue;
		}

		v = data[i];
		for ( u32 j = i; ; )
		{
			u32 next = ids[j];
			if ( next == i )
			{
				data[j] = v;
				BITSET( moved, j );
				break;
			}
			data[j] = data[next];
			BITSET( moved, j );
			j = next;
		}
	}
}

void
radix_sort_mc_16( u16 * keys, u16 * ids, u16 * tmp,
                      u32 count )
{
	u32 firsts[2][256] = {};

	// NOTE(theGiallo): count buckets occurrencies
	for ( u32 i = 0; i != count; ++i )
	{
		++firsts[0][( keys[i]      ) & 0xFF];
		++firsts[1][( keys[i] >> 8 ) & 0xFF];
	}

	// NOTE(theGiallo): calculate the position of the last element of every bucket
	u32 counts[2] = {0,0};
	for ( u32 i = 0; i != 256; ++i )
	{
		u32 v[2];
		v[0] = firsts[0][i];
		v[1] = firsts[1][i];
		firsts[0][i] = counts[0];
		firsts[1][i] = counts[1];
		counts[0] += v[0];
		counts[1] += v[1];
	}

	for ( u8 b = 0; b != 2; ++b )
	{
		for ( u32 i = 0; i != count; ++i )
		{
			tg_assert_m( ids[ i ] < count, "ids[ i ] = %d i=%d " HERE_COLOR() "\n",
			             ids[ i ], i );

			u8 id = ( keys[ ids[i] ] >> ( 8 * b ) ) & 0xFF;

			tg_assert_m( firsts[b][ id ] < count,
			             "firsts[b][ id ] = %d b=%d id=%d " HERE_COLOR() "\n",
			             firsts[b][ id ], b, id );

			tmp[ firsts[b][ id ]++ ] = ids[i];
		}
		u16 * swp = tmp;
		          tmp = ids;
		          ids = swp;
	}
}

void
radix_sort_mc_32( u32 * keys, u32 * ids, u32 * tmp,
                      u32 count )
{
	u32 firsts[4][256] = {};

	// NOTE(theGiallo): count buckets occurrencies
	for ( u32 i = 0; i != count; ++i )
	{
		++firsts[0][( keys[i]       ) & 0xFF];
		++firsts[1][( keys[i] >> 8  ) & 0xFF];
		++firsts[2][( keys[i] >> 16 ) & 0xFF];
		++firsts[3][( keys[i] >> 24 ) & 0xFF];
	}

	// NOTE(theGiallo): calculate the position of the last element of every bucket
	u32 counts[4] = {};
	for ( u32 i = 0; i != 256; ++i )
	{
		u32 v[4];
		v[0] = firsts[0][i];
		v[1] = firsts[1][i];
		v[2] = firsts[2][i];
		v[3] = firsts[3][i];
		firsts[0][i] = counts[0];
		firsts[1][i] = counts[1];
		firsts[2][i] = counts[2];
		firsts[3][i] = counts[3];
		counts[0] += v[0];
		counts[1] += v[1];
		counts[2] += v[2];
		counts[3] += v[3];
	}

	for ( u8 b=0; b!=4; ++b )
	{
		for ( u32 i = 0; i != count; ++i )
		{
			u8 id = ( keys[ ids[i] ] >> ( 8 * b ) ) & 0xFF;
			tmp[ firsts[b][ id ]++ ] = ids[i];
		}
		u32 * swp = tmp;
		          tmp = ids;
		          ids = swp;
	}
}

void
radix_sort_mc_32_16( u32 * keys, u16 * ids, u16 * tmp,
                         u32 count )
{
	u32 firsts[4][256] = {};

	// NOTE(theGiallo): count buckets occurrencies
	for ( u32 i = 0; i != count; ++i )
	{
		++firsts[0][( keys[i]       ) & 0xFF];
		++firsts[1][( keys[i] >> 8  ) & 0xFF];
		++firsts[2][( keys[i] >> 16 ) & 0xFF];
		++firsts[3][( keys[i] >> 24 ) & 0xFF];
	}

	// NOTE(theGiallo): calculate the position of the last element of every bucket
	u32 counts[4] = {};
	for ( u32 i = 0; i != 256; ++i )
	{
		u32 v[4];
		v[0] = firsts[0][i];
		v[1] = firsts[1][i];
		v[2] = firsts[2][i];
		v[3] = firsts[3][i];
		firsts[0][i] = counts[0];
		firsts[1][i] = counts[1];
		firsts[2][i] = counts[2];
		firsts[3][i] = counts[3];
		counts[0] += v[0];
		counts[1] += v[1];
		counts[2] += v[2];
		counts[3] += v[3];
	}

	for ( u8 b=0; b!=4; ++b )
	{
		for ( u32 i = 0; i != count; ++i )
		{
			u8 id = ( keys[ ids[i] ] >> ( 8 * b ) ) & 0xFF;
			tmp[ firsts[b][ id ]++ ] = ids[i];
		}
		u16 * swp = tmp;
		          tmp = ids;
		          ids = swp;
	}
}

void
radix_sort_mc_32_keys_only( u32 * keys, u32 * tmp, u32 count )
{
	u32 firsts[4][256] = {};

	// NOTE(theGiallo): count buckets occurrencies
	for ( u32 i = 0; i != count; ++i )
	{
		++firsts[0][( keys[i]       ) & 0xFF];
		++firsts[1][( keys[i] >> 8  ) & 0xFF];
		++firsts[2][( keys[i] >> 16 ) & 0xFF];
		++firsts[3][( keys[i] >> 24 ) & 0xFF];
	}

	// NOTE(theGiallo): calculate the position of the last element of every bucket
	u32 counts[4] = {};
	for ( u32 i = 0; i != 256; ++i )
	{
		u32 v[4];
		v[0] = firsts[0][i];
		v[1] = firsts[1][i];
		v[2] = firsts[2][i];
		v[3] = firsts[3][i];
		firsts[0][i] = counts[0];
		firsts[1][i] = counts[1];
		firsts[2][i] = counts[2];
		firsts[3][i] = counts[3];
		counts[0] += v[0];
		counts[1] += v[1];
		counts[2] += v[2];
		counts[3] += v[3];
	}

	for ( u8 b=0; b!=4; ++b )
	{
		for ( u32 i = 0; i != count; ++i )
		{
			u8 id = ( keys[i] >> ( 8 * b ) ) & 0xFF;
			tmp[ firsts[b][ id ]++ ] = keys[i];
		}
		u32 * swp = tmp;
		          tmp = keys;
		         keys = swp;
	}
}

void
insertion_sort_16( u16 * keys, u16 * data, u32 count )
{
	for ( u32 i = 1; i < count; ++i )
	{
		u16 k = keys[i];
		u16 d = data[i];
		if ( keys[i-1] > k )
		{
			u32 j = i;
			do
			{
				keys[j] = keys[j-1];
				data[j] = data[j-1];
			} while ( --j > 0 && keys[j-1] > k );
			keys[j] = k;
			data[j] = d;
		}
	}
}

void
insertion_sort_32( u32 * keys, u32 * data, u32 count )
{
	for ( u32 i = 1; i < count; ++i )
	{
		u32 k = keys[i];
		u32 d = data[i];
		if ( keys[i-1] > k )
		{
			u32 j = i;
			do
			{
				keys[j] = keys[j-1];
				data[j] = data[j-1];
			} while ( --j > 0 && keys[j-1] > k );
			keys[j] = k;
			data[j] = d;
		}
	}
}

void
radix_sort_bucket_32( u32 * keys, u32 * data, u32 count,
                          u8 shift )
{
	if ( count <= INSERTION_SORT_32_IS_BETTER_MAX_COUNT )
	{
		insertion_sort_32( keys, data, count );
		return;
	}

	u32 last[256] = {},
	        pointer[256];

	for ( u32 i = 0; i != count; ++i )
	{
		++last[( keys[i] >> shift ) & 0xFF];
	}

	pointer[0] = 0;

	for ( u32 i = 1; i != 256; ++i )
	{
		pointer[i] = last[i-1];
		last[i] += last[i-1];
	}

	for ( u32 i = 0; i != 256; ++i )
	{
		u32 value, data_value;
		u8 j;
		while ( pointer[i] != last[i] )
		{
			value = keys[pointer[i]];
			data_value = data[pointer[i]];
			j = ( value >> shift ) & 0xFF;
			while ( i != j )
			{
				     u32 tmp = keys[pointer[j]];
				keys[pointer[j]] = value;
				           value = tmp;

				             tmp = data[pointer[j]];
				data[pointer[j]] = data_value;
				      data_value = tmp;

				++pointer[j];

				j = ( value >> shift ) & 0xFF;
			}
			keys[pointer[j]] = value;
			data[pointer[j]] = data_value;
			++pointer[j];
		}
	}

	if ( shift )
	{
		shift -= 8;

		for ( u32 i = 0; i < 256; ++i )
		{
			u32 new_start = i ? last[i-1] : 0;
			u32 new_count = i ? last[i] - last[i-1] : last[0];
			if ( new_count )
			{
				radix_sort_bucket_32( keys + new_start, data + new_start, new_count, shift );
			}
		}
	}
}

void
radix_sort_32( u32 * keys, u32 * data, u32 count )
{
	radix_sort_bucket_32( keys, data, count, 24 );
}


void
radix_sort_bucket_16( u16 * keys, u16 * data, u32 count,
                          u8 shift )
{
	if ( count <= INSERTION_SORT_16_IS_BETTER_MAX_COUNT )
	{
		insertion_sort_16( keys, data, count );
		return;
	}

	u32 last[256] = {},
	          pointer[256];

	for ( u32 i = 0; i != count; ++i )
	{
		++last[( keys[i] >> shift ) & 0xFF];
	}

	pointer[0] = 0;

	for ( u32 i = 1; i != 256; ++i )
	{
		pointer[i] = last[i-1];
		last[i] += last[i-1];
	}

	for ( u16 i = 0; i != 256; ++i )
	{
		u16 value, data_value;
		u8 j;
		while ( pointer[i] != last[i] )
		{
			value = keys[pointer[i]];
			data_value = data[pointer[i]];
			j = ( value >> shift ) & 0xFF;
			while ( i != j )
			{
				     u16 tmp = keys[pointer[j]];
				keys[pointer[j]] = value;
				           value = tmp;

				             tmp = data[pointer[j]];
				data[pointer[j]] = data_value;
				      data_value = tmp;

				++pointer[j];

				j = ( value >> shift ) & 0xFF;
			}
			keys[pointer[j]] = value;
			data[pointer[j]] = data_value;
			++pointer[j];
		}
	}

	if ( shift )
	{
		shift -= 8;

		for ( u16 i = 0; i < 256; ++i )
		{
			u32 new_start = i ? last[i-1] : 0;
			u32 new_count = i ? last[i] - last[i-1] : last[0];
			if ( new_count )
			{
				radix_sort_bucket_16( keys + new_start, data + new_start, new_count, shift );
			}
		}
	}
}

void
radix_sort_16( u16 * keys, u16 * data, u32 count )
{
	radix_sort_bucket_16( keys, data, count, 8 );
}

void
quicksort_iterative_32( u32 * keys, u32 * data, u32 count )
{
	u32 left = 0,
	          stack[MAX_STACK_QSIT],
	          pos = 0;
	u64 seeds[2] = { 2873467L, 2379425723943L };
	for (;;)
	{
		for ( ; left + 1 < count; count++ )
		{
			if ( pos == MAX_STACK_QSIT )
			{
				pos = 0;
				count = stack[pos]; // NOTE(theGiallo): reset on stack overflow
			}
			u32 rnd = ( xorshift128plus( seeds ) >> 32 ) & U32_MAX;
			// NOTE(theGiallo): pick random pivot
			u32 pivot = keys[left + rnd % ( count - left )];
			stack[pos++] = count; // NOTE(theGiallo): sort right part later

			// NOTE(theGiallo): partitioning
			for ( u32 right = left - 1; ; )
			{
				// NOTE(theGiallo): look for greater element
				while ( keys[++right] < pivot );
				// NOTE(theGiallo): look for smaller element
				while ( pivot < keys[--count] );

				if ( right >= count )
				{
					break; // NOTE(theGiallo):  partition point found?
				}
				  u32 tmp = keys[right];
				  keys[right] = keys[count];
				  keys[count] = tmp;
				          tmp = data[right];
				  data[right] = data[count];
				  data[count] = tmp;
			}
			// NOTE(theGiallo): partitioned, continue left part
		}
		if ( !pos)
		{
			break; // NOTE(theGiallo): stack empty?
		}
		left = count; // NOTE(theGiallo): left to right is sorted
		count = stack[--pos]; // NOTE(theGiallo): get next range to sort
	}
}

void
quicksort_iterative_16( u16 * keys, u16 * data, u32 count )
{
	u32 left = 0,
	    stack[MAX_STACK_QSIT],
	    pos = 0;
	u64 seeds[2] = { 2873467L, 2379425723943L };
	for (;;)
	{
		for ( ; left + 1 < count; count++ )
		{
			if ( pos == MAX_STACK_QSIT )
			{
				pos = 0;
				count = stack[pos]; // NOTE(theGiallo): reset on stack overflow
			}
			u16 rnd = xorshift128plus( seeds ) & U16_MAX;
			// NOTE(theGiallo): pick random pivot
			u32 pivot = keys[left + rnd % ( count - left )];
			stack[pos++] = count; // NOTE(theGiallo): sort right part later

			// NOTE(theGiallo): partitioning
			for ( u32 right = left - 1; ; )
			{
				// NOTE(theGiallo): look for greater element
				while ( keys[++right] < pivot );
				// NOTE(theGiallo): look for smaller element
				while ( pivot < keys[--count] );

				if ( right >= count )
				{
					break; // NOTE(theGiallo):  partition point found?
				}
				  u16 tmp = keys[right];
				  keys[right] = keys[count];
				  keys[count] = tmp;
				          tmp = data[right];
				  data[right] = data[count];
				  data[count] = tmp;
			}
			// NOTE(theGiallo): partitioned, continue left part
		}
		if ( !pos)
		{
			break; // NOTE(theGiallo): stack empty?
		}
		left = count; // NOTE(theGiallo): left to right is sorted
		count = stack[--pos]; // NOTE(theGiallo): get next range to sort
	}
}

internal
inline
u32
u32_from_f32_for_radixsort( f32 f )
{
	u32 ret;
	u32 uf = *(u32*)&f;
	u32 mask = -s32( uf >> 31 ) | 0x80000000;
	ret = uf ^ mask;

	return ret;
}

internal
void
radix_sort_f32( f32 * keys_arr, u32 keys_arr_size, u16 * ids,
                Sort_Order sort_order,
                bool restore_keys_values )
{
#if DEBUG
	{
		f32 test_float = 3.0f;
		tg_assert(
		   f32_from_u32_after_radixsort(
		      u32_from_f32_for_radixsort( test_float ) )
		   == test_float );
	}
#endif

	for ( u32 i = 0; i < keys_arr_size; ++i )
	{
		u32 uf;
		if ( sort_order == SORT_ASCENDANTLY )
		{
			uf = u32_from_f32_for_radixsort( keys_arr[i] );
		} else
		{
			uf = u32_from_f32_for_radixsort( -keys_arr[i] );
		}
		keys_arr[i] = *(f32*)&uf;
	}

	u16 tmp[keys_arr_size];
	radix_sort_mc_32_16( (u32*)keys_arr, ids, tmp, keys_arr_size );

	if ( restore_keys_values )
	{
		for ( u32 i = 0; i < keys_arr_size; ++i )
		{
			u32 uf = f32_from_u32_after_radixsort( keys_arr[i] );
			keys_arr[i] = *(f32*)&uf;
		}
	}
}

internal
void
radix_sort_mc_32_keys_only( u32 * keys, u32 * tmp, u32 count, Sort_Order sort_order )
{
	u32 firsts[4][256] = {};

	// NOTE(theGiallo): count buckets occurrences
	if ( sort_order == SORT_ASCENDANTLY )
	{
		for ( u32 i = 0; i != count; ++i )
		{
			++firsts[0][( keys[i] >> 0  ) & 0xFF];
			++firsts[1][( keys[i] >> 8  ) & 0xFF];
			++firsts[2][( keys[i] >> 16 ) & 0xFF];
			++firsts[3][( keys[i] >> 24 ) & 0xFF];
		}
	} else
	{
		for ( u32 i = 0; i != count; ++i )
		{
			++firsts[0][0xFF - ( ( keys[i] >> 0  ) & 0xFF )];
			++firsts[1][0xFF - ( ( keys[i] >> 8  ) & 0xFF )];
			++firsts[2][0xFF - ( ( keys[i] >> 16 ) & 0xFF )];
			++firsts[3][0xFF - ( ( keys[i] >> 24 ) & 0xFF )];
		}
	}

	// NOTE(theGiallo): calculate the position of the last element of every bucket
	u32 counts[4] = {};
	for ( u32 i = 0; i != 256; ++i )
	{
		u32 v[4];
		v[0] = firsts[0][i];
		v[1] = firsts[1][i];
		v[2] = firsts[2][i];
		v[3] = firsts[3][i];
		firsts[0][i] = counts[0];
		firsts[1][i] = counts[1];
		firsts[2][i] = counts[2];
		firsts[3][i] = counts[3];
		counts[0] += v[0];
		counts[1] += v[1];
		counts[2] += v[2];
		counts[3] += v[3];
	}

	for ( u8 b=0; b!=4; ++b )
	{
		for ( u32 i = 0; i != count; ++i )
		{
			u8 id;
			if ( sort_order == SORT_ASCENDANTLY )
			{
				id = ( keys[i] >> ( 8 * b ) ) & 0xFF;
			} else
			{
				id = 0xFF - ( ( keys[i] >> ( 8 * b ) ) & 0xFF );
			}
			tmp[ firsts[b][ id ]++ ] = keys[i];
		}
		u32 * swp = tmp;
		      tmp = keys;
		      keys = swp;
	}
}

internal
void
radix_sort_mc_32_16( const u32 * keys, u16 * ids, u16 * tmp, u32 count, Sort_Order sort_order )
{
#define BYTES_COUNT 4
	u32 firsts[BYTES_COUNT][256] = {};

	// NOTE(theGiallo): count buckets occurrences
	if ( sort_order == SORT_ASCENDANTLY )
	{
		for ( u32 i = 0; i != count; ++i )
		{
			++firsts[0][( keys[i]       ) & 0xFF];
			++firsts[1][( keys[i] >> 8  ) & 0xFF];
			++firsts[2][( keys[i] >> 16 ) & 0xFF];
			++firsts[3][( keys[i] >> 24 ) & 0xFF];
		}
	} else
	{
		for ( u32 i = 0; i != count; ++i )
		{
			++firsts[0][0xFF - ( ( keys[i] >> 0  ) & 0xFF )];
			++firsts[1][0xFF - ( ( keys[i] >> 8  ) & 0xFF )];
			++firsts[2][0xFF - ( ( keys[i] >> 16 ) & 0xFF )];
			++firsts[3][0xFF - ( ( keys[i] >> 24 ) & 0xFF )];
		}
	}

	// NOTE(theGiallo): calculate the position of the last element of every bucket
	u32 counts[BYTES_COUNT] = {};
	for ( u32 i = 0; i != 256; ++i )
	{
		u32 v[BYTES_COUNT];
		v[0] = firsts[0][i];
		v[1] = firsts[1][i];
		v[2] = firsts[2][i];
		v[3] = firsts[3][i];
		firsts[0][i] = counts[0];
		firsts[1][i] = counts[1];
		firsts[2][i] = counts[2];
		firsts[3][i] = counts[3];
		counts[0] += v[0];
		counts[1] += v[1];
		counts[2] += v[2];
		counts[3] += v[3];
	}

	for ( u8 b=0; b!=BYTES_COUNT; ++b )
	{
		for ( u32 i = 0; i != count; ++i )
		{
			u8 id;
			if ( sort_order == SORT_ASCENDANTLY )
			{
				id = ( keys[ ids[i] ] >> ( 8 * b ) ) & 0xFF;
			} else
			{
				id = 0xFF - ( ( keys[ ids[i] ] >> ( 8 * b ) ) & 0xFF );
			}
			tmp[ firsts[b][ id ]++ ] = ids[i];
		}
		u16 * swp = tmp;
		      tmp = ids;
		      ids = swp;
	}
#undef BYTES_COUNT
}

internal
void
radix_sort_mc_64_16( const u64 * keys, u16 * ids, u16 * tmp, u32 count, Sort_Order sort_order )
{
#define BYTES_COUNT 8
	u32 firsts[BYTES_COUNT][256] = {};

	// NOTE(theGiallo): count buckets occurrencies
	if ( sort_order == SORT_ASCENDANTLY )
	{
		for ( u32 i = 0; i != count; ++i )
		{
			++firsts[0][( keys[i]       ) & 0xFF];
			++firsts[1][( keys[i] >> 8  ) & 0xFF];
			++firsts[2][( keys[i] >> 16 ) & 0xFF];
			++firsts[3][( keys[i] >> 24 ) & 0xFF];
			++firsts[4][( keys[i] >> 32 ) & 0xFF];
			++firsts[5][( keys[i] >> 40 ) & 0xFF];
			++firsts[6][( keys[i] >> 48 ) & 0xFF];
			++firsts[7][( keys[i] >> 56 ) & 0xFF];
		}
	} else
	{
		for ( u32 i = 0; i != count; ++i )
		{
			++firsts[0][0xFF - ( ( keys[i] >> 0  ) & 0xFF )];
			++firsts[1][0xFF - ( ( keys[i] >> 8  ) & 0xFF )];
			++firsts[2][0xFF - ( ( keys[i] >> 16 ) & 0xFF )];
			++firsts[3][0xFF - ( ( keys[i] >> 24 ) & 0xFF )];
			++firsts[4][0xFF - ( ( keys[i] >> 32 ) & 0xFF )];
			++firsts[5][0xFF - ( ( keys[i] >> 40 ) & 0xFF )];
			++firsts[6][0xFF - ( ( keys[i] >> 48 ) & 0xFF )];
			++firsts[7][0xFF - ( ( keys[i] >> 56 ) & 0xFF )];
		}
	}

	// NOTE(theGiallo): calculate the position of the last element of every bucket
	u32 counts[BYTES_COUNT] = {};
	for ( u32 i = 0; i != 256; ++i )
	{
		u32 v[BYTES_COUNT];
		v[0] = firsts[0][i];
		v[1] = firsts[1][i];
		v[2] = firsts[2][i];
		v[3] = firsts[3][i];
		v[4] = firsts[4][i];
		v[5] = firsts[5][i];
		v[6] = firsts[6][i];
		v[7] = firsts[7][i];
		firsts[0][i] = counts[0];
		firsts[1][i] = counts[1];
		firsts[2][i] = counts[2];
		firsts[3][i] = counts[3];
		firsts[4][i] = counts[4];
		firsts[5][i] = counts[5];
		firsts[6][i] = counts[6];
		firsts[7][i] = counts[7];
		counts[0] += v[0];
		counts[1] += v[1];
		counts[2] += v[2];
		counts[3] += v[3];
		counts[4] += v[4];
		counts[5] += v[5];
		counts[6] += v[6];
		counts[7] += v[7];
	}

	for ( u8 b=0; b!=BYTES_COUNT; ++b )
	{
		for ( u32 i = 0; i != count; ++i )
		{
			u8 id;
			if ( sort_order == SORT_ASCENDANTLY )
			{
				id = ( keys[ ids[i] ] >> ( 8 * b ) ) & 0xFF;
			} else
			{
				id = 0xFF - ( ( keys[ ids[i] ] >> ( 8 * b ) ) & 0xFF );
			}
			tmp[ firsts[b][ id ]++ ] = ids[i];
		}
		u16 * swp = tmp;
		      tmp = ids;
		      ids = swp;
	}
#undef BYTES_COUNT
}

internal
void
radix_sort_mc_64_32( const u64 * keys, u32 * ids, u32 * tmp, u32 count, Sort_Order sort_order )
{
#define BYTES_COUNT 8
	u32 firsts[BYTES_COUNT][256] = {};

	// NOTE(theGiallo): count buckets occurrencies
	if ( sort_order == SORT_ASCENDANTLY )
	{
		for ( u32 i = 0; i != count; ++i )
		{
			++firsts[0][( keys[i]       ) & 0xFF];
			++firsts[1][( keys[i] >> 8  ) & 0xFF];
			++firsts[2][( keys[i] >> 16 ) & 0xFF];
			++firsts[3][( keys[i] >> 24 ) & 0xFF];
			++firsts[4][( keys[i] >> 32 ) & 0xFF];
			++firsts[5][( keys[i] >> 40 ) & 0xFF];
			++firsts[6][( keys[i] >> 48 ) & 0xFF];
			++firsts[7][( keys[i] >> 56 ) & 0xFF];
		}
	} else
	{
		for ( u32 i = 0; i != count; ++i )
		{
			++firsts[0][0xFF - ( ( keys[i] >> 0  ) & 0xFF )];
			++firsts[1][0xFF - ( ( keys[i] >> 8  ) & 0xFF )];
			++firsts[2][0xFF - ( ( keys[i] >> 16 ) & 0xFF )];
			++firsts[3][0xFF - ( ( keys[i] >> 24 ) & 0xFF )];
			++firsts[4][0xFF - ( ( keys[i] >> 32 ) & 0xFF )];
			++firsts[5][0xFF - ( ( keys[i] >> 40 ) & 0xFF )];
			++firsts[6][0xFF - ( ( keys[i] >> 48 ) & 0xFF )];
			++firsts[7][0xFF - ( ( keys[i] >> 56 ) & 0xFF )];
		}
	}

	// NOTE(theGiallo): calculate the position of the last element of every bucket
	u32 counts[BYTES_COUNT] = {};
	for ( u32 i = 0; i != 256; ++i )
	{
		u32 v[BYTES_COUNT];
		v[0] = firsts[0][i];
		v[1] = firsts[1][i];
		v[2] = firsts[2][i];
		v[3] = firsts[3][i];
		v[4] = firsts[4][i];
		v[5] = firsts[5][i];
		v[6] = firsts[6][i];
		v[7] = firsts[7][i];
		firsts[0][i] = counts[0];
		firsts[1][i] = counts[1];
		firsts[2][i] = counts[2];
		firsts[3][i] = counts[3];
		firsts[4][i] = counts[4];
		firsts[5][i] = counts[5];
		firsts[6][i] = counts[6];
		firsts[7][i] = counts[7];
		counts[0] += v[0];
		counts[1] += v[1];
		counts[2] += v[2];
		counts[3] += v[3];
		counts[4] += v[4];
		counts[5] += v[5];
		counts[6] += v[6];
		counts[7] += v[7];
	}

	for ( u8 b=0; b!=BYTES_COUNT; ++b )
	{
		for ( u32 i = 0; i != count; ++i )
		{
			u8 id;
			if ( sort_order == SORT_ASCENDANTLY )
			{
				id = ( keys[ ids[i] ] >> ( 8 * b ) ) & 0xFF;
			} else
			{
				id = 0xFF - ( ( keys[ ids[i] ] >> ( 8 * b ) ) & 0xFF );
			}
			tmp[ firsts[b][ id ]++ ] = ids[i];
		}
		u32 * swp = tmp;
		      tmp = ids;
		      ids = swp;
	}
#undef BYTES_COUNT
}

// NOTE(theGiallo): -1: string0 <  string1
//                   0: string0 == string1
//                   1: string0 >  string1
// NULL is the least possible
s32
string_min( const char * string0, const char * string1 )
{
	s32 ret = 0;
	const u8 * us0 = (const u8 *) string0;
	const u8 * us1 = (const u8 *) string1;
	if ( !us0 && us1 )
	{
		ret = -1;
		return ret;
	}
	if ( !us0 && !us1 )
	{
		ret = 0;
		return ret;
	}
	if ( us0 && !us1 )
	{
		ret = 1;
		return ret;
	}

	for ( u32 i = 0; i < U32_MAX; ++i )
	{
		if ( us0[i] && us1[i] )
		{
			if ( us0[i] == us1[i] )
			{
				continue;
			}// else
			if ( us0[i] < us1[i] )
			{
				ret = -1;
			} else
			//if ( us0[i] > us1[i] )
			{
				ret = 1;
			}
		} else
		if ( us0[i] && !us1[i] )
		{
			ret = 1;
		} else
		if ( !us0[i] && us1[i] )
		{
			ret = -1;
		} else
		{
			ret = 0;
		}
		break;
	}

	return ret;
}
s32
string_min( const char * string0, s64 length0, const char * string1, s64 length1 )
{
	s32 ret = 0;
	const u8 * us0 = (const u8 *) string0;
	const u8 * us1 = (const u8 *) string1;
	if ( !us0 && us1 )
	{
		ret = -1;
		return ret;
	}
	if ( !us0 && !us1 )
	{
		ret = 0;
		return ret;
	}
	if ( us0 && !us1 )
	{
		ret = 1;
		return ret;
	}

	for ( u32 i = 0; i < U32_MAX; ++i )
	{
		if ( i < length0 && i < length1 )
		{
			if ( us0[i] == us1[i] )
			{
				continue;
			}// else
			if ( us0[i] < us1[i] )
			{
				ret = -1;
			} else
			//if ( us0[i] > us1[i] )
			{
				ret = 1;
			}
		} else
		if ( i < length0 && i >= length1 )
		{
			ret = 1;
		} else
		if ( i >= length0 && i < length1 )
		{
			ret = -1;
		} else
		{
			ret = 0;
		}
		break;
	}

	return ret;
}
bool
string_equal( const char * string0, const char * string1, s64 length_of_both )
{
	bool ret = true;
	const u8 * us0 = (const u8 *) string0;
	const u8 * us1 = (const u8 *) string1;

	for ( u32 i = 0; i < length_of_both; ++i )
	{
		if ( us0[i] == us1[i] )
		{
			continue;
		}
		ret = false;
		break;
	}

	return ret;
}

internal
void
sort_lexicographically_insertion( const char ** strings, u32 count,
                                  u32 * data, Sort_Order sort_order )
{
	for ( u32 i = 0; i < count; ++i )
	{
		const char * s = strings[i];
		u32 d;
		if ( data )
		{
			d = data[i];
		}
		s32 j;
		for ( j = i - 1; j >= 0; --j )
		{
			if ( string_min( s, strings[j] ) == ( ( sort_order == SORT_ASCENDANTLY ) ? -1 : 1 ) )
			{
				strings[j+1] = strings[j];
				if ( data )
				{
					data[j+1] = data[j];
				}
			} else
			{
				break;
			}
		}
		strings[j+1] = s;
		if ( data )
		{
			data[j+1] = d;
		}
	}
}

internal
void
sort_lexicographically_insertion_ids_only( const char *const* strings, u32 count,
                                           u32 * ids, Sort_Order sort_order )
{
	for ( u32 i = 0; i < count; ++i )
	{
		const char * s = strings[ids[i]];
		u32 id;
		id = ids[i];
		s32 j;
		for ( j = i - 1; j >= 0; --j )
		{
			if ( string_min( s, strings[ids[j]] ) == ( ( sort_order == SORT_ASCENDANTLY ) ? -1 : 1 ) )
			{
				ids[j+1] = ids[j];
			} else
			{
				break;
			}
		}
		ids[j+1] = id;
	}
}

internal
void
sort_lexicographically_insertion_ids_only_16( const char *const* strings, u32 count,
                                              u16 * ids, Sort_Order sort_order )
{
	for ( u32 i = 0; i < count; ++i )
	{
		const char * s = strings[ids[i]];
		u16 id;
		id = ids[i];
		s32 j;
		for ( j = i - 1; j >= 0; --j )
		{
			if ( string_min( s, strings[ids[j]] ) == ( ( sort_order == SORT_ASCENDANTLY ) ? -1 : 1 ) )
			{
				ids[j+1] = ids[j];
			} else
			{
				break;
			}
		}
		ids[j+1] = id;
	}
}

#define COUNT_BELOW_WHICH_INSERTION_IS_FASTER_THAN_RADIX 150
internal
bool
sort_lexicographically_radix_insertion( char const** strings, u32 count,
                                        char const** tmp,
                                        Sort_Order sort_order,
                                        u32 * data,
                                        u32 * data_tmp,
                                        u32 * lengths_strings,
                                        u32 * lengths_tmp,
                                        u32 char_idx )
{
	if ( count < COUNT_BELOW_WHICH_INSERTION_IS_FASTER_THAN_RADIX )
	{
		sort_lexicographically_insertion( strings, count, data, sort_order );
		return false;
	}
	if ( !lengths_strings )
	{
		u32 lengths_s[count];
		u32 lengths_t[count];
		for ( u32 i = 0; i < count; ++i )
		{
			u32 j;
			const u8 * s = (const u8 *)strings[i];
			for ( j = 0; j < U32_MAX && s[j]; ++j );
			lengths_s[i] = j;
		}
		return sort_lexicographically_radix_insertion( strings, count, tmp,
		                                               sort_order,
		                                               data, data_tmp,
		                                               lengths_s, lengths_t,
		                                               char_idx );
	}

	bool solution_is_in_tmp = true;

	u32 bins_count[256] = {};
	for ( u32 s_idx = 0; s_idx < count; ++s_idx )
	{
		const u8 * s = (const u8 *)strings[s_idx];
		u32 l = lengths_strings[s_idx];
		u8 c = s[char_idx < l ? char_idx : l];
		if ( sort_order == SORT_DESCENDANTLY )
		{
			c = 255 - c;
		}
		if ( c != 255 )
		{
			++bins_count[c+1];
		}
	}

	u32 * first_pos_of_bin = bins_count;
	for ( u32 i = 2; i != 256; ++i )
	{
		first_pos_of_bin[i] += first_pos_of_bin[i-1];
	}

	for ( u32 s_idx = 0; s_idx < count; ++s_idx )
	{
		const u8 * s = (const u8 *)strings[s_idx];
		u32 l = lengths_strings[s_idx];
		u8 c = s[char_idx < l ? char_idx : l];
		if ( sort_order == SORT_DESCENDANTLY )
		{
			c = 255 - c;
		}
		u32 dest = first_pos_of_bin[c]++;
		tmp[dest] = strings[s_idx];
		lengths_tmp[dest] = lengths_strings[s_idx];
		if ( data )
		{
			data_tmp[dest] = data[s_idx];
		}
	}

	for ( u32 i = 0; i != 256; ++i )
	{
		s32 first = i ? first_pos_of_bin[i-1] : 0;
		s32 bin_count = first_pos_of_bin[i] - first;
		if ( bin_count < 2 )
		{
			continue;
		}
		bool res_is_in_strings;
		if ( bin_count > COUNT_BELOW_WHICH_INSERTION_IS_FASTER_THAN_RADIX )
		{
			res_is_in_strings =
			sort_lexicographically_radix_insertion(
			   tmp + first, bin_count, strings + first,
			   sort_order,
			   data_tmp ? data_tmp + first : data_tmp,
			   data ? data + first : data,
			   lengths_strings + first, lengths_tmp + first,
			   char_idx+1 );
		} else
		{
			res_is_in_strings = false;
			sort_lexicographically_insertion(
			   tmp + first, bin_count,
			   data_tmp ? data_tmp + first : data_tmp, sort_order );
		}
		if ( res_is_in_strings )
		{
			memcpy( tmp + first, strings + first, bin_count * sizeof( tmp[0] ) );
			memcpy( lengths_tmp + first, lengths_strings + first,
			        bin_count * sizeof( lengths_tmp[0] ) );
			if ( data )
			{
				memcpy( data_tmp + first, data + first,
				        bin_count * sizeof( data[0] ) );
			}
		}
	}

	solution_is_in_tmp = false;
	memcpy( strings, tmp, count * sizeof( tmp[0] ) );
	if ( data )
	{
		memcpy( data, data_tmp, count * sizeof( data[0] ) );
	}
	return solution_is_in_tmp;
}

internal
bool
sort_lexicographically_radix_insertion_ids_only( char const*const* strings, u32 count,
                                                 Sort_Order sort_order,
                                                 u32 * ids,
                                                 u32 * ids_tmp,
                                                 u32 const * lengths_strings,
                                                 u32 char_idx )
{
	if ( count < COUNT_BELOW_WHICH_INSERTION_IS_FASTER_THAN_RADIX )
	{
		sort_lexicographically_insertion_ids_only( strings, count, ids, sort_order );
		return false;
	}
	if ( !lengths_strings )
	{
		u32 lengths_s[count];
		for ( u32 i = 0; i < count; ++i )
		{
			u32 j;
			const u8 * s = (const u8 *)strings[i];
			for ( j = 0; j < U32_MAX && s[j]; ++j );
			lengths_s[i] = j;
		}
		return sort_lexicographically_radix_insertion_ids_only( strings, count,
		                                                        sort_order,
		                                                        ids, ids_tmp,
		                                                        lengths_s,
		                                                        char_idx );
	}

	bool solution_is_in_tmp = true;

	u32 bins_count[256] = {};
	for ( u32 id_idx = 0; id_idx < count; ++id_idx )
	{
		u32 s_idx = ids[id_idx];
		const u8 * s = (const u8 *)strings[s_idx];
		u32 l = lengths_strings[s_idx];
		u8 c = s[char_idx < l ? char_idx : l];
		if ( sort_order == SORT_DESCENDANTLY )
		{
			c = 255 - c;
		}
		if ( c != 255 )
		{
			++bins_count[c+1];
		}
	}

	u32 * first_pos_of_bin = bins_count;
	for ( u32 i = 2; i != 256; ++i )
	{
		first_pos_of_bin[i] += first_pos_of_bin[i-1];
	}

	for ( u32 id_idx = 0; id_idx < count; ++id_idx )
	{
		u32 s_idx = ids[id_idx];
		const u8 * s = (const u8 *)strings[s_idx];
		u32 l = lengths_strings[s_idx];
		u8 c = s[char_idx < l ? char_idx : l];
		if ( sort_order == SORT_DESCENDANTLY )
		{
			c = 255 - c;
		}
		u32 dest = first_pos_of_bin[c]++;
		ids_tmp[dest] = s_idx;
	}

	for ( u32 i = 0; i != 256; ++i )
	{
		s32 first = i ? first_pos_of_bin[i-1] : 0;
		s32 bin_count = first_pos_of_bin[i] - first;
		if ( bin_count < 2 )
		{
			continue;
		}
		bool res_is_in_tmp;
		if ( bin_count > COUNT_BELOW_WHICH_INSERTION_IS_FASTER_THAN_RADIX )
		{
			res_is_in_tmp =
			!sort_lexicographically_radix_insertion_ids_only(
			   strings, bin_count,
			   sort_order,
			   ids_tmp + first, ids + first,
			   lengths_strings + first,
			   char_idx+1 );
		} else
		{
			res_is_in_tmp = true;
			sort_lexicographically_insertion_ids_only( strings, bin_count, ids_tmp + first, sort_order );
		}
		if ( !res_is_in_tmp )
		{
			memcpy( ids_tmp + first, ids + first,
			        bin_count * sizeof( ids[0] ) );
		}
	}

	solution_is_in_tmp = false;
	memcpy( ids, ids_tmp, count * sizeof( ids[0] ) );
	return solution_is_in_tmp;
}


internal
bool
sort_lexicographically_radix_insertion_ids_only_16( char const*const* strings, u32 count,
                                                    Sort_Order sort_order,
                                                    u16 * ids,
                                                    u16 * ids_tmp,
                                                    u32 const * lengths_strings,
                                                    u32 char_idx )
{
	if ( count < COUNT_BELOW_WHICH_INSERTION_IS_FASTER_THAN_RADIX )
	{
		sort_lexicographically_insertion_ids_only_16( strings, count, ids, sort_order );
		return false;
	}
	if ( !lengths_strings )
	{
		u32 lengths_s[count];
		for ( u32 i = 0; i < count; ++i )
		{
			u32 j;
			const u8 * s = (const u8 *)strings[i];
			for ( j = 0; j < U32_MAX && s[j]; ++j );
			lengths_s[i] = j;
		}
		return sort_lexicographically_radix_insertion_ids_only_16( strings, count,
		                                                           sort_order,
		                                                           ids, ids_tmp,
		                                                           lengths_s,
		                                                           char_idx );
	}

	bool solution_is_in_tmp = true;

	u32 bins_count[256] = {};
	for ( u32 id_idx = 0; id_idx < count; ++id_idx )
	{
		u32 s_idx = ids[id_idx];
		const u8 * s = (const u8 *)strings[s_idx];
		u32 l = lengths_strings[s_idx];
		u8 c = s[char_idx < l ? char_idx : l];
		if ( sort_order == SORT_DESCENDANTLY )
		{
			c = 255 - c;
		}
		if ( c != 255 )
		{
			++bins_count[c+1];
		}
	}

	u32 * first_pos_of_bin = bins_count;
	for ( u32 i = 2; i != 256; ++i )
	{
		first_pos_of_bin[i] += first_pos_of_bin[i-1];
	}

	for ( u32 id_idx = 0; id_idx < count; ++id_idx )
	{
		u32 s_idx = ids[id_idx];
		const u8 * s = (const u8 *)strings[s_idx];
		u32 l = lengths_strings[s_idx];
		u8 c = s[char_idx < l ? char_idx : l];
		if ( sort_order == SORT_DESCENDANTLY )
		{
			c = 255 - c;
		}
		u32 dest = first_pos_of_bin[c]++;
		ids_tmp[dest] = s_idx;
	}

	for ( u32 i = 0; i != 256; ++i )
	{
		s32 first = i ? first_pos_of_bin[i-1] : 0;
		s32 bin_count = first_pos_of_bin[i] - first;
		if ( bin_count < 2 )
		{
			continue;
		}
		bool res_is_in_tmp;
		if ( bin_count > COUNT_BELOW_WHICH_INSERTION_IS_FASTER_THAN_RADIX )
		{
			res_is_in_tmp =
			!sort_lexicographically_radix_insertion_ids_only_16(
			   strings, bin_count,
			   sort_order,
			   ids_tmp + first, ids + first,
			   lengths_strings + first,
			   char_idx+1 );
		} else
		{
			res_is_in_tmp = true;
			sort_lexicographically_insertion_ids_only_16( strings, bin_count, ids_tmp + first, sort_order );
		}
		if ( !res_is_in_tmp )
		{
			memcpy( ids_tmp + first, ids + first,
			        bin_count * sizeof( ids[0] ) );
		}
	}

	solution_is_in_tmp = false;
	memcpy( ids, ids_tmp, count * sizeof( ids[0] ) );
	return solution_is_in_tmp;
}

internal
void
sort_lexicographically( char const** strings, u32 count,
                        char const** tmp,
                        Sort_Order sort_order,
                        u32 * data,
                        u32 * data_tmp,
                        u32 * lengths_strings,
                        u32 * lengths_tmp )
{
	if ( count < COUNT_BELOW_WHICH_INSERTION_IS_FASTER_THAN_RADIX )
	{
		sort_lexicographically_insertion( strings, count, data, sort_order );
	}
	char const * tmp_local[tmp?0:count];
	if ( tmp == NULL)
	{
		tmp = tmp_local;
	}
	sort_lexicographically_radix_insertion( strings, count, tmp, sort_order,
	                                        data, data_tmp,
	                                        lengths_strings, lengths_tmp );
}

internal
void
sort_lexicographically_ids_only( char const*const* strings, u32 count,
                                 Sort_Order sort_order,
                                 u32 * ids,
                                 u32 * ids_tmp,
                                 u32 * lengths_strings )
{
	if ( count < COUNT_BELOW_WHICH_INSERTION_IS_FASTER_THAN_RADIX )
	{
		sort_lexicographically_insertion_ids_only( strings, count, ids, sort_order );
	}
	u32 ids_tmp_local[ids_tmp?0:count];
	if ( ids_tmp == NULL)
	{
		ids_tmp = ids_tmp_local;
	}
	sort_lexicographically_radix_insertion_ids_only( strings, count, sort_order,
	                                                 ids, ids_tmp,
	                                                 lengths_strings );
}

internal
void
sort_lexicographically_ids_only_16( char const*const* strings, u32 count,
                                    Sort_Order sort_order,
                                    u16 * ids,
                                    u16 * ids_tmp,
                                    u32 * lengths_strings )
{
	if ( count < COUNT_BELOW_WHICH_INSERTION_IS_FASTER_THAN_RADIX )
	{
		sort_lexicographically_insertion_ids_only_16( strings, count, ids, sort_order );
	}
	u16 ids_tmp_local[ids_tmp?0:count];
	if ( ids_tmp == NULL)
	{
		ids_tmp = ids_tmp_local;
	}
	sort_lexicographically_radix_insertion_ids_only_16( strings, count, sort_order,
	                                                    ids, ids_tmp,
	                                                    lengths_strings );
}

////////////////////////////////////////////////////////////////////////////////

#define MH_EMIT_BODY 1
#define MH_BODY_ONLY 1
#undef MH_STATIC

#define MH_TYPE ptr_u64_LL
#include "mem_hotel.inc.h"

#define MH_TYPE u32_u32_LL
#include "mem_hotel.inc.h"

#undef MH_EMIT_BODY
#undef MH_BODY_ONLY

////////////////////////////////////////////////////////////////////////////////

void *
Mem_Pool::
allocate( u64 size )
{
	u8 * ret = NULL;

	u64 tot_size = size + 8;
	for ( ptr_u64_LL ** free_node = &this->free_list;
	      *free_node;
	      free_node = &(*free_node)->next )
	{
		if ( (*free_node)->size >= tot_size )
		{
			ret = (u8*)(*free_node)->ptr;
			*(u64*)ret = size;
			ret += 8;
			u8** p = (u8**)&((*free_node)->ptr);
			*p += tot_size;
			(*free_node)->size -= tot_size;
			this->occupied += tot_size;
			if ( !(*free_node)->size )//&& free_node != &this->free_list )
			{
				::deallocate( &this->freelist_nodes_mh, *free_node );
				*free_node = (*free_node)->next;
			}
			break;
		}
	}

	return ret;
}

void
Mem_Pool::
deallocate( void * ptr )
{
	tg_assert( ptr >= this->mem_buf );
	tg_assert( ptr < this->mem_buf + this->size );
	u64 * size = (u64*)ptr;
	--size;
	void * real_ptr = size;
	tg_assert( (u8*)ptr + *size < this->mem_buf + this->size );

	u64 tot_size = *size + 8;
	this->occupied -= tot_size;

	ptr_u64_LL ** free_node = &this->free_list,
	           ** prev_node = free_node;
	for( ;
	     *free_node;
	     prev_node = free_node,
	     free_node = &(*free_node)->next )
	{
		if ( (*free_node)->ptr >= ptr )
		{
			if ( (u8*)ptr + *size == (*free_node)->ptr )
			{
				(*free_node)->ptr = real_ptr;
				(*free_node)->size += tot_size;
				if ( prev_node != free_node &&
				     (u8*)(*prev_node)->ptr + (*prev_node)->size ==
				          (*free_node)->ptr )
				{
					if ( *free_node < *prev_node )
					{
						(*free_node)->size += (*prev_node)->size;
						::deallocate( &this->freelist_nodes_mh, *prev_node );
						*prev_node = *free_node;
					} else
					{
						(*prev_node)->size += (*free_node)->size;
						(*prev_node)->next = (*free_node)->next;
						::deallocate( &this->freelist_nodes_mh, *free_node );
					}
				}
			} else
			if ( prev_node != free_node &&
			     (u8*)(*prev_node)->ptr + (*prev_node)->size == real_ptr )
			{
				(*prev_node)->size += tot_size;
			} else
			{
				ptr_u64_LL * new_node =
				   ::allocate( &this->freelist_nodes_mh );
				new_node->next = (*free_node);
				new_node->size = tot_size;
				new_node->ptr = real_ptr;
				(*free_node) = new_node;
			}
			return;
		}
	}

	ptr_u64_LL * new_node =
	   ::allocate( &this->freelist_nodes_mh );
	new_node->next = (*free_node);
	new_node->size = tot_size;
	new_node->ptr = real_ptr;
	(*free_node) = new_node;
}

////////////////////////////////////////////////////////////////////////////////

bool
u64_to_u32_hash_table_insert( u64_to_u32_Hash_Table * ht, u64 key, u32 value )
{
	bool ret = false;

	u32 hash = u64_to_u32_hash_table_hash_key( key, ht->table_size );

	if ( TG_BITTEST(ht->values_table_presence_bv, hash) )
	{
		if ( ht->list_nodes_storage_occupancy !=
		     ht->list_nodes_storage_capacity )
		{
			s32 bc = ( ht->list_nodes_storage_capacity + 254 ) / 255;
			for ( s32 b = 0; b!=bc; ++b )
			{
				if ( ht->list_nodes_storage_255_blocks_occupancy[b] != 255 )
				{
					++ht->list_nodes_storage_255_blocks_occupancy[b];
					for ( u32 i = b * 255; i != ht->table_size; ++i )
					{
						if ( !TG_BITTEST(ht->list_nodes_storage_presence_bv, i) )
						{
							TG_BITSET(ht->list_nodes_storage_presence_bv, i);
							u64_to_u32_Hash_Table::Key_Value_List_Node * new_node =
							   ht->list_nodes_storage + i;
							new_node->key     = key;
							new_node->value   = value;
							new_node->next_id = ht->values_table[hash].next_id;
							ht->values_table[hash].next_id = i;
							++ht->list_nodes_storage_occupancy;
							ret = true;
							break;
						}
					}
					break;
				}
			}
		}
	} else
	{
		TG_BITSET(ht->values_table_presence_bv, hash);
		ht->values_table[hash].key     = key;
		ht->values_table[hash].value   = value;
		ht->values_table[hash].next_id = -1;
		ret = true;
	}

	return ret;
}

bool
u64_to_u32_hash_table_change( u64_to_u32_Hash_Table * ht, u64 key, u32 value )
{
	bool ret = false;

	u32 hash = u64_to_u32_hash_table_hash_key( key, ht->table_size );

	if ( TG_BITTEST(ht->values_table_presence_bv, hash) )
	{
		u64_to_u32_Hash_Table::Key_Value_List_Node * node =
		   ht->values_table + hash;
		for(;;)
		{
			if ( node->key == key )
			{
				node->value = value;
				ret = true;
				break;
			}
			if ( node->next_id < 0 )
			{
				break;
			}
			node = ht->list_nodes_storage + node->next_id;
		}
	}

	return ret;
}

////////////////////////////////////////////////////////////////////////////////

bool
u64_to_B12_hash_table_insert( u64_to_B12_Hash_Table * ht, u64 key, B12 value )
{
	bool ret = false;

	u32 hash = u64_to_B12_hash_table_hash_key( key, ht->table_size );

	if ( TG_BITTEST(ht->values_table_presence_bv, hash) )
	{
		if ( ht->list_nodes_storage_occupancy !=
		     ht->list_nodes_storage_capacity )
		{
			s32 bc = ( ht->list_nodes_storage_capacity + 254 ) / 255;
			for ( s32 b = 0; b!=bc; ++b )
			{
				if ( ht->list_nodes_storage_255_blocks_occupancy[b] != 255 )
				{
					++ht->list_nodes_storage_255_blocks_occupancy[b];
					for ( u32 i = b * 255; i != ht->table_size; ++i )
					{
						if ( !TG_BITTEST(ht->list_nodes_storage_presence_bv, i) )
						{
							TG_BITSET(ht->list_nodes_storage_presence_bv, i);
							u64_to_B12_Hash_Table::Key_Value_List_Node * new_node =
							   ht->list_nodes_storage + i;
							new_node->key     = key;
							new_node->value   = value;
							new_node->next_id = ht->values_table[hash].next_id;
							ht->values_table[hash].next_id = i;
							++ht->list_nodes_storage_occupancy;
							ret = true;
							break;
						}
					}
					break;
				}
			}
		}
	} else
	{
		TG_BITSET(ht->values_table_presence_bv, hash);
		ht->values_table[hash].key     = key;
		ht->values_table[hash].value   = value;
		ht->values_table[hash].next_id = -1;
		ret = true;
	}

	return ret;
}

bool
u64_to_B12_hash_table_change( u64_to_B12_Hash_Table * ht, u64 key, B12 value )
{
	bool ret = false;

	u32 hash = u64_to_B12_hash_table_hash_key( key, ht->table_size );

	if ( TG_BITTEST(ht->values_table_presence_bv, hash) )
	{
		u64_to_B12_Hash_Table::Key_Value_List_Node * node =
		   ht->values_table + hash;
		for(;;)
		{
			if ( node->key == key )
			{
				node->value = value;
				ret = true;
				break;
			}
			if ( node->next_id < 0 )
			{
				break;
			}
			node = ht->list_nodes_storage + node->next_id;
		}
	}

	return ret;
}

////////////////////////////////////////////////////////////////////////////////

bool
B12_to_u32_hash_table_insert( B12_to_u32_Hash_Table * ht, B12 key, u32 value )
{
	bool ret = false;

	u32 hash = B12_to_u32_hash_table_hash_key( key, ht->table_size );

	if ( TG_BITTEST(ht->values_table_presence_bv, hash) )
	{
		if ( ht->list_nodes_storage_occupancy !=
		     ht->list_nodes_storage_capacity )
		{
			s32 bc = ( ht->list_nodes_storage_capacity + 254 ) / 255;
			for ( s32 b = 0; b!=bc; ++b )
			{
				if ( ht->list_nodes_storage_255_blocks_occupancy[b] != 255 )
				{
					++ht->list_nodes_storage_255_blocks_occupancy[b];
					for ( u32 i = b * 255; i != ht->table_size; ++i )
					{
						if ( !TG_BITTEST(ht->list_nodes_storage_presence_bv, i) )
						{
							TG_BITSET(ht->list_nodes_storage_presence_bv, i);
							B12_to_u32_Hash_Table::Key_Value_List_Node * new_node =
							   ht->list_nodes_storage + i;
							new_node->key_xy  = key.xy;
							new_node->key_z   = key.z;
							new_node->value   = value;
							new_node->next_id = ht->values_table[hash].next_id;
							ht->values_table[hash].next_id = i;
							++ht->list_nodes_storage_occupancy;
							ret = true;
							break;
						}
					}
					break;
				}
			}
		}
	} else
	{
		TG_BITSET(ht->values_table_presence_bv, hash);
		ht->values_table[hash].key_xy     = key.xy;
		ht->values_table[hash].key_z      = key.z;
		ht->values_table[hash].value   = value;
		ht->values_table[hash].next_id = -1;
		ret = true;
	}

	return ret;
}

bool
B12_to_u32_hash_table_change( B12_to_u32_Hash_Table * ht, B12 key, u32 value )
{
	bool ret = false;

	u32 hash = B12_to_u32_hash_table_hash_key( key, ht->table_size );

	if ( TG_BITTEST(ht->values_table_presence_bv, hash) )
	{
		B12_to_u32_Hash_Table::Key_Value_List_Node * node =
		   ht->values_table + hash;
		for(;;)
		{
			if ( node->key_xy == key.xy && node->key_z == key.z )
			{
				node->value = value;
				ret = true;
				break;
			}
			if ( node->next_id < 0 )
			{
				break;
			}
			node = ht->list_nodes_storage + node->next_id;
		}
	}

	return ret;
}


