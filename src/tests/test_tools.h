#ifndef _TEST_TOOLS_H
#define _TEST_TOOLS_H 1

#include "basic_types.h"
#include "macro_tools.h"

u64 tg_total_asserts = 0;
u64 tg_succeeded_asserts = 0;

#define tg_test_assert(exp) do { if ( (exp) ) { ++tg_succeeded_asserts; } else { log_err( "\"" TOSTRING( exp ) "\" failed " ); } ++tg_total_asserts; } while ( false )
#define tg_test_assert_m(exp, m, ...) do { if ( (exp) ) { ++tg_succeeded_asserts; } else { log_err( "\"" TOSTRING( exp ) "\" failed " m, __VA_ARGS__ ); } ++tg_total_asserts; } while ( false )

inline
s32
tg_test_asserts_result()
{
	s32 ret = 0;

	log_info( "passed asserts %lu / %lu", tg_succeeded_asserts, tg_total_asserts );

	if ( tg_total_asserts != tg_succeeded_asserts )
	{
		ret = 1;
	}

	return ret;
}

#endif /* ifndef _TEST_TOOLS_H */
