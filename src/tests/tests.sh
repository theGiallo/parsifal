#!/bin/bash

CPPC="clang++"

bldgrn='\e[1;32m'
bldred='\e[1;31m'
txtrst='\e[0m'

for f in *.cpp
do
	elf=bin/${f%.cpp}
	echo "################################################################################"
	echo "compiling "$f
	$CPPC $f -I"../" -std=c++11 -Wall -Wextra -Wno-unused-function -Wno-missing-braces -DDEBUG=1 -fuse-ld=gold -o $elf -g3 -O0
	ret=$?
	if [[ $ret == 0 ]]
	then
		echo "running "$elf
		$elf
		ret=$?
		if [[ $ret == 0 ]]
		then
			echo -e $bldgrn"test passed"$txtrst
		else
			echo -e $bldred"test failed"$txtrst
		fi
	else
		echo "failed to compile "$elf
	fi
	echo
done
