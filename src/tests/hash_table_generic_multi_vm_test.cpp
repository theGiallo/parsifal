#include "tests/test_tools.h"
#include "hash_table_generic_multi_vm.h"

s32
main( s32 argc, char *argv[] )
{
	(void)argc;
	(void)argv;
	s32 ret = 0;

	{
		struct
		Key
		{
			u32 a;
			u8  b;
		};

		Hash_Table_Generic_Multi_VM ht = {};
		u64 max_capacity = 1024 * 1024;
		u32 table_length = 1023;
		bool b_res;

		b_res =
		hash_table_generic_multi_vm_init( &ht, sizeof(Key), max_capacity, table_length );
		tg_test_assert( b_res );
		tg_test_assert( ht.occupancy == 0 );

		Key k = {};
		Key k1 = { .a = 1, .b = 2 };
		const char v[] = "Hello, world!";
		const char v1[] = "Hello, sailor!";
		b_res =
		hash_table_generic_multi_vm_insert( &ht, (void*)&k, (void*)v );
		tg_test_assert( b_res );
		tg_test_assert( ht.occupancy == 1 );

		HTGMVM_Element_List_VM * lp;
		HTGMVM_Element_List_VM l = {};
		HTGMVM_Element_List_VM null_l = {};

		b_res =
		hash_table_generic_multi_vm_get_all( &ht, (void*)&k, &lp );
		tg_test_assert( b_res );
		tg_test_assert( lp );
		tg_test_assert( lp->length == 1 );
		tg_test_assert( lp->first->element.value == v );
		tg_test_assert( ht.occupancy == 1 );

		b_res =
		hash_table_generic_multi_vm_insert_unique( &ht, (void*)&k, (void*)v );
		tg_test_assert( !b_res );
		tg_test_assert( ht.occupancy == 1 );

		b_res =
		hash_table_generic_multi_vm_remove_all( &ht, (void*)&k1 );
		tg_test_assert( !b_res );
		tg_test_assert( ht.occupancy == 1 );

		b_res =
		hash_table_generic_multi_vm_remove_and_get_all( &ht, (void*)&k1, &l );
		tg_test_assert( !b_res );
		tg_test_assert( 0 == memcmp( &l, &null_l, sizeof ( l ) ) );
		tg_test_assert( ht.occupancy == 1 );

		b_res =
		hash_table_generic_multi_vm_remove_and_get_all( &ht, (void*)&k, &l );
		tg_test_assert( b_res );
		tg_test_assert( l.first->element.value == v );
		tg_test_assert( ht.occupancy == 0 );

		b_res =
		hash_table_generic_multi_vm_get_all( &ht, (void*)&k, &lp );
		tg_test_assert( !b_res );
		tg_test_assert( lp == 0 );
		tg_test_assert( ht.occupancy == 0 );

		b_res =
		hash_table_generic_multi_vm_insert( &ht, (void*)&k, (void*)v );
		tg_test_assert( b_res );
		tg_test_assert( ht.occupancy == 1 );

		b_res =
		hash_table_generic_multi_vm_insert( &ht, (void*)&k, (void*)v );
		tg_test_assert( b_res );
		tg_test_assert( ht.occupancy == 2 );

		b_res =
		hash_table_generic_multi_vm_get_all( &ht, (void*)&k, &lp );
		tg_test_assert( b_res );
		tg_test_assert( lp->length == 2 );
		tg_test_assert( lp->first->element.value == v );
		tg_test_assert( lp->first->next->element.value == v );
		tg_test_assert( ht.occupancy == 2 );

		b_res =
		hash_table_generic_multi_vm_remove_and_get_all( &ht, (void*)&k, &l );
		tg_test_assert( b_res );
		tg_test_assert( l.length == 2 );
		tg_test_assert( l.first->element.value == v );
		tg_test_assert( l.first->next->element.value == v );
		tg_test_assert( ht.occupancy == 0 );
		list_vm_remove_all( &l );

		b_res =
		hash_table_generic_multi_vm_remove_and_get_all( &ht, (void*)&k, &l );
		tg_test_assert( !b_res );
		tg_test_assert( 0 == memcmp( &l, &null_l, sizeof ( l ) ) );
		tg_test_assert( ht.occupancy == 0 );

		l.first = (HTGMVM_Element_List_VM_Node*)0xcafebabe;
		b_res =
		hash_table_generic_multi_vm_remove_and_get_all( &ht, (void*)&k, &l );
		tg_test_assert( !b_res );
		tg_test_assert( 0 == memcmp( &l, &null_l, sizeof ( l ) ) );
		tg_test_assert( ht.occupancy == 0 );

		b_res =
		hash_table_generic_multi_vm_insert( &ht, (void*)&k, (void*)v );
		tg_test_assert( b_res );
		tg_test_assert( ht.occupancy == 1 );

		b_res =
		hash_table_generic_multi_vm_insert( &ht, (void*)&k1, (void*)v1 );
		tg_test_assert( b_res );
		tg_test_assert( ht.occupancy == 2 );

		b_res =
		hash_table_generic_multi_vm_insert( &ht, (void*)&k, (void*)v );
		tg_test_assert( b_res );
		tg_test_assert( ht.occupancy == 3 );

		b_res =
		hash_table_generic_multi_vm_insert( &ht, (void*)&k1, (void*)v1 );
		tg_test_assert( b_res );
		tg_test_assert( ht.occupancy == 4 );

		b_res =
		hash_table_generic_multi_vm_insert( &ht, (void*)&k, (void*)v );
		tg_test_assert( b_res );
		tg_test_assert( ht.occupancy == 5 );

		b_res =
		hash_table_generic_multi_vm_get_all( &ht, (void*)&k, &lp );
		tg_test_assert( b_res );
		tg_test_assert( lp );
		tg_test_assert( lp->length == 3 );
		for ( HTGMVM_Element_List_VM_Node * node = lp->first;
		      node;
		      node = node->next )
		{
			tg_test_assert( node->element.key == &k );
			tg_test_assert( node->element.value == v );
		}

		b_res =
		hash_table_generic_multi_vm_get_all( &ht, (void*)&k1, &lp );
		tg_test_assert( b_res );
		tg_test_assert( lp );
		tg_test_assert( lp->length == 2 );
		for ( HTGMVM_Element_List_VM_Node * node = lp->first;
		      node;
		      node = node->next )
		{
			tg_test_assert( node->element.key == &k1 );
			tg_test_assert( node->element.value == v1 );
		}

		b_res =
		hash_table_generic_multi_vm_remove_and_get_all( &ht, (void*)&k, &l );
		tg_test_assert( b_res );
		tg_test_assert( ht.occupancy == 2 );
		tg_test_assert( 0 != memcmp( &l, &null_l, sizeof ( l ) ) );
		tg_test_assert( l.length == 3 );
		for ( HTGMVM_Element_List_VM_Node * node = l.first;
		      node;
		      node = node->next )
		{
			tg_test_assert( node->element.key == &k );
			tg_test_assert( node->element.value == v );
		}
		list_vm_remove_all( &l );

		b_res =
		hash_table_generic_multi_vm_remove_all( &ht, (void*)&k1 );
		tg_test_assert( b_res );
		tg_test_assert( ht.occupancy == 0 );

		b_res =
		hash_table_generic_multi_vm_remove_all( &ht, (void*)&k1 );
		tg_test_assert( !b_res );
		tg_test_assert( ht.occupancy == 0 );

		b_res =
		hash_table_generic_multi_vm_remove_and_get_all( &ht, (void*)&k, &l );
		tg_test_assert( !b_res );
		tg_test_assert( ht.occupancy == 0 );
		tg_test_assert( 0 == memcmp( &l, &null_l, sizeof ( l ) ) );

		hash_table_generic_multi_vm_destroy( &ht );
	}
	{
		Hash_Table_Generic_Multi_VM ht = {};
		u64 max_capacity = 1024 * 1024;
		u32 table_length = 1023;
		bool b_res;

		HTGMVM_Element_List_VM * lp;
		HTGMVM_Element_List_VM l = {};
		HTGMVM_Element_List_VM null_l = {};

		b_res =
		hash_table_generic_multi_vm_init( &ht, -1, max_capacity, table_length );
		tg_test_assert( b_res );
		tg_test_assert( ht.occupancy == 0 );

		const char * k = "Hey, I'm a key";
		const char * k1 = "I'm one too";
		const char v[] = "Hello, world!";
		const char v1[] = "Hello, sailor!";
		b_res =
		hash_table_generic_multi_vm_insert( &ht, (void*)&k, (void*)v );
		tg_test_assert( b_res );
		tg_test_assert( ht.occupancy == 1 );

		b_res =
		hash_table_generic_multi_vm_get_all( &ht, (void*)&k, &lp );
		tg_test_assert( b_res );
		tg_test_assert( lp );
		tg_test_assert( lp->length == 1 );
		tg_test_assert( lp->first->element.value == v );
		tg_test_assert( ht.occupancy == 1 );

		b_res =
		hash_table_generic_multi_vm_insert_unique( &ht, (void*)&k, (void*)v );
		tg_test_assert( !b_res );
		tg_test_assert( ht.occupancy == 1 );

		b_res =
		hash_table_generic_multi_vm_remove_all( &ht, (void*)&k1 );
		tg_test_assert( !b_res );
		tg_test_assert( ht.occupancy == 1 );

		b_res =
		hash_table_generic_multi_vm_remove_and_get_all( &ht, (void*)&k1, &l );
		tg_test_assert( !b_res );
		tg_test_assert( 0 == memcmp( &l, &null_l, sizeof ( l ) ) );
		tg_test_assert( ht.occupancy == 1 );

		b_res =
		hash_table_generic_multi_vm_remove_and_get_all( &ht, (void*)&k, &l );
		tg_test_assert( b_res );
		tg_test_assert( 0 != memcmp( &l, &null_l, sizeof ( l ) ) );
		tg_test_assert( l.first->element.value == v );
		tg_test_assert( l.length == 1 );
		tg_test_assert( ht.occupancy == 0 );

		b_res =
		hash_table_generic_multi_vm_get_all( &ht, (void*)&k, &lp );
		tg_test_assert( !b_res );
		tg_test_assert( lp == 0 );
		tg_test_assert( ht.occupancy == 0 );

		b_res =
		hash_table_generic_multi_vm_insert( &ht, (void*)&k, (void*)v );
		tg_test_assert( b_res );
		tg_test_assert( ht.occupancy == 1 );

		b_res =
		hash_table_generic_multi_vm_insert( &ht, (void*)&k, (void*)v );
		tg_test_assert( b_res );
		tg_test_assert( ht.occupancy == 2 );

		b_res =
		hash_table_generic_multi_vm_get_all( &ht, (void*)&k, &lp );
		tg_test_assert( b_res );
		tg_test_assert( lp );
		tg_test_assert( lp->length == 2 );
		tg_test_assert( lp->first->element.value == v );
		tg_test_assert( lp->first->next->element.value == v );
		tg_test_assert( ht.occupancy == 2 );

		b_res =
		hash_table_generic_multi_vm_remove_and_get_all( &ht, (void*)&k, &l );
		tg_test_assert( b_res );
		tg_test_assert( 0 != memcmp( &l, &null_l, sizeof ( l ) ) );
		tg_test_assert( l.first->element.value == v );
		tg_test_assert( l.first->next->element.value == v );
		tg_test_assert( ht.occupancy == 0 );

		b_res =
		hash_table_generic_multi_vm_remove_and_get_all( &ht, (void*)&k, &l );
		tg_test_assert( !b_res );
		tg_test_assert( 0 == memcmp( &l, &null_l, sizeof ( l ) ) );
		tg_test_assert( ht.occupancy == 0 );

		l.first = (HTGMVM_Element_List_VM_Node*)0xcafebabe;
		b_res =
		hash_table_generic_multi_vm_remove_and_get_all( &ht, (void*)&k, &l );
		tg_test_assert( !b_res );
		tg_test_assert( 0 == memcmp( &l, &null_l, sizeof ( l ) ) );
		tg_test_assert( ht.occupancy == 0 );

		b_res =
		hash_table_generic_multi_vm_insert( &ht, (void*)&k, (void*)v );
		tg_test_assert( b_res );
		tg_test_assert( ht.occupancy == 1 );

		b_res =
		hash_table_generic_multi_vm_insert( &ht, (void*)&k1, (void*)v1 );
		tg_test_assert( b_res );
		tg_test_assert( ht.occupancy == 2 );

		b_res =
		hash_table_generic_multi_vm_insert( &ht, (void*)&k, (void*)v );
		tg_test_assert( b_res );
		tg_test_assert( ht.occupancy == 3 );

		b_res =
		hash_table_generic_multi_vm_insert( &ht, (void*)&k1, (void*)v1 );
		tg_test_assert( b_res );
		tg_test_assert( ht.occupancy == 4 );

		b_res =
		hash_table_generic_multi_vm_insert( &ht, (void*)&k, (void*)v );
		tg_test_assert( b_res );
		tg_test_assert( ht.occupancy == 5 );

		b_res =
		hash_table_generic_multi_vm_get_all( &ht, (void*)&k, &lp );
		tg_test_assert( b_res );
		tg_test_assert( lp->length == 3 );
		for ( HTGMVM_Element_List_VM_Node * node = lp->first;
		      node;
		      node = node->next )
		{
			tg_test_assert( node->element.key == &k );
			tg_test_assert( node->element.value == v );
		}

		b_res =
		hash_table_generic_multi_vm_get_all( &ht, (void*)&k1, &lp );
		tg_test_assert( b_res );
		tg_test_assert( lp->length == 2 );
		for ( HTGMVM_Element_List_VM_Node * node = lp->first;
		      node;
		      node = node->next )
		{
			tg_test_assert( node->element.key == &k1 );
			tg_test_assert( node->element.value == v1 );
		}

		b_res =
		hash_table_generic_multi_vm_remove_and_get_all( &ht, (void*)&k, &l );
		tg_test_assert( b_res );
		tg_test_assert( ht.occupancy == 2 );
		tg_test_assert( l.length == 3 );
		for ( HTGMVM_Element_List_VM_Node * node = l.first;
		      node;
		      node = node->next )
		{
			tg_test_assert( node->element.key == &k );
			tg_test_assert( node->element.value == v );
		}
		list_vm_remove_all( &l );

		b_res =
		hash_table_generic_multi_vm_remove_all( &ht, (void*)&k1 );
		tg_test_assert( b_res );
		tg_test_assert( ht.occupancy == 0 );

		b_res =
		hash_table_generic_multi_vm_remove_all( &ht, (void*)&k1 );
		tg_test_assert( !b_res );
		tg_test_assert( ht.occupancy == 0 );

		b_res =
		hash_table_generic_multi_vm_remove_and_get_all( &ht, (void*)&k, &l );
		tg_test_assert( !b_res );
		tg_test_assert( ht.occupancy == 0 );
		tg_test_assert( 0 == memcmp( &l, &null_l, sizeof ( l ) ) );

		hash_table_generic_multi_vm_destroy( &ht );
	}

	ret = tg_test_asserts_result();
	return ret;
}

#include "hash_table_generic_multi_vm.cpp"
#include "pool_allocator_generic_vm.cpp"
#include "utility.cpp"
#include "tgmath.cpp"
#include "system.cpp"
