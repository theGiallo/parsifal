#include "stack_generic_vm.h"
#include "macro_tools.h"
#include "system.h"
#include "tests/test_tools.h"

s32
main( s32 argc, char *argv[] )
{
	(void)argc;
	(void)argv;

	s32 ret = 0;

	#pragma pack(push,1)
	struct
	Some_Struct
	{
		union
		{
			u64 the_u64;
			u8 eleven_bytes[11];
		};
	};
	#pragma pack(pop)

	Stack_Generic_VM stack = {};
	u64 max_capacity = 1024;
	u64 initial_capacity = 16;
	u64 capacity_increment_adder = 0;
	f64 capacity_increment_multiplier = 1.5;

	tg_test_assert(
	   stack_generic_vm_init( &stack, sizeof( Some_Struct ), max_capacity,
	                          initial_capacity, capacity_increment_multiplier,
	                          capacity_increment_adder ) );

	u64 real_initial_capacity = stack.current_capacity;
	u64 real_max_capacity     = stack.max_capacity;
	for ( u64 i = 0; i != real_initial_capacity; ++i )
	{
		tg_test_assert( stack.current_capacity != stack.occupancy );
		tg_test_assert( stack.current_capacity != stack.max_capacity );

		Some_Struct e = {};
		e.the_u64 = 0xcafebabe;
		tg_test_assert_m( stack_generic_vm_push( &stack, &e ), "i = %lu", i );

		Some_Struct pe = {};
		tg_test_assert_m( stack_generic_vm_peek( &stack, &pe ), "i = %lu", i );
		tg_test_assert( pe.the_u64 == e.the_u64 );

		Some_Struct * ppe = 0;
		tg_test_assert_m( ( ppe = (Some_Struct*) stack_generic_vm_peek( &stack ) ), "i = %lu", i );
		tg_test_assert( ppe->the_u64 == e.the_u64 );
	}
	tg_test_assert( stack.current_capacity == stack.occupancy );

	for ( u64 i = real_initial_capacity; i != real_max_capacity; ++i )
	{
		tg_test_assert_m( stack.occupancy != stack.max_capacity,
		                  "stack.occupancy = %lu stack.max_capacity = %lu",
		                  stack.occupancy, stack.max_capacity );
		Some_Struct e = {};
		e.the_u64 = 0xcafebabe;
		tg_test_assert( stack_generic_vm_push( &stack, &e ) );

		Some_Struct pe = {};
		tg_test_assert( stack_generic_vm_peek( &stack, &pe ) );
		tg_test_assert( pe.the_u64 == e.the_u64 );

		Some_Struct * ppe = 0;
		tg_test_assert( ( ppe = (Some_Struct*) stack_generic_vm_peek( &stack ) ) );
		tg_test_assert( ppe->the_u64 == e.the_u64 );
	}
	tg_test_assert_m( stack.current_capacity == stack.max_capacity,
	                  "stack.current_capacity = %lu stack.max_capacity = %lu",
	                  stack.current_capacity, stack.max_capacity );
	tg_test_assert_m( stack.current_capacity == stack.occupancy,
	                  "stack.current_capacity = %lu stack.occupancy = %lu",
	                  stack.current_capacity, stack.occupancy );

	{
		Some_Struct e = {};
		tg_test_assert( !stack_generic_vm_push( &stack, &e ) );
	}

	for ( u64 i = 0; i != real_max_capacity; ++i )
	{
		if ( i % 2 )
		{
			Some_Struct e = {};
			tg_test_assert( stack_generic_vm_pop( &stack, &e ) );
			tg_test_assert_m( e.the_u64 == 0xcafebabe, "i = %lu", i );
		} else
		{
			Some_Struct * pe = 0;
			tg_test_assert( ( pe = (Some_Struct*) stack_generic_vm_pop( &stack ) ) );
			tg_test_assert_m( pe->the_u64 == 0xcafebabe, "i = %lu", i );
		}
		tg_test_assert( stack.current_capacity == stack.max_capacity );
		tg_test_assert( stack.current_capacity != stack.occupancy );
	}
	tg_test_assert( stack.occupancy == 0 );

	stack_generic_vm_destroy( &stack );

	Stack_Generic_VM null_stack = {};
	tg_test_assert( 0 == memcmp( &stack, &null_stack, sizeof( stack ) ) );

	ret = tg_test_asserts_result();
	return ret;
}

#include "system.cpp"
#include "stack_generic_vm.cpp"
