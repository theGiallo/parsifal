#include "tests/test_tools.h"
#include "hash_table_generic_vm.h"

s32
main( s32 argc, char *argv[] )
{
	(void)argc;
	(void)argv;
	s32 ret = 0;

	{
		struct
		Key
		{
			u32 a;
			u8  b;
		};

		Hash_Table_Generic_VM ht = {};
		u64 max_capacity = 1024 * 1024;
		u32 table_length = 1023;
		bool b_res;

		b_res =
		hash_table_generic_vm_init( &ht, sizeof(Key), max_capacity, table_length );
		tg_test_assert( b_res );
		tg_test_assert( ht.occupancy == 0 );

		Key k = {};
		Key k1 = { .a = 1, .b = 2 };
		const char v[] = "Hello, world!";
		const char v1[] = "Hello, sailor!";
		b_res =
		hash_table_generic_vm_insert( &ht, (void*)&k, (void*)v );
		tg_test_assert( b_res );
		tg_test_assert( ht.occupancy == 1 );

		const char * vv;
		b_res =
		hash_table_generic_vm_get_one( &ht, (void*)&k, (void**)&vv );
		tg_test_assert( b_res );
		tg_test_assert( vv == v );
		tg_test_assert( ht.occupancy == 1 );

		b_res =
		hash_table_generic_vm_insert_unique( &ht, (void*)&k, (void*)v );
		tg_test_assert( !b_res );
		tg_test_assert( ht.occupancy == 1 );

		b_res =
		hash_table_generic_vm_remove_one( &ht, (void*)&k1 );
		tg_test_assert( !b_res );
		tg_test_assert( ht.occupancy == 1 );

		b_res =
		hash_table_generic_vm_remove_and_get_one( &ht, (void*)&k1, (void**)&vv );
		tg_test_assert( !b_res );
		tg_test_assert( vv == 0 );
		tg_test_assert( ht.occupancy == 1 );

		const char * vvv;
		b_res =
		hash_table_generic_vm_remove_and_get_one( &ht, (void*)&k, (void**)&vvv );
		tg_test_assert( b_res );
		tg_test_assert( vvv == v );
		tg_test_assert( ht.occupancy == 0 );

		const char * vvvv = 0;
		b_res =
		hash_table_generic_vm_get_one( &ht, (void*)&k, (void**)&vvvv );
		tg_test_assert( !b_res );
		tg_test_assert( vvvv == 0 );
		tg_test_assert( ht.occupancy == 0 );

		b_res =
		hash_table_generic_vm_insert( &ht, (void*)&k, (void*)v );
		tg_test_assert( b_res );
		tg_test_assert( ht.occupancy == 1 );

		b_res =
		hash_table_generic_vm_insert( &ht, (void*)&k, (void*)v );
		tg_test_assert( b_res );
		tg_test_assert( ht.occupancy == 2 );

		b_res =
		hash_table_generic_vm_get_one( &ht, (void*)&k, (void**)&vv );
		tg_test_assert( b_res );
		tg_test_assert( vv == v );
		tg_test_assert( ht.occupancy == 2 );

		vvv = 0;
		b_res =
		hash_table_generic_vm_remove_and_get_one( &ht, (void*)&k, (void**)&vvv );
		tg_test_assert( b_res );
		tg_test_assert( vvv == v );
		tg_test_assert( ht.occupancy == 1 );

		vvv = 0;
		b_res =
		hash_table_generic_vm_remove_and_get_one( &ht, (void*)&k, (void**)&vvv );
		tg_test_assert( b_res );
		tg_test_assert( vvv == v );
		tg_test_assert( ht.occupancy == 0 );

		vvv = (const char*)0xcafebabe;
		b_res =
		hash_table_generic_vm_remove_and_get_one( &ht, (void*)&k, (void**)&vvv );
		tg_test_assert( !b_res );
		tg_test_assert( vvv == 0 );
		tg_test_assert( ht.occupancy == 0 );

		b_res =
		hash_table_generic_vm_insert( &ht, (void*)&k, (void*)v );
		tg_test_assert( b_res );
		tg_test_assert( ht.occupancy == 1 );

		b_res =
		hash_table_generic_vm_insert( &ht, (void*)&k1, (void*)v1 );
		tg_test_assert( b_res );
		tg_test_assert( ht.occupancy == 2 );

		b_res =
		hash_table_generic_vm_insert( &ht, (void*)&k, (void*)v );
		tg_test_assert( b_res );
		tg_test_assert( ht.occupancy == 3 );

		b_res =
		hash_table_generic_vm_insert( &ht, (void*)&k1, (void*)v1 );
		tg_test_assert( b_res );
		tg_test_assert( ht.occupancy == 4 );

		b_res =
		hash_table_generic_vm_insert( &ht, (void*)&k, (void*)v );
		tg_test_assert( b_res );
		tg_test_assert( ht.occupancy == 5 );

		HTGVM_Element_List_VM got_list = {};
		s64 got_list_max_capacity = 128,
		    got_list_initial_capacity = 4;
		list_vm_init( &got_list, got_list_max_capacity, got_list_initial_capacity );

		b_res =
		hash_table_generic_vm_get_all( &ht, (void*)&k, &got_list );
		tg_test_assert( b_res );
		tg_test_assert( got_list.length == 3 );
		for ( HTGVM_Element_List_VM_Node * node = got_list.first;
		      node;
		      node = node->next )
		{
			tg_test_assert( node->element.key == &k );
			tg_test_assert( node->element.value == v );
		}
		list_vm_remove_all( &got_list );

		b_res =
		hash_table_generic_vm_get_all( &ht, (void*)&k1, &got_list );
		tg_test_assert( b_res );
		tg_test_assert( got_list.length == 2 );
		for ( HTGVM_Element_List_VM_Node * node = got_list.first;
		      node;
		      node = node->next )
		{
			tg_test_assert( node->element.key == &k1 );
			tg_test_assert( node->element.value == v1 );
		}
		list_vm_remove_all( &got_list );

		b_res =
		hash_table_generic_vm_remove_and_get_all( &ht, (void*)&k, &got_list );
		tg_test_assert( b_res );
		tg_test_assert( ht.occupancy == 2 );
		tg_test_assert( got_list.length == 3 );
		for ( HTGVM_Element_List_VM_Node * node = got_list.first;
		      node;
		      node = node->next )
		{
			tg_test_assert( node->element.key == &k );
			tg_test_assert( node->element.value == v );
		}
		list_vm_remove_all( &got_list );

		b_res =
		hash_table_generic_vm_remove_all( &ht, (void*)&k1 );
		tg_test_assert( b_res );
		tg_test_assert( ht.occupancy == 0 );

		b_res =
		hash_table_generic_vm_remove_all( &ht, (void*)&k1 );
		tg_test_assert( !b_res );
		tg_test_assert( ht.occupancy == 0 );

		b_res =
		hash_table_generic_vm_remove_and_get_all( &ht, (void*)&k, &got_list );
		tg_test_assert( !b_res );
		tg_test_assert( ht.occupancy == 0 );
		tg_test_assert( got_list.length == 0 );

		pool_allocator_generic_vm_destroy( &got_list.nodes_allocator );

		hash_table_generic_vm_destroy( &ht );
	}
	{
		Hash_Table_Generic_VM ht = {};
		u64 max_capacity = 1024 * 1024;
		u32 table_length = 1023;
		bool b_res;

		b_res =
		hash_table_generic_vm_init( &ht, -1, max_capacity, table_length );
		tg_test_assert( b_res );
		tg_test_assert( ht.occupancy == 0 );

		const char * k = "Hey, I'm a key";
		const char * k1 = "I'm one too";
		const char v[] = "Hello, world!";
		const char v1[] = "Hello, sailor!";
		b_res =
		hash_table_generic_vm_insert( &ht, (void*)&k, (void*)v );
		tg_test_assert( b_res );
		tg_test_assert( ht.occupancy == 1 );

		const char * vv;
		b_res =
		hash_table_generic_vm_get_one( &ht, (void*)&k, (void**)&vv );
		tg_test_assert( b_res );
		tg_test_assert( vv == v );
		tg_test_assert( ht.occupancy == 1 );

		b_res =
		hash_table_generic_vm_insert_unique( &ht, (void*)&k, (void*)v );
		tg_test_assert( !b_res );
		tg_test_assert( ht.occupancy == 1 );

		b_res =
		hash_table_generic_vm_remove_one( &ht, (void*)&k1 );
		tg_test_assert( !b_res );
		tg_test_assert( ht.occupancy == 1 );

		b_res =
		hash_table_generic_vm_remove_and_get_one( &ht, (void*)&k1, (void**)&vv );
		tg_test_assert( !b_res );
		tg_test_assert( vv == 0 );
		tg_test_assert( ht.occupancy == 1 );

		const char * vvv;
		b_res =
		hash_table_generic_vm_remove_and_get_one( &ht, (void*)&k, (void**)&vvv );
		tg_test_assert( b_res );
		tg_test_assert( vvv == v );
		tg_test_assert( ht.occupancy == 0 );

		const char * vvvv = 0;
		b_res =
		hash_table_generic_vm_get_one( &ht, (void*)&k, (void**)&vvvv );
		tg_test_assert( !b_res );
		tg_test_assert( vvvv == 0 );
		tg_test_assert( ht.occupancy == 0 );

		b_res =
		hash_table_generic_vm_insert( &ht, (void*)&k, (void*)v );
		tg_test_assert( b_res );
		tg_test_assert( ht.occupancy == 1 );

		b_res =
		hash_table_generic_vm_insert( &ht, (void*)&k, (void*)v );
		tg_test_assert( b_res );
		tg_test_assert( ht.occupancy == 2 );

		b_res =
		hash_table_generic_vm_get_one( &ht, (void*)&k, (void**)&vv );
		tg_test_assert( b_res );
		tg_test_assert( vv == v );
		tg_test_assert( ht.occupancy == 2 );

		vvv = 0;
		b_res =
		hash_table_generic_vm_remove_and_get_one( &ht, (void*)&k, (void**)&vvv );
		tg_test_assert( b_res );
		tg_test_assert( vvv == v );
		tg_test_assert( ht.occupancy == 1 );

		vvv = 0;
		b_res =
		hash_table_generic_vm_remove_and_get_one( &ht, (void*)&k, (void**)&vvv );
		tg_test_assert( b_res );
		tg_test_assert( vvv == v );
		tg_test_assert( ht.occupancy == 0 );

		vvv = (const char*)0xcafebabe;
		b_res =
		hash_table_generic_vm_remove_and_get_one( &ht, (void*)&k, (void**)&vvv );
		tg_test_assert( !b_res );
		tg_test_assert( vvv == 0 );
		tg_test_assert( ht.occupancy == 0 );

		b_res =
		hash_table_generic_vm_insert( &ht, (void*)&k, (void*)v );
		tg_test_assert( b_res );
		tg_test_assert( ht.occupancy == 1 );

		b_res =
		hash_table_generic_vm_insert( &ht, (void*)&k1, (void*)v1 );
		tg_test_assert( b_res );
		tg_test_assert( ht.occupancy == 2 );

		b_res =
		hash_table_generic_vm_insert( &ht, (void*)&k, (void*)v );
		tg_test_assert( b_res );
		tg_test_assert( ht.occupancy == 3 );

		b_res =
		hash_table_generic_vm_insert( &ht, (void*)&k1, (void*)v1 );
		tg_test_assert( b_res );
		tg_test_assert( ht.occupancy == 4 );

		b_res =
		hash_table_generic_vm_insert( &ht, (void*)&k, (void*)v );
		tg_test_assert( b_res );
		tg_test_assert( ht.occupancy == 5 );

		HTGVM_Element_List_VM got_list = {};
		s64 got_list_max_capacity = 128,
		    got_list_initial_capacity = 4;
		list_vm_init( &got_list, got_list_max_capacity, got_list_initial_capacity );

		b_res =
		hash_table_generic_vm_get_all( &ht, (void*)&k, &got_list );
		tg_test_assert( b_res );
		tg_test_assert( got_list.length == 3 );
		for ( HTGVM_Element_List_VM_Node * node = got_list.first;
		      node;
		      node = node->next )
		{
			tg_test_assert( node->element.key == &k );
			tg_test_assert( node->element.value == v );
		}
		list_vm_remove_all( &got_list );

		b_res =
		hash_table_generic_vm_get_all( &ht, (void*)&k1, &got_list );
		tg_test_assert( b_res );
		tg_test_assert( got_list.length == 2 );
		for ( HTGVM_Element_List_VM_Node * node = got_list.first;
		      node;
		      node = node->next )
		{
			tg_test_assert( node->element.key == &k1 );
			tg_test_assert( node->element.value == v1 );
		}
		list_vm_remove_all( &got_list );

		b_res =
		hash_table_generic_vm_remove_and_get_all( &ht, (void*)&k, &got_list );
		tg_test_assert( b_res );
		tg_test_assert( ht.occupancy == 2 );
		tg_test_assert( got_list.length == 3 );
		for ( HTGVM_Element_List_VM_Node * node = got_list.first;
		      node;
		      node = node->next )
		{
			tg_test_assert( node->element.key == &k );
			tg_test_assert( node->element.value == v );
		}
		list_vm_remove_all( &got_list );

		b_res =
		hash_table_generic_vm_remove_all( &ht, (void*)&k1 );
		tg_test_assert( b_res );
		tg_test_assert( ht.occupancy == 0 );

		b_res =
		hash_table_generic_vm_remove_all( &ht, (void*)&k1 );
		tg_test_assert( !b_res );
		tg_test_assert( ht.occupancy == 0 );

		b_res =
		hash_table_generic_vm_remove_and_get_all( &ht, (void*)&k, &got_list );
		tg_test_assert( !b_res );
		tg_test_assert( ht.occupancy == 0 );
		tg_test_assert( got_list.length == 0 );

		pool_allocator_generic_vm_destroy( &got_list.nodes_allocator );

		hash_table_generic_vm_destroy( &ht );
	}

	ret = tg_test_asserts_result();
	return ret;
}

#include "hash_table_generic_vm.cpp"
#include "pool_allocator_generic_vm.cpp"
#include "utility.cpp"
#include "tgmath.cpp"
#include "system.cpp"
