#include "pool_allocator_generic_vm.h"
#include "macro_tools.h"
#include "system.h"
#include "tests/test_tools.h"

s32
main( s32 argc, char *argv[] )
{
	(void)argc;
	(void)argv;
	s32 ret = 0;

	#pragma pack(push,1)
	struct
	Some_Struct
	{
		union
		{
			u64 the_u64;
			u8 eleven_bytes[11];
		};
	};
	#pragma pack(pop)

	Pool_Allocator_Generic_VM pool = {};
	s64 max_capacity = 1024;
	s64 initial_capacity = 4;
	s64 capacity_increment = 0;
	pool_allocator_generic_vm_init( &pool, sizeof( Some_Struct ), max_capacity, initial_capacity, capacity_increment );

	tg_test_assert( pool.current_capacity >= initial_capacity );
	tg_test_assert( pool.max_capacity >= max_capacity );
	tg_test_assert( pool.element_size == sizeof( Some_Struct ) );
	tg_test_assert( pool.vm_start );
	tg_test_assert( !pool_allocator_generic_vm_is_max_capacity_full( &pool ) );
	tg_test_assert( !pool_allocator_generic_vm_is_current_capacity_full( &pool ) );

	s64 real_initial_capacity = pool.current_capacity;
	s64 real_max_capacity = pool.max_capacity;
	Some_Struct * elements[real_max_capacity];
	for ( s32 i = 0; i != real_initial_capacity; ++i )
	{
		tg_test_assert( !pool_allocator_generic_vm_is_current_capacity_full( &pool ) );
		tg_test_assert( !pool_allocator_generic_vm_is_max_capacity_full( &pool ) );
		elements[i] = (Some_Struct*) pool_allocator_generic_vm_allocate( &pool );
		tg_test_assert_m( elements[i], "i = %d", i );
		elements[i]->the_u64 = 0xcafebabe;
	}
	tg_test_assert( pool_allocator_generic_vm_is_current_capacity_full( &pool ) );

	for ( s32 i = real_initial_capacity; i != real_max_capacity; ++i )
	{
		tg_test_assert( !pool_allocator_generic_vm_is_max_capacity_full( &pool ) );
		elements[i] = (Some_Struct*) pool_allocator_generic_vm_allocate( &pool );
		tg_test_assert_m( elements[i], "i = %d", i );
		elements[i]->the_u64 = 0xcafebabe;
	}
	tg_test_assert( pool_allocator_generic_vm_is_max_capacity_full( &pool ) );
	tg_test_assert( pool_allocator_generic_vm_is_current_capacity_full( &pool ) );

	tg_test_assert( NULL == pool_allocator_generic_vm_allocate( &pool ) );

	for ( s32 i = 0; i != real_max_capacity; ++i )
	{
		pool_allocator_generic_vm_deallocate( &pool, elements[i] );
		tg_test_assert_m( elements[i]->the_u64 != 0xcafebabe, "i = %d", i );
		tg_test_assert( !pool_allocator_generic_vm_is_max_capacity_full( &pool ) );
		tg_test_assert( !pool_allocator_generic_vm_is_current_capacity_full( &pool ) );
	}
	tg_test_assert( pool.occupancy == 0 );

	pool_allocator_generic_vm_destroy( &pool );

	tg_test_assert( pool == Pool_Allocator_Generic_VM({}) );

	initial_capacity = 0;
	capacity_increment = 128;
	pool_allocator_generic_vm_init( &pool, sizeof( Some_Struct ), max_capacity, initial_capacity, capacity_increment );

	tg_test_assert( pool.current_capacity == initial_capacity );
	tg_test_assert( pool.max_capacity >= max_capacity );
	tg_test_assert( pool.element_size == sizeof( Some_Struct ) );
	tg_test_assert( pool.vm_start );
	tg_test_assert( !pool_allocator_generic_vm_is_max_capacity_full( &pool ) );
	tg_test_assert( pool_allocator_generic_vm_is_current_capacity_full( &pool ) );

	real_max_capacity = pool.max_capacity;
	s64 target_capacity = 64;
	bool res_b =
	pool_allocator_generic_vm_up_capacity_to( &pool, target_capacity );

	tg_test_assert( res_b );
	tg_test_assert( pool.current_capacity >= target_capacity );
	tg_test_assert( !pool_allocator_generic_vm_is_max_capacity_full( &pool ) );
	tg_test_assert( !pool_allocator_generic_vm_is_current_capacity_full( &pool ) );

	{
		s64 real_target_capacity = pool.current_capacity;

		Some_Struct * elements[real_max_capacity];
		for ( s32 i = 0; i != real_target_capacity; ++i )
		{
			tg_test_assert( !pool_allocator_generic_vm_is_current_capacity_full( &pool ) );
			tg_test_assert( !pool_allocator_generic_vm_is_max_capacity_full( &pool ) );
			elements[i] = (Some_Struct*) pool_allocator_generic_vm_allocate( &pool );
			tg_test_assert_m( elements[i], "i = %d", i );
			elements[i]->the_u64 = 0xcafebabe;
		}
		tg_test_assert( pool_allocator_generic_vm_is_current_capacity_full( &pool ) );
	}

	pool_allocator_generic_vm_destroy( &pool );

	tg_assert( pool == Pool_Allocator_Generic_VM({}) );

	max_capacity = 1025 * 1024;
	initial_capacity = -1;
	capacity_increment = 0;
	Proc_Mem_Info proc_mem_info_before = sys_get_proc_mem_info();
	for ( s32 i = 0; i != 1024; ++i )
	{
		pool_allocator_generic_vm_init( &pool, sizeof( Some_Struct ), max_capacity, initial_capacity, capacity_increment );

		tg_test_assert( pool.current_capacity == pool.max_capacity );
		tg_test_assert( pool.max_capacity >= max_capacity );
		tg_test_assert( pool.element_size == sizeof( Some_Struct ) );
		tg_test_assert( pool.vm_start );
		tg_test_assert( !pool_allocator_generic_vm_is_max_capacity_full( &pool ) );
		tg_test_assert( !pool_allocator_generic_vm_is_current_capacity_full( &pool ) );

		//Proc_Mem_Info proc_mem_info_middle = sys_get_proc_mem_info();
		// NOTE(theGiallo): this can be false because OS could decide to keep
		// the address space for us, in case we want it again.
		// tg_test_assert( proc_mem_info_middle.bytes_physical >= proc_mem_info_before.bytes_physical + pool.reserved_size );
		pool_allocator_generic_vm_destroy( &pool );

		tg_assert( pool == Pool_Allocator_Generic_VM({}) );
		Proc_Mem_Info proc_mem_info_after = sys_get_proc_mem_info();
		// NOTE(theGiallo): this is arbitrary, I know
		tg_test_assert( proc_mem_info_before.bytes_physical <= 1.1f * proc_mem_info_after.bytes_physical );
	}
	Proc_Mem_Info proc_mem_info_after = sys_get_proc_mem_info();
	// NOTE(theGiallo): this is arbitrary, I know
	tg_test_assert( proc_mem_info_before.bytes_physical <= 1.1f * proc_mem_info_after.bytes_physical );

	ret = tg_test_asserts_result();
	return ret;
}
#include "system.cpp"
#include "pool_allocator_generic_vm.cpp"
