#include "typechecker.h"
#include "system.h"
#include "stack_generic_vm.h"


bool
type_checker_init( Type_Checker * tc, u64 max_nodes, u64 max_contexts_nodes )
{
	u64 element_size = sizeof ( Hash_Table_Generic_VM_Element_List_VM_Node );
	s64 initial_capacity = 0;
	s64 capacity_increment = 0;
	s64 max_capacity = max_nodes;

	pool_allocator_generic_vm_init( &tc->htgvm_lists_nodes_pool, element_size,
	                                max_capacity, initial_capacity,
	                                capacity_increment );


	element_size = sizeof ( TC_Context_p_List_VM_Node );
	initial_capacity = 0;
	capacity_increment = 0;
	max_capacity = max_contexts_nodes;

	pool_allocator_generic_vm_init( &tc->contexts_children_lists_nodes_pool,
	                                element_size, max_capacity,
	                                initial_capacity, capacity_increment );


	element_size = sizeof ( TC_Context );
	initial_capacity = 0;
	capacity_increment = 0;
	max_capacity = max_contexts_nodes;

	pool_allocator_generic_vm_init( &tc->contexts_pool, element_size,
	                                max_capacity, initial_capacity,
	                                capacity_increment );


	element_size = sizeof ( TC_Decl );
	initial_capacity = 0;
	capacity_increment = 0;
	max_capacity = max_contexts_nodes;

	pool_allocator_generic_vm_init( &tc->declarations_pool, element_size,
	                                max_capacity, initial_capacity,
	                                capacity_increment );


	element_size = sizeof ( TC_Type );
	initial_capacity = 0;
	capacity_increment = 0;
	max_capacity = max_nodes;

	pool_allocator_generic_vm_init( &tc->types_pool, element_size,
	                                max_capacity, initial_capacity,
	                                capacity_increment );


	element_size = sizeof ( TC_Type_Context );
	initial_capacity = 0;
	capacity_increment = 0;
	max_capacity = max_nodes;

	pool_allocator_generic_vm_init( &tc->type_contexts_pool, element_size,
	                                max_capacity, initial_capacity,
	                                capacity_increment );

	tc->root_context = {};
}

bool
type_checker_init_type_context( Type_Checker * tc, TC_Type_Context * type_context )
{
	bool ret;
	u64 max_capacity = 0,
	    table_length = 0;

	bool b_res =
	hash_table_generic_multi_vm_init( &type_context->variables_declarations_by_name,
	                                  -1, max_capacity, table_length, 0,
	                                  &tc->htgvm_lists_nodes_pool );
	ret = b_res;
	if ( !ret )
	{
		return ret;
	}

	b_res =
	hash_table_generic_multi_vm_init( &type_context->types_declarations_by_name,
	                                  -1, max_capacity, table_length, 0,
	                                  &tc->htgvm_lists_nodes_pool );
	ret = b_res;
	if ( !ret )
	{
		return ret;
	}

	b_res =
	hash_table_generic_multi_vm_init( &type_context->usings_declarations_by_name,
	                                  -1, max_capacity, table_length, 0,
	                                  &tc->htgvm_lists_nodes_pool );
	ret = b_res;
	if ( !ret )
	{
		return ret;
	}

	ret = true;

	return ret;
}


bool
type_checker_init_context( Type_Checker * tc, TC_Context * context )
{
	bool ret;
	u64 max_capacity = 0,
	    table_length = 0;

	bool b_res =
	hash_table_generic_multi_vm_init( &context->functions_declarations_by_name,
	                                  -1, max_capacity, table_length, 0,
	                                  &tc->htgvm_lists_nodes_pool );
	ret = b_res;
	if ( !ret )
	{
		return ret;
	}

	b_res =
	hash_table_generic_multi_vm_init( &context->variables_declarations_by_name,
	                                  -1, max_capacity, table_length, 0,
	                                  &tc->htgvm_lists_nodes_pool );
	ret = b_res;
	if ( !ret )
	{
		return ret;
	}

	b_res =
	hash_table_generic_multi_vm_init( &context->types_declarations_by_name,
	                                  -1, max_capacity, table_length, 0,
	                                  &tc->htgvm_lists_nodes_pool );
	ret = b_res;
	if ( !ret )
	{
		return ret;
	}

	b_res =
	hash_table_generic_multi_vm_init( &context->usings_declarations_by_name,
	                                  -1, max_capacity, table_length, 0,
	                                  &tc->htgvm_lists_nodes_pool );
	ret = b_res;
	if ( !ret )
	{
		return ret;
	}

	list_vm_init( &context->children_sorted,
	              &tc->contexts_children_lists_nodes_pool );
	ret = true;

	return ret;
}

void
type_checker_first_pass( Type_Checker * tc, AST * ast )
{
	if ( !ast->root )
	{
		return;
	}

	Stack_Generic_VM contexts_stack = {};
	stack_generic_vm_init( &contexts_stack, sizeof( TC_Context * ), tc->contexts_pool.max_capacity, 1024 );

	tc->root_context.file_range = ast->root->file_range;

	TC_Context * current_context = &tc->root_context;
	stack_generic_vm_push( &contexts_stack, &current_context );

	for ( AST_Node_p_List_VM_Node * le = ast->root->children.first;
	      le;
	      le = le->next )
	{
		AST_Node * child = le->element;
		switch ( child->type )
		{
			case AST_Node_Type::FUNCTION_DECLARATION:
			{
				TC_Context * c = (TC_Context*) pool_allocator_generic_vm_allocate_clean( &tc->contexts_pool );
				TC_Decl * decl = (TC_Decl*)    pool_allocator_generic_vm_allocate_clean( &tc->declarations_pool );
				decl->ast_node = child;
				decl->file_coordinate = child->file_range.first;
				decl->type.type_type  = TC_Type_enum::FUNCTION;

				AST_Node_p_List_VM_Node * child_child = child->children.first;

				AST_Node * name_node  = child_child->element;
				child_child = child_child->next;

				AST_Node * arg_struct = child_child->element;
				child_child = child_child->next;

				tg_assert( arg_struct->type == AST_Node_Type::ARGUMENT_STRUCT );
				TC_Type * args_type = (TC_Type*) pool_allocator_generic_vm_allocate_clean( &tc->types_pool );
				args_type->type_type = TC_Type_enum::STRUCT;
				// TODO(theGiallo, 2018-06-17): type_checker_first_pass_struct( tc, arg_struct->children.first->element, args_type );
				// TC_Type_List_VM * tl = args_type->structure.members_types;
				decl->type.function.arguments_type = args_type;

				AST_Node * next = child_child->element;
				child_child = child_child->next;

				decl->type.function.return_type = NULL;
				if ( next->type == AST_Node_Type::RETURN_TYPE )
				{
					TC_Type * ret_type = (TC_Type*) pool_allocator_generic_vm_allocate_clean( &tc->types_pool );
					// TODO(theGiallo, 2018-06-17): type_checker_first_pass_type( tc, next->children.first->element, ret_type );
					decl->type.function.return_type = ret_type;
					next = child_child->element;
					child_child = child_child->next;
				}

				hash_table_generic_multi_vm_insert(
				   &current_context->functions_declarations_by_name,
				   name_node->text_element_nodes.first->element.text.text,
				   decl );

				// TODO(theGiallo, 2018-06-17): type_checker_first_pass_add_arguments_to_block_context( tc, c, args_type );

				// TODO(theGiallo, 2018-06-17): type_checker_first_pass_block( tc, next, block );
				c->file_range = child->file_range;
				stack_generic_vm_push( &contexts_stack, &c );
				break;
			}
			case AST_Node_Type::STRUCT_DECLARATION:
			{
				TC_Decl * decl = (TC_Decl*) pool_allocator_generic_vm_allocate_clean( &tc->declarations_pool );
				decl->ast_node = child;
				decl->file_coordinate = child->file_range.first;
				decl->type.type_type  = TC_Type_enum::STRUCT;

				AST_Node_p_List_VM_Node * child_child = child->children.first;

				AST_Node * name_node  = child_child->element;
				child_child = child_child->next;

				AST_Node * struct_body_node  = child_child->element;
				child_child = child_child->next;

				// TODO(theGiallo, 2018-06-17): type_checker_first_pass_struct( tc, struct_body_node, &decl->type );

				hash_table_generic_multi_vm_insert(
				   &current_context->functions_declarations_by_name,
				   name_node->text_element_nodes.first->element.text.text,
				   decl );

				break;
			}
			case AST_Node_Type::BITFIELD_DECLARATION:
			{
				TC_Decl * decl = (TC_Decl*) pool_allocator_generic_vm_allocate_clean( &tc->declarations_pool );
				decl->ast_node = child;
				decl->file_coordinate = child->file_range.first;
				decl->type.type_type  = TC_Type_enum::BITFIELD;
				decl->type.bitfield.context = (TC_Type_Context*) pool_allocator_generic_vm_allocate_clean( &tc->type_contexts_pool );
				type_checker_init_type_context( tc, decl->type.bitfield.context );

				AST_Node_p_List_VM_Node * child_child = child->children.first;

				AST_Node * name_node  = child_child->element;
				decl->type.name = name_node->text_element_nodes.first->element.text.text;
				child_child = child_child->next;

				while ( child_child )
				{
					AST_Node * member_node  = child_child->element;
					child_child = child_child->next;
					TC_Type member = {};//(TC_Type*)pool_allocator_generic_vm_allocate_clean( &tc->types_pool );
					member.type_type = TC_Type_enum::BITFIELD_MEMBER;
					if ( member_node->type == AST_Node_Type::NAME )
					{
						member.name                       = member_node->text_element_nodes.first->element.text.text;
						member.bitfield_member.bits_count = 1;
						member.bitfield_member.known      = true;
					} else
					{
						member.name = member_node->children.first->element->text_element_nodes.first->element.text.text;
						member.bitfield_member.expression = member_node->children.first->next->element;
						// TODO(theGiallo, 2018-06-24): should we compute directly here if the expression is known?
						// Or at least if it's a literal?
						tg_assert( member_node->children.first->element->type == AST_Node_Type::NAME );
						tg_assert( member_node->children.first->next->element->type == AST_Node_Type::EXPRESSION );
						tg_assert( member.bitfield_member.expression->type == AST_Node_Type::EXPRESSION );
						tg_assert( member_node->type                       == AST_Node_Type::MANY_BITS_DECLARATION );
					}

					list_vm_push_last( decl->type.bitfield.members_types, &member );
					hash_table_generic_multi_vm_insert(
					   &decl->type.bitfield.context->variables_declarations_by_name,
					   member_node->text_element_nodes.first->element.text.text,
					   &member_node );
				}

				hash_table_generic_multi_vm_insert(
				   &current_context->functions_declarations_by_name,
				   name_node->text_element_nodes.first->element.text.text,
				   decl );

				break;
			}
			case AST_Node_Type::ENUM_DECLARATION:
			{
				TC_Decl * decl = (TC_Decl*) pool_allocator_generic_vm_allocate_clean( &tc->declarations_pool );
				decl->ast_node = child;
				decl->file_coordinate = child->file_range.first;
				decl->type.type_type  = TC_Type_enum::ENUM;

				AST_Node_p_List_VM_Node * child_child = child->children.first;

				AST_Node * type_node  = child_child->element;
				child_child = child_child->next;

				AST_Node * name_node  = child_child->element;
				child_child = child_child->next;

				TC_Type_Context * type_context = 0; // TODO(theGiallo, 2018-06-24): allocate clean from an allocator

				while ( child_child )
				{
					AST_Node * member_node  = child_child->element;
					child_child = child_child->next;
					TC_Type * member = (TC_Type*)pool_allocator_generic_vm_allocate_clean( &tc->types_pool );
					member->type_type = TC_Type_enum::ENUM_MEMBER;
					AST_Node * node_enum_member_name = 0;
					if ( member_node->type == AST_Node_Type::EXPR_TERMINAL )
					{
						tg_assert( member_node->children.first->element->type       == AST_Node_Type::TYPE );
						tg_assert( member_node->children.first->next->element->type == AST_Node_Type::NAME );
						node_enum_member_name = member_node->children.first->next->element;
						//member->enumeration_member.known = false;
					} else
					{
						AST_Node * node_bin_expr = member_node->children.first->element;
						tg_assert( node_bin_expr->type == AST_Node_Type::EXPR_BINARY_OP_PREC_12 );

						AST_Node * node_expr_terminal = node_bin_expr->children.first->element;
						tg_assert( node_expr_terminal->type == AST_Node_Type::EXPR_TERMINAL );
						AST_Node * node_name_type = node_expr_terminal->children.first->element;
						tg_assert( node_name_type->type == AST_Node_Type::TYPE );
						AST_Node * node_name_name = node_name_type->children.first->element;
						tg_assert( node_name_name->type == AST_Node_Type::NAME );
						node_enum_member_name = node_name_name;

						AST_Node * node_bin_op_12 = node_bin_expr->children.first->next->element;
						tg_assert( node_bin_op_12->type == AST_Node_Type::BINARY_OPERATOR_PREC_12 );
						AST_Node * node_assignment_op = node_bin_op_12->children.first->element;
						tg_assert( node_assignment_op->type == AST_Node_Type::ASSIGNMENT_OP );

						AST_Node * node_r_expr = node_bin_expr->children.first->next->next->element;
						member->enumeration_member.assigned_expression = node_r_expr;
						//if ( node_r_expr->type == AST_Node_Type::EXPR_TERMINAL )
						//{
						//} else
						//{
						//}
					}
					hash_table_generic_multi_vm_insert( &type_context->variables_declarations_by_name,
					                                    node_enum_member_name->text_element_nodes.first->element.text.text,
					                                    member );
				}
				break;
			}
			default:
			// NOTE(theGiallo): for now USING is not enable in global scope
				break;
		}
	}
}
