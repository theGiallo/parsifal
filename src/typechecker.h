#ifndef __TYPE_CHECKER_H__
#define __TYPE_CHECKER_H__ 1
#include "basic_types.h"
#include "macro_tools.h"
#include "parser.h"
#include "hash_table_generic_vm.h"
#include "hash_table_generic_multi_vm.h"

// TODO(theGiallo, 2018-04-15): think about auto-cast to types with same
// sub-types. E.g. structures with same member types.

#define TYPE_TYPES(ENTRY) \
	ENTRY(UNKNOWN        )\
	ENTRY(BASE           )\
	ENTRY(STRING         )\
	ENTRY(POINTER        )\
	ENTRY(ARRAY          )\
	ENTRY(RANGE          )\
	ENTRY(STRUCT         )\
	ENTRY(UNION          )\
	ENTRY(BITFIELD       )\
	ENTRY(BITFIELD_MEMBER)\
	ENTRY(ENUM           )\
	ENTRY(ENUM_MEMBER    )\
	ENTRY(FUNCTION       )\

#define TYPE_TYPES_AS_ENUM(a) a,
#define TYPE_TYPES_AS_STRING(a) TOSTRING(a),

enum class
TC_Type_enum : u8
{
	TYPE_TYPES(TYPE_TYPES_AS_ENUM)
	//---
	_COUNT
};
const char* tc_type_strings[] = {
	TYPE_TYPES(TYPE_TYPES_AS_STRING)
};

#define BASE_TYPES(ENTRY) \
	ENTRY(U8    , u8    )\
	ENTRY(U16   , u16   )\
	ENTRY(U32   , u32   )\
	ENTRY(U64   , u64   )\
	ENTRY(S8    , s8    )\
	ENTRY(S16   , s16   )\
	ENTRY(S32   , s32   )\
	ENTRY(S64   , s64   )\
	ENTRY(F16   , f16   )\
	ENTRY(F32   , f32   )\
	ENTRY(F64   , f64   )\
	ENTRY(BOOL  , bool  )\
	ENTRY(BOOL8 , bool8 )\
	ENTRY(BOOL16, bool16)\
	ENTRY(BOOL32, bool32)\
	ENTRY(BOOL64, bool64)\

#define BASE_TYPES_AS_ENUM(a,b) a,
#define BASE_TYPES_AS_STRING(a,b) TOSTRING(b),

union
Base_Type_Value
{
	u8   _u8;
	u16  _u16;
	u32  _u32;
	u64  _u64;
	s8   _s8;
	s16  _s16;
	s32  _s32;
	s64  _s64;
	u16  _f16;
	f32  _f32;
	f64  _f64;
	bool _bool;
	u8   _bool8;
	u16  _bool16;
	u32  _bool32;
	u64  _bool64;
};

enum class
TC_Base_Type_enum : u8
{
	BASE_TYPES(BASE_TYPES_AS_ENUM)
	//---
	_COUNT
};
const char* tc_base_type_strings[] = {
	BASE_TYPES(BASE_TYPES_AS_STRING)
};

struct
TC_Type_Context;

struct TC_Type_List_VM;
struct
TC_Type
{
	TC_Type_enum   type_type;
	u8           * name;
	union
	{
		struct
		{
			TC_Type_List_VM * possible_types;
		} unknown;
		struct
		{
			TC_Base_Type_enum base_type;
		} base;
		struct
		{
			TC_Type * type;
		} pointer;
		struct
		{
			TC_Type * type;
			u64       length;
			bool      length_unknown;
		} array;
		struct
		{
			// TODO(theGiallo, 2018-04-15): should string be a base type?
		} string;
		struct
		{
			TC_Base_Type_enum base_type;
			u64 length;
		} range;
		struct
		{
			TC_Type * arguments_type;
			TC_Type * return_type;
		} function;
		struct
		{
			TC_Type_List_VM * members_types;
			TC_Type_Context * context;
		} structure;
		struct
		{
			TC_Type         * base_type;
			TC_Type_List_VM * members;
			TC_Type_Context * context;
		} enumeration;
		struct
		{
			AST_Node        * assigned_expression;
			bool              known;
			Base_Type_Value   value;
		} enumeration_member;
		struct
		{
			TC_Type_List_VM * members_types;
			TC_Type_Context * context;
		} bitfield;
		struct
		{
			AST_Node * expression;
			u32        bits_count;
			bool       known;
		} bitfield_member;
		struct
		{
			// NOTE(theGiallo): this will be owned by some declaration
			TC_Type * tc_type;
		} other_type;
	};
};

#define LIST_VM_TYPE TC_Type
#include "list_vm.inc.h"

struct
TC_Decl
{
	File_Coordinate file_coordinate;
	AST_Node *      ast_node;
	TC_Type         type;
};

struct
TC_Context;
typedef TC_Context * TC_Context_p;

#define LIST_VM_TYPE TC_Context_p
#include "list_vm.inc.h"

struct
TC_Type_Context
{
	File_Coordinates_Range       file_range;
	Hash_Table_Generic_Multi_VM  variables_declarations_by_name;
	Hash_Table_Generic_Multi_VM  types_declarations_by_name;
	Hash_Table_Generic_Multi_VM  usings_declarations_by_name;
};

struct
TC_Context
{
	File_Coordinates_Range       file_range;
	Hash_Table_Generic_Multi_VM  functions_declarations_by_name;
	Hash_Table_Generic_Multi_VM  variables_declarations_by_name;
	Hash_Table_Generic_Multi_VM  types_declarations_by_name;
	Hash_Table_Generic_Multi_VM  usings_declarations_by_name;
	TC_Context_p_List_VM         children_sorted;

	// NOTE(theGiallo): data for type inference
	Hash_Table_Generic_Multi_VM undecided_named_nodes_by_name;
	Hash_Table_Generic_Multi_VM undecided_unnamed_nodes_by_name;
};

struct
Type_Checker
{
	TC_Context                root_context;
	Pool_Allocator_Generic_VM htgvm_lists_nodes_pool;
	Pool_Allocator_Generic_VM contexts_children_lists_nodes_pool;
	Pool_Allocator_Generic_VM contexts_pool;
	Pool_Allocator_Generic_VM declarations_pool;
	Pool_Allocator_Generic_VM types_pool;
	Pool_Allocator_Generic_VM type_contexts_pool;
};

bool
type_checker_init( Type_Checker * tc, u64 max_type_nodes, u64 max_contexts_nodes );

void
type_checker_first_pass( Type_Checker * tc, AST * ast );

#endif /* ifndef __TYPE_CHECKER_H__ */
