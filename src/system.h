#ifndef __SYSTEM_H__
#define __SYSTEM_H__ 1

#if (defined(linux) || defined(__linux) || defined(__linux__))
#define LINUX 1
#elif defined(__APPLE__)
#define MAC 1
#elif defined(WIN32) || defined(_WIN32) || defined(__CYGWIN__) || defined(__MINGW32__)
#define WINDOWS 1
#elif
#error "unsupported or undetected platform"
#endif

#include "macro_tools.h"

#if LINUX
#include <sys/mman.h>
#include <unistd.h> // NOTE(theGiallo): getpagesize
#elif WINDOWS
#include <Windows.h>
#endif

#include <string.h> // NOTE(theGiallo): memset

#include "basic_types.h"

// NOTE(theGiallo): for file last modified date
#include <sys/stat.h>

#define SYS_ERRNO_TO_SYS_ERROR( e ) ( - ( e + (s64)0xFFFFFFFF ) )
#define SYS_ERR_IS_ERRNO( e ) ( -(e) >= (s64)0xFFFFFFFF )
#define SYS_ERRNO_FROM_ERR( e ) ( (-(e)) >> 32 )

// NOTE(theGiallo): return codes
#define SYS_ERR_EXPAND_AS_ENUM(a,b,c) a = b,
#define SYS_ERR_EXPAND_AS_TABLE_ENTRY(a,b,c) { LPR_TOSTRING( a ), c },
#define SYS_ERR_TABLE(ENTRY) \
        ENTRY( SYS_ERR_TOO_MANY_WINDOWS,        1, "" ) \
        ENTRY( SYS_ERR_CREATING_WINDOW ,        2, "" ) \
        ENTRY( SYS_ERR_CREATING_CONTEXT,        3, "" ) \
        ENTRY( SYS_ERR_NO_DISPLAYS,             4, "" ) \
        ENTRY( SYS_ERR_MAKING_WINDOW_CURRENT,   5, "" ) \
        ENTRY( SYS_ERR_LOADING_GL_FUNCTIONS,    6, "" ) \
        ENTRY( SYS_ERR_FILE_OPEN_FAILED,        7, "" ) \
        ENTRY( SYS_ERR_MEMORY_BUFFER_TOO_SMALL, 8, "" )

#define SYS_ERR_TO_TABLE_ID(e) (-(e)-1)
enum
Sys_Err_Codes : u8
{
	SYS_ERR_TABLE( SYS_ERR_EXPAND_AS_ENUM )
	// ---
	SYS_ERR_COUNT_PLUS_ONE
};

s64
get_file_size( char * file_path );

s64
read_entire_file( char * file_path, void * mem, s64 mem_size );

// NOTE(theGiallo): virtual memory
#if LINUX
#define sys_mem_reserve( size_to_reserve ) mmap( NULL, ( size_to_reserve ), PROT_NONE, MAP_PRIVATE | MAP_ANON, -1, 0 )
#define sys_mem_commit( ptr, size ) ( mprotect( ( ptr ), ( size ), PROT_READ | PROT_WRITE ) == 0 )
#define sys_mem_uncommit( ptr, size ) ( mprotect( ( ptr ), ( size ), PROT_NONE ) == 0 )
#define sys_page_size() getpagesize()
#define sys_mem_free( ptr, size ) munmap( ptr, size )
#elif WINDOWS
#define sys_mem_reserve( size_to_reserve ) VirtualAlloc( NULL, ( size_to_reserve ), MEM_RESERVE, PAGE_NOACCESS )
inline
bool
sys_mem_commit( ptr, size )
{
	bool ret;
	DWORD old_prot;
	ret = VirtualProtect( ( ptr ), ( size ), PAGE_READWRITE, &old_prot );
	return ret;
}
inline
bool
sys_mem_uncommit( ptr, size )
{
	bool ret;
	DWORD old_prot;
	ret = VirtualProtect( ( ptr ), ( size ), PAGE_NOACCESS, &old_prot );
	return ret;
}
inline
s32
sys_page_size()
{
	s32 ret;

	SYSTEM_INFO system_info;
	GetSystemInfo( &system_info );
	ret = system_info.dwPageSize;

	return ret;
}
#define sys_mem_free( ptr, size ) VirtualFree( ptr, 0, MEM_RELEASE )
#endif
struct
Proc_Mem_Info
{
	u64 bytes_virtual;
	u64 bytes_physical;
};
Proc_Mem_Info
sys_get_proc_mem_info();

// NOTE(theGiallo): timings
#if LINUX
#include <time.h>
inline
u64
ns_time()
{
	u64 ret = {};

	timespec t;
	s64 r = clock_gettime( CLOCK_PROCESS_CPUTIME_ID, &t );
	if ( r == -1 )
	{
		log_err( "error: clock_gettime( CLOCK_PROCESS_CPUTIME_ID, &t ) failed" );
	} else
	{
		ret = t.tv_sec * (u64)1e9 + t.tv_nsec;
	}

	return ret;
}
#elif WINDOWS
inline
u64
ns_time()
{
	u64 ret = {};

	// NOTE(theGiallo): https://msdn.microsoft.com/en-us/library/windows/desktop/dn553408(v=vs.85).aspx
	static LARGE_INTEGER Frequency =
	   ( QueryPerformanceFrequency( &Frequency ), Frequency );
	LARGE_INTEGER LLTime;

	QueryPerformanceCounter( &LLTime );

	ret = ( ElapsedMicroseconds.QuadPart * 1e9 ) / Frequency.QuadPart;

	return ret;
}
#endif

#endif /* ifndef __SYSTEM_H__ */
