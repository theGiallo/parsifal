#ifndef __MACRO_TOOLS_H__
#define __MACRO_TOOLS_H__ 1

#ifndef APP_NAME
#define APP_NAME "parsifal"
#endif

#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)
#define HERE() __FILE__ ":" TOSTRING(__LINE__)
#define HERE_COLOR() CNSL_PURPLE " @" CNSL_CYAN __FILE__ CNSL_GREEN ":" CNSL_WHITE TOSTRING(__LINE__) CNSL_RESET
#define FMT_PTR "0x%016lx"

#ifdef NO_CONSOLE_COLORS
	#define CNSL_RESET
	#define CNSL_BLACK
	#define CNSL_RED
	#define CNSL_GREEN
	#define CNSL_YELLOW
	#define CNSL_BLUE
	#define CNSL_PURPLE
	#define CNSL_CYAN
	#define CNSL_WHITE
#else
	#define CNSL_RESET  "\033[00m"
	#define CNSL_BLACK  "\033[0;30m"
	#define CNSL_RED    "\033[0;31m"
	#define CNSL_GREEN  "\033[0;32m"
	#define CNSL_YELLOW "\033[0;33m"
	#define CNSL_BLUE   "\033[0;34m"
	#define CNSL_PURPLE "\033[0;35m"
	#define CNSL_CYAN   "\033[0;36m"
	#define CNSL_WHITE  "\033[0;37m"
#endif

#ifndef PERR
	#if defined(ANDROID) || defined(__ANDROID__)
		#define PERR(f, ...) __android_log_print( ANDROID_LOG_ERROR, APP_NAME,\
		                                              f, ##__VA_ARGS__ )
	#else
		#include <stdio.h>
		#define PERR(f, ...) fprintf( stderr, "[" APP_NAME "] " f "\n",\
		                                  ##__VA_ARGS__ )
	#endif
#endif
#ifndef PINFO
	#if defined(ANDROID) || defined(__ANDROID__)
		#define PINFO(f, ...) __android_log_print( ANDROID_LOG_DEBUG, APP_NAME,\
		                                               f, ##__VA_ARGS__ )
	#else
		#include <stdio.h>
		#define PINFO(f, ...) printf( "[" APP_NAME "] " f "\n", ##__VA_ARGS__ )
	#endif
#endif
#ifndef PDBG
	#if defined(ANDROID) || defined(__ANDROID__)
		#define PINFO(f, ...) __android_log_print( ANDROID_LOG_DEBUG, APP_NAME,\
		                                               f, ##__VA_ARGS__ )
	#else
		#include <stdio.h>
		#define PDBG(f, ...) printf( "[" APP_NAME "] " f "\n", ##__VA_ARGS__ )
	#endif
#endif

#if DEBUG
	#ifndef BREAK
		#if defined(__GNUC__) || defined(__GNUG__) || defined(__clang__)
			#if defined(__i386__) || defined(__x86_64__)
				// NOTE(theGiallo): the repeated INT instruction is a hack to make
				// gdb give the correct line # with gcc/g++ (clang works with only 1)
				#if defined(__clang__)
					#define BREAK() __asm__ volatile( "int $0x03" )
				#else
					#define BREAK() __asm__ volatile( "int $0x03;int $0x03" )
				#endif
			#elif defined(__thumb__)
				#define BREAK() __asm__ volatile( ".inst 0xde01" )
			#elif defined(__arm__) && !defined(__thumb__)
				#define BREAK() __asm__ volatile( ".inst 0xe7f001f0" )
			#elif defined(__aarch64__)
				#define BREAK() __asm__ volatile( ".inst 0xd4200000" )
			#elif defined(_WIN32)
				#define BREAK() __builtin_trap()
			#else
				#include <signal.h>
				#define BREAK() raise(SIGTRAP)
			#endif
		#elif defined(_MSC_VER)
			#include <intrin.h>
			#define BREAK() __debugbreak()
		#else
			#include <stdlib.h>
			#define BREAK() abort()
		#endif
	#endif

	#ifndef tg_assert
	#define tg_assert(e) \
	   if (e) (void) 0; else { \
	   log_err( "Failed assert: '" "%s" \
	   "'\n",  TOSTRING(e) ); \
	   BREAK(); }
	#endif
	#ifndef tg_assert_m
	#define tg_assert_m(e, fmt, ...) \
	   if (e) (void) 0; else { \
	   log_err( "Failed assert: '" "%s" \
	   "'\nMessage:\n" fmt, TOSTRING(e), ##__VA_ARGS__ ); \
	   BREAK(); }
	#endif

	#ifndef ILLEGAL_PATH
	#define ILLEGAL_PATH() \
	                   log_err( "Illegal path reached" ); BREAK()
	#endif
	#ifndef log_dbg
	#define log_dbg( f, ... ) PDBG( CNSL_YELLOW "[DEBUG] " f HERE_COLOR(), ##__VA_ARGS__ )
	#endif
#else
	#ifndef tg_assert
	#define tg_assert(e, ...) ((void) 0)
	#endif
	#ifndef tg_assert_m
	#define tg_assert_m(e, fmt, ...) ((void) 0)
	#endif
	#ifndef ILLEGAL_PATH
	#define ILLEGAL_PATH() ((void) 0)
	#endif
	#ifndef log_dbg
	#define log_dbg( f, ... ) ((void) 0)
	#endif
#endif
#ifndef log_err

#define log_err( f, ... ) PERR( CNSL_RED "[ERROR] " f\
                                          HERE_COLOR(),\
                                          ##__VA_ARGS__ )
#endif
#ifndef log_info
#define log_info( f, ... ) PINFO( "[INFO ] " f, ##__VA_ARGS__ )
#endif

#include <errno.h>
#define PERROR_CLEAN(...) do{perror(__VA_ARGS__); errno=0;}while(false)

#define COPY_STRUCT(dst_p, src_p) memcpy( dst_p, src_p, sizeof(*(src_p)) )



/* NOTE(theGiallo): example of usage:
	u8 moved[BITNSLOTS( 0xFFFF )] = ZERO_STRUCT;
	BITSET( moved, 10 )
	tg_assert( BITTEST( moved, 10 ), "BITSET is not working!" );
	BITCLEAR( moved, 10 )
	tg_assert( !BITTEST( moved, 10 ), "BITCLEAR is not working!" );
*/
#if NO_BITVECTOR
#define BITSET(a, b) (a)[b] = true
#define BITCLEAR(a, b) (a)[b] = false
#define BITTEST(a, b) (a)[b]
#define BITNSLOTS(nb) (nb)
#else
#ifndef CHAR_BIT
	#ifdef CHAR_BIT
		#define CHAR_BIT CHAR_BIT
	#else
		#if NO_LIMITS_H
			// NOTE(theGiallo): this is not safe, maybe is better to disable
			// bitvectors if you can't be sure.
			#define CHAR_BIT ( 8 * sizeof( u8 ) )
		#else
			#include <limits.h>
		#endif
	#endif
#endif

#define BITMASK(b) (1 << ((b) % CHAR_BIT))
#define BITSLOT(b) ((b) / CHAR_BIT)
#define BITSET(a, b) ((a)[BITSLOT(b)] |= BITMASK(b))
#define BITCLEAR(a, b) ((a)[BITSLOT(b)] &= ~BITMASK(b))
#define BITTEST(a, b) ((a)[BITSLOT(b)] & BITMASK(b))
#define BITNSLOTS(nb) ((nb + CHAR_BIT - 1) / CHAR_BIT)
#endif

#define MIN(a,b) ((a)<(b)?(a):(b))
#define MAX(a,b) ((a)<(b)?(a):(b))

#endif
