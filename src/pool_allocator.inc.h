/*

Copyright (c) <2017> <Gianluca Alloisio>

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgement in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.

*/
/*
   NOTE(theGiallo): Pool Allocator 2017-10-09
 */


#define PA_GLUE2(a,b) a##b
#define PA_GLUE2M(a,b) PA_GLUE2(a,b)
#define PA_GLUE2MM(a,b) PA_GLUE2M(a,b)
#define PA_GLUE3(a,b,c) a##b##c
#define PA_GLUE3M(a,b,c) PA_GLUE3(a,b,c)

#ifndef PA_TYPE
#error  You have to define PA_TYPE and optionally PA_SHORT_TYPE before \
including list.inc.h
#define PA_TYPE int
#endif

#ifdef PA_STATIC
#define PA_DEF static
#else
#define PA_DEF extern
#endif

#ifndef PA_TYPE_SHORT
#define PA_TYPE_SHORT PA_TYPE
#endif

#define PA_FULPA_TYPE PA_GLUE2M(PA_TYPE,_Pool_Allocator)
#define PA_SHORT_TYPE PA_GLUE2M(PA_TYPE_SHORT,_PA)
#define PA_NODE_TYPE PA_GLUE2MM(PA_FULPA_TYPE,_Node)
#define PA_NODE_SHORT_TYPE PA_GLUE2MM(PA_SHORT_TYPE,_Node)
#define PA_NODE_TYPE_ST PA_GLUE2MM(PA_FULPA_TYPE,_Node_st)

#if ! PA_BODY_ONLY

typedef
struct
PA_NODE_TYPE_ST
{
	PA_SIZE_T id_next;
} PA_NODE_TYPE, PA_NODE_SHORT_TYPE;
typedef
struct
PA_GLUE2M(PA_FULPA_TYPE,_st)
{
	PA_NODE_TYPE * first_free;

} PA_FULPA_TYPE, PA_SHORT_TYPE;

PA_DEF
pool_allocator_init(
   //  TODO(theGiallo, 2017-10-09): unfinished

#endif
