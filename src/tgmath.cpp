#include "tgmath.h"

u64
xorshift128plus( u64 seeds[2] )
{
	u64 x       = seeds[0];
	u64 const y = seeds[1];
	seeds[0] = y;
	x ^= x << 23; // a
	x ^= x >> 17; // b
	x ^= y ^ (y >> 26); // c
	seeds[1] = x;
	return x + y;
}
