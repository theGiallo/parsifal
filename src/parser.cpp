#include "parser.h"
#include "utility.h"
#include "macro_tools.h"
#include "utf8.h"
#include <stdlib.h>
#include <string.h>

bool
ucs4_is_alpha( u32 ucs4 );
bool
ucs4_is_num  ( u32 ucs4 );
bool
ucs4_is_blank( u32 ucs4 );
bool
ucs4_is_accepted_symbol( u32 ucs4 );

internal
inline
bool
text_element_node_is_symbol_char( Text_Element_List_VM_Node * node, u8 c )
{
	bool ret =
	      node
	   && node->element.type == Text_Element_Type::SYMBOL
	   && node->element.text.text[0] == c;
	if ( !ret )
	{
		if ( !node )
		{
			log_dbg( "node is null" );
		} else
		if ( node->element.type != Text_Element_Type::SYMBOL )
		{
			log_dbg( "node is not SYMBOL but %s", text_element_type_strings[(u32)node->element.type] );
			put_bytes( node->element.text.text, node->element.text.length );
			putchar( '\n' );
		} else
		{
			log_dbg( "node is '%c' and not '%c'", node->element.text.text[0], c );
		}
	} else
	{
		log_dbg( "matched '%c'", c );
	}

	return ret;
}

internal
inline
bool
text_element_node_is_word( Text_Element_List_VM_Node * node, Text_Piece text_piece )
{
	bool ret =
	      node
	   && node->element.type == Text_Element_Type::WORD
	   && text_piece_equals( &node->element.text, &text_piece );
	return ret;
}

internal
Text_Element_List_VM_Node *
find_node_end_of_line( Text_Element_List_VM_Node * from )
{
	Text_Element_List_VM_Node * ret = 0;
	for ( Text_Element_List_VM_Node * node = from;
	      node;
	      node = node->next )
	{
		if ( node->element.type == Text_Element_Type::LINE_FEED )
		{
			ret = node;
			break;
		}
	}

	return ret;
}
internal
Text_Element_List_VM_Node *
find_node_symbol_char( Text_Element_List_VM_Node * from, u8 c )
{
	Text_Element_List_VM_Node * ret = 0;
	for ( Text_Element_List_VM_Node * node = from;
	      node;
	      node = node->next )
	{
		//#if DEBUG
		//printf( "node = '" );
		//put_bytes( node->element.text.text, node->element.text.length );
		//printf( "'\n" );
		//#endif
		if ( text_element_node_is_symbol_char( node, c ) )
		{
			ret = node;
			break;
		}
	}

	return ret;
}

internal
s64
total_text_length_until( Text_Element_List_VM_Node * from, Text_Element_List_VM_Node * until )
{
	s64 ret = 0;

	for ( Text_Element_List_VM_Node * node = from;
	      node && node != until;
	      node = node->next )
	{
		ret += node->element.text.length;
	}

	return ret;
}

internal
void
copy_text_into_blob_until( Text_Element_List_VM_Node * from,
                           Text_Element_List_VM_Node * until,
                           u8 *target_text )
{
	for ( Text_Element_List_VM_Node * node = from;
	      node && node != until;
	      node = node->next )
	{
		//#if DEBUG
		//printf( "node: '" );
		//#endif
		for ( u32 i = 0; i != node->element.text.length; ++i )
		{
			*target_text = node->element.text.text[i];

			//#if DEBUG
			//put_bytes( target_text, 1 );
			//#endif

			++target_text;
		}
		//#if DEBUG
		//printf( "'\n" );
		//#endif
	}
}

internal
s64
total_text_length( Text_Element_List_VM_Node * from, Text_Element_List_VM_Node * to )
{
	s64 ret = 0;

	for ( Text_Element_List_VM_Node * node = from;
	      node;
	      node = node == to ? NULL : node->next )
	{
		ret += node->element.text.length;
	}

	return ret;
}

internal
void
copy_text_into_blob( Text_Element_List_VM_Node * from,
                     Text_Element_List_VM_Node * to,
                     u8 *target_text )
{
	for ( Text_Element_List_VM_Node * node = from;
	      node;
	      node = ( node == to ? NULL : node->next ) )
	{
		//#if DEBUG
		//printf( "node: '" );
		//#endif
		for ( u32 i = 0; i != node->element.text.length; ++i )
		{
			*target_text = node->element.text.text[i];

			//#if DEBUG
			//put_bytes( comment_text, 1 );
			//#endif

			++target_text;
		}
		//#if DEBUG
		//printf( "'\n" );
		//#endif
	}
}

internal
inline
AST_Node *
ast_new_node( AST * ast )
{
	AST_Node * ret =
	   ( AST_Node * ) pool_allocator_generic_vm_allocate_clean( &ast->nodes_allocator );
	list_vm_init( &ret->text_element_nodes, &ast->text_elements_allocator );
	list_vm_init( &ret->children, &ast->nodes_p_allocator );
	return ret;
}

#if 0
internal
inline
AST_Node *
skip( Text_Element_List_VM_Node * node, Text_Element_Type t )
{
	if ( node && node->type == Text_Element_Type::BLANKS )
	{
		return node->next;
	}
	return node;
}
#endif


////////////////////////////////////////////////////////////////////////////////
// internals

struct
AST_Eating_Context
{
	AST * ast;
	struct
	Partial_Context
	{
		Text_Element_List_VM_Node * current_node;
		AST_Node_p_List_VM eaten_ast_nodes;
		bool is_or;
	};
	bool just_failed;
	Mem_Stack partial_contexts_stack;
	struct
	File
	{
		u8 name[256*4];
		u8 * relative_path;
		u64 bytes_size;
		u32 lines_count;
	};
	File file;
};

struct
AST_Eat_Flags
{
	u32 variable_declaration_semicolon_is_optional : 1;
	u32 expression_semicolon_needed : 1;
	u32 expression_column_after : 1;
	u32 expression_square_br_after : 1;
	u32 name_allow_keywords : 1;
	u32 if_while_else_disallow_label : 1;
	u32 struct_body_allow_anonymous_structs : 1;
	u32 type_allow_only_typenames : 1;
};

struct
Matcher_Context
{
	Text_Element_List_VM_Node * start_node;
	Text_Element_List_VM_Node * current_node;
	bool match_started;
};

internal
inline
bool
match( Matcher_Context * ctx, u8 symbol )
{
	//if ( !ctx->match_started )
	//{
	//    ctx->start_node = ctx->current_node;
	//    ctx->match_started = true;
	//}
	if ( !ctx->match_started )
	{
		return false;
	}

	if ( text_element_node_is_symbol_char( ctx->current_node, symbol ) )
	{
		ctx->current_node = ctx->current_node->next;
		return true;
	}

	ctx->current_node = ctx->start_node;
	return false;
}

internal
inline
bool
match_word( Matcher_Context * ctx, const char * str )
{
	//if ( !ctx->match_started )
	//{
	//    ctx->start_node = ctx->current_node;
	//    ctx->match_started = true;
	//}
	if ( !ctx->match_started )
	{
		return false;
	}

	if ( text_element_node_is_word( ctx->current_node, { (u8*)str, str_bytes_to_null( str ) } ) )
	{
		ctx->current_node = ctx->current_node->next;
		return true;
	}

	ctx->current_node = ctx->start_node;
	return false;
}

internal
inline
bool
opt_bk( Matcher_Context * ctx )
{
	//if ( !ctx->match_started )
	//{
	//    ctx->start_node = ctx->current_node;
	//    ctx->match_started = true;
	//}
	if ( !ctx->match_started )
	{
		return false;
	}

	while ( ctx->current_node
	     && (    ctx->current_node->element.type == Text_Element_Type::BLANKS
	          || ctx->current_node->element.type == Text_Element_Type::LINE_FEED ) )
	{
		ctx->current_node = ctx->current_node->next;
	}

	return true;
}

internal
inline
bool
match( Matcher_Context * ctx, Text_Piece text_piece )
{
	// TODO(theGiallo, 2017-10-20): should this check be removed and start_match used?
	//if ( !ctx->match_started )
	//{
	//    ctx->start_node = ctx->current_node;
	//    ctx->match_started = true;
	//}
	if ( !ctx->match_started )
	{
		return false;
	}

	if ( text_element_node_is_word( ctx->current_node, text_piece ) )
	{
		ctx->current_node = ctx->current_node->next;
		return true;
	}

	ctx->current_node = ctx->start_node;
	return false;
}

internal
inline
bool
start_match( Matcher_Context * ctx )
{
	//if ( ctx->match_started )
	//{
	//    return false;
	//}

	ctx->match_started = true;
	ctx->start_node = ctx->current_node;
	return true;
}

internal
inline
bool
end_match( Matcher_Context * ctx )
{
	if ( !ctx->match_started )
	{
		return false;
	}

	ctx->start_node = ctx->current_node;
	ctx->match_started = false;
	return true;
}

internal
inline
Text_Element
ast_clone_text_element( AST * ast, Text_Element_List_VM_Node * node, u32 cut_length = 0 )
{
	Text_Element ret;

	tg_assert( ast );
	tg_assert( node );

	Text_Element * e = & node->element;

	u32  length = cut_length ? MIN( cut_length, e->text.length ) : e->text.length;
	u8 * text =
	   (u8*) freelist_allocator_allocate( &ast->text_allocator,
	                                      length );
	tg_assert( text );
	memcpy( text, e->text.text, length );
	ret.type        = e->type;
	ret.text.length = length;
	ret.text.text   = text;
	ret.in_file_pos = e->in_file_pos;

	return ret;
}

internal
inline
Text_Element
ast_merge_text_elements_until( AST * ast, Text_Element_List_VM_Node * from, Text_Element_List_VM_Node * until )
{
	Text_Element ret;

	tg_assert( ast );
	tg_assert( from );

	u32  text_length = total_text_length_until( from, until );
	u8 * text =
	   (u8*) freelist_allocator_allocate( &ast->text_allocator,
	                                      text_length );
	tg_assert( text );
	copy_text_into_blob_until( from, until, text );
	ret.type        = Text_Element_Type::BLOB;
	ret.text.length = text_length;
	ret.text.text   = text;
	ret.in_file_pos = from->element.in_file_pos;

	return ret;
}

internal
inline
AST_Eating_Context::Partial_Context *
current_pctx( AST_Eating_Context * ctx )
{
	Mem_Stack * pcs = &ctx->partial_contexts_stack;
	AST_Eating_Context::Partial_Context * ret =
	   (AST_Eating_Context::Partial_Context*)
	   ( pcs->mem_buf + ( pcs->first_free - sizeof( AST_Eating_Context::Partial_Context ) ) );
	return ret;
}
internal
inline
AST_Eating_Context::Partial_Context *
parent_pctx( AST_Eating_Context * ctx )
{
	Mem_Stack * pcs = &ctx->partial_contexts_stack;
	size_t parent_loc = pcs->first_free - 2 * sizeof( AST_Eating_Context::Partial_Context );
	AST_Eating_Context::Partial_Context * ret =
	   parent_loc == 0 ? 0 :
	   (AST_Eating_Context::Partial_Context*)
	   ( pcs->mem_buf + parent_loc );
	return ret;
}
internal
inline
AST_Eating_Context::Partial_Context *
parent_pctx( AST_Eating_Context::Partial_Context * pctx )
{
	AST_Eating_Context::Partial_Context * ret = ( pctx - 1 );
	return ret;
}
internal
void
restart_pctx( AST_Eating_Context::Partial_Context * pctx )
{
	pctx->current_node = parent_pctx( pctx )->current_node;
}
internal
s32
pctx_level( AST_Eating_Context * ctx )
{
	Mem_Stack * pcs = &ctx->partial_contexts_stack;
	size_t s = sizeof( AST_Eating_Context::Partial_Context );
	tg_assert( pcs->first_free % s == 0 );
	s32 ret = pcs->first_free / s;
	return ret;
}

internal
inline
AST_Eating_Context::Partial_Context *
last_eaten_pctx( AST_Eating_Context * ctx )
{
	Mem_Stack * pcs = &ctx->partial_contexts_stack;
	AST_Eating_Context::Partial_Context * ret =
	   (AST_Eating_Context::Partial_Context*)
	   ( pcs->mem_buf + pcs->first_free );
	return ret;
}

internal
inline
bool
ast_node_child_is_identifier( AST_Node * node )
{
	bool ret = false;
	tg_assert( node );
	tg_assert( node->children.first );
	ret =
	(    node->children.first->element->type == AST_Node_Type::NAME
	  || (    node->children.first->element->type == AST_Node_Type::TYPE
	       && node->children.first->element->children.first->element->type == AST_Node_Type::NAME ) );
	return ret;
}

internal
bool
ast_eat_start( AST_Eating_Context * ctx )
{
	{
		char indent[512] = {};
		for ( s32 i = 0, il = pctx_level( ctx ) * 4; i != il; ++i )
		{
			indent[i] = ' ';
		}
		log_dbg( "%s%s current_pctx( ctx ) = " FMT_PTR, indent, __PRETTY_FUNCTION__, (size_t)current_pctx( ctx ) );
	}
	bool ret = true;

	AST_Eating_Context::Partial_Context * parent = current_pctx( ctx );
	AST_Eating_Context::Partial_Context * pc =
	   (AST_Eating_Context::Partial_Context*)
	   ctx->partial_contexts_stack.push( sizeof( AST_Eating_Context::Partial_Context ) );
	tg_assert( pc );
	if ( !pc )
	{
		// TODO(theGiallo, 2017-10-20): set error
		ret = false;
	}
	*pc = {};

	pc->current_node = parent->current_node;
	pc->eaten_ast_nodes = {};
	list_vm_init( &pc->eaten_ast_nodes, &ctx->ast->nodes_p_allocator );

	ctx->just_failed = false;

	return ret;
}

internal
bool
ast_eat_end( AST_Eating_Context * ctx )
{
	{
		char indent[512] = {};
		for ( s32 i = 0, il = pctx_level( ctx ) * 4; i != il; ++i )
		{
			indent[i] = ' ';
		}
		log_dbg( "%s%s current_pctx( ctx ) = " FMT_PTR, indent, __PRETTY_FUNCTION__, (size_t)current_pctx( ctx ) );
	}
	auto ocn = current_pctx( ctx )->current_node;

	ctx->partial_contexts_stack.first_free -= sizeof( AST_Eating_Context::Partial_Context );

	// NOTE(theGiallo): everywhere in the code we take the last pc and update
	// the pc current node manually.
	// I'm disabling this so I can do temporary checks without saving the pc
	// old value.
	// TODO(theGiallo, 2017-12-06): Maybe it's more convenient to so it here and
	// remove the manual update around.
	#if 0
	if ( !ctx->just_failed )
	{
		current_pctx( ctx )->current_node = ocn;
	}
	#endif
	return true;
}

internal
bool
ast_eat_or_pre( AST_Eating_Context * ctx )
{
	{
		char indent[512] = {};
		for ( s32 i = 0, il = pctx_level( ctx ) * 4; i != il; ++i )
		{
			indent[i] = ' ';
		}
		log_dbg( "%s%s current_pctx( ctx ) = " FMT_PTR, indent, __PRETTY_FUNCTION__, (size_t)current_pctx( ctx ) );
	}
	ast_eat_start( ctx );
	current_pctx( ctx )->is_or = true;
	{
		char indent[512] = {};
		for ( s32 i = 0, il = pctx_level( ctx ) * 4; i != il; ++i )
		{
			indent[i] = ' ';
		}
		log_dbg( "%s%s current_pctx( ctx ) = " FMT_PTR, indent, __PRETTY_FUNCTION__, (size_t)current_pctx( ctx ) );
	}
	return true;
}
internal
bool
ast_eat_or_end( AST_Eating_Context * ctx )
{
	{
		char indent[512] = {};
		for ( s32 i = 0, il = pctx_level( ctx ) * 4; i != il; ++i )
		{
			indent[i] = ' ';
		}
		log_dbg( "%s%s current_pctx( ctx ) = " FMT_PTR, indent, __PRETTY_FUNCTION__, (size_t)current_pctx( ctx ) );
	}
	tg_assert( current_pctx( ctx )->is_or );
	ctx->just_failed = true;
	ast_eat_end( ctx );
	ctx->just_failed = true;
	ast_eat_end( ctx );
	{
		char indent[512] = {};
		for ( s32 i = 0, il = pctx_level( ctx ) * 4; i != il; ++i )
		{
			indent[i] = ' ';
		}
		log_dbg( "%s%s current_pctx( ctx ) = " FMT_PTR, indent, __PRETTY_FUNCTION__, (size_t)current_pctx( ctx ) );
	}
	return false;
}
internal
bool
ast_eat_or_post( AST_Eating_Context * ctx )
{
	{
		char indent[512] = {};
		for ( s32 i = 0, il = pctx_level( ctx ) * 4; i < il; ++i )
		{
			indent[i] = ' ';
		}
		log_dbg( "%s%s current_pctx( ctx ) = " FMT_PTR, indent, __PRETTY_FUNCTION__, (size_t)current_pctx( ctx ) );
	}
	tg_assert( !ctx->just_failed );
	tg_assert( current_pctx( ctx )->is_or );
	ast_eat_end( ctx );
	{
		char indent[512] = {};
		for ( s32 i = 0, il = pctx_level( ctx ) * 4; i != il; ++i )
		{
			indent[i] = ' ';
		}
		log_dbg( "%s%s current_pctx( ctx ) = " FMT_PTR, indent, __PRETTY_FUNCTION__, (size_t)current_pctx( ctx ) );
	}

	AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );
	AST_Eating_Context::Partial_Context *  pc =    current_pctx( ctx );

	pc->current_node = lpc->current_node;

	list_vm_move_list_last( &pc->eaten_ast_nodes, &lpc->eaten_ast_nodes );

	return true;
}

internal
bool
ast_eat( AST_Eating_Context * ctx, AST_Node_Type type, AST_Eat_Flags flags = {} )
{
	bool ret = false;

	char indent[1024] = {};
	for ( s32 i = 0, il = pctx_level( ctx ) * 4; i < il; ++i )
	{
		indent[i] = ' ';
	}
	log_dbg( "%strying to parse %s", indent, ast_node_type_strings[(u32)type] );

	if ( ctx->just_failed )
	{
		ctx->just_failed = false;
	}

	AST_Eating_Context::Partial_Context * pc = current_pctx( ctx );
	//Text_Element_List_VM_Node * old_current_node = pc->current_node;

	File_Coordinate file_coord_last_start = pc->current_node->element.in_file_pos;
#define UPDATE_LAST_FILE_POS() \
	do                                                                          \
	{                                                                           \
		if ( pc->current_node )                                                 \
		{                                                                       \
			file_coord_last_start = pc->current_node->element.in_file_pos;      \
		} else                                                                  \
		{                                                                       \
			file_coord_last_start.line = ctx->file.lines_count;                 \
			file_coord_last_start.utf8_chars_in_line = 0;                       \
			file_coord_last_start.bytes_from_file_start = ctx->file.bytes_size; \
		}                                                                       \
	} while ( false )                                                           \

#define TEST() \
	if ( a )          \
	{                 \
		for(;;)       \
		{             \
			foo( b ); \
		}             \
	}                 \

	switch ( type )
	{
		case AST_Node_Type::NAME:
		{
			if ( !pc->current_node )
			{
				break;
			}

			Matcher_Context m = {};
			m.current_node = pc->current_node;
			if (   start_match( &m )
			    && opt_bk( &m )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
			}

			UPDATE_LAST_FILE_POS();

			log_dbg( "%sgoing to check if this is a literal and not a name", indent );
			put_bytes( (u8*)indent, str_bytes_to_null( indent ) );
			put_bytes( pc->current_node->element.text.text,
			           pc->current_node->element.text.length );
			putchar( '\n' );
			if ( (    ast_eat_start( ctx )
			       && ast_eat( ctx, AST_Node_Type::LITERAL_REAL )
			       && ast_eat_end( ctx )
			     )
			  || (    ast_eat_start( ctx )
			       && ast_eat( ctx, AST_Node_Type::LITERAL_INTEGER )
			       && ast_eat_end( ctx )
			     )
			  )
			{
				log_dbg( "%sthis is a literal not a name", indent );
				put_bytes( (u8*)indent, str_bytes_to_null( indent ) );
				put_bytes( pc->current_node->element.text.text,
				           pc->current_node->element.text.length );
				putchar( '\n' );

				AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );
				tg_assert( pc != lpc );
				tg_assert( pc->current_node != lpc->current_node );

				break;
			}

			if ( pc->current_node->element.type != Text_Element_Type::WORD )
			{
				log_dbg( "%sexpecting WORD, found %s", indent, text_element_type_strings[(u32)pc->current_node->element.type] );
				put_bytes( (u8*)indent, str_bytes_to_null( indent ) );
				put_bytes( pc->current_node->element.text.text,
				           pc->current_node->element.text.length );
				putchar( '\n' );
				break;
			}
			// TODO(theGiallo, 2017-11-06): exclude keywords and parse them separately?
			const char * keywords[] = {
				"return",
				"break",
				"continue",
				"switch",
				"val",
				"using",
				"bitfield",
				"union",
				"const",
			};
			const char * keyword = NULL;
			bool is_keyword = false;
			for ( s32 i = 0; i != ARRAY_COUNT( keywords ); ++i )
			{
				keyword = keywords[i];
				if ( string_equal( (char*)pc->current_node->element.text.text,
				                   pc->current_node->element.text.length,
				                   keyword ) )
				{
					is_keyword = true;
					break;
				}
			}
			if ( flags.name_allow_keywords == 0 && is_keyword )
			{
				log_dbg( "%sfound WORD but it's keyword '%s'", indent, keyword );
				break;
			}
			Text_Element text_element = ast_clone_text_element( ctx->ast, pc->current_node );
			log_dbg( "%sfound WORD is a valid %s", indent, ast_node_type_strings[(u32)type] );
			put_bytes( (u8*)indent, str_bytes_to_null( indent ) );
			put_bytes( text_element.text.text,
			           text_element.text.length );
			putchar( '\n' );
			AST_Node * ast_node = ast_new_node( ctx->ast );
			ast_node->type = type;
			list_vm_push_last( &ast_node->text_element_nodes, &text_element );
			list_vm_push_last( &pc->eaten_ast_nodes, &ast_node );
			pc->current_node = pc->current_node->next;
			ast_node->file_range.first = file_coord_last_start;
			UPDATE_LAST_FILE_POS();
			ast_node->file_range.last = file_coord_last_start;
			ret = true;
			break;
		}
		case AST_Node_Type::LITERAL_INTEGER:
		{
			// TODO(theGiallo, 2017-12-05): move this check out of switch?
			if ( !pc->current_node )
			{
				break;
			}

			Matcher_Context m = {};
			m.current_node = pc->current_node;
			if (   start_match( &m )
			    && opt_bk( &m )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
			}

			UPDATE_LAST_FILE_POS();

			if ( pc->current_node->element.type != Text_Element_Type::WORD )
			{
				log_dbg( "%sexpecting WORD, found %s", indent, text_element_type_strings[(u32)pc->current_node->element.type] );
				put_bytes( (u8*)indent, str_bytes_to_null( indent ) );
				put_bytes( pc->current_node->element.text.text,
				           pc->current_node->element.text.length );
				putchar( '\n' );
				break;
			}

			Literal_Type literal_type = Literal_Type::_COUNT;
			bool valid_literal = true;
			bool is_hexadecimal = false;
			for ( u32 i = 0, l = pc->current_node->element.text.length;
			      i != l;
			      ++i )
			{
				if ( !ascii_is_digit( pc->current_node->element.text.text[i] ) )
				{
					for ( s32 l_id = 0; l_id != u32(Literal_Type::_COUNT); ++l_id )
					{
						const char * literal = literal_types_strings[l_id];
						if ( string_equal( (char*)pc->current_node->element.text.text + i,
						                   pc->current_node->element.text.length - i,
						                   literal, str_bytes_to_null( literal ) ) )
						{
							literal_type = Literal_Type(l_id);
							log_dbg( "%sfound literal type %s", indent, literal );
							break;
						}
					}
					if ( literal_type != Literal_Type::_COUNT )
					{
						break;
					}
					if ( ascii_is_hexadecimal_digit( pc->current_node->element.text.text[i] ) )
					{
						is_hexadecimal = true;
					} else
					{
						valid_literal = false;
						break;
					}
				}
			}

			Text_Piece lt = pc->current_node->element.text;
			if ( literal_type != Literal_Type::_COUNT )
			{
				lt.length -= str_bytes_to_null( literal_types_strings[u32(literal_type)] );
				if ( !lt.length )
				{
					log_dbg( "%sthis name is only literal type, so probably is a NAME or a TYPE", indent );
					break;
				}
				if ( ! literal_type_is_integer( literal_type ) )
				{
					log_dbg( "%sliteral type is real not integer", indent );
					break;
				}
			}

			if ( is_hexadecimal && literal_type_is_hexadecimal( literal_type ) )
			{
				log_dbg( "hexadecimal has correct literal type" );
			} else
			if ( is_hexadecimal && !literal_type_is_hexadecimal( literal_type ) )
			{
				// TODO(theGiallo, 2017-12-05): print hexadecimal literal types
				log_dbg( "%shexadecimal literal has incorrect literal type", indent );
				break;
			} else
			if ( !valid_literal )
			{
				log_dbg( "%s WORD is not all digits or has an invalid literal type", indent );
				put_bytes( (u8*)indent, str_bytes_to_null( indent ) );
				put_bytes( pc->current_node->element.text.text,
				           pc->current_node->element.text.length );
				putchar( '\n' );
				break;
			} else
			if ( literal_type_is_octal( literal_type ) && !is_octal( lt ) )
			{
				log_dbg( "%sliteral has octal literal type but has not only octal digits", indent );
				break;
			} else
			if ( literal_type_is_binary( literal_type ) && !is_binary( lt ) )
			{
				log_dbg( "%sliteral has binary literal type but has not only binary digits", indent );
				break;
			}

			Text_Element text_element =
			   ast_clone_text_element( ctx->ast, pc->current_node, lt.length );
			log_dbg( "%sfound WORD is valid %s", indent, ast_node_type_strings[u32(type)] );
			put_bytes( (u8*)indent, str_bytes_to_null( indent ) );
			put_bytes( text_element.text.text,
			           text_element.text.length );
			putchar( '\n' );
			AST_Node * ast_node = ast_new_node( ctx->ast );
			ast_node->type = type;
			list_vm_push_last( &ast_node->text_element_nodes, &text_element );

			if ( literal_type != Literal_Type::_COUNT )
			{
				AST_Node * literal_type_node = ast_new_node( ctx->ast );
				literal_type_node->type = AST_Node_Type::LITERAL_TYPE;
				Text_Element te = { .text.text = (u8*)literal_types_strings[u32(literal_type)],
				                    .text.length = str_bytes_to_null( (u8*)literal_types_strings[u32(literal_type)] ),
				                    .type = Text_Element_Type::WORD };
				log_dbg( "%s literal type:", indent );
				put_bytes( (u8*)indent, str_bytes_to_null( indent ) );
				put_bytes( te.text.text,
				           te.text.length );
				putchar( '\n' );
				list_vm_push_last( &literal_type_node->text_element_nodes, &te );
				list_vm_push_last( &ast_node->children, &literal_type_node );
			}
			list_vm_push_last( &pc->eaten_ast_nodes, &ast_node );
			pc->current_node = pc->current_node->next;
			ast_node->file_range.first = file_coord_last_start;
			UPDATE_LAST_FILE_POS();
			ast_node->file_range.last = file_coord_last_start;
			ret = true;
			break;
		}
		case AST_Node_Type::LITERAL_REAL:
		{
			// TODO(theGiallo, 2017-12-05): move this check out of switch?
			if ( !pc->current_node )
			{
				break;
			}

			Matcher_Context m = {};
			m.current_node = pc->current_node;
			if (   start_match( &m )
			    && opt_bk( &m )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
			}

			UPDATE_LAST_FILE_POS();

			if ( pc->current_node->element.type != Text_Element_Type::WORD )
			{
				log_dbg( "%sexpecting WORD, found %s", indent, text_element_type_strings[(u32)pc->current_node->element.type] );
				put_bytes( (u8*)indent, str_bytes_to_null( indent ) );
				put_bytes( pc->current_node->element.text.text,
				           pc->current_node->element.text.length );
				putchar( '\n' );
				break;
			}

			// TODO(theGiallo, 2017-12-13): this code is shit, refactor it!

			Literal_Type literal_type = Literal_Type::_COUNT;
			bool valid_literal = true;
			bool scientific_notation = false;
			bool exponent = false;
			bool exponent_sign = false;
			for ( u32 i = 0, l = pc->current_node->element.text.length;
			      i != l;
			      ++i )
			{
				u8 c = pc->current_node->element.text.text[i];
				if ( !ascii_is_digit( c ) )
				{
					for ( s32 l_id = 0; l_id != u32(Literal_Type::_COUNT); ++l_id )
					{
						const char * literal = literal_types_strings[l_id];
						if ( string_equal( (char*)pc->current_node->element.text.text + i,
						                   pc->current_node->element.text.length - i,
						                   literal, str_bytes_to_null( literal ) ) )
						{
							literal_type = Literal_Type( l_id );
							log_dbg( "%sfound literal type %s", indent, literal );
							break;
						}
					}
					if ( literal_type == Literal_Type::_COUNT
					  && ( c == 'e' || c == 'E' ) )
					{
						if ( scientific_notation )
						{
							log_dbg( "%sscientific notation exponent marker already encountered", indent );
							valid_literal = false;
							break;
						} else
						{
							log_dbg( "%sfound scientific notation exponent marker", indent );
							scientific_notation = true;
							continue;
						}
					} else
					if ( ! literal_type_is_real( literal_type ) )
					{
						valid_literal = false;
						log_dbg( "%sliteral type is not real: %s", indent, literal_types_strings[u32(literal_type)] );
					}
					break;
				} else
				{
					if ( scientific_notation )
					{
						exponent = true;
					}
				}
			}

			if ( scientific_notation && !exponent && literal_type == Literal_Type::_COUNT )
			{
				m.current_node = pc->current_node = pc->current_node->next;
				if ( ! (    start_match( &m )
				         && match( &m, '-' )
				         && end_match( &m )
				       )
				   )
				{
					log_dbg( "%sno exponent sign found after scientific notation marker", indent );
					break;
				}
				m.current_node = pc->current_node = pc->current_node->next;

				exponent_sign = true;

				for ( u32 i = 0, l = pc->current_node->element.text.length;
				      i != l;
				      ++i )
				{
					u8 c = pc->current_node->element.text.text[i];
					if ( !ascii_is_digit( c ) )
					{
						for ( s32 l_id = 0; l_id != u32(Literal_Type::_COUNT); ++l_id )
						{
							const char * literal = literal_types_strings[l_id];
							if ( string_equal( (char*)pc->current_node->element.text.text + i,
							                   pc->current_node->element.text.length - i,
							                   literal, str_bytes_to_null( literal ) ) )
							{
								literal_type = Literal_Type( l_id );
								log_dbg( "%sfound literal type %s", indent, literal );
								break;
							}
						}
						if ( literal_type == Literal_Type::_COUNT
						  && ( c == 'e' || c == 'E' ) )
						{
							if ( scientific_notation )
							{
								log_dbg( "%sscientific notation exponent marker already encountered", indent );
								valid_literal = false;
								break;
							} else
							{
								ILLEGAL_PATH();
							}
						} else
						if ( literal_type == Literal_Type::_COUNT )
						{
							valid_literal = false;
							log_dbg( "%sliteral contains an invalid character ('%c'): %s", indent, c, literal_types_strings[u32(literal_type)] );
						} else
						if ( ! literal_type_is_real( literal_type ) )
						{
							valid_literal = false;
							log_dbg( "%sliteral type is not real: %s", indent, literal_types_strings[u32(literal_type)] );
						}
						break;
					} else
					{
						tg_assert( scientific_notation );
						exponent = true;
					}
				}
			}

			if ( !valid_literal )
			{
				log_dbg( "%s WORD is not all digits or has an invalid literal type", indent );
				put_bytes( (u8*)indent, str_bytes_to_null( indent ) );
				put_bytes( pc->current_node->element.text.text,
				           pc->current_node->element.text.length );
				putchar( '\n' );
				break;
			}

			Text_Element_List_VM_Node * starting_node = pc->current_node;

			if ( literal_type == Literal_Type::_COUNT && !exponent && !exponent_sign )
			{
				m.current_node = pc->current_node = pc->current_node->next;
				if ( ! (    start_match( &m )
				         && match( &m, '.' )
				         && end_match( &m )
				       )
				   )
				{
					log_dbg( "%sno literal type found and no dot found, so it's an integer", indent );
					break;
				}
				m.current_node = pc->current_node = pc->current_node->next;

				if ( pc->current_node->element.type != Text_Element_Type::WORD )
				{
					log_dbg( "%sexpecting WORD after dot, found %s", indent,
					         text_element_type_strings[(u32)pc->current_node->element.type] );
					put_bytes( (u8*)indent, str_bytes_to_null( indent ) );
					put_bytes( pc->current_node->element.text.text,
					           pc->current_node->element.text.length );
					putchar( '\n' );
					break;
				}

				for ( u32 i = 0, l = pc->current_node->element.text.length;
				      i != l;
				      ++i )
				{
					u8 c = pc->current_node->element.text.text[i];
					if ( !ascii_is_digit( c ) )
					{
						for ( s32 l_id = 0; l_id != u32(Literal_Type::_COUNT); ++l_id )
						{
							const char * literal = literal_types_strings[l_id];
							if ( string_equal( (char*)pc->current_node->element.text.text + i,
							                   pc->current_node->element.text.length - i,
							                   literal, str_bytes_to_null( literal ) ) )
							{
								literal_type = Literal_Type( l_id );
								log_dbg( "%sfound literal type %s", indent, literal );
								break;
							}
						}
						if ( literal_type == Literal_Type::_COUNT
						  && ( c == 'e' || c == 'E' ) )
						{
							if ( scientific_notation )
							{
								log_dbg( "%sscientific notation exponent marker already encountered", indent );
								valid_literal = false;
								break;
							} else
							{
								log_dbg( "%sfound scientific notation exponent marker", indent );
								scientific_notation = true;
								continue;
							}
						} else
						if ( literal_type == Literal_Type::_COUNT )
						{
							valid_literal = false;
							log_dbg( "%sliteral contains an invalid character ('%c'): %s", indent, c, literal_types_strings[u32(literal_type)] );
						} else
						if ( ! literal_type_is_real( literal_type ) )
						{
							valid_literal = false;
							log_dbg( "%sliteral type is not real: %s", indent, literal_types_strings[u32(literal_type)] );
						}
						break;
					} else
					{
						if ( scientific_notation )
						{
							exponent = true;
						}
					}
				}
			}

			if ( !valid_literal )
			{
				log_dbg( "%s WORD is not all digits or has an invalid literal type", indent );
				put_bytes( (u8*)indent, str_bytes_to_null( indent ) );
				put_bytes( pc->current_node->element.text.text,
				           pc->current_node->element.text.length );
				putchar( '\n' );
				break;
			}

			if ( scientific_notation && !exponent && literal_type == Literal_Type::_COUNT )
			{
				m.current_node = pc->current_node = pc->current_node->next;
				if ( ! (    start_match( &m )
				         && match( &m, '-' )
				         && end_match( &m )
				       )
				   )
				{
					log_dbg( "%sno exponent sign found after scientific notation marker", indent );
					break;
				}
				m.current_node = pc->current_node = pc->current_node->next;

				exponent_sign = true;

				for ( u32 i = 0, l = pc->current_node->element.text.length;
				      i != l;
				      ++i )
				{
					u8 c = pc->current_node->element.text.text[i];
					if ( !ascii_is_digit( c ) )
					{
						for ( s32 l_id = 0; l_id != u32(Literal_Type::_COUNT); ++l_id )
						{
							const char * literal = literal_types_strings[l_id];
							if ( string_equal( (char*)pc->current_node->element.text.text + i,
							                   pc->current_node->element.text.length - i,
							                   literal, str_bytes_to_null( literal ) ) )
							{
								literal_type = Literal_Type( l_id );
								log_dbg( "%sfound literal type %s", indent, literal );
								break;
							}
						}
						if ( literal_type == Literal_Type::_COUNT
						  && ( c == 'e' || c == 'E' ) )
						{
							if ( scientific_notation )
							{
								log_dbg( "%sscientific notation exponent marker already encountered", indent );
								valid_literal = false;
								break;
							} else
							{
								ILLEGAL_PATH();
							}
						} else
						if ( literal_type == Literal_Type::_COUNT )
						{
							valid_literal = false;
							log_dbg( "%sliteral contains an invalid character ('%c'): %s", indent, c, literal_types_strings[u32(literal_type)] );
						} else
						if ( ! literal_type_is_real( literal_type ) )
						{
							valid_literal = false;
							log_dbg( "%sliteral type is not real: %s", indent, literal_types_strings[u32(literal_type)] );
						}
						break;
					} else
					{
						tg_assert( scientific_notation );
						exponent = true;
					}
				}
			}

			if ( !valid_literal )
			{
				log_dbg( "%s WORD is not all digits or has an invalid literal type", indent );
				put_bytes( (u8*)indent, str_bytes_to_null( indent ) );
				put_bytes( pc->current_node->element.text.text,
				           pc->current_node->element.text.length );
				putchar( '\n' );
				break;
			}

			if ( scientific_notation && !exponent )
			{
				log_dbg( "%sfound scientific notation exponent marker but no exponent", indent );
				break;
			}

			Text_Element text_element =
			   ast_merge_text_elements_until( ctx->ast, starting_node, pc->current_node->next );
			log_dbg( "%sfound literal is valid %s", indent, ast_node_type_strings[u32(type)] );
			put_bytes( (u8*)indent, str_bytes_to_null( indent ) );
			put_bytes( text_element.text.text,
			           text_element.text.length );
			putchar( '\n' );
			if ( literal_type != Literal_Type::_COUNT )
			{
				text_element.text.length -= str_bytes_to_null( literal_types_strings[u32(literal_type)] );
				if ( !text_element.text.length )
				{
					log_dbg( "%sthis name is only literal type, so probably is a NAME or a TYPE", indent );
					break;
				}
			}

			AST_Node * ast_node = ast_new_node( ctx->ast );
			ast_node->type = type;
			list_vm_push_last( &ast_node->text_element_nodes, &text_element );

			if ( literal_type != Literal_Type::_COUNT )
			{
				AST_Node * literal_type_node = ast_new_node( ctx->ast );
				literal_type_node->type = AST_Node_Type::LITERAL_TYPE;
				Text_Element te = { .text.text = (u8*)literal_types_strings[u32(literal_type)],
				                    .text.length = str_bytes_to_null( (u8*)literal_types_strings[u32(literal_type)] ),
				                    .type = Text_Element_Type::WORD };
				log_dbg( "%s literal type:", indent );
				put_bytes( (u8*)indent, str_bytes_to_null( indent ) );
				put_bytes( te.text.text,
				           te.text.length );
				putchar( '\n' );
				list_vm_push_last( &literal_type_node->text_element_nodes, &te );
				list_vm_push_last( &ast_node->children, &literal_type_node );
			}
			list_vm_push_last( &pc->eaten_ast_nodes, &ast_node );
			pc->current_node = pc->current_node->next;
			ast_node->file_range.first = file_coord_last_start;
			UPDATE_LAST_FILE_POS();
			ast_node->file_range.last = file_coord_last_start;
			ret = true;
			break;
		}
		case AST_Node_Type::LITERAL_STRING:
		{
			// TODO(theGiallo, 2017-12-05): move this check out of switch?
			if ( !pc->current_node )
			{
				break;
			}

			Matcher_Context m = {};
			m.current_node = pc->current_node;
			if (   start_match( &m )
			    && opt_bk( &m )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
			}

			UPDATE_LAST_FILE_POS();

			if ( pc->current_node->element.type != Text_Element_Type::SYMBOL )
			{
				log_dbg( "%sexpecting SYMBOL, found %s", indent, text_element_type_strings[(u32)pc->current_node->element.type] );
				put_bytes( (u8*)indent, str_bytes_to_null( indent ) );
				put_bytes( pc->current_node->element.text.text,
				           pc->current_node->element.text.length );
				putchar( '\n' );
				break;
			}

			Text_Element_List_VM_Node * starting_node = pc->current_node;
			if ( ! ( start_match( &m )
			      && match( &m, '"' )
			      && end_match( &m )
			       )
			   )
			{
				log_dbg( "%sexpecting '\"'", indent );
				break;
			}
			bool terminated = false;
			while ( ! ( start_match( &m )
			         && match( &m, '"' )
			         && end_match( &m )
			         && ( terminated = true )
			          )
			      )
			{
				if (    start_match( &m )
				     && match( &m, '\\' )
				     && match( &m, '"' )
				     && end_match( &m )
				   )
				{
					continue;
				} else
				{
					m.current_node = m.current_node->next;
				}
			}
			if ( ! terminated )
			{
				log_dbg( "%sstring literal does not terminate, reached end of file", indent );
			}
			pc->current_node = m.current_node;

			Text_Element text_element =
			   ast_merge_text_elements_until( ctx->ast, starting_node, pc->current_node );
			log_dbg( "%sfound literal is valid %s", indent, ast_node_type_strings[u32(type)] );
			put_bytes( (u8*)indent, str_bytes_to_null( indent ) );
			put_bytes( text_element.text.text,
			           text_element.text.length );
			putchar( '\n' );

			AST_Node * ast_node = ast_new_node( ctx->ast );
			ast_node->type = type;
			list_vm_push_last( &ast_node->text_element_nodes, &text_element );

			list_vm_push_last( &pc->eaten_ast_nodes, &ast_node );
			//pc->current_node = pc->current_node->next;
			ast_node->file_range.first = file_coord_last_start;
			UPDATE_LAST_FILE_POS();
			ast_node->file_range.last = file_coord_last_start;
			ret = true;
			break;
		}
		case AST_Node_Type::LITERAL_UCS4:
		{
			// TODO(theGiallo, 2017-12-05): move this check out of switch?
			if ( !pc->current_node )
			{
				break;
			}

			Matcher_Context m = {};
			m.current_node = pc->current_node;
			if (   start_match( &m )
			    && opt_bk( &m )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
			}

			UPDATE_LAST_FILE_POS();

			if ( pc->current_node->element.type != Text_Element_Type::SYMBOL )
			{
				log_dbg( "%sexpecting SYMBOL, found %s", indent, text_element_type_strings[(u32)pc->current_node->element.type] );
				put_bytes( (u8*)indent, str_bytes_to_null( indent ) );
				put_bytes( pc->current_node->element.text.text,
				           pc->current_node->element.text.length );
				putchar( '\n' );
				break;
			}

			Text_Element_List_VM_Node * starting_node = pc->current_node;
			if ( ! ( start_match( &m )
			      && match( &m, '\'' )
			      && end_match( &m )
			       )
			   )
			{
				log_dbg( "%sexpecting '\\''", indent );
				break;
			}
			pc->current_node = m.current_node;
			log_dbg( "%sfound left '", indent );

			if (    start_match( &m )
			     && match( &m, '\\' )
			     && end_match( &m )
			   )
			{
				if ( (    start_match( &m )
				       && match( &m, '\'' )
				       && end_match( &m )
				     )
				  ||  (    start_match( &m )
				       && match( &m, '\\' )
				       && end_match( &m )
				     )
				  || ( m.current_node->element.type == Text_Element_Type::WORD
				    && m.current_node->element.text.length == 1
				    && (    m.current_node->element.text.text[0] == 'n'
				         || m.current_node->element.text.text[0] == 't'
				         || m.current_node->element.text.text[0] == 'r'
				         || m.current_node->element.text.text[0] == 'a'
				         || m.current_node->element.text.text[0] == 'b'
				         || m.current_node->element.text.text[0] == 'f'
				       )
				    && ( m.current_node = m.current_node->next, true )
				     )
				   )
				{
					log_dbg( "%sfound escaped char", indent );
					pc->current_node = m.current_node;
				} else
				{
					log_dbg( "%sescaped sequence is not a valid char", indent );
					break;
				}
			} else
			{
				Text_Piece * tp = &pc->current_node->element.text;
				u8 * next = utf8_next_char_ptr( tp->text, tp->text, tp->length );
				if ( next != tp->text + tp->length )
				{
					log_dbg( "%sexpecting a single utf-8 character after '", indent );
					break;
				}
				log_dbg( "%sfound a valid single character word after '", indent );
				m.current_node = pc->current_node = pc->current_node->next;
			}

			if ( ! ( start_match( &m )
			      && match( &m, '\'' )
			      && end_match( &m )
			       )
			   )
			{
				log_dbg( "%sexpecting '\\''", indent );
				break;
			}
			pc->current_node = m.current_node;
			log_dbg( "%sfound right '", indent );

			Text_Element text_element =
			   ast_merge_text_elements_until( ctx->ast, starting_node, pc->current_node );
			log_dbg( "%sfound literal is valid %s", indent, ast_node_type_strings[u32(type)] );
			put_bytes( (u8*)indent, str_bytes_to_null( indent ) );
			put_bytes( text_element.text.text,
			           text_element.text.length );
			putchar( '\n' );

			AST_Node * ast_node = ast_new_node( ctx->ast );
			ast_node->type = type;
			list_vm_push_last( &ast_node->text_element_nodes, &text_element );

			list_vm_push_last( &pc->eaten_ast_nodes, &ast_node );
			//pc->current_node = pc->current_node->next;
			ast_node->file_range.first = file_coord_last_start;
			UPDATE_LAST_FILE_POS();
			ast_node->file_range.last = file_coord_last_start;
			ret = true;
			break;
		}
		case AST_Node_Type::RANGE_LITERAL:
		{
			// TODO(theGiallo, 2017-12-05): move this check out of switch?
			if ( !pc->current_node )
			{
				break;
			}

			Matcher_Context m = {};
			m.current_node = pc->current_node;
			if (   start_match( &m )
			    && opt_bk( &m )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
			}

			UPDATE_LAST_FILE_POS();

			if ( pc->current_node->element.type != Text_Element_Type::SYMBOL )
			{
				log_dbg( "%sexpecting SYMBOL, found %s", indent, text_element_type_strings[(u32)pc->current_node->element.type] );
				put_bytes( (u8*)indent, str_bytes_to_null( indent ) );
				put_bytes( pc->current_node->element.text.text,
				           pc->current_node->element.text.length );
				putchar( '\n' );
				break;
			}

			//Text_Element_List_VM_Node * starting_node = pc->current_node;
			if ( ! ( start_match( &m )
			      && match( &m, '[' )
			      && end_match( &m )
			       )
			   )
			{
				log_dbg( "%sexpecting '['", indent );
				break;
			}

			AST_Node * range_extreme_left = NULL;
			pc->current_node = m.current_node;
			log_dbg( "%sfound [", indent );

			if ( ast_eat_start( ctx )
			  && ast_eat( ctx, AST_Node_Type::EXPRESSION )
			  && ast_eat_end( ctx )
			  )
			{
				AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );
				if ( lpc->current_node == pc->current_node )
				{
					log_dbg( "%sexpected range left extreme", indent );
					break;
				}
				log_dbg( "%sfound range left extreme", indent );
				m.current_node = pc->current_node = lpc->current_node;

				range_extreme_left = ast_new_node( ctx->ast );
				range_extreme_left->type = AST_Node_Type::RANGE_EXTREME;

				list_vm_move_list_last( &range_extreme_left->children, &lpc->eaten_ast_nodes );
			} else
			{
				log_dbg( "%sexpected range left extreme", indent );
				break;
			}

			if (    start_match( &m )
			     && opt_bk( &m )
			     && match( &m, '|' )
			     && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
				log_dbg( "%sfound | for left extreme", indent );

				AST_Node * range_extreme_left_exclude = ast_new_node( ctx->ast );
				range_extreme_left_exclude->type = AST_Node_Type::RANGE_EXTREME_EXCLUDE;
				list_vm_push_first( &range_extreme_left->children, &range_extreme_left_exclude );
			}

			if ( ! ( start_match( &m )
			      && opt_bk( &m )
			      && match( &m, '.' )
			      && match( &m, '.' )
			      && end_match( &m )
			       )
			   )
			{
				log_dbg( "%sexpecting '..'", indent );
				break;
			}
			pc->current_node = m.current_node;
			log_dbg( "%sfound '..'", indent );

			AST_Node * range_extreme_right_exclude = NULL;
			if (    start_match( &m )
			     && opt_bk( &m )
			     && match( &m, '|' )
			     && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
				log_dbg( "%sfound | for right extreme", indent );

				range_extreme_right_exclude = ast_new_node( ctx->ast );
				range_extreme_right_exclude->type = AST_Node_Type::RANGE_EXTREME_EXCLUDE;
			}

			AST_Node * range_extreme_right = NULL;
			if ( ast_eat_start( ctx )
			  && ast_eat( ctx, AST_Node_Type::EXPRESSION )
			  && ast_eat_end( ctx )
			  )
			{
				log_dbg( "%sfound range left extreme", indent );
				AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );
				m.current_node = pc->current_node = lpc->current_node;

				range_extreme_right = ast_new_node( ctx->ast );
				range_extreme_right->type = AST_Node_Type::RANGE_EXTREME;

				if ( range_extreme_right_exclude )
				{
					list_vm_push_first( &range_extreme_right->children, &range_extreme_right_exclude );
				}
				list_vm_move_list_last( &range_extreme_right->children, &lpc->eaten_ast_nodes );
			} else
			{
				log_dbg( "%sexpected range left extreme", indent );
				break;
			}

			if ( ! ( start_match( &m )
			      && opt_bk( &m )
			      && match( &m, ']' )
			      && end_match( &m )
			       )
			   )
			{
				log_dbg( "%sexpecting ']'", indent );
				break;
			}
			pc->current_node = m.current_node;
			log_dbg( "%sfound ]", indent );

			AST_Node * step_node = NULL;
			if (    start_match( &m )
			     && opt_bk( &m )
			     && match_word( &m, "by" )
			     && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
				log_dbg( "%sfound 'by'", indent );

				bool step_mult = false;
				if (    start_match( &m )
				     && opt_bk( &m )
				     && match( &m, '*' )
				     && end_match( &m )
				   )
				{
					pc->current_node = m.current_node;
					log_dbg( "%sfound '*'", indent );
					step_mult = true;
				}

				s8 step_sign = 0;
				if (    start_match( &m )
				     && opt_bk( &m )
				     && match( &m, '+' )
				     && end_match( &m )
				   )
				{
					pc->current_node = m.current_node;
					log_dbg( "%sfound '+'", indent );
					step_sign = 1;
				} else
				if (    start_match( &m )
				     && opt_bk( &m )
				     && match( &m, '-' )
				     && end_match( &m )
				   )
				{
					pc->current_node = m.current_node;
					log_dbg( "%sfound '-'", indent );
					step_sign = -1;
				}

				if ( ast_eat_start( ctx )
				  && ast_eat( ctx, AST_Node_Type::EXPR_TERMINAL )
				  && ast_eat_end( ctx )
				   )
				{
					pc->current_node = m.current_node;
					log_dbg( "%sfound step expression", indent );
					step_node = ast_new_node( ctx->ast );
					step_node->type = AST_Node_Type::RANGE_STEP;

					if ( step_mult )
					{
						AST_Node * mult_node = ast_new_node( ctx->ast );
						mult_node->type = AST_Node_Type::RANGE_STEP_MULT;
						list_vm_push_last( &step_node->children, &mult_node );
					}

					if ( step_sign )
					{
						AST_Node * sign_node = ast_new_node( ctx->ast );
						sign_node->type = AST_Node_Type::RANGE_STEP_SIGN;

						AST_Node * sign_value_node = ast_new_node( ctx->ast );
						sign_value_node->type = step_sign == 1 ? AST_Node_Type::PLUS : AST_Node_Type::MINUS;

						list_vm_push_last( &sign_node->children, &sign_value_node );
						list_vm_push_last( &step_node->children, &sign_node );
					}

					AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );
					m.current_node = pc->current_node = lpc->current_node;
					list_vm_move_list_last( &step_node->children, &lpc->eaten_ast_nodes );
				} else
				{
					log_dbg( "%sexpected step expression", indent );
					break;
				}
			}

			log_dbg( "%sfound literal is valid %s", indent, ast_node_type_strings[u32(type)] );

			AST_Node * range_node = ast_new_node( ctx->ast );
			range_node->type = type;

			list_vm_push_last( &range_node->children, &range_extreme_left );
			list_vm_push_last( &range_node->children, &range_extreme_right );
			if ( step_node )
			{
				list_vm_push_last( &range_node->children, &step_node );
			}

			list_vm_push_last( &pc->eaten_ast_nodes, &range_node );
			//pc->current_node = pc->current_node->next;
			range_node->file_range.first = file_coord_last_start;
			UPDATE_LAST_FILE_POS();
			range_node->file_range.last = file_coord_last_start;
			ret = true;
			break;
		}
		case AST_Node_Type::COMMA_OP:
		{
			if ( !pc->current_node )
			{
				break;
			}
			Matcher_Context m = {};
			m.current_node = pc->current_node;
			if (   start_match( &m )
			    && opt_bk( &m )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
			}

			UPDATE_LAST_FILE_POS();

			if ( pc->current_node->element.type != Text_Element_Type::SYMBOL )
			{
				log_dbg( "%sexpecting SYMBOL, found %s", indent, text_element_type_strings[(u32)pc->current_node->element.type] );
				put_bytes( pc->current_node->element.text.text,
				           pc->current_node->element.text.length );
				putchar( '\n' );
				break;
			}

			if (    start_match( &m )
			     && opt_bk( &m )
			     && match( &m, ',' )
			     && end_match( &m )
			   )
			{
				AST_Node * ast_node = ast_new_node( ctx->ast );
				ast_node->type = type;

				Text_Element text_element = ast_merge_text_elements_until( ctx->ast, pc->current_node, m.current_node );

				list_vm_push_last( &ast_node->text_element_nodes, &text_element );

				list_vm_push_last( &pc->eaten_ast_nodes, &ast_node );

				pc->current_node = m.current_node;
				ast_node->file_range.first = file_coord_last_start;
				UPDATE_LAST_FILE_POS();
				ast_node->file_range.last = file_coord_last_start;
				ret = true;
			}
			break;
		}
		case AST_Node_Type::SEMICOLON:
		{
			if ( !pc->current_node )
			{
				break;
			}
			Matcher_Context m = {};
			m.current_node = pc->current_node;
			if (   start_match( &m )
			    && opt_bk( &m )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
			}

			UPDATE_LAST_FILE_POS();

			if ( pc->current_node->element.type != Text_Element_Type::SYMBOL )
			{
				log_dbg( "%sexpecting SYMBOL, found %s", indent, text_element_type_strings[(u32)pc->current_node->element.type] );
				put_bytes( pc->current_node->element.text.text,
				           pc->current_node->element.text.length );
				putchar( '\n' );
				break;
			}

			if (    start_match( &m )
			     && opt_bk( &m )
			     && match( &m, ';' )
			     && end_match( &m )
			   )
			{
				AST_Node * ast_node = ast_new_node( ctx->ast );
				ast_node->type = type;

				Text_Element text_element = ast_merge_text_elements_until( ctx->ast, pc->current_node, m.current_node );

				list_vm_push_last( &ast_node->text_element_nodes, &text_element );

				list_vm_push_last( &pc->eaten_ast_nodes, &ast_node );

				pc->current_node = m.current_node;
				ast_node->file_range.first = file_coord_last_start;
				UPDATE_LAST_FILE_POS();
				ast_node->file_range.last = file_coord_last_start;
				ret = true;
			}
			break;
		}
		case AST_Node_Type::ASSIGNMENT_OP:
		{
			if ( !pc->current_node )
			{
				break;
			}
			Matcher_Context m = {};
			m.current_node = pc->current_node;
			if (   start_match( &m )
			    && opt_bk( &m )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
			}

			UPDATE_LAST_FILE_POS();

			if (    start_match( &m )
			     && opt_bk( &m )
			     && match( &m, '=' )
			     && end_match( &m )
			   )
			{
				AST_Node * ast_node = ast_new_node( ctx->ast );
				ast_node->type = type;

				Text_Element text_element = ast_merge_text_elements_until( ctx->ast, pc->current_node, m.current_node );

				AST_Node * op_node = ast_new_node( ctx->ast );
				op_node->type = type;
				list_vm_push_last( &op_node->text_element_nodes, &text_element );

				list_vm_push_last( &ast_node->children, &op_node );
				list_vm_push_last( &pc->eaten_ast_nodes, &ast_node );

				pc->current_node = m.current_node;
				ast_node->file_range.first = file_coord_last_start;
				UPDATE_LAST_FILE_POS();
				ast_node->file_range.last = file_coord_last_start;
				ret = true;
			}
			break;
		}
		case AST_Node_Type::THIS_OP:
		{
			if ( !pc->current_node )
			{
				break;
			}
			Matcher_Context m = {};
			m.current_node = pc->current_node;
			if (   start_match( &m )
			    && opt_bk( &m )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
			}

			UPDATE_LAST_FILE_POS();

			if (    start_match( &m )
			     && opt_bk( &m )
			     && match( &m, '.' )
			     && end_match( &m )
			   )
			{
				AST_Node * ast_node = ast_new_node( ctx->ast );
				ast_node->type = type;

				Text_Element text_element = ast_merge_text_elements_until( ctx->ast, pc->current_node, m.current_node );

				AST_Node * op_node = ast_new_node( ctx->ast );
				op_node->type = type;
				list_vm_push_last( &op_node->text_element_nodes, &text_element );

				list_vm_push_last( &ast_node->children, &op_node );
				list_vm_push_last( &pc->eaten_ast_nodes, &ast_node );

				pc->current_node = m.current_node;
				ast_node->file_range.first = file_coord_last_start;
				UPDATE_LAST_FILE_POS();
				ast_node->file_range.last = file_coord_last_start;
				ret = true;
			}
			break;
		}
		case AST_Node_Type::BINARY_OPERATOR_PREC_0:
		{
			if ( !pc->current_node )
			{
				break;
			}
			Matcher_Context m = {};
			m.current_node = pc->current_node;
			if (   start_match( &m )
			    && opt_bk( &m )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
			}

			UPDATE_LAST_FILE_POS();

			AST_Node_Type op_type = AST_Node_Type::BINARY_FUN_OPERATOR;
			if (   start_match( &m )
			    && match( &m, '.' )
			    && match( &m, '\\' )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
				if ( m.current_node->element.type == Text_Element_Type::BLANKS )
				{
					log_dbg( "%safter '.\\' there should be no blanks", indent );
					break;
				}
				op_type = AST_Node_Type::DOT_BINARY_FUN_OPERATOR;
			}
			if (    ast_eat_start( ctx )
			     && ast_eat( ctx, AST_Node_Type::NAME )
			     && ast_eat_end( ctx ) )
			{
				log_dbg( "%sfound name", indent );
			} else
			{
				log_dbg( "%sexpected name", indent );
				break;
			}

			AST_Eating_Context::Partial_Context * e_pc = last_eaten_pctx( ctx );

			pc->current_node = e_pc->current_node;

			AST_Node * ast_node = ast_new_node( ctx->ast );
			ast_node->type = type;
			list_vm_push_last( &pc->eaten_ast_nodes, &ast_node );

			AST_Node * bfun_node = ast_new_node( ctx->ast );
			bfun_node->type = op_type;
			list_vm_move_list_last( &bfun_node->children, &e_pc->eaten_ast_nodes );
			list_vm_push_last( &ast_node->children, &bfun_node );

			ast_node->file_range.first  = file_coord_last_start;
			bfun_node->file_range.first = file_coord_last_start;
			UPDATE_LAST_FILE_POS();
			ast_node->file_range.last  = file_coord_last_start;
			bfun_node->file_range.last = file_coord_last_start;

			ret = true;

			break;
		}
		case AST_Node_Type::BINARY_OPERATOR_PREC_1:
		{
			if ( !pc->current_node )
			{
				break;
			}
			Matcher_Context m = {};
			m.current_node = pc->current_node;
			if (   start_match( &m )
			    && opt_bk( &m )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
			}

			UPDATE_LAST_FILE_POS();

			if ( pc->current_node->element.type != Text_Element_Type::SYMBOL )
			{
				log_dbg( "%sexpecting SYMBOL, found %s", indent, text_element_type_strings[(u32)pc->current_node->element.type] );
				put_bytes( pc->current_node->element.text.text,
				           pc->current_node->element.text.length );
				putchar( '\n' );
				break;
			}

			AST_Node_Type op_type = AST_Node_Type::UNKNOWN;
			if ( (    start_match( &m )
			       && opt_bk( &m )
			       && match( &m, ':' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::CONCAT_OP, true ) )
			  || (    start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '.' )
			       && match( &m, ':' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::DOT_CONCAT_OP, true ) )
			   )
			{
				AST_Node * ast_node = ast_new_node( ctx->ast );
				ast_node->type = type;

				Text_Element text_element = ast_merge_text_elements_until( ctx->ast, pc->current_node, m.current_node );

				AST_Node * op_node = ast_new_node( ctx->ast );
				op_node->type = op_type;
				list_vm_push_last( &op_node->text_element_nodes, &text_element );

				list_vm_push_last( &ast_node->children, &op_node );
				list_vm_push_last( &pc->eaten_ast_nodes, &ast_node );

				pc->current_node = m.current_node;

				ast_node->file_range.first = file_coord_last_start;
				op_node->file_range.first  = file_coord_last_start;
				UPDATE_LAST_FILE_POS();
				ast_node->file_range.last = file_coord_last_start;
				op_node->file_range.last  = file_coord_last_start;

				ret = true;
			}
			break;
		}
		case AST_Node_Type::BINARY_OPERATOR_PREC_2:
		{
			if ( !pc->current_node )
			{
				break;
			}
			Matcher_Context m = {};
			m.current_node = pc->current_node;
			if (   start_match( &m )
			    && opt_bk( &m )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
			}

			UPDATE_LAST_FILE_POS();

			if ( pc->current_node->element.type != Text_Element_Type::SYMBOL )
			{
				log_dbg( "%sexpecting SYMBOL, found %s", indent, text_element_type_strings[(u32)pc->current_node->element.type] );
				put_bytes( pc->current_node->element.text.text,
				           pc->current_node->element.text.length );
				putchar( '\n' );
				break;
			}

			AST_Node_Type op_type = AST_Node_Type::UNKNOWN;
			if ( (    start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '*' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::MULT, true ) )
			  || (    ( m.current_node = pc->current_node, true  )
			       && start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '/' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::DIV, true ) )
			  || (    ( m.current_node = pc->current_node, true  )
			       && start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '%' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::MODULE, true ) )
			  || (    start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '.' )
			       && match( &m, '*' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::DOT_MULT, true ) )
			  || (    ( m.current_node = pc->current_node, true  )
			       && start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '.' )
			       && match( &m, '/' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::DOT_DIV, true ) )
			  || (    ( m.current_node = pc->current_node, true  )
			       && start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '.' )
			       && match( &m, '%' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::DOT_MODULE, true ) )
			   )
			{
				AST_Node * ast_node = ast_new_node( ctx->ast );
				ast_node->type = type;

				Text_Element text_element = ast_merge_text_elements_until( ctx->ast, pc->current_node, m.current_node );

				AST_Node * op_node = ast_new_node( ctx->ast );
				op_node->type = op_type;
				list_vm_push_last( &op_node->text_element_nodes, &text_element );

				list_vm_push_last( &ast_node->children, &op_node );
				list_vm_push_last( &pc->eaten_ast_nodes, &ast_node );

				pc->current_node = m.current_node;

				ast_node->file_range.first = file_coord_last_start;
				op_node->file_range.first  = file_coord_last_start;
				UPDATE_LAST_FILE_POS();
				ast_node->file_range.last = file_coord_last_start;
				op_node->file_range.last  = file_coord_last_start;

				ret = true;
			}
			break;
		}
		case AST_Node_Type::BINARY_OPERATOR_PREC_3:
		{
			if ( !pc->current_node )
			{
				break;
			}
			Matcher_Context m = {};
			m.current_node = pc->current_node;
			if (   start_match( &m )
			    && opt_bk( &m )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
			}

			UPDATE_LAST_FILE_POS();

			if ( pc->current_node->element.type != Text_Element_Type::SYMBOL )
			{
				log_dbg( "%sexpecting SYMBOL, found %s", indent, text_element_type_strings[(u32)pc->current_node->element.type] );
				put_bytes( pc->current_node->element.text.text,
				           pc->current_node->element.text.length );
				putchar( '\n' );
				break;
			}

			AST_Node_Type op_type = AST_Node_Type::UNKNOWN;
			if ( (    start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '+' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::PLUS, true ) )
			  || (    ( m.current_node = pc->current_node, true  )
			       && start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '-' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::MINUS, true ) )
			  || (    start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '.' )
			       && match( &m, '+' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::DOT_PLUS, true ) )
			  || (    ( m.current_node = pc->current_node, true  )
			       && start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '.' )
			       && match( &m, '-' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::DOT_MINUS, true ) )
			   )
			{
				AST_Node * ast_node = ast_new_node( ctx->ast );
				ast_node->type = type;

				Text_Element text_element = ast_merge_text_elements_until( ctx->ast, pc->current_node, m.current_node );

				AST_Node * op_node = ast_new_node( ctx->ast );
				op_node->type = op_type;
				list_vm_push_last( &op_node->text_element_nodes, &text_element );

				list_vm_push_last( &ast_node->children, &op_node );
				list_vm_push_last( &pc->eaten_ast_nodes, &ast_node );

				pc->current_node = m.current_node;

				ast_node->file_range.first = file_coord_last_start;
				op_node->file_range.first  = file_coord_last_start;
				UPDATE_LAST_FILE_POS();
				ast_node->file_range.last = file_coord_last_start;
				op_node->file_range.last  = file_coord_last_start;

				ret = true;
			}
			break;
		}
		case AST_Node_Type::BINARY_OPERATOR_PREC_4:
		{
			if ( !pc->current_node )
			{
				break;
			}
			Matcher_Context m = {};
			m.current_node = pc->current_node;
			if (   start_match( &m )
			    && opt_bk( &m )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
			}

			UPDATE_LAST_FILE_POS();

			if ( pc->current_node->element.type != Text_Element_Type::SYMBOL )
			{
				log_dbg( "%sexpecting SYMBOL, found %s", indent, text_element_type_strings[(u32)pc->current_node->element.type] );
				put_bytes( pc->current_node->element.text.text,
				           pc->current_node->element.text.length );
				putchar( '\n' );
				break;
			}

			AST_Node_Type op_type = AST_Node_Type::UNKNOWN;
			if ( (    start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '>' )
			       && match( &m, '>' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::RIGHT_SHIFT, true ) )
			  || (    ( m.current_node = pc->current_node, true  )
			       && start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '<' )
			       && match( &m, '<' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::LEFT_SHIFT, true ) )
			  || (    start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '.' )
			       && match( &m, '>' )
			       && match( &m, '>' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::DOT_RIGHT_SHIFT, true ) )
			  || (    ( m.current_node = pc->current_node, true  )
			       && start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '.' )
			       && match( &m, '<' )
			       && match( &m, '<' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::DOT_LEFT_SHIFT, true ) )
			   )
			{
				AST_Node * ast_node = ast_new_node( ctx->ast );
				ast_node->type = type;

				Text_Element text_element = ast_merge_text_elements_until( ctx->ast, pc->current_node, m.current_node );

				AST_Node * op_node = ast_new_node( ctx->ast );
				op_node->type = op_type;
				list_vm_push_last( &op_node->text_element_nodes, &text_element );

				list_vm_push_last( &ast_node->children, &op_node );
				list_vm_push_last( &pc->eaten_ast_nodes, &ast_node );

				pc->current_node = m.current_node;

				ast_node->file_range.first = file_coord_last_start;
				op_node->file_range.first  = file_coord_last_start;
				UPDATE_LAST_FILE_POS();
				ast_node->file_range.last = file_coord_last_start;
				op_node->file_range.last  = file_coord_last_start;

				ret = true;
			}
			break;
		}
		case AST_Node_Type::BINARY_OPERATOR_PREC_5:
		{
			if ( !pc->current_node )
			{
				break;
			}
			Matcher_Context m = {};
			m.current_node = pc->current_node;
			if (   start_match( &m )
			    && opt_bk( &m )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
			}

			UPDATE_LAST_FILE_POS();

			if ( pc->current_node->element.type != Text_Element_Type::SYMBOL )
			{
				log_dbg( "%sexpecting SYMBOL, found %s", indent, text_element_type_strings[(u32)pc->current_node->element.type] );
				put_bytes( pc->current_node->element.text.text,
				           pc->current_node->element.text.length );
				putchar( '\n' );
				break;
			}

			AST_Node_Type op_type = AST_Node_Type::UNKNOWN;
			if ( (    start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '>' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::GREATER_THAN, true ) )
			  || (    ( m.current_node = pc->current_node, true  )
			       && start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '<' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::LESS_THAN, true ) )
			  || (    ( m.current_node = pc->current_node, true  )
			       && start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '>' )
			       && match( &m, '=' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::GREATER_EQUAL_THAN, true ) )
			  || (    ( m.current_node = pc->current_node, true  )
			       && start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '<' )
			       && match( &m, '=' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::LESS_EQUAL_THAN, true ) )
			  || (    start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '.' )
			       && match( &m, '>' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::DOT_GREATER_THAN, true ) )
			  || (    ( m.current_node = pc->current_node, true  )
			       && start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '.' )
			       && match( &m, '<' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::DOT_LESS_THAN, true ) )
			  || (    ( m.current_node = pc->current_node, true  )
			       && start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '.' )
			       && match( &m, '>' )
			       && match( &m, '=' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::DOT_GREATER_EQUAL_THAN, true ) )
			  || (    ( m.current_node = pc->current_node, true  )
			       && start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '.' )
			       && match( &m, '<' )
			       && match( &m, '=' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::DOT_LESS_EQUAL_THAN, true ) )
			   )
			{
				AST_Node * ast_node = ast_new_node( ctx->ast );
				ast_node->type = type;

				Text_Element text_element = ast_merge_text_elements_until( ctx->ast, pc->current_node, m.current_node );

				AST_Node * op_node = ast_new_node( ctx->ast );
				op_node->type = op_type;
				list_vm_push_last( &op_node->text_element_nodes, &text_element );

				list_vm_push_last( &ast_node->children, &op_node );
				list_vm_push_last( &pc->eaten_ast_nodes, &ast_node );

				pc->current_node = m.current_node;

				ast_node->file_range.first = file_coord_last_start;
				op_node->file_range.first  = file_coord_last_start;
				UPDATE_LAST_FILE_POS();
				ast_node->file_range.last = file_coord_last_start;
				op_node->file_range.last  = file_coord_last_start;

				ret = true;
			}
			break;
		}
		case AST_Node_Type::BINARY_OPERATOR_PREC_6:
		{
			if ( !pc->current_node )
			{
				break;
			}
			Matcher_Context m = {};
			m.current_node = pc->current_node;
			if (   start_match( &m )
			    && opt_bk( &m )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
			}

			UPDATE_LAST_FILE_POS();

			if ( pc->current_node->element.type != Text_Element_Type::SYMBOL )
			{
				log_dbg( "%sexpecting SYMBOL, found %s", indent, text_element_type_strings[(u32)pc->current_node->element.type] );
				put_bytes( pc->current_node->element.text.text,
				           pc->current_node->element.text.length );
				putchar( '\n' );
				break;
			}

			AST_Node_Type op_type = AST_Node_Type::UNKNOWN;
			if ( (    start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '=' )
			       && match( &m, '=' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::EQUAL, true ) )
			  || (    ( m.current_node = pc->current_node, true  )
			       && start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '!' )
			       && match( &m, '=' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::NOT_EQUAL, true ) )
			  || (    start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '.' )
			       && match( &m, '=' )
			       && match( &m, '=' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::DOT_EQUAL, true ) )
			  || (    ( m.current_node = pc->current_node, true  )
			       && start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '.' )
			       && match( &m, '!' )
			       && match( &m, '=' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::DOT_NOT_EQUAL, true ) )
			   )
			{
				AST_Node * ast_node = ast_new_node( ctx->ast );
				ast_node->type = type;

				Text_Element text_element = ast_merge_text_elements_until( ctx->ast, pc->current_node, m.current_node );

				AST_Node * op_node = ast_new_node( ctx->ast );
				op_node->type = op_type;
				list_vm_push_last( &op_node->text_element_nodes, &text_element );

				list_vm_push_last( &ast_node->children, &op_node );
				list_vm_push_last( &pc->eaten_ast_nodes, &ast_node );

				pc->current_node = m.current_node;

				ast_node->file_range.first = file_coord_last_start;
				op_node->file_range.first  = file_coord_last_start;
				UPDATE_LAST_FILE_POS();
				ast_node->file_range.last = file_coord_last_start;
				op_node->file_range.last  = file_coord_last_start;

				ret = true;
			}
			break;
		}
		case AST_Node_Type::BINARY_OPERATOR_PREC_7:
		{
			if ( !pc->current_node )
			{
				break;
			}
			Matcher_Context m = {};
			m.current_node = pc->current_node;
			if (   start_match( &m )
			    && opt_bk( &m )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
			}

			UPDATE_LAST_FILE_POS();

			if ( pc->current_node->element.type != Text_Element_Type::SYMBOL )
			{
				log_dbg( "%sexpecting SYMBOL, found %s", indent, text_element_type_strings[(u32)pc->current_node->element.type] );
				put_bytes( pc->current_node->element.text.text,
				           pc->current_node->element.text.length );
				putchar( '\n' );
				break;
			}

			AST_Node_Type op_type = AST_Node_Type::UNKNOWN;
			if ( (    start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '&' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::BITWISE_AND, true ) )
			  || (    start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '.' )
			       && match( &m, '&' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::DOT_BITWISE_AND, true ) )
			   )
			{
				AST_Node * ast_node = ast_new_node( ctx->ast );
				ast_node->type = type;

				Text_Element text_element = ast_merge_text_elements_until( ctx->ast, pc->current_node, m.current_node );

				AST_Node * op_node = ast_new_node( ctx->ast );
				op_node->type = op_type;
				list_vm_push_last( &op_node->text_element_nodes, &text_element );

				list_vm_push_last( &ast_node->children, &op_node );
				list_vm_push_last( &pc->eaten_ast_nodes, &ast_node );

				pc->current_node = m.current_node;

				ast_node->file_range.first = file_coord_last_start;
				op_node->file_range.first  = file_coord_last_start;
				UPDATE_LAST_FILE_POS();
				ast_node->file_range.last = file_coord_last_start;
				op_node->file_range.last  = file_coord_last_start;

				ret = true;
			}
			break;
		}
		case AST_Node_Type::BINARY_OPERATOR_PREC_8:
		{
			if ( !pc->current_node )
			{
				break;
			}
			Matcher_Context m = {};
			m.current_node = pc->current_node;
			if (   start_match( &m )
			    && opt_bk( &m )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
			}

			UPDATE_LAST_FILE_POS();

			if ( pc->current_node->element.type != Text_Element_Type::SYMBOL )
			{
				log_dbg( "%sexpecting SYMBOL, found %s", indent, text_element_type_strings[(u32)pc->current_node->element.type] );
				put_bytes( pc->current_node->element.text.text,
				           pc->current_node->element.text.length );
				putchar( '\n' );
				break;
			}

			AST_Node_Type op_type = AST_Node_Type::UNKNOWN;
			if ( (    start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '^' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::BITWISE_XOR, true ) )
			  || (    start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '.' )
			       && match( &m, '^' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::DOT_BITWISE_XOR, true ) )
			   )
			{
				AST_Node * ast_node = ast_new_node( ctx->ast );
				ast_node->type = type;

				Text_Element text_element = ast_merge_text_elements_until( ctx->ast, pc->current_node, m.current_node );

				AST_Node * op_node = ast_new_node( ctx->ast );
				op_node->type = op_type;
				list_vm_push_last( &op_node->text_element_nodes, &text_element );

				list_vm_push_last( &ast_node->children, &op_node );
				list_vm_push_last( &pc->eaten_ast_nodes, &ast_node );

				pc->current_node = m.current_node;

				ast_node->file_range.first = file_coord_last_start;
				op_node->file_range.first  = file_coord_last_start;
				UPDATE_LAST_FILE_POS();
				ast_node->file_range.last = file_coord_last_start;
				op_node->file_range.last  = file_coord_last_start;

				ret = true;
			}
			break;
		}
		case AST_Node_Type::BINARY_OPERATOR_PREC_9:
		{
			if ( !pc->current_node )
			{
				break;
			}
			Matcher_Context m = {};
			m.current_node = pc->current_node;
			if (   start_match( &m )
			    && opt_bk( &m )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
			}

			UPDATE_LAST_FILE_POS();

			if ( pc->current_node->element.type != Text_Element_Type::SYMBOL )
			{
				log_dbg( "%sexpecting SYMBOL, found %s", indent, text_element_type_strings[(u32)pc->current_node->element.type] );
				put_bytes( pc->current_node->element.text.text,
				           pc->current_node->element.text.length );
				putchar( '\n' );
				break;
			}

			AST_Node_Type op_type = AST_Node_Type::UNKNOWN;
			if ( (    start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '|' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::BITWISE_OR, true ) )
			  || (    start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '.' )
			       && match( &m, '|' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::DOT_BITWISE_OR, true ) )
			   )
			{
				AST_Node * ast_node = ast_new_node( ctx->ast );
				ast_node->type = type;

				Text_Element text_element = ast_merge_text_elements_until( ctx->ast, pc->current_node, m.current_node );

				AST_Node * op_node = ast_new_node( ctx->ast );
				op_node->type = op_type;
				list_vm_push_last( &op_node->text_element_nodes, &text_element );

				list_vm_push_last( &ast_node->children, &op_node );
				list_vm_push_last( &pc->eaten_ast_nodes, &ast_node );

				pc->current_node = m.current_node;

				ast_node->file_range.first = file_coord_last_start;
				op_node->file_range.first  = file_coord_last_start;
				UPDATE_LAST_FILE_POS();
				ast_node->file_range.last = file_coord_last_start;
				op_node->file_range.last  = file_coord_last_start;

				ret = true;
			}
			break;
		}
		case AST_Node_Type::BINARY_OPERATOR_PREC_10:
		{
			if ( !pc->current_node )
			{
				break;
			}
			Matcher_Context m = {};
			m.current_node = pc->current_node;
			if (   start_match( &m )
			    && opt_bk( &m )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
			}

			UPDATE_LAST_FILE_POS();

			if ( pc->current_node->element.type != Text_Element_Type::SYMBOL )
			{
				log_dbg( "%sexpecting SYMBOL, found %s", indent, text_element_type_strings[(u32)pc->current_node->element.type] );
				put_bytes( pc->current_node->element.text.text,
				           pc->current_node->element.text.length );
				putchar( '\n' );
				break;
			}

			AST_Node_Type op_type = AST_Node_Type::UNKNOWN;
			if ( (    start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '&' )
			       && match( &m, '&' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::LOGICAL_AND, true ) )
			  || (    start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '.' )
			       && match( &m, '&' )
			       && match( &m, '&' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::DOT_LOGICAL_AND, true ) )
			   )
			{
				AST_Node * ast_node = ast_new_node( ctx->ast );
				ast_node->type = type;

				Text_Element text_element = ast_merge_text_elements_until( ctx->ast, pc->current_node, m.current_node );

				AST_Node * op_node = ast_new_node( ctx->ast );
				op_node->type = op_type;
				list_vm_push_last( &op_node->text_element_nodes, &text_element );

				list_vm_push_last( &ast_node->children, &op_node );
				list_vm_push_last( &pc->eaten_ast_nodes, &ast_node );

				pc->current_node = m.current_node;

				ast_node->file_range.first = file_coord_last_start;
				op_node->file_range.first  = file_coord_last_start;
				UPDATE_LAST_FILE_POS();
				ast_node->file_range.last = file_coord_last_start;
				op_node->file_range.last  = file_coord_last_start;

				ret = true;
			}
			break;
		}
		case AST_Node_Type::BINARY_OPERATOR_PREC_11:
		{
			if ( !pc->current_node )
			{
				break;
			}
			Matcher_Context m = {};
			m.current_node = pc->current_node;
			if (   start_match( &m )
			    && opt_bk( &m )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
			}

			UPDATE_LAST_FILE_POS();

			if ( pc->current_node->element.type != Text_Element_Type::SYMBOL )
			{
				log_dbg( "%sexpecting SYMBOL, found %s", indent, text_element_type_strings[(u32)pc->current_node->element.type] );
				put_bytes( pc->current_node->element.text.text,
				           pc->current_node->element.text.length );
				putchar( '\n' );
				break;
			}

			AST_Node_Type op_type = AST_Node_Type::UNKNOWN;
			if ( (    start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '|' )
			       && match( &m, '|' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::LOGICAL_OR, true ) )
			  || (    start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '.' )
			       && match( &m, '|' )
			       && match( &m, '|' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::DOT_LOGICAL_OR, true ) )
			   )
			{
				AST_Node * ast_node = ast_new_node( ctx->ast );
				ast_node->type = type;
				list_vm_push_last( &pc->eaten_ast_nodes, &ast_node );

				Text_Element text_element = ast_merge_text_elements_until( ctx->ast, pc->current_node, m.current_node );

				AST_Node * op_node = ast_new_node( ctx->ast );
				op_node->type = op_type;
				list_vm_push_last( &op_node->text_element_nodes, &text_element );

				list_vm_push_last( &ast_node->children, &op_node );
				list_vm_push_last( &pc->eaten_ast_nodes, &ast_node );

				pc->current_node = m.current_node;

				ast_node->file_range.first = file_coord_last_start;
				op_node->file_range.first  = file_coord_last_start;
				UPDATE_LAST_FILE_POS();
				ast_node->file_range.last = file_coord_last_start;
				op_node->file_range.last  = file_coord_last_start;

				ret = true;
			}
			break;
		}
		case AST_Node_Type::BINARY_OPERATOR_PREC_12:
		{
			if ( !pc->current_node )
			{
				break;
			}
			Matcher_Context m = {};
			m.current_node = pc->current_node;
			if (   start_match( &m )
			    && opt_bk( &m )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
			}

			UPDATE_LAST_FILE_POS();

			if ( pc->current_node->element.type != Text_Element_Type::SYMBOL )
			{
				log_dbg( "%sexpecting SYMBOL, found %s", indent, text_element_type_strings[(u32)pc->current_node->element.type] );
				put_bytes( pc->current_node->element.text.text,
				           pc->current_node->element.text.length );
				putchar( '\n' );
				break;
			}

			AST_Node_Type op_type = AST_Node_Type::UNKNOWN;
			if ( (    start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '=' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::ASSIGNMENT_OP, true ) )
			  || (    ( m.current_node = pc->current_node, true  )
			       && start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '+' )
			       && match( &m, '=' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::PLUS_ASSIGNMENT_OP, true ) )
			  || (    ( m.current_node = pc->current_node, true  )
			       && start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '-' )
			       && match( &m, '=' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::MINUS_ASSIGNMENT_OP, true ) )
			  || (    ( m.current_node = pc->current_node, true  )
			       && start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '*' )
			       && match( &m, '=' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::MULT_ASSIGNMENT_OP, true ) )
			  || (    ( m.current_node = pc->current_node, true  )
			       && start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '/' )
			       && match( &m, '=' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::DIV_ASSIGNMENT_OP, true ) )
			  || (    ( m.current_node = pc->current_node, true  )
			       && start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '%' )
			       && match( &m, '=' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::MODULE_ASSIGNMENT_OP, true ) )
			  || (    ( m.current_node = pc->current_node, true  )
			       && start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '<' )
			       && match( &m, '<' )
			       && match( &m, '=' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::LEFT_SHIFT_ASSIGNMENT_OP, true ) )
			  || (    ( m.current_node = pc->current_node, true  )
			       && start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '>' )
			       && match( &m, '>' )
			       && match( &m, '=' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::RIGHT_SHIFT_ASSIGNMENT_OP, true ) )
			  || (    ( m.current_node = pc->current_node, true  )
			       && start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '&' )
			       && match( &m, '=' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::BIN_AND_ASSIGNMENT_OP, true ) )
			  || (    ( m.current_node = pc->current_node, true  )
			       && start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '|' )
			       && match( &m, '=' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::BIN_OR_ASSIGNMENT_OP, true ) )
			  || (    ( m.current_node = pc->current_node, true  )
			       && start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '^' )
			       && match( &m, '=' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::BIN_XOR_ASSIGNMENT_OP, true ) )
			  || (    start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '.' )
			       && match( &m, '=' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::DOT_ASSIGNMENT_OP, true ) )
			  || (    ( m.current_node = pc->current_node, true  )
			       && start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '.' )
			       && match( &m, '+' )
			       && match( &m, '=' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::DOT_PLUS_ASSIGNMENT_OP, true ) )
			  || (    ( m.current_node = pc->current_node, true  )
			       && start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '.' )
			       && match( &m, '-' )
			       && match( &m, '=' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::DOT_MINUS_ASSIGNMENT_OP, true ) )
			  || (    ( m.current_node = pc->current_node, true  )
			       && start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '.' )
			       && match( &m, '*' )
			       && match( &m, '=' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::DOT_MULT_ASSIGNMENT_OP, true ) )
			  || (    ( m.current_node = pc->current_node, true  )
			       && start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '.' )
			       && match( &m, '/' )
			       && match( &m, '=' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::DOT_DIV_ASSIGNMENT_OP, true ) )
			  || (    ( m.current_node = pc->current_node, true  )
			       && start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '.' )
			       && match( &m, '%' )
			       && match( &m, '=' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::DOT_MODULE_ASSIGNMENT_OP, true ) )
			  || (    ( m.current_node = pc->current_node, true  )
			       && start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '.' )
			       && match( &m, '<' )
			       && match( &m, '<' )
			       && match( &m, '=' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::DOT_LEFT_SHIFT_ASSIGNMENT_OP, true ) )
			  || (    ( m.current_node = pc->current_node, true  )
			       && start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '.' )
			       && match( &m, '>' )
			       && match( &m, '>' )
			       && match( &m, '=' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::DOT_RIGHT_SHIFT_ASSIGNMENT_OP, true ) )
			  || (    ( m.current_node = pc->current_node, true  )
			       && start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '.' )
			       && match( &m, '&' )
			       && match( &m, '=' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::DOT_BIN_AND_ASSIGNMENT_OP, true ) )
			  || (    ( m.current_node = pc->current_node, true  )
			       && start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '.' )
			       && match( &m, '|' )
			       && match( &m, '=' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::DOT_BIN_OR_ASSIGNMENT_OP, true ) )
			  || (    ( m.current_node = pc->current_node, true  )
			       && start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '.' )
			       && match( &m, '^' )
			       && match( &m, '=' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::DOT_BIN_XOR_ASSIGNMENT_OP, true ) )
			   )
			{
				AST_Node * ast_node = ast_new_node( ctx->ast );
				ast_node->type = type;

				Text_Element text_element = ast_merge_text_elements_until( ctx->ast, pc->current_node, m.current_node );

				AST_Node * op_node = ast_new_node( ctx->ast );
				op_node->type = op_type;
				list_vm_push_last( &op_node->text_element_nodes, &text_element );

				list_vm_push_last( &ast_node->children, &op_node );
				list_vm_push_last( &pc->eaten_ast_nodes, &ast_node );

				pc->current_node = m.current_node;

				ast_node->file_range.first = file_coord_last_start;
				op_node->file_range.first  = file_coord_last_start;
				UPDATE_LAST_FILE_POS();
				ast_node->file_range.last = file_coord_last_start;
				op_node->file_range.last  = file_coord_last_start;

				ret = true;
			}
			break;
		}
		case AST_Node_Type::BINARY_OPERATOR_PREC_13:
		{
			if ( !pc->current_node )
			{
				break;
			}
			Matcher_Context m = {};
			m.current_node = pc->current_node;
			if (   start_match( &m )
			    && opt_bk( &m )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
			}

			UPDATE_LAST_FILE_POS();

			if ( pc->current_node->element.type != Text_Element_Type::SYMBOL )
			{
				log_dbg( "%sexpecting SYMBOL, found %s", indent, text_element_type_strings[(u32)pc->current_node->element.type] );
				put_bytes( pc->current_node->element.text.text,
				           pc->current_node->element.text.length );
				putchar( '\n' );
				break;
			}

			AST_Node_Type op_type = AST_Node_Type::UNKNOWN;
			if ( (    start_match( &m )
			       && opt_bk( &m )
			       && match( &m, ',' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::COMMA_OP, true ) )
			   )
			{
				AST_Node * ast_node = ast_new_node( ctx->ast );
				ast_node->type = type;

				Text_Element text_element = ast_merge_text_elements_until( ctx->ast, pc->current_node, m.current_node );

				list_vm_push_last( &ast_node->text_element_nodes, &text_element );

				list_vm_push_last( &pc->eaten_ast_nodes, &ast_node );

				pc->current_node = m.current_node;

				ast_node->file_range.first = file_coord_last_start;
				UPDATE_LAST_FILE_POS();
				ast_node->file_range.last = file_coord_last_start;

				ret = true;
			}
			break;
		}
		case AST_Node_Type::UNARY_OPERATOR_PREFIX:
		{
			if ( !pc->current_node )
			{
				break;
			}
			Matcher_Context m = {};
			m.current_node = pc->current_node;
			if (   start_match( &m )
			    && opt_bk( &m )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
			}

			UPDATE_LAST_FILE_POS();

			if ( pc->current_node->element.type != Text_Element_Type::SYMBOL )
			{
				log_dbg( "%sexpecting SYMBOL, found %s", indent, text_element_type_strings[(u32)pc->current_node->element.type] );
				put_bytes( pc->current_node->element.text.text,
				           pc->current_node->element.text.length );
				putchar( '\n' );
				break;
			}

			AST_Node_Type op_type = AST_Node_Type::UNKNOWN;
			if ( (    start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '+' )
			       && match( &m, '+' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::INCREMENT, true ) )
			  || (    ( m.current_node = pc->current_node, true  )
			       && start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '-' )
			       && match( &m, '-' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::DECREMENT, true ) )
			  || (    ( m.current_node = pc->current_node, true  )
			       && start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '+' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::PLUS, true ) )
			  || (    ( m.current_node = pc->current_node, true  )
			       && start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '-' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::MINUS, true ) )
			  || (    ( m.current_node = pc->current_node, true  )
			       && start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '!' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::LOGICAL_NOT, true ) )
			  || (    ( m.current_node = pc->current_node, true  )
			       && start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '~' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::BITWISE_NOT, true ) )
			  || (    ( m.current_node = pc->current_node, true  )
			       && start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '<' )
			       && match( &m, '|' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::DEREFERENCE_OP, true ) )
			  || (    ( m.current_node = pc->current_node, true  )
			       && start_match( &m )
			       && opt_bk( &m )
			       && match( &m, '>' )
			       && match( &m, '|' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::GET_REFERENCE_OP, true ) )
			   )
			{
				AST_Node * ast_node = ast_new_node( ctx->ast );
				ast_node->type = type;

				Text_Element text_element = ast_merge_text_elements_until( ctx->ast, pc->current_node, m.current_node );

				AST_Node * op_node = ast_new_node( ctx->ast );
				op_node->type = op_type;
				list_vm_push_last( &op_node->text_element_nodes, &text_element );

				list_vm_push_last( &ast_node->children, &op_node );
				list_vm_push_last( &pc->eaten_ast_nodes, &ast_node );

				pc->current_node = m.current_node;

				ast_node->file_range.first = file_coord_last_start;
				op_node->file_range.first  = file_coord_last_start;
				UPDATE_LAST_FILE_POS();
				ast_node->file_range.last = file_coord_last_start;
				op_node->file_range.last  = file_coord_last_start;

				ret = true;
			}
			#if 0
			// NOTE(theGiallo): removed. Now it's all FUNCTION_CALL.
			else
			if (   start_match( &m )
			    && opt_bk( &m )
			    && match( &m, '(' )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
				log_dbg( "%sfound '('", indent );

				AST_Node * ast_node = ast_new_node( ctx->ast );
				ast_node->type = type;

				AST_Node * op_node = ast_new_node( ctx->ast );
				op_node->type = AST_Node_Type::TYPE_CAST;

				if (     ast_eat_start( ctx )
				     && ast_eat( ctx, AST_Node_Type::TYPE )
				     && ast_eat_end( ctx )
				   )
				{
					log_dbg( "%sfound type", indent );
					AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );
					list_vm_move_list_last( &op_node->children, &lpc->eaten_ast_nodes );
					m.current_node = pc->current_node = lpc->current_node;
				} else
				{
					log_dbg( "%stype not found", indent );
					break;
				}

				if (   start_match( &m )
				    && opt_bk( &m )
				    && match( &m, ')' )
				    && end_match( &m )
				   )
				{
					pc->current_node = m.current_node;
					log_dbg( "%sfound ')'", indent );
				} else
				{
					log_dbg( "%sexpected ')'", indent );
					break;
				}

				list_vm_push_last( &ast_node->children, &op_node );

				list_vm_push_last( &pc->eaten_ast_nodes, &ast_node );

				ret = true;
			}
			#endif
			break;
		}
		case AST_Node_Type::UNARY_OPERATOR_POSTFIX:
		{
			if ( !pc->current_node )
			{
				break;
			}
			Matcher_Context m = {};
			m.current_node = pc->current_node;
			if (   start_match( &m )
			    && opt_bk( &m )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
			}

			UPDATE_LAST_FILE_POS();

			if ( pc->current_node->element.type != Text_Element_Type::SYMBOL )
			{
				log_dbg( "%sexpecting SYMBOL, found %s", indent, text_element_type_strings[(u32)pc->current_node->element.type] );
				put_bytes( pc->current_node->element.text.text,
				           pc->current_node->element.text.length );
				putchar( '\n' );
				break;
			}

			AST_Node_Type op_type = AST_Node_Type::UNKNOWN;
			if ( (    start_match( &m )
			       && match( &m, '+' )
			       && match( &m, '+' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::INCREMENT, true ) )
			  || (    ( m.current_node = pc->current_node, true  )
			       && start_match( &m )
			       && match( &m, '-' )
			       && match( &m, '-' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::DECREMENT, true ) )
			  || (    start_match( &m )
			       && match( &m, '.' )
			       && match( &m, '+' )
			       && match( &m, '+' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::DOT_INCREMENT, true ) )
			  || (    ( m.current_node = pc->current_node, true  )
			       && start_match( &m )
			       && match( &m, '.' )
			       && match( &m, '-' )
			       && match( &m, '-' )
			       && end_match( &m )
			       && ( op_type = AST_Node_Type::DOT_DECREMENT, true ) )
			   )
			{
				AST_Node * ast_node = ast_new_node( ctx->ast );
				ast_node->type = type;

				Text_Element text_element = ast_merge_text_elements_until( ctx->ast, pc->current_node, m.current_node );

				AST_Node * op_node = ast_new_node( ctx->ast );
				op_node->type = op_type;
				list_vm_push_last( &op_node->text_element_nodes, &text_element );

				list_vm_push_last( &ast_node->children, &op_node );
				list_vm_push_last( &pc->eaten_ast_nodes, &ast_node );

				pc->current_node = m.current_node;

				ast_node->file_range.first = file_coord_last_start;
				op_node->file_range.first  = file_coord_last_start;
				UPDATE_LAST_FILE_POS();
				ast_node->file_range.last = file_coord_last_start;
				op_node->file_range.last  = file_coord_last_start;

				ret = true;
			} else
			if (    start_match( &m )
			     && match( &m, '[' )
			     && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;

				AST_Eating_Context::Partial_Context * lpc = NULL;
				if (    ast_eat_start( ctx )
				     && ast_eat( ctx, AST_Node_Type::EXPRESSION, { .expression_square_br_after = 1 } )
				     && ast_eat_end( ctx )
				   )
				{
					log_dbg( "%sfound EXPRESSION", indent );

					lpc = last_eaten_pctx( ctx );
					m.current_node = pc->current_node = lpc->current_node;
					op_type = AST_Node_Type::ARRAY_ACCESS;
				} else
				if (    start_match( &m )
				     && opt_bk( &m )
				     && match( &m, '*' )
				     && end_match( &m )
				   )
				{
					log_dbg( "%sfound '*'", indent );
					op_type = AST_Node_Type::ARRAY_EACH;
				} else
				{
					log_dbg( "%sexpecting EXPRESSION", indent );
					break;
				}

				if ( ! (    start_match( &m )
				         && opt_bk( &m )
				         && match( &m, ']' )
				         && end_match( &m )
				       )
				   )
				{
					log_dbg( "%sexpecting ']' found %s", indent, text_element_type_strings[(u32)pc->current_node->element.type] );
					put_bytes( pc->current_node->element.text.text,
					           pc->current_node->element.text.length );
					putchar( '\n' );
					break;
				}

				pc->current_node = m.current_node;

				AST_Node * ast_node = ast_new_node( ctx->ast );
				ast_node->type = type;

				AST_Node * op_node = ast_new_node( ctx->ast );
				op_node->type = op_type;

				if ( lpc )
				{
					list_vm_move_list_last( &op_node->children, &lpc->eaten_ast_nodes );
				}

				list_vm_push_last( &ast_node->children, &op_node );

				list_vm_push_last( &pc->eaten_ast_nodes, &ast_node );

				ast_node->file_range.first = file_coord_last_start;
				op_node->file_range.first  = file_coord_last_start;
				UPDATE_LAST_FILE_POS();
				ast_node->file_range.last = file_coord_last_start;
				op_node->file_range.last  = file_coord_last_start;

				ret = true;
			} else
			if (   start_match( &m )
			    && match( &m, '(' )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
				log_dbg( "%sfound '('", indent );

				AST_Node * ast_node = ast_new_node( ctx->ast );
				ast_node->type = type;

				AST_Node * op_node = ast_new_node( ctx->ast );
				op_node->type = AST_Node_Type::FUNCTION_CALL;

				if (    ast_eat_start( ctx )
				     && ast_eat( ctx, AST_Node_Type::STRUCT_LITERAL_BODY )
				     && ast_eat_end( ctx )
				   )
				{
					log_dbg( "%sfound struct literal body", indent );
					AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );
					list_vm_move_list_last( &op_node->children, &lpc->eaten_ast_nodes );
					m.current_node = pc->current_node = lpc->current_node;
				} else
				{
					log_dbg( "%sstruct literal body not found, maybe it's a void function call", indent );
				}

				if (   start_match( &m )
				    && opt_bk( &m )
				    && match( &m, ')' )
				    && end_match( &m )
				   )
				{
					pc->current_node = m.current_node;
					log_dbg( "%sfound ')'", indent );
				} else
				{
					log_dbg( "%sexpected ')'", indent );
					break;
				}

				list_vm_push_last( &ast_node->children, &op_node );

				list_vm_push_last( &pc->eaten_ast_nodes, &ast_node );

				ast_node->file_range.first = file_coord_last_start;
				op_node ->file_range.first = file_coord_last_start;
				UPDATE_LAST_FILE_POS();
				ast_node->file_range.last = file_coord_last_start;
				op_node ->file_range.last = file_coord_last_start;

				ret = true;
			}
			break;
		}
		case AST_Node_Type::STRUCT_MEMBER_ACCESS_OP:
		{
			if ( !pc->current_node )
			{
				break;
			}
			Matcher_Context m = {};
			m.current_node = pc->current_node;
			if (   start_match( &m )
			    && opt_bk( &m )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
			}

			UPDATE_LAST_FILE_POS();

			if ( pc->current_node->element.type != Text_Element_Type::SYMBOL )
			{
				log_dbg( "%sexpecting SYMBOL, found %s", indent, text_element_type_strings[(u32)pc->current_node->element.type] );
				put_bytes( pc->current_node->element.text.text,
				           pc->current_node->element.text.length );
				putchar( '\n' );
				break;
			}

			if (    start_match( &m )
			     && match( &m, '.' )
			     && end_match( &m )
			   )
			{
				AST_Node * ast_node = ast_new_node( ctx->ast );
				ast_node->type = type;

				Text_Element text_element = ast_merge_text_elements_until( ctx->ast, pc->current_node, m.current_node );

				list_vm_push_last( &ast_node->text_element_nodes, &text_element );

				list_vm_push_last( &pc->eaten_ast_nodes, &ast_node );

				pc->current_node = m.current_node;

				ast_node->file_range.first = file_coord_last_start;
				UPDATE_LAST_FILE_POS();
				ast_node->file_range.last = file_coord_last_start;

				ret = true;
			}
			break;
		}
		case AST_Node_Type::TERNARY_OPERATOR_0:
		{
			if ( !pc->current_node )
			{
				break;
			}
			Matcher_Context m = {};
			m.current_node = pc->current_node;
			if (   start_match( &m )
			    && opt_bk( &m )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
			}

			UPDATE_LAST_FILE_POS();

			if ( pc->current_node->element.type != Text_Element_Type::SYMBOL )
			{
				log_dbg( "%sexpecting SYMBOL, found %s", indent, text_element_type_strings[(u32)pc->current_node->element.type] );
				put_bytes( pc->current_node->element.text.text,
				           pc->current_node->element.text.length );
				putchar( '\n' );
				break;
			}

			if (    start_match( &m )
			     && match( &m, '?' )
			     && opt_bk( &m )
			     && match( &m, ':' )
			     && match( &m, '>' )
			     && end_match( &m )
			   )
			{
				AST_Node * ast_node = ast_new_node( ctx->ast );
				ast_node->type = type;
				list_vm_push_last( &pc->eaten_ast_nodes, &ast_node );

				Text_Element text_element = ast_merge_text_elements_until( ctx->ast, pc->current_node, m.current_node );

				list_vm_push_last( &ast_node->text_element_nodes, &text_element );

				pc->current_node = m.current_node;

				ast_node->file_range.first = file_coord_last_start;
				UPDATE_LAST_FILE_POS();
				ast_node->file_range.last = file_coord_last_start;

				ret = true;
			}
			break;
		}
		case AST_Node_Type::TERNARY_OPERATOR_1:
		{
			if ( !pc->current_node )
			{
				break;
			}
			Matcher_Context m = {};
			m.current_node = pc->current_node;
			if (   start_match( &m )
			    && opt_bk( &m )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
			}

			UPDATE_LAST_FILE_POS();

			if ( pc->current_node->element.type != Text_Element_Type::SYMBOL )
			{
				log_dbg( "%sexpecting SYMBOL, found %s", indent, text_element_type_strings[(u32)pc->current_node->element.type] );
				put_bytes( pc->current_node->element.text.text,
				           pc->current_node->element.text.length );
				putchar( '\n' );
				break;
			}

			AST_Node_Type op_type = AST_Node_Type::UNKNOWN;
			if ( (    start_match( &m )
			       && match( &m, ':' )
			       && match( &m, '<' )
			       && end_match( &m )
			       && ( op_type = type, true ) )
			   )
			{
				AST_Node * ast_node = ast_new_node( ctx->ast );
				ast_node->type = type;
				list_vm_push_last( &pc->eaten_ast_nodes, &ast_node );

				Text_Element text_element = ast_merge_text_elements_until( ctx->ast, pc->current_node, m.current_node );

				list_vm_push_last( &ast_node->text_element_nodes, &text_element );

				pc->current_node = m.current_node;

				ast_node->file_range.first = file_coord_last_start;
				UPDATE_LAST_FILE_POS();
				ast_node->file_range.last = file_coord_last_start;

				ret = true;
			}
			break;
		}
		case AST_Node_Type::EXPR_TERMINAL:
		{
			Matcher_Context m = {};
			m.current_node = pc->current_node;

			if (    start_match( &m )
			     && opt_bk( &m )
			     && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
			}

			UPDATE_LAST_FILE_POS();

			AST_Eating_Context::Partial_Context * et_pc = 0;

			if (
			     (
			          (
			               (    ast_eat_start( ctx )
			                 && ast_eat( ctx, AST_Node_Type::STRUCT_LITERAL )
			                 && ast_eat_end( ctx )
			               )
			            || (    ast_eat_start( ctx )
			                 && ast_eat( ctx, AST_Node_Type::LITERAL_REAL )
			                 && ast_eat_end( ctx )
			               )
			            || (    ast_eat_start( ctx )
			                 && ast_eat( ctx, AST_Node_Type::LITERAL_INTEGER )
			                 && ast_eat_end( ctx )
			               )
			            || (    ast_eat_start( ctx )
			                 && ast_eat( ctx, AST_Node_Type::LITERAL_STRING )
			                 && ast_eat_end( ctx )
			               )
			            || (    ast_eat_start( ctx )
			                 && ast_eat( ctx, AST_Node_Type::LITERAL_UCS4 )
			                 && ast_eat_end( ctx )
			               )
			            || (    ast_eat_start( ctx )
			                 && ast_eat( ctx, AST_Node_Type::TYPE )
			                 && ast_eat_end( ctx )
			               )
			            || (    ast_eat_start( ctx )
			                 && ast_eat( ctx, AST_Node_Type::NAME )
			                 && ast_eat_end( ctx )
			               )
			            || (    ast_eat_start( ctx )
			                 && ast_eat( ctx, AST_Node_Type::RANGE_LITERAL )
			                 && ast_eat_end( ctx )
			               )
			          )
			       && ( et_pc = last_eaten_pctx( ctx ),
			            pc->current_node = et_pc->current_node,
			            true )
			     )
			  || (    (    ( m.current_node = pc->current_node, true  )
			            && start_match( &m )
			            && opt_bk( &m )
			            && match( &m, '(' )
			            && end_match( &m )
			          )
			       && ( pc->current_node = m.current_node, true )
			       && (    ast_eat_start( ctx )
			            && ast_eat( ctx, AST_Node_Type::EXPRESSION )
			            && ast_eat_end( ctx )
			            && ( et_pc = last_eaten_pctx( ctx ),
			                 pc->current_node = et_pc->current_node,
			                 true )
			          )
			       && (    ( m.current_node = pc->current_node, true  )
			            && start_match( &m )
			            && opt_bk( &m )
			            && match( &m, ')' )
			            && end_match( &m )
			          )
			       && ( pc->current_node = m.current_node, true )
			     )
			   )
			{
				log_dbg( "%sfound expression terminal", indent );
			} else
			{
				log_dbg( "%sexpected expression terminal", indent );
				break;
			}

			AST_Node * expr_node = ast_new_node( ctx->ast );
			expr_node->type = type;
			list_vm_move_list_last( &expr_node->children, &et_pc->eaten_ast_nodes );
			list_vm_push_last( &pc->eaten_ast_nodes, &expr_node );

			expr_node->file_range.first = file_coord_last_start;
			UPDATE_LAST_FILE_POS();
			expr_node->file_range.last = file_coord_last_start;

			ret = true;

			break;
		}
		case AST_Node_Type::EXPR_POSTFIX_OP:
		{
			AST_Node_Type this_op = AST_Node_Type::UNARY_OPERATOR_POSTFIX;
			AST_Node_Type next_exp = AST_Node_Type::EXPR_TERMINAL;
			bool expr_is_this_type = false;
			if ( (    ast_eat_start( ctx )
			       && ast_eat( ctx, next_exp )
			       && ast_eat( ctx, this_op )
			       && ( expr_is_this_type = true )
			       && ast_eat_end( ctx )
			     )
			  || (    ast_eat_start( ctx )
			       && ast_eat( ctx, next_exp )
			       && ast_eat_end( ctx )
			     )
			   )
			{
				log_dbg( "%sfound %s", indent, ast_node_type_strings[(u32)type] );
			} else
			{
				log_dbg( "%sexpected %s", indent, ast_node_type_strings[(u32)type] );
				break;
			}

			AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );

			pc->current_node = lpc->current_node;

			if ( expr_is_this_type )
			{
				log_dbg( "%s expr is this type (%s)", indent, ast_node_type_strings[(u32)type] );
				AST_Node * expr_node = ast_new_node( ctx->ast );
				expr_node->type = type;
				list_vm_move_list_last( &expr_node->children, &lpc->eaten_ast_nodes );

				expr_node->file_range.first = file_coord_last_start;
				UPDATE_LAST_FILE_POS();
				expr_node->file_range.last  = file_coord_last_start;

				while (
				      ast_eat_start( ctx )
				   && ast_eat( ctx, this_op )
				   && ast_eat_end( ctx )
				      )
				{
					log_dbg( "%sfound another expr of type %s", indent, ast_node_type_strings[(u32)type] );

					AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );
					pc->current_node = lpc->current_node;

					AST_Node * left_expr_node = expr_node;
					expr_node = ast_new_node( ctx->ast );
					expr_node->type = type;

					expr_node->file_range.first = left_expr_node->file_range.first;
					UPDATE_LAST_FILE_POS();
					expr_node->file_range.last = file_coord_last_start;

					list_vm_push_last( &expr_node->children, &left_expr_node );
					list_vm_move_list_last( &expr_node->children, &lpc->eaten_ast_nodes );
				}

				list_vm_push_last( &pc->eaten_ast_nodes, &expr_node );
			} else
			{
				log_dbg( "%sexpr is not this type (%s)", indent, ast_node_type_strings[(u32)type] );
				list_vm_move_list_last( &pc->eaten_ast_nodes, &lpc->eaten_ast_nodes );
			}

			ret = true;

			break;
		}
		case AST_Node_Type::EXPR_PREFIX_OP:
		{
			AST_Node_Type this_op = AST_Node_Type::UNARY_OPERATOR_PREFIX;
			AST_Node_Type next_exp = AST_Node_Type::EXPR_STRUCT_ACCESS;
			bool expr_is_this_type = false;
			if ( (    ast_eat_start( ctx )
			       && ast_eat( ctx, this_op )
			       && ast_eat( ctx, type )
			       && ( expr_is_this_type = true )
			       && ast_eat_end( ctx )
			     )
			  || (    ast_eat_start( ctx )
			       && ast_eat( ctx, next_exp )
			       && ast_eat_end( ctx )
			     )
			   )
			{
				log_dbg( "%sfound %s", indent, ast_node_type_strings[(u32)type] );
			} else
			{
				log_dbg( "%sexpected %s", indent, ast_node_type_strings[(u32)type] );
				break;
			}

			AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );

			pc->current_node = lpc->current_node;

			if ( expr_is_this_type )
			{
				log_dbg( "%s expr is this type (%s)", indent, ast_node_type_strings[(u32)type] );
				AST_Node * expr_node = ast_new_node( ctx->ast );
				expr_node->type = type;

				expr_node->file_range.first = file_coord_last_start;
				UPDATE_LAST_FILE_POS();
				expr_node->file_range.last = file_coord_last_start;

				list_vm_move_list_last( &expr_node->children, &lpc->eaten_ast_nodes );
				list_vm_push_last( &pc->eaten_ast_nodes, &expr_node );
			} else
			{
				log_dbg( "%sexpr is not this type (%s)", indent, ast_node_type_strings[(u32)type] );
				list_vm_move_list_last( &pc->eaten_ast_nodes, &lpc->eaten_ast_nodes );
			}

			ret = true;

			break;
		}
		case AST_Node_Type::EXPR_STRUCT_ACCESS:
		case AST_Node_Type::EXPR_BINARY_OP_PREC_0:
		case AST_Node_Type::EXPR_BINARY_OP_PREC_1:
		case AST_Node_Type::EXPR_BINARY_OP_PREC_2:
		case AST_Node_Type::EXPR_BINARY_OP_PREC_3:
		case AST_Node_Type::EXPR_BINARY_OP_PREC_4:
		case AST_Node_Type::EXPR_BINARY_OP_PREC_5:
		case AST_Node_Type::EXPR_BINARY_OP_PREC_6:
		case AST_Node_Type::EXPR_BINARY_OP_PREC_7:
		case AST_Node_Type::EXPR_BINARY_OP_PREC_8:
		case AST_Node_Type::EXPR_BINARY_OP_PREC_9:
		case AST_Node_Type::EXPR_BINARY_OP_PREC_10:
		case AST_Node_Type::EXPR_BINARY_OP_PREC_11:
		case AST_Node_Type::EXPR_BINARY_OP_PREC_13:
		{
			AST_Node_Type this_bop =
			type == AST_Node_Type::EXPR_STRUCT_ACCESS
			?
			AST_Node_Type::STRUCT_MEMBER_ACCESS_OP
			:
			AST_Node_Type( u32( AST_Node_Type::BINARY_OPERATOR_PREC_0 )
			             + ( u32( type ) - u32( AST_Node_Type::EXPR_BINARY_OP_PREC_0 ) ) );
			AST_Node_Type next_bexp = AST_Node_Type( u32( type ) - 1 );

			tg_assert( ( type      != AST_Node_Type::EXPR_BINARY_OP_PREC_0 )
			        || ( next_bexp == AST_Node_Type::EXPR_PREFIX_OP ) );
			tg_assert( ( type      != AST_Node_Type::EXPR_STRUCT_ACCESS )
			        || ( next_bexp ==
			                          AST_Node_Type::EXPR_POSTFIX_OP
			           ) );

			bool expr_is_this_type = false;

			// NOTE(theGiallo): left to right associativity

			#if 0
			if ( (    ast_eat_start( ctx )
			       && ast_eat( ctx, next_bexp )
			       && ast_eat( ctx, this_bop )
			       && ast_eat( ctx, type )
			       && ( expr_is_this_type = true )
			       && ast_eat_end( ctx )
			       )
			  || (    ast_eat_start( ctx )
			       && ast_eat( ctx, next_bexp )
			       && ast_eat_end( ctx ) )
			   )
			#else
			if (    ast_eat_start( ctx )
			     && ast_eat( ctx, next_bexp )
			     && ast_eat_or_pre( ctx )
			     && (    (    ast_eat( ctx, this_bop )
			               && ast_eat( ctx, next_bexp )
			               && ( expr_is_this_type = true )
			             )
			          || true
			        )
			     && ast_eat_or_post( ctx )
			     && ast_eat_end( ctx )
			   )
			#endif
			{
				log_dbg( "%sfound %s", indent, ast_node_type_strings[(u32)type] );
			} else
			{
				log_dbg( "%sexpected %s", indent, ast_node_type_strings[(u32)type] );
				break;
			}

			AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );

			pc->current_node = lpc->current_node;

			if ( expr_is_this_type )
			{
				log_dbg( "%sexpr is this type %s", indent, ast_node_type_strings[(u32)type] );
				AST_Node * expr_node = ast_new_node( ctx->ast );
				expr_node->type = type;
				list_vm_move_list_last( &expr_node->children, &lpc->eaten_ast_nodes );

				expr_node->file_range.first = file_coord_last_start;
				UPDATE_LAST_FILE_POS();
				expr_node->file_range.last = file_coord_last_start;

				while (
				      ast_eat_start( ctx )
				   && ast_eat( ctx, this_bop )
				   && ast_eat( ctx, next_bexp )
				   && ast_eat_end( ctx )
				      )
				{
					log_dbg( "%sfound another expr of type %s", indent, ast_node_type_strings[(u32)type] );

					AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );
					pc->current_node = lpc->current_node;

					AST_Node * left_expr_node = expr_node;
					expr_node = ast_new_node( ctx->ast );
					expr_node->type = type;

					expr_node->file_range.first = left_expr_node->file_range.first;
					UPDATE_LAST_FILE_POS();
					expr_node->file_range.last = file_coord_last_start;

					list_vm_push_last( &expr_node->children, &left_expr_node );
					list_vm_move_list_last( &expr_node->children, &lpc->eaten_ast_nodes );
				}

				list_vm_push_last( &pc->eaten_ast_nodes, &expr_node );
			} else
			{
				log_dbg( "%sexpr is not this type %s", indent, ast_node_type_strings[(u32)type] );
				list_vm_move_list_last( &pc->eaten_ast_nodes, &lpc->eaten_ast_nodes );
			}

			ret = true;

			break;
		}
		case AST_Node_Type::EXPR_BINARY_OP_PREC_12:
		{
			AST_Node_Type this_bop =
			AST_Node_Type( u32( AST_Node_Type::BINARY_OPERATOR_PREC_0 )
			             + ( u32( type ) - u32( AST_Node_Type::EXPR_BINARY_OP_PREC_0 ) ) );
			AST_Node_Type next_bexp = AST_Node_Type( u32( type ) - 1 );

			tg_assert( ( type != AST_Node_Type::EXPR_BINARY_OP_PREC_0 )
			        || next_bexp == AST_Node_Type::EXPR_PREFIX_OP );

			bool expr_is_this_type = false;

			// NOTE(theGiallo): right to left associativity

			#if 0
			if ( (    ast_eat_start( ctx )
			       && ast_eat( ctx, next_bexp )
			       && ast_eat( ctx, this_bop )
			       && ast_eat( ctx, type )
			       && ( expr_is_this_type = true )
			       && ast_eat_end( ctx )
			       )
			  || (    ast_eat_start( ctx )
			       && ast_eat( ctx, next_bexp )
			       && ast_eat_end( ctx ) )
			   )
			#else
			if (    ast_eat_start( ctx )
			     && ast_eat( ctx, next_bexp )
			     && ast_eat_or_pre( ctx )
			     && (    (    ast_eat( ctx, this_bop )
			               && ast_eat( ctx, type )
			               && ( expr_is_this_type = true )
			             )
			          || true
			        )
			     && ast_eat_or_post( ctx )
			     && ast_eat_end( ctx )
			   )
			#endif
			{
				log_dbg( "%sfound %s", indent, ast_node_type_strings[(u32)type] );
			} else
			{
				log_dbg( "%sexpected %s", indent, ast_node_type_strings[(u32)type] );
				break;
			}

			AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );

			pc->current_node = lpc->current_node;

			if ( expr_is_this_type )
			{
				log_dbg( "%sexpr is this type %s", indent, ast_node_type_strings[(u32)type] );
				AST_Node * expr_node = ast_new_node( ctx->ast );
				expr_node->type = type;

				expr_node->file_range.first = file_coord_last_start;
				UPDATE_LAST_FILE_POS();
				expr_node->file_range.last = file_coord_last_start;

				list_vm_move_list_last( &expr_node->children, &lpc->eaten_ast_nodes );

				list_vm_push_last( &pc->eaten_ast_nodes, &expr_node );
			} else
			{
				log_dbg( "%sexpr is not this type %s", indent, ast_node_type_strings[(u32)type] );
				list_vm_move_list_last( &pc->eaten_ast_nodes, &lpc->eaten_ast_nodes );
			}

			ret = true;

			break;
		}
		case AST_Node_Type::EXPR_TERNARY_CONDITIONAL:
		{
			AST_Node_Type next_bexp = AST_Node_Type( u32( type ) - 1 );

			bool expr_is_this_type = false;

			// NOTE(theGiallo): right to left associativity

			#if 0
			if ( (    ast_eat_start( ctx )
			       && ast_eat( ctx, next_bexp )
			       && ast_eat( ctx, this_bop )
			       && ast_eat( ctx, type )
			       && ( expr_is_this_type = true )
			       && ast_eat_end( ctx )
			       )
			  || (    ast_eat_start( ctx )
			       && ast_eat( ctx, next_bexp )
			       && ast_eat_end( ctx ) )
			   )
			#else
			if (    ast_eat_start( ctx )
			     && ast_eat( ctx, next_bexp )
			     && ast_eat_or_pre( ctx )
			     && (    (    ast_eat( ctx, AST_Node_Type::TERNARY_OPERATOR_0 )
			               && ast_eat( ctx, AST_Node_Type::EXPRESSION, {.expression_column_after = 1} )
			               && ast_eat( ctx, AST_Node_Type::TERNARY_OPERATOR_1 )
			               && ast_eat( ctx, type )
			               && ( expr_is_this_type = true )
			             )
			          || true
			        )
			     && ast_eat_or_post( ctx )
			     && ast_eat_end( ctx )
			   )
			#endif
			{
				log_dbg( "%sfound %s", indent, ast_node_type_strings[(u32)type] );
			} else
			{
				log_dbg( "%sexpected %s", indent, ast_node_type_strings[(u32)type] );
				break;
			}

			AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );

			pc->current_node = lpc->current_node;

			if ( expr_is_this_type )
			{
				log_dbg( "%sexpr is this type %s", indent, ast_node_type_strings[(u32)type] );
				AST_Node * expr_node = ast_new_node( ctx->ast );
				expr_node->type = type;
				list_vm_move_list_last( &expr_node->children, &lpc->eaten_ast_nodes );

				expr_node->file_range.first = file_coord_last_start;
				UPDATE_LAST_FILE_POS();
				expr_node->file_range.last = file_coord_last_start;

				list_vm_push_last( &pc->eaten_ast_nodes, &expr_node );
			} else
			{
				log_dbg( "%sexpr is not this type %s", indent, ast_node_type_strings[(u32)type] );
				list_vm_move_list_last( &pc->eaten_ast_nodes, &lpc->eaten_ast_nodes );
			}

			ret = true;

			break;
		}
		case AST_Node_Type::EXPRESSION:
		{
			if (    ast_eat_start( ctx )
			     && ast_eat( ctx, AST_Node_Type::EXPR_BINARY_OP_PREC_13 )
			     && ast_eat_end( ctx )
			   )
			{
				log_dbg( "%sfound expression", indent );
			} else
			{
				log_dbg( "%sexpected expression", indent );
				break;
			}

			AST_Eating_Context::Partial_Context * e_pc = last_eaten_pctx( ctx );

			pc->current_node = e_pc->current_node;
			Matcher_Context m = {};
			m.current_node = pc->current_node;

			AST_Node * expr_node = ast_new_node( ctx->ast );
			expr_node->type = AST_Node_Type::EXPRESSION;

			expr_node->file_range.first = file_coord_last_start;
			UPDATE_LAST_FILE_POS();
			expr_node->file_range.last = file_coord_last_start;

			list_vm_move_list_last( &expr_node->children, &e_pc->eaten_ast_nodes );
			list_vm_push_last( &pc->eaten_ast_nodes, &expr_node );

			if (   flags.expression_semicolon_needed == 1
			    && start_match( &m )
			    && opt_bk( &m )
			    && match( &m, ';' )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;

				UPDATE_LAST_FILE_POS();
				expr_node->file_range.last = file_coord_last_start;

				log_dbg( "%sfound ';'", indent );
				ret = true;
			} else
			if (   flags.expression_column_after == 1
			    && start_match( &m )
			    && opt_bk( &m )
			    && match( &m, ':' )
			    && end_match( &m )
			   )
			{
				log_dbg( "%sfound ':'", indent );
				ret = true;
			} else
			if (   flags.expression_square_br_after == 1
			    && start_match( &m )
			    && opt_bk( &m )
			    && match( &m, ']' )
			    && end_match( &m )
			   )
			{
				log_dbg( "%sfound ']'", indent );
				ret = true;
			} else
			{
				ret = flags.expression_semicolon_needed == 0;
			}

			break;
		}
		case AST_Node_Type::TYPE:
		{
			AST_Node * type_node = NULL;
			bool is_bitfield = false;
			Matcher_Context m = {};
			m.current_node = pc->current_node;

			if (    ast_eat_start( ctx )
			     && ast_eat( ctx, AST_Node_Type::NAME )
			     && ast_eat_end( ctx ) )
			{
				AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );
				type_node = ast_new_node( ctx->ast );
				type_node->type = AST_Node_Type::TYPE;

				pc->current_node = lpc->current_node;

				type_node->file_range.first = file_coord_last_start;
				UPDATE_LAST_FILE_POS();
				type_node->file_range.last = file_coord_last_start;

				Text_Piece * tp = &lpc->eaten_ast_nodes.first->element->text_element_nodes.first->element.text;
				if ( !string_equal( tp->text, tp->length, "typeof" ) )
				{
					list_vm_push_last( &type_node->children, &lpc->eaten_ast_nodes.first->element );
					log_dbg( "%sfound name (type?)", indent );
				} else
				{
					log_dbg( "%sfound 'typeof'", indent );

					Matcher_Context m = {};
					m.current_node = pc->current_node;

					if ( !(    start_match( &m )
					        && opt_bk( &m )
					        && match( &m, '(' )
					        && end_match( &m ) )
					   )
					{
						log_dbg( "%sexpected '('", indent );
						break;
					}
					log_dbg( "%sfound '('", indent );
					pc->current_node = m.current_node;

					if (    ast_eat_start( ctx )
					     && ast_eat( ctx, AST_Node_Type::EXPRESSION )
					     && ast_eat_end( ctx )
					   )
					{
						AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );
						AST_Node * typeof_node = ast_new_node( ctx->ast );
						typeof_node->type = AST_Node_Type::TYPEOF;
						typeof_node->file_range = lpc->eaten_ast_nodes.first->element->file_range;
						list_vm_push_last( &typeof_node->children, &lpc->eaten_ast_nodes.first->element );
						list_vm_push_last( &type_node->children, &typeof_node );

						m.current_node = pc->current_node = lpc->current_node;
						log_dbg( "%sfound type", indent );
					} else
					{
						log_dbg( "%sexpected type", indent );
						break;
					}

					if ( !(    start_match( &m )
					        && opt_bk( &m )
					        && match( &m, ')' )
					        && end_match( &m ) )
					   )
					{
						log_dbg( "%sexpected ')'", indent );
						break;
					}
					log_dbg( "%sfound ')'", indent );
					pc->current_node = m.current_node;

					UPDATE_LAST_FILE_POS();
					type_node->file_range.last = file_coord_last_start;
				}
			} else
			if ( flags.type_allow_only_typenames )
			{
				log_dbg( "%sallowing only typenames and none found", indent );
				break;
			} else
			if (    ast_eat_start( ctx )
			     && ast_eat( ctx, AST_Node_Type::BITFIELD_ANONYMOUS )
			     && ast_eat_end( ctx ) )
			{
				AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );
				type_node = ast_new_node( ctx->ast );
				type_node->type = AST_Node_Type::TYPE;

				pc->current_node = lpc->current_node;

				type_node->file_range.first = file_coord_last_start;
				UPDATE_LAST_FILE_POS();
				type_node->file_range.last = file_coord_last_start;

				list_vm_push_last( &type_node->children, &lpc->eaten_ast_nodes.first->element );
				log_dbg( "%sfound anonymous bitfield", indent );
				is_bitfield = true;
			} else
			if (    start_match( &m )
			     && opt_bk( &m )
			     && match( &m, '(' )
			     && end_match( &m )
			   )
			{
				log_dbg( "%sfound '('", indent );
				pc->current_node = m.current_node;

				AST_Node * in_type = 0;
				if (    ast_eat_start( ctx )
				     && ast_eat( ctx, AST_Node_Type::TYPE )
				     && ast_eat_end( ctx )
				   )
				{
					AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );
					in_type = lpc->eaten_ast_nodes.first->element;

					m.current_node = pc->current_node = lpc->current_node;
					log_dbg( "%sfound in type", indent );
				} else
				{
					log_dbg( "%sexpected in type", indent );
					goto TYPE_break_switch;
				}

				if ( !(    start_match( &m )
				        && opt_bk( &m )
				        && match( &m, '-' )
				        && match( &m, '>' )
				        && end_match( &m ) )
				   )
				{
					log_dbg( "%sexpected '->'", indent );
					goto TYPE_break_switch;
				}
				pc->current_node = m.current_node;
				log_dbg( "%sfound '->'", indent );

				AST_Node * out_type = 0;
				if (    ast_eat_start( ctx )
				     && ast_eat( ctx, AST_Node_Type::TYPE )
				     && ast_eat_end( ctx )
				   )
				{
					AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );
					out_type = lpc->eaten_ast_nodes.first->element;

					m.current_node = pc->current_node = lpc->current_node;
					log_dbg( "%sfound out type", indent );
				} else
				{
					log_dbg( "%sexpected out type", indent );
					goto TYPE_break_switch;
				}

				if ( !(    start_match( &m )
				        && opt_bk( &m )
				        && match( &m, ')' )
				        && end_match( &m ) )
				   )
				{
					log_dbg( "%sexpected ')'", indent );
					goto TYPE_break_switch;
				}
				pc->current_node = m.current_node;
				log_dbg( "%sfound ')'", indent );

				type_node = ast_new_node( ctx->ast );
				type_node->type = AST_Node_Type::TYPE;
				AST_Node * fn_type_node = ast_new_node( ctx->ast );
				fn_type_node->type = AST_Node_Type::FUNCTION_TYPE;

				type_node   ->file_range.first = file_coord_last_start;
				fn_type_node->file_range.first = file_coord_last_start;
				UPDATE_LAST_FILE_POS();
				type_node   ->file_range.last = file_coord_last_start;
				fn_type_node->file_range.last = file_coord_last_start;

				list_vm_push_first( &fn_type_node->children, &in_type );
				list_vm_push_last( &fn_type_node->children, &out_type );
				list_vm_push_last( &type_node->children, &fn_type_node );
			} else
			if (    start_match( &m )
			     && opt_bk( &m )
			     && match( &m, '{' )
			     && end_match( &m )
			   )
			{
				log_dbg( "%sfound '{'", indent );
				pc->current_node = m.current_node;

				if (    ast_eat_start( ctx )
				     && ast_eat( ctx, AST_Node_Type::STRUCT_BODY )
				     && ast_eat_end( ctx )
				   )
				{
					AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );
					type_node = ast_new_node( ctx->ast );
					type_node->type = AST_Node_Type::TYPE;
					list_vm_push_last( &type_node->children, &lpc->eaten_ast_nodes.first->element );

					m.current_node = pc->current_node = lpc->current_node;
					log_dbg( "%sfound struct body", indent );
				} else
				{
					log_dbg( "%sexpected struct body", indent );
					goto TYPE_break_switch;
				}

				if (    start_match( &m )
				     && opt_bk( &m )
				     && match( &m, '}' )
				     && end_match( &m )
				   )
				{
					pc->current_node = m.current_node;

					type_node->file_range.first = file_coord_last_start;
					UPDATE_LAST_FILE_POS();
					type_node->file_range.last = file_coord_last_start;

					log_dbg( "%sfound '}'", indent );
				} else
				{
					log_dbg( "%sexpected '}'", indent );
					goto TYPE_break_switch;
				}
			} else
			{
				log_dbg( "%sno valid type found", indent );
				goto TYPE_break_switch;
			}

			m.current_node = pc->current_node;

			if ( !is_bitfield )
			{
				for (;;)
				{
					Text_Element_List_VM_Node * start_teln = m.current_node;
					if (    start_match( &m )
					     && opt_bk( &m )
					     && match( &m, ':' )
					     && match( &m, ':' )
					     && end_match( &m )
					   )
					{
						pc->current_node = m.current_node;
						Text_Element_List_VM_Node * stop_teln = m.current_node;
						log_dbg( "%sfound '::'", indent );

						AST_Eating_Context::Partial_Context * lpc = NULL;

						if (    ast_eat_start( ctx )
						     && ast_eat( ctx, AST_Node_Type::NAME )
						     && ast_eat_end( ctx )
						   )
						{
							log_dbg( "%sfound NAME", indent );

							lpc = last_eaten_pctx( ctx );
							m.current_node = pc->current_node = lpc->current_node;
						} else
						{
							log_dbg( "%sexpected NAME", indent );
							goto TYPE_break_switch;
						}

						AST_Node * type_access_node = ast_new_node( ctx->ast );
						type_access_node->type = AST_Node_Type::TYPE_ACCESS;
						Text_Element text_element = ast_merge_text_elements_until( ctx->ast, start_teln, stop_teln );
						list_vm_push_last( &type_access_node->text_element_nodes, &text_element );

						AST_Node * node_to_push_below = type_node->children.first->element;
						list_vm_remove_first( &type_node->children );
						list_vm_push_first( &type_access_node->children, &node_to_push_below );
						list_vm_push_last( &type_access_node->children, &lpc->eaten_ast_nodes.first->element );
						list_vm_push_first( &type_node->children, &type_access_node );

						type_access_node->file_range.first = node_to_push_below->file_range.first;
						type_access_node->file_range.last = lpc->eaten_ast_nodes.first->element->file_range.last;
						type_node->file_range.last = type_access_node->file_range.last;
					} else
					{
						break;
					}
				}
			}

			for (;;)
			{
				AST_Node * op_node = NULL;
				Text_Element_List_VM_Node * start_node = pc->current_node;
				if (    start_match( &m )
				     && opt_bk( &m )
				     && match( &m, '[' )
				     && end_match( &m )
				   )
				{
					pc->current_node = m.current_node;
					log_dbg( "%sfound '['", indent );

					AST_Eating_Context::Partial_Context * lpc = NULL;

					if (    ast_eat_start( ctx )
					     && ast_eat( ctx, AST_Node_Type::EXPRESSION, { .expression_square_br_after = 1 } )
					     && ast_eat_end( ctx )
					   )
					{
						log_dbg( "%sfound EXPRESSION", indent );

						lpc = last_eaten_pctx( ctx );
						m.current_node = pc->current_node = lpc->current_node;
					}

					if ( ! (    start_match( &m )
					         && opt_bk( &m )
					         && match( &m, ']' )
					         && end_match( &m )
					       )
					   )
					{
						log_dbg( "%sexpecting ']' found %s", indent, text_element_type_strings[(u32)pc->current_node->element.type] );
						put_bytes( pc->current_node->element.text.text,
						           pc->current_node->element.text.length );
						putchar( '\n' );
						//goto TYPE_break_switch;
						pc->current_node = start_node;
						break;
					}
					pc->current_node = m.current_node;
					log_dbg( "%sfound ']'", indent );

					op_node = ast_new_node( ctx->ast );
					op_node->type = AST_Node_Type::TYPE_ARRAY;

					if ( lpc )
					{
						list_vm_move_list_last( &op_node->children, &lpc->eaten_ast_nodes );
					}

				} else
				if (    start_match( &m )
				     && opt_bk( &m )
				     && match( &m, '<' )
				     && match( &m, '|' )
				     && end_match( &m )
				   )
				{
					op_node = ast_new_node( ctx->ast );
					op_node->type = AST_Node_Type::TYPE_REFERENCE;

					Text_Element text_element = ast_merge_text_elements_until( ctx->ast, pc->current_node, m.current_node );
					list_vm_push_last( &op_node->text_element_nodes, &text_element );

					pc->current_node = m.current_node;
				} else
				{
					break;
				}

				AST_Node * node_to_push_below = type_node->children.first->element;
				list_vm_remove_first( &type_node->children );
				list_vm_push_last( &op_node->children, &node_to_push_below );
				list_vm_push_first( &type_node->children, &op_node );

				op_node->file_range.first = node_to_push_below->file_range.first;
				UPDATE_LAST_FILE_POS();
				op_node->file_range.last = file_coord_last_start;
			}

			list_vm_push_last( &pc->eaten_ast_nodes, &type_node );
			ret = true;
			TYPE_break_switch:void(0);
			break;
		}
		case AST_Node_Type::VARIABLE_DECLARATION:
		case AST_Node_Type::CONSTANT_DECLARATION:
		{
			Matcher_Context m = {};
			m.current_node = pc->current_node;
			if ( type == AST_Node_Type::CONSTANT_DECLARATION )
			{
				if ( start_match( &m )
				  && opt_bk( &m )
				  && match_word( &m, "const" )
				  && end_match( &m )
				   )
				{
					pc->current_node = m.current_node;
					log_dbg( "%sfound 'const'", indent );
				} else
				{
					log_dbg( "%sexpected 'const'", indent );
					break;
				}
			}

			AST_Node_Type assignment_type    = AST_Node_Type::EXPR_BINARY_OP_PREC_12;
			AST_Node_Type assignment_op_type = AST_Node_Type::BINARY_OPERATOR_PREC_12;
			if (    ast_eat_start( ctx )
			     && ast_eat( ctx, AST_Node_Type::TYPE )
			     && ast_eat( ctx, assignment_type )
			     && ast_eat_end( ctx ) )
			{
				AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );
				AST_Node * expr = lpc->eaten_ast_nodes.first->next->element;
				AST_Node * node_name       = 0;
				AST_Node * node_assignment = 0;
				AST_Node * node_value      = 0;
				log_dbg( "%stype = %s", indent,
				         ast_node_type_strings[u32(expr->type)] );
				if (    expr->type == AST_Node_Type::EXPR_TERMINAL
				     && ast_node_child_is_identifier( expr ) )
				{
					log_dbg( "%sfound only name after type", indent );
					node_name = expr->children.first->element;
				} else
				if ( expr->type == assignment_type
				  && expr->children.first
				  && expr->children.first->element->type == AST_Node_Type::EXPR_TERMINAL
				  && ast_node_child_is_identifier( expr->children.first->element ) )
				{
					log_dbg( "%sfound name and assignment after type", indent );
					node_assignment = expr;
					node_name = node_assignment->children.first->element;
					log_dbg( "%stype = %s", indent,
					         ast_node_type_strings[u32(node_assignment->children.first->next->element->type)] );
					tg_assert( node_assignment->children.first->next->element->type == AST_Node_Type::BINARY_OPERATOR_PREC_12 );
					tg_assert( node_assignment->children.first->next->element->children.first->element->type == AST_Node_Type::ASSIGNMENT_OP );
					node_value = node_assignment->children.first->next->next->element;
				} else
				{
					log_dbg( "%sno variable name or assignment after type", indent );
					break;
				}
				AST_Node * var_node = ast_new_node( ctx->ast );
				var_node->type = type;
				AST_Node * type_node = lpc->eaten_ast_nodes.first->element;
				list_vm_push_last( &var_node->children, &type_node );
				list_vm_push_last( &var_node->children, &expr );
				list_vm_push_last( &pc->eaten_ast_nodes, &var_node );
				var_node->file_range = node_name->file_range;
				if ( node_assignment )
				{
					var_node->file_range.last = node_assignment->file_range.last;
				}

				pc->current_node = lpc->current_node;
				m.current_node = pc->current_node;
				log_dbg( "%sfound type and name", indent );

				while (    type_node
				        && start_match( &m )
				        && opt_bk( &m )
				        && match( &m, ',' )
				        && end_match( &m )
				      )
				{
					log_dbg( "%sfound comma between variables", indent );
					pc->current_node = m.current_node;

					if (    ast_eat_start( ctx )
					     && ast_eat( ctx, assignment_type )
					     && ast_eat_end( ctx ) )
					{
						AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );
						AST_Node * expr = lpc->eaten_ast_nodes.first->element;
						AST_Node * node_name       = 0;
						AST_Node * node_assignment = 0;
						AST_Node * node_value      = 0;

						log_dbg( "%stype = %s", indent,
						         ast_node_type_strings[u32(expr->type)] );

						if ( expr->type == AST_Node_Type::EXPR_TERMINAL
						     && ast_node_child_is_identifier( expr ) )
						{
							log_dbg( "%sfound only name after type", indent );
							node_name = expr->children.first->element;
						} else
						if ( expr->type == assignment_type
						  && expr->children.first
						  && expr->children.first->element->type == AST_Node_Type::EXPR_TERMINAL
						  && ast_node_child_is_identifier( expr->children.first->element ) )
						{
							log_dbg( "%sfound name and assignment after type", indent );
							node_assignment = expr;
							node_name = node_assignment->children.first->element;
							log_dbg( "%stype = %s", indent,
							         ast_node_type_strings[u32(node_assignment->children.first->next->element->type)] );
							tg_assert( node_assignment->children.first->next->element->type == assignment_op_type );
							tg_assert( node_assignment->children.first->next->element->children.first->element->type == AST_Node_Type::ASSIGNMENT_OP );
							node_value = node_assignment->children.first->next->next->element;
						} else
						{
							log_dbg( "%sno variable name or assignment after type", indent );
							break;
						}

						AST_Node * var_node = ast_new_node( ctx->ast );
						// TODO(theGiallo, 2017-10-23): should we make a copy of
						// type_node avoiding the tree to be a graph?
						var_node->type = type;
						list_vm_push_last( &var_node->children, &type_node );
						list_vm_push_last( &var_node->children, &expr );
						list_vm_push_last( &pc->eaten_ast_nodes, &var_node );

						var_node->file_range = node_name->file_range;
						if ( node_assignment )
						{
							var_node->file_range.last = node_assignment->file_range.last;
						}

						pc->current_node = lpc->current_node;
						m.current_node = pc->current_node;
						log_dbg( "%sfound 1 name", indent );
					} else
					{
						goto VARIABLE_DECLARATION_break_switch;
					}
				}
				if (    start_match( &m )
				     && opt_bk( &m )
				     && match( &m, ';' )
				     && end_match( &m )
				   )
				{
					log_dbg( "%sfound semicolon between arguments", indent );
					pc->current_node = m.current_node;
					type_node = NULL;
					ret = true;
				} else
				if (    flags.variable_declaration_semicolon_is_optional
				     && start_match( &m )
				     && opt_bk( &m )
				     && match( &m, '}' )
				     && end_match( &m )
				   )
				{
					// NOTE(theGiallo): we don't eat the '}'
					ret = true;
					log_dbg( "%sclosed curly ahead", indent );
				} else
				{
					log_dbg( "%sexpected semicolon", indent );
				}
			}
			VARIABLE_DECLARATION_break_switch:
			break;
		}
		case AST_Node_Type::STRUCT_BODY:
		{
			AST_Node * struct_body_node = ast_new_node( ctx->ast );
			struct_body_node->type = AST_Node_Type::STRUCT_BODY;
			list_vm_push_last( &pc->eaten_ast_nodes, &struct_body_node );

			Matcher_Context m = {};
			m.current_node = pc->current_node;

			for ( s32 num_decls = 0; ; ++num_decls )
			{
				if (    ast_eat_start( ctx )
				     && ast_eat( ctx, AST_Node_Type::VARIABLE_DECLARATION, {.variable_declaration_semicolon_is_optional = 1 } )
				     && ast_eat_end( ctx ) )
				{
					AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );
					list_vm_move_list_last( &struct_body_node->children, &lpc->eaten_ast_nodes );

					pc->current_node = lpc->current_node;
					m.current_node = pc->current_node;
					log_dbg( "%sfound variable declaration(s)", indent );
					continue;
				} else
				if ( (    ast_eat_start( ctx )
				       && ast_eat( ctx, AST_Node_Type::USING )
				       && ast_eat_end( ctx ) )
				  || (    ast_eat_start( ctx )
				       && ast_eat( ctx, AST_Node_Type::STRUCT_DECLARATION )
				       && ast_eat_end( ctx ) )
				  || (    ast_eat_start( ctx )
				       && ast_eat( ctx, AST_Node_Type::BITFIELD_DECLARATION )
				       && ast_eat_end( ctx ) )
				  || (    ast_eat_start( ctx )
				       && ast_eat( ctx, AST_Node_Type::UNION_DECLARATION )
				       && ast_eat_end( ctx ) )
				  || (    ast_eat_start( ctx )
				       && ast_eat( ctx, AST_Node_Type::UNION_ANONYMOUS )
				       && ast_eat_end( ctx ) )
				  || (    ast_eat_start( ctx )
				       && ast_eat( ctx, AST_Node_Type::ENUM_DECLARATION )
				       && ast_eat_end( ctx ) )
				   )
				{
					AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );
					list_vm_move_list_last( &struct_body_node->children, &lpc->eaten_ast_nodes );

					pc->current_node = lpc->current_node;
					m.current_node = pc->current_node;
					if ( struct_body_node->children.last->element->type == AST_Node_Type::USING )
					{
						log_dbg( "%sfound 'using' expression", indent );
					} else
					{
						log_dbg( "%sfound inner struct declaration", indent );
					}
					--num_decls;
					continue;
				}
				if ( flags.struct_body_allow_anonymous_structs )
				{
					bool got_error = false;
					do
					{
						if ( start_match( &m )
						  && opt_bk( &m )
						  && match( &m, '{' )
						  && end_match( &m ) )
						{
							log_dbg( "%sfound '{'", indent );
							pc->current_node = m.current_node;
						} else
						{
							log_dbg( "%sexpected '{'", indent );
							got_error = true;
							break;
						}

						if ( ast_eat_start( ctx )
						  && ast_eat( ctx, AST_Node_Type::STRUCT_BODY )
						  && ast_eat_end( ctx ) )
						{
							log_dbg( "%sfound struct body", indent );

							AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );
							list_vm_move_list_last( &struct_body_node->children, &lpc->eaten_ast_nodes );

							pc->current_node = lpc->current_node;
							m.current_node = pc->current_node;
						} else
						{
							log_dbg( "%sexpected struct body", indent );
							got_error = true;
							break;
						}

						if ( start_match( &m )
						  && opt_bk( &m )
						  && match( &m, '}' )
						  && end_match( &m ) )
						{
							log_dbg( "%sfound '}'", indent );
							pc->current_node = m.current_node;
						} else
						{
							log_dbg( "%sexpected '}'", indent );
							got_error = true;
							break;
						}
					} while ( false );
					if ( !got_error )
					{
						continue;
					}
				}


				struct_body_node->file_range.first = file_coord_last_start;
				UPDATE_LAST_FILE_POS();
				struct_body_node->file_range.last = file_coord_last_start;

				// NOTE(theGiallo): reach here if error
				log_dbg( "%send of struct body parsing", indent );
				ret = num_decls != 0;
				break;
			}
			break;
		}
		case AST_Node_Type::LABEL:
		{
			Matcher_Context m = {};
			m.current_node = pc->current_node;

			if (   start_match( &m )
			    && opt_bk( &m )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
			}

			UPDATE_LAST_FILE_POS();

			if (   start_match( &m )
			    && match( &m, '^' )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
				log_dbg( "%sfound '^'", indent );
			} else
			{
				log_dbg( "%sexpected '^'", indent );
				break;
			}

			if ( pc->current_node->element.type != Text_Element_Type::WORD )
			{
				log_dbg( "%sexpected word after '^'", indent );
				break;
			}

			if ( ast_eat_start( ctx )
			  && ast_eat( ctx, AST_Node_Type::NAME )
			  && ast_eat_end( ctx ) )
			{
				log_dbg( "%sfound name", indent );
			} else
			{
				log_dbg( "%sexpected name", indent );
				break;
			}

			AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );
			pc->current_node = lpc->current_node;

			AST_Node * label_node = ast_new_node( ctx->ast );
			label_node->type = type;

			label_node->file_range.first = file_coord_last_start;
			UPDATE_LAST_FILE_POS();
			label_node->file_range.last = file_coord_last_start;

			list_vm_push_last( &label_node->children, &lpc->eaten_ast_nodes.first->element );
			list_vm_push_last( &pc->eaten_ast_nodes, &label_node );

			ret = true;

			break;
		}
		case AST_Node_Type::BREAK:
		case AST_Node_Type::CONTINUE:
		{
			if ( ast_eat_start( ctx )
			  && ast_eat( ctx, AST_Node_Type::NAME, {.name_allow_keywords = 1} )
			  && ast_eat_end( ctx ) )
			{
				log_dbg( "%sfound name", indent );
			} else
			{
				log_dbg( "%sexpected name", indent );
				break;
			}

			AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );
			const char * construct_string = type == AST_Node_Type::BREAK ? "break" : "continue";

			Text_Piece * name = &lpc->eaten_ast_nodes.first->element->text_element_nodes.first->element.text;
			if ( !string_equal(
			        (char*)name->text,
			        name->length,
			        construct_string )
			   )
			{
				char name_buf[512] = {};
				tg_assert( sizeof( name_buf ) > name->length );
				memcpy( name_buf, name->text, name->length );
				log_dbg( "%sexpected '%s' found '%s'", indent, construct_string, name_buf );
				break;
			}

			pc->current_node = lpc->current_node;

			AST_Node * break_node = ast_new_node( ctx->ast );
			break_node->type = type;

			if (    ast_eat_start( ctx )
			     && ast_eat( ctx, AST_Node_Type::LABEL )
			     && ast_eat_end( ctx ) )
			{
				log_dbg( "%sfound label", indent );
				list_vm_push_first( &break_node->children, &lpc->eaten_ast_nodes.first->element );
			} else
			{
				log_dbg( "%sno label found", indent );
			}

			lpc = last_eaten_pctx( ctx );

			pc->current_node = lpc->current_node;

			Matcher_Context m = {};
			m.current_node = pc->current_node;

			if (   start_match( &m )
			    && opt_bk( &m )
			    && match( &m, ';' )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
				log_dbg( "%sfound ';'", indent );
			} else
			{
				log_dbg( "%sexpected ';'", indent );
				break;
			}

			list_vm_push_last( &pc->eaten_ast_nodes, &break_node );

			break_node->file_range.first = file_coord_last_start;
			UPDATE_LAST_FILE_POS();
			break_node->file_range.last = file_coord_last_start;

			ret = true;

			break;
		}
		case AST_Node_Type::RETURN:
		{
			if ( ast_eat_start( ctx )
			  && ast_eat( ctx, AST_Node_Type::NAME, {.name_allow_keywords = 1} )
			  && ast_eat_end( ctx ) )
			{
				log_dbg( "%sfound name", indent );
			} else
			{
				log_dbg( "%sexpected name", indent );
				break;
			}

			AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );

			Text_Piece * name = &lpc->eaten_ast_nodes.first->element->text_element_nodes.first->element.text;
			if ( !string_equal(
			        "return",
			        strlen( "return" ),
			        (char*)name->text,
			        name->length ) )
			{
				char name_buf[512] = {};
				tg_assert( sizeof( name_buf ) > name->length );
				memcpy( name_buf, name->text, name->length );
				log_dbg( "%sexpected return found '%s'", indent, name_buf );
				break;
			}

			pc->current_node = lpc->current_node;

			if ( ! ( ast_eat_start( ctx )
			      && ast_eat( ctx, AST_Node_Type::EXPRESSION, {.expression_semicolon_needed = 1} )
			      && ast_eat_end( ctx ) ) )
			{
				log_dbg( "%sexpected return expression", indent );
				break;
			}

			lpc = last_eaten_pctx( ctx );

			pc->current_node = lpc->current_node;

			AST_Node * return_node = ast_new_node( ctx->ast );
			return_node->type = type;
			list_vm_move_list_last( &return_node->children, &lpc->eaten_ast_nodes );
			list_vm_push_last( &pc->eaten_ast_nodes, &return_node );

			return_node->file_range.first = file_coord_last_start;
			UPDATE_LAST_FILE_POS();
			return_node->file_range.last = file_coord_last_start;

			ret = true;

			break;
		}
		case AST_Node_Type::USING:
		{
			if ( ast_eat_start( ctx )
			  && ast_eat( ctx, AST_Node_Type::NAME, {.name_allow_keywords = 1} )
			  && ast_eat_end( ctx ) )
			{
				log_dbg( "%sfound name", indent );
			} else
			{
				log_dbg( "%sexpected name", indent );
				break;
			}

			AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );

			Text_Piece * name = &lpc->eaten_ast_nodes.first->element->text_element_nodes.first->element.text;
			if ( !string_equal(
			        (char*)name->text,
			        name->length,
			        "using" ) )
			{
				char name_buf[512] = {};
				tg_assert( sizeof( name_buf ) > name->length );
				memcpy( name_buf, name->text, name->length );
				log_dbg( "%sexpected 'using' found '%s'", indent, name_buf );
				break;
			}

			pc->current_node = lpc->current_node;

			if ( ! ( ast_eat_start( ctx )
			      && ast_eat( ctx, AST_Node_Type::EXPRESSION, {.expression_semicolon_needed = 1} )
			      && ast_eat_end( ctx ) ) )
			{
				log_dbg( "%sexpected using expression", indent );
				break;
			}

			lpc = last_eaten_pctx( ctx );

			pc->current_node = lpc->current_node;

			AST_Node * using_node = ast_new_node( ctx->ast );
			using_node->type = type;
			list_vm_move_list_last( &using_node->children, &lpc->eaten_ast_nodes );
			list_vm_push_last( &pc->eaten_ast_nodes, &using_node );

			using_node->file_range.first = file_coord_last_start;
			UPDATE_LAST_FILE_POS();
			using_node->file_range.last = file_coord_last_start;

			ret = true;

			break;
		}
		case AST_Node_Type::CODE_BLOCK:
		{
			Matcher_Context m = {};
			m.current_node = pc->current_node;

			if (   start_match( &m )
			    && opt_bk( &m )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
			}

			UPDATE_LAST_FILE_POS();

			if (   start_match( &m )
			    && opt_bk( &m )
			    && match( &m, '{' )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
				log_dbg( "%sfound '{'", indent );
			} else
			{
				log_dbg( "%sexpected '{'", indent );
				break;
			}

			// NOTE(theGiallo): parse function body

			AST_Node * code_block_node = ast_new_node( ctx->ast );
			code_block_node->type = type;
			code_block_node->file_range.first = file_coord_last_start;

			#if 0
			while ( (    ast_eat_start( ctx )
			          && ast_eat( ctx, AST_Node_Type::FUNCTION_DECLARATION )
			          && ast_eat_end( ctx ) )
			     || (    ast_eat_start( ctx )
			          && ast_eat( ctx, AST_Node_Type::STRUCT_DECLARATION   )
			          && ast_eat_end( ctx ) )
			     || (    ast_eat_start( ctx )
			          && ast_eat( ctx, AST_Node_Type::BITFIELD_DECLARATION   )
			          && ast_eat_end( ctx ) )
			     || (    ast_eat_start( ctx )
			          && ast_eat( ctx, AST_Node_Type::UNION_DECLARATION   )
			          && ast_eat_end( ctx ) )
			     || (    ast_eat_start( ctx )
			          && ast_eat( ctx, AST_Node_Type::UNION_ANONYMOUS   )
			          && ast_eat_end( ctx ) )
			     || (    ast_eat_start( ctx )
			          && ast_eat( ctx, AST_Node_Type::ENUM_ANONYMOUS   )
			          && ast_eat_end( ctx ) )
			     || (    ast_eat_start( ctx )
			          && ast_eat( ctx, AST_Node_Type::VARIABLE_DECLARATION )
			          && ast_eat_end( ctx ) )
				 //|| (    ast_eat_start( ctx )
				 //     && ast_eat( ctx, AST_Node_Type::EXPRESSION          , {.expression_semicolon_needed = 1} )
				 //     && ast_eat_end( ctx ) )
			      )
			#else
			while (    ast_eat_start( ctx )
			        && ast_eat_or_pre( ctx )
			        && (    ast_eat( ctx, AST_Node_Type::FUNCTION_DECLARATION )
			             || ast_eat( ctx, AST_Node_Type::STRUCT_DECLARATION   )
			             || ast_eat( ctx, AST_Node_Type::BITFIELD_DECLARATION )
			             || ast_eat( ctx, AST_Node_Type::UNION_DECLARATION    )
			             || ast_eat( ctx, AST_Node_Type::UNION_ANONYMOUS      )
			             || ast_eat( ctx, AST_Node_Type::ENUM_DECLARATION     )
			             || ast_eat( ctx, AST_Node_Type::VARIABLE_DECLARATION )
			             || ast_eat( ctx, AST_Node_Type::CONSTANT_DECLARATION )
			             || ast_eat( ctx, AST_Node_Type::IF                   )
			             || ast_eat( ctx, AST_Node_Type::WHILE                )
			             || ast_eat( ctx, AST_Node_Type::SWITCH               )
			             || ast_eat( ctx, AST_Node_Type::FOR                  )
			             || ast_eat( ctx, AST_Node_Type::LOOP                 )
			             || ast_eat( ctx, AST_Node_Type::EXPRESSION          , {.expression_semicolon_needed = 1} )
			             || ast_eat( ctx, AST_Node_Type::RETURN               )
			             || ast_eat( ctx, AST_Node_Type::BREAK                )
			             || ast_eat( ctx, AST_Node_Type::CONTINUE             )
			             || ast_eat( ctx, AST_Node_Type::USING                )
			             || ast_eat( ctx, AST_Node_Type::CODE_BLOCK           )
			             || ast_eat_or_end( ctx )
			           )
			        && ast_eat_or_post( ctx )
			        && ast_eat_end( ctx )
			      )
			#endif
			{
				log_dbg( "%ssucceeded", indent );
				AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );
				list_vm_move_list_last( &code_block_node->children, &lpc->eaten_ast_nodes );
				pc->current_node = lpc->current_node;
				m.current_node = pc->current_node;
			}
			log_dbg( "%sexited while", indent );
			//{
			//	AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );
			//	m.current_node = lpc->current_node;
			//}


			if (   start_match( &m )
			    && opt_bk( &m )
			    && match( &m, '}' )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
				log_dbg( "%sfound '}'", indent );
			} else
			{
				log_dbg( "%sexpected '}'", indent );
				break;
			}

			list_vm_push_last( &pc->eaten_ast_nodes, &code_block_node );

			UPDATE_LAST_FILE_POS();
			code_block_node->file_range.last = file_coord_last_start;

			ret = true;

			break;
		}
		case AST_Node_Type::IF:
		case AST_Node_Type::WHILE:
		{
			Matcher_Context m = {};
			m.current_node = pc->current_node;

			AST_Node * label_node = NULL;
			if (    ast_eat_start( ctx )
			     && ast_eat( ctx, AST_Node_Type::LABEL )
			     && ast_eat_end( ctx ) )
			{
				AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );
				label_node = lpc->eaten_ast_nodes.first->element;

				m.current_node = pc->current_node = lpc->current_node;

				UPDATE_LAST_FILE_POS();

				log_dbg( "%sfound label", indent );
			} else
			{
				log_dbg( "no label found" );
			}
			if ( label_node && flags.if_while_else_disallow_label )
			{
				log_err( "%slabels are allowed only before an if/while/else", indent );
				ILLEGAL_PATH();
				break;
			}

			const char * construct_string = type == AST_Node_Type::IF ? "if" : "while";

			if ( start_match( &m )
			     && opt_bk( &m )
			     && match_word( &m, construct_string )
			     && end_match( &m )
			   )
			{
				log_dbg( "%sfound '%s'", indent, construct_string );
			} else
			{
				log_dbg( "%sexpected '%s'", indent, construct_string);
				break;
			}
			pc->current_node = m.current_node;

			AST_Node * if_node = ast_new_node( ctx->ast );
			if_node->type = type;
			if_node->file_range.first = file_coord_last_start;
			list_vm_push_last( &pc->eaten_ast_nodes, &if_node );
			if ( label_node )
			{
				list_vm_push_first( &if_node->children, &label_node );
				if_node->file_range.first = label_node->file_range.first;
			}

			#if 0
			if (   start_match( &m )
			    && opt_bk( &m )
			    && match( &m, '(' )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
				log_dbg( "%sfound '('", indent );
			} else
			{
				log_dbg( "%sexpected '('", indent );
				break;
			}
			#endif

			if (    ast_eat_start( ctx )
			     && ast_eat( ctx, AST_Node_Type::EXPRESSION )
			     && ast_eat_end( ctx ) )
			{
				AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );
				list_vm_move_list_last( &if_node->children, &lpc->eaten_ast_nodes );

				m.current_node = pc->current_node = lpc->current_node;
				log_dbg( "%sfound %s expression", indent, construct_string );
			} else
			{
				log_dbg( "expected an expression" );
				break;
			}

			#if 0
			if (   start_match( &m )
			    && opt_bk( &m )
			    && match( &m, ')' )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
				log_dbg( "%sfound ')'", indent );
			} else
			{
				log_dbg( "%sexpected ')'", indent );
				break;
			}
			#endif

			if (   ast_eat_start( ctx )
			    && ast_eat( ctx, AST_Node_Type::CODE_BLOCK )
			    && ast_eat_end( ctx )
			   )
			{
				log_dbg( "%sfound code block", indent );
				AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );
				list_vm_push_last( &if_node->children, &lpc->eaten_ast_nodes.first->element );
				m.current_node = pc->current_node = lpc->current_node;
			} else
			{
				log_dbg( "%sexpected code block", indent );
				break;
			}

			flags.if_while_else_disallow_label = 1;

			UPDATE_LAST_FILE_POS();

			if (    start_match( &m )
			     && opt_bk( &m )
			     && match_word( &m, "else" )
			     && end_match( &m )
			   )
			{
				log_dbg( "%sfound 'else'", indent );
				pc->current_node = m.current_node;

				if (   (    ast_eat_start( ctx )
				         && ast_eat( ctx, AST_Node_Type::IF, flags )
				         && ast_eat_end( ctx )
				       )
				    || (    ast_eat_start( ctx )
				         && ast_eat( ctx, AST_Node_Type::WHILE, flags)
				         && ast_eat_end( ctx )
				       )
				    || (    ast_eat_start( ctx )
				         && ast_eat( ctx, AST_Node_Type::CODE_BLOCK )
				         && ast_eat_end( ctx )
				       )
				   )
				{
					AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );
					AST_Node * else_node = ast_new_node( ctx->ast );
					else_node->type = AST_Node_Type::ELSE;

					list_vm_move_list_last( &else_node->children, &lpc->eaten_ast_nodes );
					list_vm_push_last( &if_node->children, &else_node );
					m.current_node = pc->current_node = lpc->current_node;

					else_node->file_range.first = file_coord_last_start;
					UPDATE_LAST_FILE_POS();
					else_node->file_range.last = file_coord_last_start;
				} else
				{
					log_dbg( "%sexpected else code block or if", indent );
					break;
				}
			} else
			{
				log_dbg( "%sno else", indent );
			}

			if_node->file_range.last = file_coord_last_start;

			ret = true;

			break;
		}
		case AST_Node_Type::SWITCH:
		{
			Matcher_Context m = {};
			m.current_node = pc->current_node;

			AST_Node * label_node = NULL;
			if (    ast_eat_start( ctx )
			     && ast_eat( ctx, AST_Node_Type::LABEL )
			     && ast_eat_end( ctx ) )
			{
				AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );
				label_node = lpc->eaten_ast_nodes.first->element;

				m.current_node = pc->current_node = lpc->current_node;

				log_dbg( "%sfound label", indent );
			} else
			{
				log_dbg( "no label found" );
			}

			const char * construct_string = "switch";

			if ( start_match( &m )
			     && opt_bk( &m )
			     && match_word( &m, construct_string )
			     && end_match( &m )
			   )
			{
				log_dbg( "%sfound '%s'", indent, construct_string );
			} else
			{
				log_dbg( "%sexpected '%s'", indent, construct_string);
				break;
			}
			pc->current_node = m.current_node;

			AST_Node * switch_node = ast_new_node( ctx->ast );
			switch_node->type = type;
			switch_node->file_range.first = file_coord_last_start;
			if ( label_node )
			{
				list_vm_push_first( &switch_node->children, &label_node );
				switch_node->file_range.first = label_node->file_range.first;
			}

			#if 0
			if (   start_match( &m )
			    && opt_bk( &m )
			    && match( &m, '(' )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
				log_dbg( "%sfound '('", indent );
			} else
			{
				log_dbg( "%sexpected '('", indent );
				break;
			}
			#endif

			if (    ast_eat_start( ctx )
			     && ast_eat( ctx, AST_Node_Type::EXPRESSION )
			     && ast_eat_end( ctx ) )
			{
				AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );
				list_vm_move_list_last( &switch_node->children, &lpc->eaten_ast_nodes );

				m.current_node = pc->current_node = lpc->current_node;
				log_dbg( "%sfound %s expression", indent, construct_string );
			} else
			{
				log_dbg( "expected an expression" );
				break;
			}

			#if 0
			if (   start_match( &m )
			    && opt_bk( &m )
			    && match( &m, ')' )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
				log_dbg( "%sfound ')'", indent );
			} else
			{
				log_dbg( "%sexpected ')'", indent );
				break;
			}
			#endif

			flags.if_while_else_disallow_label = 1;

			UPDATE_LAST_FILE_POS();

			bool some_vals_found = false;
			while ( start_match( &m )
			     && opt_bk( &m )
			     && match_word( &m, "val" )
			     && end_match( &m )
			   )
			{
				some_vals_found = true;
				log_dbg( "%sfound 'val'", indent );
				pc->current_node = m.current_node;

				AST_Node * val_node = ast_new_node( ctx->ast );
				val_node->type = AST_Node_Type::VAL;

				if (    ast_eat_start( ctx )
				     && ast_eat( ctx, AST_Node_Type::EXPRESSION )
				     && ast_eat_end( ctx ) )
				{
					AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );
					list_vm_push_first( &val_node->children, &lpc->eaten_ast_nodes.first->element );

					m.current_node = pc->current_node = lpc->current_node;
					log_dbg( "%sfound val expression", indent );
				} else
				{
					log_dbg( "expected an expression" );
					break;
				}

				if (    ast_eat_start( ctx )
				     && ast_eat( ctx, AST_Node_Type::CODE_BLOCK )
				     && ast_eat_end( ctx )
				   )
				{
					AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );
					list_vm_push_last( &val_node->children, &lpc->eaten_ast_nodes.first->element );
					list_vm_push_last( &switch_node->children, &val_node );
					m.current_node = pc->current_node = lpc->current_node;
				} else
				{
					log_dbg( "%sexpected val code block", indent );
					break;
				}

				val_node->file_range.first = file_coord_last_start;
				UPDATE_LAST_FILE_POS();
				val_node->file_range.last = file_coord_last_start;
			}
			if ( ! some_vals_found )
			{
				log_dbg( "%sexpected at least a 'val'", indent );
				break;
			}

			list_vm_push_last( &pc->eaten_ast_nodes, &switch_node );

			switch_node->file_range.last = file_coord_last_start;

			ret = true;

			break;
		}
		case AST_Node_Type::FOR:
		{
			Matcher_Context m = {};
			m.current_node = pc->current_node;

			AST_Node * label_node = NULL;
			if (    ast_eat_start( ctx )
			     && ast_eat( ctx, AST_Node_Type::LABEL )
			     && ast_eat_end( ctx ) )
			{
				AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );
				label_node = lpc->eaten_ast_nodes.first->element;

				m.current_node = pc->current_node = lpc->current_node;

				log_dbg( "%sfound label", indent );
			} else
			{
				log_dbg( "no label found" );
			}

			const char * construct_string = "for";

			if (    start_match( &m )
			     && opt_bk( &m )
			     && match_word( &m, construct_string )
			     && end_match( &m )
			   )
			{
				log_dbg( "%sfound '%s'", indent, construct_string );
			} else
			{
				log_dbg( "%sexpected '%s'", indent, construct_string);
				break;
			}
			pc->current_node = m.current_node;

			AST_Node * for_node = ast_new_node( ctx->ast );
			for_node->type = type;

			for_node->file_range.first = file_coord_last_start;

			list_vm_push_last( &pc->eaten_ast_nodes, &for_node );
			if ( label_node )
			{
				list_vm_push_first( &for_node->children, &label_node );
				for_node->file_range.first = label_node->file_range.first;
			}

			#if 0
			if (   start_match( &m )
			    && opt_bk( &m )
			    && match( &m, '(' )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
				log_dbg( "%sfound '('", indent );
			} else
			{
				log_dbg( "%sexpected '('", indent );
				break;
			}
			#endif

			if (    ast_eat_start( ctx )
			     && ast_eat( ctx, AST_Node_Type::EXPRESSION )
			     && ast_eat_end( ctx ) )
			{
				AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );
				AST_Node * ranges_node = ast_new_node( ctx->ast );
				ranges_node->type = AST_Node_Type::FOR_RANGES;
				list_vm_move_list_last( &ranges_node->children, &lpc->eaten_ast_nodes );
				list_vm_push_last( &for_node->children, &ranges_node );

				m.current_node = pc->current_node = lpc->current_node;
				ranges_node->file_range.first = ranges_node->children.first->element->file_range.first;
				UPDATE_LAST_FILE_POS();
				ranges_node->file_range.last = file_coord_last_start;

				log_dbg( "%sfound %s expression", indent, construct_string );
			} else
			{
				log_dbg( "expected an expression" );
				break;
			}

			if (    start_match( &m )
			     && opt_bk( &m )
			     && match( &m, '-' )
			     && match( &m, '>' )
			     && end_match( &m )
			   )
			{
				log_dbg( "%sfound '->'", indent );
				pc->current_node = m.current_node;

				if (    ast_eat_start( ctx )
				     && ast_eat( ctx, AST_Node_Type::NAME )
				     && ast_eat( ctx, AST_Node_Type::NAME )
				     && ast_eat( ctx, AST_Node_Type::SEMICOLON )
				     && ast_eat( ctx, AST_Node_Type::NAME )
				     && ast_eat( ctx, AST_Node_Type::NAME )
				     && ast_eat_end( ctx ) )
				{
					AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );

					AST_Node * index_node = ast_new_node( ctx->ast );
					index_node->type = AST_Node_Type::FOR_INDEX;

					AST_Node * counter_node = ast_new_node( ctx->ast );
					counter_node->type = AST_Node_Type::FOR_COUNTER;

					AST_Node_p_List_VM_Node * n = lpc->eaten_ast_nodes.first;

					AST_Node * index_type_node = n->element;
					index_type_node->type = AST_Node_Type::TYPE;
					n = n->next;

					AST_Node * index_name_node = n->element;
					n = n->next;

					// NOTE(theGiallo): semicolon
					n = n->next;

					AST_Node * counter_type_node = n->element;
					counter_type_node->type = AST_Node_Type::TYPE;
					n = n->next;

					AST_Node * counter_name_node = n->element;
					n = n->next;

					list_vm_push_last( &index_node->children, &index_type_node );
					list_vm_push_last( &index_node->children, &index_name_node );
					index_node->file_range.first = index_type_node->file_range.first;
					index_node->file_range.last  = index_name_node->file_range.last;
					list_vm_push_last( &counter_node->children, &counter_type_node );
					list_vm_push_last( &counter_node->children, &counter_name_node );
					counter_node->file_range.first = counter_type_node->file_range.first;
					counter_node->file_range.last  = counter_name_node->file_range.last;
					list_vm_push_last( &for_node->children, &index_node );
					list_vm_push_last( &for_node->children, &counter_node );
					for_node->file_range.first = index_node->file_range.first;
					for_node->file_range.last  = counter_node->file_range.last;

					m.current_node = pc->current_node = lpc->current_node;
					log_dbg( "%sfound %s index variable with type and counter variable with type", indent, construct_string );
				} else
				if (    ast_eat_start( ctx )
				     && ast_eat( ctx, AST_Node_Type::NAME )
				     && ast_eat( ctx, AST_Node_Type::NAME )
				     && ast_eat( ctx, AST_Node_Type::COMMA_OP )
				     && ast_eat( ctx, AST_Node_Type::NAME )
				     && ast_eat_end( ctx ) )
				{
					AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );

					AST_Node * index_node = ast_new_node( ctx->ast );
					index_node->type = AST_Node_Type::FOR_INDEX;

					AST_Node * counter_node = ast_new_node( ctx->ast );
					counter_node->type = AST_Node_Type::FOR_COUNTER;

					AST_Node_p_List_VM_Node * n = lpc->eaten_ast_nodes.first;

					AST_Node * type_node = n->element;
					type_node->type = AST_Node_Type::TYPE;
					n = n->next;

					AST_Node * index_name_node = n->element;
					n = n->next;

					// NOTE(theGiallo): comma
					n = n->next;

					AST_Node * counter_name_node = n->element;
					n = n->next;

					list_vm_push_last( &index_node->children, &type_node );
					list_vm_push_last( &index_node->children, &index_name_node );
					index_node->file_range.first =       type_node->file_range.first;
					index_node->file_range.last  = index_name_node->file_range.last;
					list_vm_push_last( &counter_node->children, &type_node );
					list_vm_push_last( &counter_node->children, &counter_name_node );
					counter_node->file_range = counter_name_node->file_range;
					list_vm_push_last( &for_node->children, &index_node );
					list_vm_push_last( &for_node->children, &counter_node );
					for_node->file_range.first = index_node->file_range.first;
					for_node->file_range.last  = counter_node->file_range.last;

					m.current_node = pc->current_node = lpc->current_node;
					log_dbg( "%sfound %s index and counter variables with type", indent, construct_string );
				} else
				if (    ast_eat_start( ctx )
				     && ast_eat( ctx, AST_Node_Type::NAME )
				     && ast_eat( ctx, AST_Node_Type::COMMA_OP )
				     && ast_eat( ctx, AST_Node_Type::NAME )
				     && ast_eat_end( ctx ) )
				{
					AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );

					AST_Node * index_node = ast_new_node( ctx->ast );
					index_node->type = AST_Node_Type::FOR_INDEX;

					AST_Node * counter_node = ast_new_node( ctx->ast );
					counter_node->type = AST_Node_Type::FOR_COUNTER;

					AST_Node_p_List_VM_Node * n = lpc->eaten_ast_nodes.first;

					AST_Node * index_name_node = n->element;
					n = n->next;

					// NOTE(theGiallo): comma
					n = n->next;

					AST_Node * counter_name_node = n->element;
					n = n->next;

					list_vm_push_last( &index_node->children, &index_name_node );
					index_node->file_range = index_name_node->file_range;
					list_vm_push_last( &counter_node->children, &counter_name_node );
					counter_node->file_range = counter_name_node->file_range;
					list_vm_push_last( &for_node->children, &index_node );
					list_vm_push_last( &for_node->children, &counter_node );
					for_node->file_range.first = index_node->file_range.first;
					for_node->file_range.last  = counter_node->file_range.last;

					m.current_node = pc->current_node = lpc->current_node;
					log_dbg( "%sfound %s index and counter variables without type", indent, construct_string );
				} else
				if (    ast_eat_start( ctx )
				     && ast_eat( ctx, AST_Node_Type::NAME )
				     && ast_eat( ctx, AST_Node_Type::NAME )
				     && ast_eat_end( ctx ) )
				{
					AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );

					AST_Node * index_node = ast_new_node( ctx->ast );
					index_node->type = AST_Node_Type::FOR_INDEX;

					AST_Node_p_List_VM_Node * n = lpc->eaten_ast_nodes.first;

					AST_Node * type_node = n->element;
					type_node->type = AST_Node_Type::TYPE;
					n = n->next;

					AST_Node * index_name_node = n->element;
					n = n->next;

					list_vm_push_last( &index_node->children, &type_node );
					list_vm_push_last( &index_node->children, &index_name_node );
					index_node->file_range.first =       type_node->file_range.first;
					index_node->file_range.last  = index_name_node->file_range.last;
					list_vm_push_last( &for_node->children, &index_node );
					for_node->file_range = index_node->file_range;

					m.current_node = pc->current_node = lpc->current_node;
					log_dbg( "%sfound %s index variable with type", indent, construct_string );
				} else
				if (    ast_eat_start( ctx )
				     && ast_eat( ctx, AST_Node_Type::NAME )
				     && ast_eat_end( ctx ) )
				{
					AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );
					AST_Node * index_node = ast_new_node( ctx->ast );
					index_node->type = AST_Node_Type::FOR_INDEX;
					list_vm_move_list_last( &index_node->children, &lpc->eaten_ast_nodes );
					index_node->file_range = index_node->children.first->element->file_range;
					list_vm_push_last( &for_node->children, &index_node );
					for_node->file_range = index_node->file_range;

					m.current_node = pc->current_node = lpc->current_node;
					log_dbg( "%sfound %s index variable without type", indent, construct_string );
				} else
				{
					log_dbg( "expected a variable declaration after '->'" );
					break;
				}
			} else
			{
				log_dbg( "%s no '->' found", indent );
			}

			#if 0
			if (   start_match( &m )
			    && opt_bk( &m )
			    && match( &m, ')' )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
				log_dbg( "%sfound ')'", indent );
			} else
			{
				log_dbg( "%sexpected ')'", indent );
				break;
			}
			#endif

			if (   ast_eat_start( ctx )
			    && ast_eat( ctx, AST_Node_Type::CODE_BLOCK )
			    && ast_eat_end( ctx )
			   )
			{
				log_dbg( "%sfound code block", indent );
				AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );
				list_vm_push_last( &for_node->children, &lpc->eaten_ast_nodes.first->element );
				m.current_node = pc->current_node = lpc->current_node;
			} else
			{
				log_dbg( "%sexpected code block", indent );
				break;
			}

			UPDATE_LAST_FILE_POS();
			for_node->file_range.last = file_coord_last_start;

			ret = true;

			break;
		}
		case AST_Node_Type::LOOP:
		{
			Matcher_Context m = {};
			m.current_node = pc->current_node;

			AST_Node * label_node = NULL;
			if (    ast_eat_start( ctx )
			     && ast_eat( ctx, AST_Node_Type::LABEL )
			     && ast_eat_end( ctx ) )
			{
				AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );
				label_node = lpc->eaten_ast_nodes.first->element;

				m.current_node = pc->current_node = lpc->current_node;

				log_dbg( "%sfound label", indent );
			} else
			{
				log_dbg( "no label found" );
			}

			const char * construct_string = "loop";

			if (   start_match( &m )
			    && opt_bk( &m )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
			}

			UPDATE_LAST_FILE_POS();

			if (    start_match( &m )
			     && match_word( &m, construct_string )
			     && end_match( &m )
			   )
			{
				log_dbg( "%sfound '%s'", indent, construct_string );
			} else
			{
				log_dbg( "%sexpected '%s'", indent, construct_string);
				break;
			}
			pc->current_node = m.current_node;

			AST_Node * loop_node = ast_new_node( ctx->ast );
			loop_node->type = type;
			loop_node->file_range.first = file_coord_last_start;

			if (   start_match( &m )
			    && opt_bk( &m )
			    && match( &m, '(' )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
				log_dbg( "%sfound '('", indent );
			} else
			{
				log_dbg( "%sexpected '('", indent );
				break;
			}

			if (   start_match( &m )
			    && opt_bk( &m )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
			}

			UPDATE_LAST_FILE_POS();

			bool found_boot = false;
			if (   start_match( &m )
			    && match_word( &m, "boot" )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
				log_dbg( "%sfound 'boot'", indent );
				found_boot = true;
			} else
			{
				log_dbg( "%sno 'boot' keyword", indent );
			}

			AST_Node * boot_node = NULL;
			if (   ast_eat_start( ctx )
			    && ast_eat( ctx, AST_Node_Type::CODE_BLOCK )
			    && ast_eat_end( ctx )
			   )
			{
				log_dbg( "%sfound boot code block", indent );
				AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );
				m.current_node = pc->current_node = lpc->current_node;

				boot_node = ast_new_node( ctx->ast );
				boot_node->type = AST_Node_Type::LOOP_BOOT;

				boot_node->file_range.first = file_coord_last_start;
				UPDATE_LAST_FILE_POS();
				boot_node->file_range.last = file_coord_last_start;

				list_vm_push_last( &boot_node->children, &lpc->eaten_ast_nodes.first->element );
			} else
			{
				log_dbg( "%sexpected boot code block", indent );
				break;
			}

			if (   start_match( &m )
			    && opt_bk( &m )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
			}

			UPDATE_LAST_FILE_POS();

			bool found_check = false;
			if (   start_match( &m )
			    && match_word( &m, "check" )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
				log_dbg( "%sfound 'check'", indent );
				found_check = true;
			} else
			{
				log_dbg( "%sno 'check' keyword", indent );
			}

			if (   start_match( &m )
			    && opt_bk( &m )
			    && match( &m, '(' )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
				log_dbg( "%sfound '('", indent );
			} else
			{
				log_dbg( "%sexpected '('", indent );
				break;
			}

			AST_Node * check_node = NULL;
			if (    ast_eat_start( ctx )
			     && ast_eat( ctx, AST_Node_Type::EXPRESSION )
			     && ast_eat_end( ctx ) )
			{
				log_dbg( "%sfound check expression", indent );
				AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );

				m.current_node = pc->current_node = lpc->current_node;

				check_node = ast_new_node( ctx->ast );
				check_node->type = AST_Node_Type::LOOP_CHECK;

				list_vm_move_list_last( &check_node->children, &lpc->eaten_ast_nodes );
			} else
			{
				log_dbg( "expected check expression" );
				break;
			}

			if (   start_match( &m )
			    && opt_bk( &m )
			    && match( &m, ')' )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
				log_dbg( "%sfound ')'", indent );
			} else
			{
				log_dbg( "%sexpected ')'", indent );
				break;
			}

			check_node->file_range.first = file_coord_last_start;
			UPDATE_LAST_FILE_POS();
			check_node->file_range.last = file_coord_last_start;

			if (   start_match( &m )
			    && opt_bk( &m )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
			}

			UPDATE_LAST_FILE_POS();

			bool found_advance = false;
			if (   start_match( &m )
			    && match_word( &m, "advance" )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
				log_dbg( "%sfound 'advance'", indent );
				found_advance = true;
			} else
			{
				log_dbg( "%sno 'advance' keyword", indent );
			}

			AST_Node * advance_node = NULL;
			if (   ast_eat_start( ctx )
			    && ast_eat( ctx, AST_Node_Type::CODE_BLOCK )
			    && ast_eat_end( ctx )
			   )
			{
				log_dbg( "%sfound advance code block", indent );
				AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );

				m.current_node = pc->current_node = lpc->current_node;

				advance_node = ast_new_node( ctx->ast );
				advance_node->type = AST_Node_Type::LOOP_ADVANCE;

				advance_node->file_range.first = file_coord_last_start;
				UPDATE_LAST_FILE_POS();
				advance_node->file_range.last = file_coord_last_start;

				list_vm_push_last( &advance_node->children, &lpc->eaten_ast_nodes.first->element );
			} else
			{
				log_dbg( "%sexpected advance code block", indent );
				break;
			}

			bool found_epilogue = false;
			if (   start_match( &m )
			    && opt_bk( &m )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
			}

			UPDATE_LAST_FILE_POS();

			if (   start_match( &m )
			    && match_word( &m, "epilogue" )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
				log_dbg( "%sfound 'epilogue'", indent );
				found_epilogue = true;
			} else
			{
				log_dbg( "%sno 'epilogue' keyword", indent );
			}

			AST_Node * epilogue_node = ast_new_node( ctx->ast );
			epilogue_node->type = AST_Node_Type::LOOP_EPILOGUE;
			if (   ast_eat_start( ctx )
			    && ast_eat( ctx, AST_Node_Type::CODE_BLOCK )
			    && ast_eat_end( ctx )
			   )
			{
				log_dbg( "%sfound epilogue code block", indent );
				AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );

				m.current_node = pc->current_node = lpc->current_node;

				epilogue_node->file_range.first = file_coord_last_start;
				UPDATE_LAST_FILE_POS();
				epilogue_node->file_range.last = file_coord_last_start;

				list_vm_push_last( &epilogue_node->children, &lpc->eaten_ast_nodes.first->element );
			} else
			{
				log_dbg( "%sno epilogue code block", indent );
			}

			if (   start_match( &m )
			    && opt_bk( &m )
			    && match( &m, ')' )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
				log_dbg( "%sfound ')'", indent );
			} else
			{
				log_dbg( "%sexpected ')'", indent );
				break;
			}

			UPDATE_LAST_FILE_POS();
			AST_Node * code_block_node = NULL;
			if (   ast_eat_start( ctx )
			    && ast_eat( ctx, AST_Node_Type::CODE_BLOCK )
			    && ast_eat_end( ctx )
			   )
			{
				log_dbg( "%sfound code block", indent );
				AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );
				code_block_node = lpc->eaten_ast_nodes.first->element;

				m.current_node = pc->current_node = lpc->current_node;
			} else
			{
				log_dbg( "%sexpected code block", indent );
				break;
			}

			tg_assert( loop_node );
			tg_assert( boot_node );
			tg_assert( check_node );
			tg_assert( advance_node );
			tg_assert( epilogue_node );
			tg_assert( code_block_node );

			list_vm_push_last( &pc->eaten_ast_nodes, &loop_node );
			if ( label_node )
			{
				list_vm_push_first( &loop_node->children, &label_node );
				loop_node->file_range.first = label_node->file_range.first;
			}

			UPDATE_LAST_FILE_POS();
			loop_node->file_range.last = file_coord_last_start;

			list_vm_push_last( &loop_node->children, &boot_node );
			list_vm_push_last( &loop_node->children, &check_node );
			list_vm_push_last( &loop_node->children, &advance_node );
			list_vm_push_last( &loop_node->children, &epilogue_node );
			list_vm_push_last( &loop_node->children, &code_block_node );
			ret = true;

			break;
		}
		case AST_Node_Type::FUNCTION_DECLARATION:
		{
			if ( ast_eat_start( ctx )
			  && ast_eat( ctx, AST_Node_Type::NAME )
			  && ast_eat_end( ctx ) )
			{
				log_dbg( "%sfound name", indent );
			} else
			{
				log_dbg( "%sexpected name", indent );
				break;
			}

			AST_Eating_Context::Partial_Context * fn_name_pc = last_eaten_pctx( ctx );

			pc->current_node = fn_name_pc->current_node;

			AST_Node * function_declaration_node = ast_new_node( ctx->ast );
			AST_Node * fn_name_node = fn_name_pc->eaten_ast_nodes.first->element;

			function_declaration_node->file_range.first = fn_name_node->file_range.first;

			function_declaration_node->type = AST_Node_Type::FUNCTION_DECLARATION;
			list_vm_push_last( &function_declaration_node->children, &fn_name_node );
			list_vm_push_last( &pc->eaten_ast_nodes, &function_declaration_node );

			Matcher_Context m = {};
			m.current_node = pc->current_node;

			if (   start_match( &m )
			    && opt_bk( &m )
			    && match( &m, '(' )
			    && opt_bk( &m )
			    && match( &m, '{' )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
				log_dbg( "%sfound '({'", indent );
			} else
			{
				log_dbg( "%sexpected '({'", indent );
				break;
			}

			if (    ast_eat_start( ctx )
			     && ast_eat( ctx, AST_Node_Type::STRUCT_BODY )
			     && ast_eat_end( ctx ) )
			{
				AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );
				AST_Node * as_node = ast_new_node( ctx->ast );
				as_node->type = AST_Node_Type::ARGUMENT_STRUCT;
				list_vm_move_list_last( &as_node->children, &lpc->eaten_ast_nodes );
				list_vm_push_last( &function_declaration_node->children, &as_node );

				as_node->file_range = as_node->children.first->element->file_range;

				pc->current_node = lpc->current_node;
				m.current_node = pc->current_node;
				log_dbg( "%sfound struct body", indent );
			}
			// NOTE(theGiallo): no else break for no arguments
			#if 0
			else
			{
				log_dbg( "expected a struct body" );
				break;
			}
			#endif

			if (   start_match( &m )
			    && opt_bk( &m )
			    && match( &m, '}' )
			    && opt_bk( &m )
			    && match( &m, ')' )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
				log_dbg( "%sfound ')}'", indent );
			} else
			{
				log_dbg( "%sexpected ')}'", indent );
				break;
			}

			if (   start_match( &m )
			    && opt_bk( &m )
			    && match( &m, '-' )
			    && match( &m, '>' )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
				log_dbg( "%sfound '->'", indent );
				if (    ast_eat_start( ctx )
				     && ast_eat( ctx, AST_Node_Type::TYPE )
				     && ast_eat_end( ctx ) )
				{
					AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );
					AST_Node * ast_node = ast_new_node( ctx->ast );
					ast_node->type = AST_Node_Type::RETURN_TYPE;
					list_vm_push_last( &ast_node->children, &lpc->eaten_ast_nodes.first->element );
					list_vm_push_last( &function_declaration_node->children, &ast_node );

					ast_node->file_range = ast_node->children.first->element->file_range;

					pc->current_node = lpc->current_node;
					m.current_node = pc->current_node;
					log_dbg( "%sfound return type", indent );
				} else
				{
					log_dbg( "%sexpected return type after '->'", indent );
					break;
				}
			} else
			{
				log_dbg( "%sno return type'", indent );
			}

			if (   ast_eat_start( ctx )
			    && ast_eat( ctx, AST_Node_Type::CODE_BLOCK )
			    && ast_eat_end( ctx )
			   )
			{
				log_dbg( "%sfound code block", indent );
				AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );
				list_vm_push_last( &function_declaration_node->children, &lpc->eaten_ast_nodes.first->element );
				//m.current_node = pc->current_node;
				pc->current_node = lpc->current_node;

				function_declaration_node->file_range.last = function_declaration_node->children.last->element->file_range.last;
			} else
			{
				log_dbg( "%sexpected code block", indent );
				break;
			}

			ret = true;

			break;
		}
		case AST_Node_Type::UNION_DECLARATION:
		case AST_Node_Type::UNION_ANONYMOUS:
		{
			Matcher_Context m = {};
			m.current_node = pc->current_node;

			const char * bf_string = "union";
			if ( start_match( &m )
			     && opt_bk( &m )
			     && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
			}

			UPDATE_LAST_FILE_POS();

			if ( start_match( &m )
			     && match_word( &m, bf_string )
			     && end_match( &m )
			   )
			{
				log_dbg( "%sfound '%s'", indent, bf_string );
			} else
			{
				log_dbg( "%sexpected '%s'", indent, bf_string);
				break;
			}
			pc->current_node = m.current_node;

			AST_Node * union_node = ast_new_node( ctx->ast );
			union_node->type = type;
			union_node->file_range.first = file_coord_last_start;

			bool has_name =
			   (    ast_eat_start( ctx )
			     && ast_eat( ctx, AST_Node_Type::NAME )
			     && ast_eat_end( ctx ) );

			if ( type == AST_Node_Type::UNION_DECLARATION )
			{
				if ( has_name )
				{
					log_dbg( "%sfound name", indent );

					AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );

					m.current_node = pc->current_node = lpc->current_node;

					list_vm_push_last( &union_node->children, &lpc->eaten_ast_nodes.first->element );
				} else
				{
					log_dbg( "%sexpected name", indent );
					break;
				}
			}
			if ( type == AST_Node_Type::UNION_ANONYMOUS )
			{
				if ( has_name )
				{
					log_dbg( "%sunexpected name for anonymous union", indent );
					break;
				} else
				{
					log_dbg( "%sno name, as expected", indent );
				}
			}

			if (   start_match( &m )
			    && opt_bk( &m )
			    && match( &m, '{' )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
				log_dbg( "%sfound '{'", indent );
			} else
			{
				log_dbg( "%sexpected '{'", indent );
				break;
			}

			if (    ast_eat_start( ctx )
			     && ast_eat( ctx, AST_Node_Type::STRUCT_BODY, {.struct_body_allow_anonymous_structs = 1} )
			     && ast_eat_end( ctx ) )
			{
				log_dbg( "%sfound a struct body inside union's {}", indent );

				AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );

				m.current_node = pc->current_node = lpc->current_node;

				list_vm_push_last( &union_node->children, &lpc->eaten_ast_nodes.first->element );
			} else
			{
				log_dbg( "%sexpected a struct body inside union's {}", indent );
				break;
			}

			if (   start_match( &m )
			    && opt_bk( &m )
			    && match( &m, '}' )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
				log_dbg( "%sfound '}'", indent );
			} else
			{
				log_dbg( "%sexpected '}'", indent );
				break;
			}

			UPDATE_LAST_FILE_POS();
			union_node->file_range.last = file_coord_last_start;

			list_vm_push_last( &pc->eaten_ast_nodes, &union_node );
			ret = true;
			break;
		}
		case AST_Node_Type::ENUM_DECLARATION:
		{
			Matcher_Context m = {};
			m.current_node = pc->current_node;

			const char * bf_string = "enum";
			if ( start_match( &m )
			     && opt_bk( &m )
			     && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
			}

			UPDATE_LAST_FILE_POS();

			if ( start_match( &m )
			     && match_word( &m, bf_string )
			     && end_match( &m )
			   )
			{
				log_dbg( "%sfound '%s'", indent, bf_string );
			} else
			{
				log_dbg( "%sexpected '%s'", indent, bf_string);
				break;
			}
			pc->current_node = m.current_node;

			AST_Node * enum_node = ast_new_node( ctx->ast );
			enum_node->type = type;
			enum_node->file_range.first = file_coord_last_start;

			if (    ast_eat_start( ctx )
			     && ast_eat( ctx, AST_Node_Type::TYPE, {.type_allow_only_typenames = 1} )
			     && ast_eat_end( ctx ) )
			{
				log_dbg( "%sfound type name", indent );

				AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );

				m.current_node = pc->current_node = lpc->current_node;

				list_vm_push_last( &enum_node->children, &lpc->eaten_ast_nodes.first->element );
			} else
			{
				log_dbg( "%sexpected type name", indent );
				break;
			}

			if (    ast_eat_start( ctx )
			     && ast_eat( ctx, AST_Node_Type::NAME )
			     && ast_eat_end( ctx ) )
			{
				log_dbg( "%sfound name", indent );

				AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );

				m.current_node = pc->current_node = lpc->current_node;

				list_vm_push_last( &enum_node->children, &lpc->eaten_ast_nodes.first->element );
			} else
			{
				log_dbg( "%sexpected name", indent );
				break;
			}

			if (   start_match( &m )
			    && opt_bk( &m )
			    && match( &m, '{' )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
				log_dbg( "%sfound '{'", indent );
			} else
			{
				log_dbg( "%sexpected '{'", indent );
				break;
			}

			AST_Node_Type assignment_type = AST_Node_Type::EXPR_BINARY_OP_PREC_12;
			while ( ast_eat_start( ctx )
			     && ast_eat( ctx, assignment_type )
			     && ast_eat_end( ctx ) )
			{
				log_dbg( "%sfound assignment", indent );

				AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );

				AST_Node * expr            = lpc->eaten_ast_nodes.first->element;
				AST_Node * node_name       = 0;
				AST_Node * node_assignment = 0;
				AST_Node * node_value      = 0;

				log_dbg( "%stype = %s", indent,
				         ast_node_type_strings[u32(expr->type)] );
				if (    expr->type == AST_Node_Type::EXPR_TERMINAL
				     && ast_node_child_is_identifier( expr ) )
				{
					log_dbg( "%sfound only name after type", indent );
					node_name = expr->children.first->element;
				} else
				if ( expr->type == assignment_type
				  && expr->children.first
				  && expr->children.first->element->type == AST_Node_Type::EXPR_TERMINAL
				  && ast_node_child_is_identifier( expr->children.first->element ) )
				{
					log_dbg( "%sfound name and assignment after type", indent );
					node_assignment = expr;
					node_name = node_assignment->children.first->element;
					log_dbg( "%stype = %s", indent,
					         ast_node_type_strings[u32(node_assignment->children.first->next->element->type)] );
					tg_assert( node_assignment->children.first->next->element->type == AST_Node_Type::BINARY_OPERATOR_PREC_12 );
					tg_assert( node_assignment->children.first->next->element->children.first->element->type == AST_Node_Type::ASSIGNMENT_OP );
					node_value = node_assignment->children.first->next->next->element;
				} else
				{
					log_dbg( "%sno variable name or assignment after type", indent );
					break;
				}

				m.current_node = pc->current_node = lpc->current_node;

				list_vm_push_last( &enum_node->children, &expr );

				if (   start_match( &m )
				    && opt_bk( &m )
				    && match( &m, ',' )
				    && end_match( &m )
				   )
				{
					pc->current_node = m.current_node;
					log_dbg( "%sfound ','", indent );
				} else
				{
					log_dbg( "%sno ',' found", indent );
					break;
				}
			}

			if (   start_match( &m )
			    && opt_bk( &m )
			    && match( &m, '}' )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
				log_dbg( "%sfound '}'", indent );
			} else
			{
				log_dbg( "%sexpected '}'", indent );
				break;
			}

			UPDATE_LAST_FILE_POS();
			enum_node->file_range.last = file_coord_last_start;

			list_vm_push_last( &pc->eaten_ast_nodes, &enum_node );
			ret = true;
			break;
		}
		case AST_Node_Type::BITFIELD_DECLARATION:
		case AST_Node_Type::BITFIELD_ANONYMOUS:
		{
			Matcher_Context m = {};
			m.current_node = pc->current_node;

			const char * bf_string = "bitfield";
			if ( start_match( &m )
			     && opt_bk( &m )
			     && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
			}

			UPDATE_LAST_FILE_POS();

			if ( start_match( &m )
			     && match_word( &m, bf_string )
			     && end_match( &m )
			   )
			{
				log_dbg( "%sfound '%s'", indent, bf_string );
			} else
			{
				log_dbg( "%sexpected '%s'", indent, bf_string);
				break;
			}
			pc->current_node = m.current_node;

			AST_Node * bf_node = ast_new_node( ctx->ast );
			bf_node->type = type;
			bf_node->file_range.first = file_coord_last_start;

			bool has_name =
			   (    ast_eat_start( ctx )
			     && ast_eat( ctx, AST_Node_Type::NAME )
			     && ast_eat_end( ctx ) );

			if ( type == AST_Node_Type::BITFIELD_DECLARATION )
			{
				if ( has_name )
				{
					log_dbg( "%sfound name", indent );

					AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );

					m.current_node = pc->current_node = lpc->current_node;

					list_vm_push_last( &bf_node->children, &lpc->eaten_ast_nodes.first->element );
				} else
				{
					log_dbg( "%sexpected name", indent );
					break;
				}
			}
			if ( type == AST_Node_Type::BITFIELD_ANONYMOUS )
			{
				if ( has_name )
				{
					log_dbg( "%sunexpected name for anonymous bitfield", indent );
					break;
				} else
				{
					log_dbg( "%sno name, as expected", indent );
				}
			}

			if (   start_match( &m )
			    && opt_bk( &m )
			    && match( &m, '{' )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
				log_dbg( "%sfound '{'", indent );
			} else
			{
				log_dbg( "%sexpected '{'", indent );
				break;
			}

			bool at_least_a_name = false;
			while (    ast_eat_start( ctx )
			        && ast_eat( ctx, AST_Node_Type::NAME )
			        && ast_eat_end( ctx ) )
			{
				at_least_a_name = true;

				log_dbg( "%sfound name", indent );
				AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );
				AST_Node * name_node = lpc->eaten_ast_nodes.first->element;

				m.current_node = pc->current_node = lpc->current_node;

				if (   start_match( &m )
				    && opt_bk( &m )
				    && match( &m, '[' )
				    && end_match( &m )
				   )
				{
					pc->current_node = m.current_node;
					log_dbg( "%sfound '['", indent );

					AST_Node * mbits_node = NULL;
					if (    ast_eat_start( ctx )
					     && ast_eat( ctx, AST_Node_Type::EXPRESSION, {.expression_square_br_after=1} )
					     && ast_eat_end( ctx ) )
					{
						AST_Eating_Context::Partial_Context * e_lpc = last_eaten_pctx( ctx );
						m.current_node = pc->current_node = e_lpc->current_node;
						AST_Node * expr_node = e_lpc->eaten_ast_nodes.first->element;
						mbits_node = ast_new_node( ctx->ast );
						mbits_node->type = AST_Node_Type::MANY_BITS_DECLARATION;
						mbits_node->file_range.first = name_node->file_range.first;
						list_vm_push_first( &mbits_node->children, &name_node );
						list_vm_push_last(  &mbits_node->children, &expr_node );
						list_vm_push_last( &bf_node->children, &mbits_node );
					} else
					{
						log_dbg( "%sexpected expression between '[]'", indent );
						goto BITFIELD_break_switch;
					}

					if (   start_match( &m )
					    && opt_bk( &m )
					    && match( &m, ']' )
					    && end_match( &m )
					   )
					{
						pc->current_node = m.current_node;
						UPDATE_LAST_FILE_POS();
						mbits_node->file_range.last = file_coord_last_start;
						log_dbg( "%sfound ']'", indent );
					} else
					{
						log_dbg( "%sexpected ']'", indent );
						goto BITFIELD_break_switch;
					}
				} else
				{
					list_vm_push_last( &bf_node->children, &name_node );
				}

				if (   start_match( &m )
				    && opt_bk( &m )
				    && match( &m, ';' )
				    && end_match( &m )
				   )
				{
					pc->current_node = m.current_node;
					log_dbg( "%sfound ';'", indent );
				} else
				{
					log_dbg( "%sexpected ';'", indent );
					goto BITFIELD_break_switch;
				}
			}

			if ( !at_least_a_name )
			{
				log_dbg( "%sexpected a name", indent );
				break;
			}

			if (   start_match( &m )
			    && opt_bk( &m )
			    && match( &m, '}' )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
				log_dbg( "%sfound '}'", indent );
			} else
			{
				log_dbg( "%sexpected '}'", indent );
				break;
			}

			UPDATE_LAST_FILE_POS();
			bf_node->file_range.last = file_coord_last_start;

			list_vm_push_last( &pc->eaten_ast_nodes, &bf_node );
			ret = true;
			BITFIELD_break_switch:void(0);
			break;
		}
		case AST_Node_Type::STRUCT_DECLARATION:
		{
			if ( ast_eat_start( ctx )
			  && ast_eat( ctx, AST_Node_Type::NAME )
			  && ast_eat_end( ctx ) )
			{
				log_dbg( "%sfound name", indent );
			} else
			{
				log_dbg( "%sexpected name", indent );
				break;
			}

			AST_Eating_Context::Partial_Context * st_name_pc = last_eaten_pctx( ctx );

			pc->current_node = st_name_pc->current_node;

			AST_Node * struct_decl_node = ast_new_node( ctx->ast );
			struct_decl_node->type = AST_Node_Type::STRUCT_DECLARATION;
			list_vm_push_last( &struct_decl_node->children, &st_name_pc->eaten_ast_nodes.first->element );
			list_vm_push_last( &pc->eaten_ast_nodes, &struct_decl_node );
			struct_decl_node->file_range.first = struct_decl_node->children.first->element->file_range.first;

			Matcher_Context m = {};
			m.current_node = pc->current_node;

			if (   start_match( &m )
			    && opt_bk( &m )
			    && match( &m, '{' )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
				log_dbg( "%sfound '{'", indent );
			} else
			{
				log_dbg( "%sexpected '{'", indent );
				break;
			}

			if (    ast_eat_start( ctx )
			     && ast_eat( ctx, AST_Node_Type::STRUCT_BODY )
			     && ast_eat_end( ctx ) )
			{
				AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );
				list_vm_move_list_last( &struct_decl_node->children, &lpc->eaten_ast_nodes );

				pc->current_node = lpc->current_node;
				m.current_node = pc->current_node;
				log_dbg( "%sfound struct body", indent );
			} else
			{
				log_dbg( "%sexpected a struct body", indent );
				break;
			}

			if (   start_match( &m )
			    && opt_bk( &m )
			    && match( &m, '}' )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
				log_dbg( "%sfound '}'", indent );
			} else
			{
				log_dbg( "%sexpected '}'", indent );
				break;
			}

			UPDATE_LAST_FILE_POS();
			struct_decl_node->file_range.last = file_coord_last_start;

			ret = true;

			break;
		}
		case AST_Node_Type::STRUCT_LITERAL_BODY:
		{
			Matcher_Context m = {};
			m.current_node = pc->current_node;

			AST_Node * struct_literal_body_node = ast_new_node( ctx->ast );
			struct_literal_body_node->type = type;

			if ( start_match( &m )
			     && opt_bk( &m )
			     && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
			}

			UPDATE_LAST_FILE_POS();
			struct_literal_body_node->file_range.first = file_coord_last_start;

			AST_Node_Type assignment_type = AST_Node_Type::EXPR_BINARY_OP_PREC_12;

			bool is_empty = true;
			bool is_broken = false;
			do
			{
				pc->current_node = m.current_node;

				AST_Eating_Context::Partial_Context * lpc, * tpc;

				if (    ast_eat_start( ctx )
				     && (
				             (    ast_eat_start( ctx )
				               && ast_eat( ctx, AST_Node_Type::THIS_OP )
				               && ast_eat( ctx, AST_Node_Type::NAME )
				               && ast_eat( ctx, AST_Node_Type::ASSIGNMENT_OP )
				               && ast_eat_end( ctx )
				               && ( ( tpc = current_pctx( ctx ) )->current_node = ( lpc = last_eaten_pctx( ctx ) )->current_node,
				                    list_vm_move_list_last( &tpc->eaten_ast_nodes, &lpc->eaten_ast_nodes ),
				                    true )
				             )
				          || true
				        )
				     && ast_eat( ctx, assignment_type )
				     && ast_eat_end( ctx ) )
				{
					lpc = last_eaten_pctx( ctx );

					AST_Node * struct_member_node = ast_new_node( ctx->ast );
					struct_member_node->type = AST_Node_Type::STRUCT_MEMBER_VALUE;
					list_vm_move_list_last( &struct_member_node->children, &lpc->eaten_ast_nodes );
					list_vm_push_last( &struct_literal_body_node->children, &struct_member_node );

					pc->current_node = lpc->current_node;
					m.current_node = pc->current_node;

					struct_member_node->file_range.first = file_coord_last_start;
					UPDATE_LAST_FILE_POS();
					struct_member_node->file_range.last = file_coord_last_start;

					log_dbg( "%sfound member value", indent );
				} else
				{
					log_dbg( "%sexpected member value", indent );
					is_broken = true;
					break;
				}
				is_empty = false;
			} while (    start_match( &m )
			          && match( &m, ',' )
			          && end_match( &m ) );

			if ( is_empty )
			{
				log_dbg( "%sempty struct literal body?", indent );
			} else
			if ( ! is_broken )
			{
				log_dbg( "%sfound a %s", indent, ast_node_type_strings[u32(type)] );
				list_vm_push_last( &pc->eaten_ast_nodes, &struct_literal_body_node );
				struct_literal_body_node->file_range.last = file_coord_last_start;
				ret = true;
			}

			break;
		}
		case AST_Node_Type::STRUCT_LITERAL:
		{
			Matcher_Context m = {};
			m.current_node = pc->current_node;

			if ( start_match( &m )
			     && opt_bk( &m )
			     && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
			}

			UPDATE_LAST_FILE_POS();

			if (   start_match( &m )
			    && match( &m, '{' )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
				log_dbg( "%sfound '{'", indent );
			} else
			{
				log_dbg( "%sexpected '{'", indent );
				break;
			}

			AST_Node * struct_literal_node = ast_new_node( ctx->ast );
			struct_literal_node->type = AST_Node_Type::STRUCT_LITERAL;
			struct_literal_node->file_range.first = file_coord_last_start;

			if (     ast_eat_start( ctx )
			     && ast_eat( ctx, AST_Node_Type::STRUCT_LITERAL_BODY )
			     && ast_eat_end( ctx )
			   )
			{
				log_dbg( "%sfound struct literal body", indent );
				AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( ctx );
				list_vm_move_list_last( &struct_literal_node->children, &lpc->eaten_ast_nodes );
				m.current_node = pc->current_node = lpc->current_node;
			} else
			{
				log_dbg( "%sstruct literal body not found, maybe it's a zero literal '{}'", indent );
			}

			if (   start_match( &m )
			    && opt_bk( &m )
			    && match( &m, '}' )
			    && end_match( &m )
			   )
			{
				pc->current_node = m.current_node;
				log_dbg( "%sfound '}'", indent );
			} else
			{
				log_dbg( "%sexpected '}'", indent );
				break;
			}

			UPDATE_LAST_FILE_POS();
			struct_literal_node->file_range.last = file_coord_last_start;

			list_vm_push_last( &pc->eaten_ast_nodes, &struct_literal_node );
			ret = true;

			break;
		}
		default:
			break;
	}



	ctx->just_failed = !pc->is_or && !ret;

	if ( ! ret )
	{
		// TODO(theGiallo, 2017-10-20): dealloc (also sub-nodes)
		list_vm_remove_all( &pc->eaten_ast_nodes );

		if ( ctx->just_failed )
		{
			log_dbg( "%spc->is_or = %s", indent, BTOS(pc->is_or) );
			log_dbg( "%sret = %s", indent, BTOS(ret) );
			log_dbg( "%s%s current_pctx( ctx ) = " FMT_PTR, indent, __PRETTY_FUNCTION__, (size_t)current_pctx( ctx ) );
			ast_eat_end( ctx ); // TODO(theGiallo, 2017-10-20): popping partial context is ok only if we don't do ORs
		} else
		{
			log_dbg( "%spc->is_or = %s", indent, BTOS(pc->is_or) );
			log_dbg( "%sret = %s", indent, BTOS(ret) );
			log_dbg( "%s%s current_pctx( ctx ) = " FMT_PTR, indent, __PRETTY_FUNCTION__, (size_t)current_pctx( ctx ) );
			restart_pctx( pc );
		}
	}

	return ret;
}

// TODO(theGiallo, 2017-10-19): write the dealloc of ast nodes and such


// internals
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// public interface

void
parser_init( Parser * parser )
{
	tg_assert( parser );
	{
		s64 max_capacity = 32L * 1024L * 1024L * 1024L / sizeof( *parser->errors.first );
		s64 initial_capacity = 0;
		bool b_res = list_vm_init( &parser->errors, max_capacity, initial_capacity );
		if ( !b_res )
		{
			log_err( "failed to initialize list_vm for errors with max capacity %luB", max_capacity );
			ILLEGAL_PATH();
		}
		//s64 max_capacity = PARSER_ERRORS_MAX_COUNT;
		//list_init_malloc( &parser->errors, capacity );
	}
	{
		s64 max_capacity = 32L * 1024L * 1024L * 1024L / sizeof( *parser->text_elements.first );
		s64 initial_capacity = 0;
		bool b_res = list_vm_init( &parser->text_elements, max_capacity, initial_capacity );
		if ( !b_res )
		{
			log_err( "failed to initialize list_vm for text elements with max capacity %luB", max_capacity );
			ILLEGAL_PATH();
		}
		//s64 capacity = PARSER_TEXT_ELEMENTS_MAX_COUNT;
		//list_init_malloc( &parser->text_elements, capacity );
	}
	{
		s64 element_size =  sizeof( AST_Node );
		s64 max_capacity     = ( 32L * 1024L * 1024L * 1024L ) / element_size;
		s64 initial_capacity = 1024;
		//void * memory = malloc( capacity * element_size );
		//pool_allocator_generic_init( &parser->ast.nodes_allocator, memory, capacity, element_size );
		pool_allocator_generic_vm_init( &parser->ast.nodes_allocator, element_size, max_capacity, initial_capacity );
		if ( !parser->ast.nodes_allocator.vm_start )
		{
			log_err( "failed to initialize pool allocator vm for ast nodes with max capacity %luB and initial capacity %luB", max_capacity, initial_capacity );
			ILLEGAL_PATH();
		}
	}
	{
		s64 element_size =  sizeof( AST_Node_p_List_VM_Node );
		s64 max_capacity     = ( 32L * 1024L * 1024L * 1024L ) / element_size;
		s64 initial_capacity = 1024;
		//void * memory = malloc( capacity * element_size );
		//pool_allocator_generic_init( &parser->ast.nodes_p_allocator, memory, capacity, element_size );
		pool_allocator_generic_vm_init( &parser->ast.nodes_p_allocator, element_size, max_capacity, initial_capacity );
		if ( !parser->ast.nodes_p_allocator.vm_start )
		{
			log_err( "failed to initialize pool allocator vm for ast nodes pointers with max capacity %luB and initial capacity %luB", max_capacity, initial_capacity );
			ILLEGAL_PATH();
		}
	}
	{
		s64 element_size =  sizeof( Text_Element_List_VM_Node );
		s64 max_capacity     = ( 32L * 1024L * 1024L * 1024L ) / element_size;
		s64 initial_capacity = 1024;
		//void * memory = malloc( capacity * element_size );
		//pool_allocator_generic_init( &parser->ast.text_elements_allocator, memory, capacity, element_size );
		pool_allocator_generic_vm_init( &parser->ast.text_elements_allocator, element_size, max_capacity, initial_capacity );
		if ( !parser->ast.text_elements_allocator.vm_start )
		{
			log_err( "failed to initialize pool allocator vm for ast text elements with max capacity %luB and initial capacity %luB", max_capacity, initial_capacity );
			ILLEGAL_PATH();
		}
	}
	{
		Memory_Area ma;
		ma.memory_bytes_size = AST_TEXT_TOTAL_MAX_SIZE;
		ma.memory = malloc( ma.memory_bytes_size );
		tg_assert( ma.memory );
		freelist_allocator_init( &parser->ast.text_allocator, ma );
	}
}

bool
parser_parse_file( Parser * parser, void * file_mem, s64 file_size )
{
	bool ret = false;

	u8 * file_bytes = (u8*) file_mem;
	u8 * file_bytes_cursor = file_bytes;
	Text_Element current_text_element = {};
#if 0
	#define PUSH_CURRENT_ELEMENT_AND_CLEAR_IT() \
		do { \
		list_vm_push_last( &parser->text_elements, &current_text_element );                     \
		printf( "pushing %s '", text_element_type_strings[(u32)current_text_element.type] ); \
		put_bytes( current_text_element.text.text, current_text_element.text.length );       \
		printf( "'\n" );                                                                     \
		current_text_element = {};                                                           \
		} while ( false )
#else
	#define PUSH_CURRENT_ELEMENT_AND_CLEAR_IT() \
		do { \
		list_vm_push_last( &parser->text_elements, &current_text_element ); \
		current_text_element = {};                                       \
		} while ( false )
#endif

	#define SET_CURRENT_ELEMENT(t) \
		do { \
		current_text_element.type = t; \
		current_text_element.text.text = file_bytes_cursor; \
		current_text_element.text.length = utf8_char_length; \
		current_text_element.in_file_pos = current_file_pos; \
		} while ( false )



	File_Coordinate current_file_pos = {};
	// TODO(theGiallo, 2017-11-07): there's a read outside of blob here,
	// detected with Valgrind, FIXME
	for (;;)
	{
		UTF8_Fullchar utf8_fc = utf8_full_char( file_bytes_cursor );
		u8 * next = utf8_next_char_ptr( file_bytes_cursor, file_bytes, file_size );
		u32 utf8_char_length = (s64)next - (s64)file_bytes_cursor;
		if ( !next )
		{
			break;
		}
		#if 0
		printf( "current utf8 char is '" );
		utf8_print_char( utf8_fc );
		printf( "'\n" );
		#endif

		u32 ucs4 = utf8_fullchar_to_ucs4( utf8_fc );

		if ( ucs4_is_blank( ucs4 ) )
		{
			if ( current_text_element.type == Text_Element_Type::BLANKS )
			{
				current_text_element.text.length += utf8_char_length;
			} else
			if ( current_text_element.type != Text_Element_Type::NONE )
			{
				PUSH_CURRENT_ELEMENT_AND_CLEAR_IT();
			}
			if ( current_text_element.type == Text_Element_Type::NONE )
			{
				SET_CURRENT_ELEMENT( Text_Element_Type::BLANKS );
			}
		} else
		if (    ucs4 == '_'
		     || ucs4_is_alpha( ucs4 )
		     || ucs4_is_num  ( ucs4 ) )
		{
			if ( current_text_element.type == Text_Element_Type::WORD )
			{
				current_text_element.text.length += utf8_char_length;
			} else
			if ( current_text_element.type != Text_Element_Type::NONE )
			{
				PUSH_CURRENT_ELEMENT_AND_CLEAR_IT();
			}
			if ( current_text_element.type == Text_Element_Type::NONE )
			{
				SET_CURRENT_ELEMENT( Text_Element_Type::WORD );
			}
		} else
		if ( ucs4 == '\n' )
		{
			if ( current_text_element.type != Text_Element_Type::NONE )
			{
				PUSH_CURRENT_ELEMENT_AND_CLEAR_IT();
			}
			SET_CURRENT_ELEMENT( Text_Element_Type::LINE_FEED );
			PUSH_CURRENT_ELEMENT_AND_CLEAR_IT();
		} else
		if ( ucs4_is_accepted_symbol( ucs4 ) )
		{
			if ( current_text_element.type != Text_Element_Type::NONE )
			{
				PUSH_CURRENT_ELEMENT_AND_CLEAR_IT();
			}
			SET_CURRENT_ELEMENT( Text_Element_Type::SYMBOL );
			PUSH_CURRENT_ELEMENT_AND_CLEAR_IT();
		} else
		{
			if ( current_text_element.type != Text_Element_Type::NONE )
			{
				PUSH_CURRENT_ELEMENT_AND_CLEAR_IT();
			}
			SET_CURRENT_ELEMENT( Text_Element_Type::INVALID );
			PUSH_CURRENT_ELEMENT_AND_CLEAR_IT();
		}

		if ( ucs4 == '\n' )
		{
			++current_file_pos.line;
			current_file_pos.utf8_chars_in_line = 0;
		} else
		{
			current_file_pos.bytes_from_file_start += utf8_char_length;
			++current_file_pos.utf8_chars_in_line;
		}

		file_bytes_cursor = next;
	}


	// NOTE(theGiallo): strip comments

	for ( Text_Element_List_VM_Node * node      = parser->text_elements.first,
	                                * next_node = node ? node->next : NULL,
	                                * prev_node = NULL;
	      node;
	      prev_node = node, node = next_node, next_node = node ? node->next : NULL )
	{
		if ( text_element_node_is_symbol_char(      node, '/' )
		  && text_element_node_is_symbol_char( next_node, '/' ) )
		{
			Text_Element_List_VM_Node * end_of_line = find_node_end_of_line( next_node->next );

			next_node = end_of_line ? end_of_line->next : NULL;
			//putchar( '\n' );
			// NOTE(theGiallo): we keep the end of line on purpose
			log_dbg( "removing comment..." );
			if ( prev_node )
			{
				while ( prev_node->next != end_of_line )
				{
					put_bytes( prev_node->element.text.text, prev_node->element.text.length );
					list_vm_remove_next( &parser->text_elements, prev_node );
				}
			} else
			{
				while ( parser->text_elements.first != end_of_line )
				{
					put_bytes( parser->text_elements.first->element.text.text,
					           parser->text_elements.first->element.text.length );
					list_vm_remove_first( &parser->text_elements );
				}
			}
			fflush( stdout );
			log_dbg( "removed comment" );
			node = prev_node;
		} else
		{
			//put_bytes( node->element.text.text, node->element.text.length );
			//fflush( stdout );
		}
	}
	putchar( '\n' );


	// NOTE(theGiallo): build AST

	AST * ast = &parser->ast;
	ast->root = ast_new_node( ast );
	ast->root->type = AST_Node_Type::ROOT;
	AST_Node * current_ast_node = ast->root;
	AST_Eating_Context ast_eat_ctx = {};
	ast_eat_ctx.ast = ast;
	Mem_Stack pcs = {};
	pcs.total_size = 4 * 4 * 1024;
	pcs.mem_buf = (u8*)malloc( pcs.total_size ); // TODO(theGiallo, 2017-10-20): free this
	tg_assert( pcs.mem_buf );
	ast_eat_ctx.partial_contexts_stack = pcs;
	{
		//AST_Eating_Context::Partial_Context * pc =
		//   (AST_Eating_Context::Partial_Context*)
		   ast_eat_ctx.partial_contexts_stack.push( sizeof( AST_Eating_Context::Partial_Context ) );

		current_pctx( &ast_eat_ctx )->current_node = parser->text_elements.first;
	}

	for ( Text_Element_List_VM_Node * node      = parser->text_elements.first,
	                                * next_node = node ? node->next : NULL;
	      node;
	      node = next_node, next_node = node ? node->next : NULL )
	{
		//#if DEBUG
		//printf( "node = '" );
		//put_bytes( node->element.text.text, node->element.text.length );
		//printf( "'\n" );
		//printf( "next_node = '" );
		//if ( next_node )
		//{
		//	put_bytes( next_node->element.text.text, next_node->element.text.length );
		//} else
		//{
		//	printf( "NULL" );
		//}
		//printf( "'\n" );
		//#endif

		current_pctx( &ast_eat_ctx )->current_node = node;

		{
			if (    (    ast_eat_start( &ast_eat_ctx )
			          && ast_eat( &ast_eat_ctx, AST_Node_Type::FUNCTION_DECLARATION )
			          && ast_eat_end( &ast_eat_ctx ) )
			     || (    ast_eat_start( &ast_eat_ctx )
			          && ast_eat( &ast_eat_ctx, AST_Node_Type::STRUCT_DECLARATION   )
			          && ast_eat_end( &ast_eat_ctx ) )
			     || (    ast_eat_start( &ast_eat_ctx )
			          && ast_eat( &ast_eat_ctx, AST_Node_Type::BITFIELD_DECLARATION )
			          && ast_eat_end( &ast_eat_ctx ) )
			     || (    ast_eat_start( &ast_eat_ctx )
			          && ast_eat( &ast_eat_ctx, AST_Node_Type::UNION_DECLARATION    )
			          && ast_eat_end( &ast_eat_ctx ) )
			     || (    ast_eat_start( &ast_eat_ctx )
			          && ast_eat( &ast_eat_ctx, AST_Node_Type::UNION_ANONYMOUS      )
			          && ast_eat_end( &ast_eat_ctx ) )
			//     || (    ast_eat_start( &ast_eat_ctx )
			//          && ast_eat( &ast_eat_ctx, AST_Node_Type::VARIABLE_DECLARATION )
			//          && ast_eat_end( &ast_eat_ctx ) )
			//     || (    ast_eat_start( &ast_eat_ctx )
			//          && ast_eat( &ast_eat_ctx, AST_Node_Type::CONSTANT_DECLARATION )
			//          && ast_eat_end( &ast_eat_ctx ) )
			// TODO(theGiallo, 2017-12-30): enable using in global scope?
			// TODO(theGiallo, 2017-12-30): maybe use CODE_BLOCK parsing directly.
			//     || (    ast_eat_start( &ast_eat_ctx )
			//          && ast_eat( &ast_eat_ctx, AST_Node_Type::USING   )
			//          && ast_eat_end( &ast_eat_ctx ) )
			   )
			{
				AST_Eating_Context::Partial_Context * lpc = last_eaten_pctx( &ast_eat_ctx );
				list_vm_move_list_last( &ast->root->children, &lpc->eaten_ast_nodes );
				next_node = lpc->current_node;
				continue;
			}
		}

		// NOTE(theGiallo): default to UNKNOWN
		{
			Text_Element * e = &node->element;
			if ( e->type == Text_Element_Type::LINE_FEED )
			{
				continue;
			}
			if ( !e->text.text[0] )
			{
				log_dbg( "probably EOF" );
				continue;
			}
			log_dbg( "would default to UNKNOWN type %s length = %u %x", text_element_type_strings[(u32)e->type], e->text.length, e->text.text[0] );
			putchar( '\'' );
			put_bytes( e->text.text, e->text.length );
			printf( "'\n" );
			ILLEGAL_PATH();
			log_dbg( "defaulting to UNKNOWN type %s", text_element_type_strings[(u32)e->type] );

			Text_Element this_text_element = {};
			this_text_element.type = e->type;
			this_text_element.text.length = e->text.length;
			this_text_element.text.text =
			   (u8*)freelist_allocator_allocate( &ast->text_allocator, e->text.length );
			tg_assert( this_text_element.text.text );
			memcpy( this_text_element.text.text, e->text.text, e->text.length );
			AST_Node * this_node = ast_new_node( ast );
			this_node->type = AST_Node_Type::UNKNOWN;
			list_vm_push_last( &this_node->text_element_nodes, &this_text_element );

			list_vm_push_last( &current_ast_node->children, &this_node );
		}
	}


	return ret;
}

#define INDENT() \
for ( s32 i = 0; i != indent; ++i ) \
{\
	putchar( '\t' );\
}\

#define INDENT_(increment) \
for ( s32 i = 0; i != indent + increment; ++i ) \
{\
	putchar( '\t' );\
}\


internal
void
print_colored_text_elements( Text_Element_List_VM * text_elements, s32 indent = 0 )
{
	if ( text_elements->first )
	{
		INDENT()
		printf( "line: %u\n", 1 + text_elements->first->element.in_file_pos.line );
		INDENT()
		printf( "utf-8 char: %u\n", text_elements->first->element.in_file_pos.utf8_chars_in_line );
		INDENT()
		printf( "bytes from file start: %u\n", text_elements->first->element.in_file_pos.bytes_from_file_start );
	}

	bool fed = true;
	for ( Text_Element_List_VM_Node * node = text_elements->first;
	      node;
	      node = node->next )
	{
		if ( fed )
		{
			INDENT()
			fed = false;
		}
		Text_Element * e = &node->element;
		// NOTE(theGiallo): printing colors
		// CNSL_RESET
		// CNSL_BLACK
		// CNSL_RED
		// CNSL_GREEN
		// CNSL_YELLOW
		// CNSL_BLUE
		// CNSL_PURPLE
		// CNSL_CYAN
		// CNSL_WHITE
		switch ( e->type )
		{
			case Text_Element_Type::WORD:
				put_bytes( (u8*)CNSL_BLUE, ARRAY_COUNT(CNSL_BLUE) - 1 );
				break;
			case Text_Element_Type::SYMBOL:
				put_bytes( (u8*)CNSL_PURPLE, ARRAY_COUNT(CNSL_PURPLE) - 1 );
				break;
			case Text_Element_Type::LINE_FEED:
				put_bytes( (u8*)CNSL_YELLOW, ARRAY_COUNT(CNSL_YELLOW) - 1 );
				printf( u8"↵" );
				break;
			case Text_Element_Type::BLANKS:
				put_bytes( (u8*)CNSL_RESET, ARRAY_COUNT(CNSL_RESET) - 1 );
				break;
			case Text_Element_Type::INVALID:
				put_bytes( (u8*)CNSL_RED, ARRAY_COUNT(CNSL_RED) - 1 );
				break;
			case Text_Element_Type::BLOB:
				put_bytes( (u8*)CNSL_GREEN, ARRAY_COUNT(CNSL_GREEN) - 1 );
				break;
			default:
				ILLEGAL_PATH();
		}
		put_bytes( e->text.text, e->text.length );
		fed = e->type == Text_Element_Type::LINE_FEED;
	}
	put_bytes( (u8*)CNSL_RESET, ARRAY_COUNT(CNSL_RESET) - 1 );
}

void
parser_print_debug_parsed_elements( Parser * parser )
{
	print_colored_text_elements( &parser->text_elements );
	put_bytes( (u8*)"\n", 1 );
}

internal
void
print_ast_node_recursive( AST_Node * node, AST_Node * father, s32 indent = 0 )
{
	INDENT()
	printf( "---------- node 0x%016lx (child of 0x%016lx):\n", (size_t)node, (size_t)father );
	INDENT()
	printf( "type: %s\n", ast_node_type_strings[(u32)node->type] );
	INDENT()
	printf( "file range:\n" );
	INDENT_(1)
	printf( "from: \n" );
	INDENT_(2)
	printf( "line: %u\n", 1 + node->file_range.first.line );
	INDENT_(2)
	printf( "utf-8 char: %u\n", node->file_range.first.utf8_chars_in_line );
	INDENT_(2)
	printf( "bytes from file start: %u\n", node->file_range.first.bytes_from_file_start );
	INDENT_(1)
	printf( "to: \n" );
	INDENT_(2)
	printf( "line: %u\n", 1 + node->file_range.last.line );
	INDENT_(2)
	printf( "utf-8 char: %u\n", node->file_range.last.utf8_chars_in_line );
	INDENT_(2)
	printf( "bytes from file start: %u\n", node->file_range.last.bytes_from_file_start );
	INDENT_(2)
	printf( "text elements:\n" );
	print_colored_text_elements( &node->text_element_nodes, indent );
	PUT_BYTES( "\n" );
	INDENT()
	PUT_BYTES( "/text_elements\n" );
	INDENT()
	printf( "children:\n" );
	for ( AST_Node_p_List_VM_Node * node_p = node->children.first;
	      node_p;
	      node_p = node_p->next )
	{
		print_ast_node_recursive( node_p->element, node, indent + 1 );
	}
}

void
parser_print_debug_ast( Parser * parser )
{
	print_ast_node_recursive( parser->ast.root, NULL );
}


// public interface
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// unicode temporary stuff

bool
ucs4_is_alpha( u32 ucs4 )
{
	bool ret =
	   ( ucs4 >= 'a' && ucs4 <= 'z' )
	|| ( ucs4 >= 'A' && ucs4 <= 'Z' );

	return ret;

}
bool
ucs4_is_num  ( u32 ucs4 )
{
	bool ret = ( ucs4 >= '0' && ucs4 <= '9' );

	return ret;

}
bool
ucs4_is_blank( u32 ucs4 )
{
	bool ret =
	(    ucs4 == ' '
	  || ucs4 == '\t' );

	return ret;

}
bool
ucs4_is_accepted_symbol( u32 ucs4 )
{
	bool ret = false;
	u8 symbols[] = ",.<>?/|\\`;:'@#~[]{}!\"$%^&*()+-=";
	for ( s32 i = 0; i != ARRAY_COUNT( symbols ); ++i )
	{
		ret = ( ret || ( symbols[i] == ucs4 ) );
	}

	return ret;
}

// unicode temporary stuff
////////////////////////////////////////////////////////////////////////////////



#define LIST_VM_BODY_ONLY 1
#define LIST_VM_EMIT_BODY 1
#define LIST_VM_TYPE Text_Element
#include "list_vm.inc.h"
#define LIST_VM_TYPE Parser_Error
#include "list_vm.inc.h"
#define LIST_VM_TYPE AST_Node_p
#include "list_vm.inc.h"
#undef LIST_VM_EMIT_BODY
