/*

Copyright (c) <2017> <Gianluca Alloisio>

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgement in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.

*/
/*
   NOTE(theGiallo): List - Mono-Directional List 2017-10-08
 */

#include "pool_allocator_generic.h"

#define L_GLUE2(a,b) a##b
#define L_GLUE2M(a,b) L_GLUE2(a,b)
#define L_GLUE2MM(a,b) L_GLUE2M(a,b)
#define L_GLUE3(a,b,c) a##b##c
#define L_GLUE3M(a,b,c) L_GLUE3(a,b,c)

#ifndef LIST_TYPE
#error  You have to define LIST_TYPE and optionally LIST_SHORT_TYPE before \
including list.inc.h
#define LIST_TYPE int
#endif

#ifdef LIST_STATIC
#define LIST_DEF static
#else
#define LIST_DEF extern
#endif

#ifndef LIST_TYPE_SHORT
#define LIST_TYPE_SHORT LIST_TYPE
#endif

#define LIST_FULL_TYPE L_GLUE2M(LIST_TYPE,_List)
#define LIST_SHORT_TYPE L_GLUE2M(LIST_TYPE_SHORT,_List)
#define LIST_NODE_TYPE L_GLUE2MM(LIST_FULL_TYPE,_Node)
#define LIST_NODE_SHORT_TYPE L_GLUE2MM(LIST_SHORT_TYPE,_Node)
#define LIST_NODE_TYPE_ST L_GLUE2MM(LIST_FULL_TYPE,_Node_st)

#if ! LIST_BODY_ONLY

typedef
struct
LIST_NODE_TYPE_ST
{
	LIST_TYPE           element;
	LIST_NODE_TYPE_ST * next;
} LIST_NODE_TYPE, LIST_NODE_SHORT_TYPE;
typedef
struct
L_GLUE2M(LIST_FULL_TYPE,_st)
{
	LIST_NODE_TYPE * first;
	LIST_NODE_TYPE * last;
	u64 length;
	Pool_Allocator_Generic * nodes_allocator;
} LIST_FULL_TYPE, LIST_SHORT_TYPE;

LIST_DEF
void
list_init( LIST_FULL_TYPE * list, Pool_Allocator_Generic * nodes_allocator );

LIST_DEF
bool
list_init_malloc( LIST_FULL_TYPE * list, s64 capacity );

LIST_DEF
bool
list_push_first( LIST_FULL_TYPE * list, LIST_TYPE * element );

LIST_DEF
bool
list_push_last( LIST_FULL_TYPE * list, LIST_TYPE * element );

LIST_DEF
bool
list_move_list_first( LIST_FULL_TYPE * list, LIST_FULL_TYPE * list_to_prepend );

LIST_DEF
bool
list_move_list_last( LIST_FULL_TYPE * list, LIST_FULL_TYPE * list_to_append );

LIST_DEF
bool
list_insert_after( LIST_FULL_TYPE * list, LIST_TYPE * element, LIST_NODE_TYPE * node );

LIST_DEF
bool
list_remove_next( LIST_FULL_TYPE * list, LIST_NODE_TYPE * node );

LIST_DEF
bool
list_remove_first( LIST_FULL_TYPE * list );

LIST_DEF
void
list_remove_all( LIST_FULL_TYPE * list );

#endif // LIST_BODY_ONLY

////////////////////////////////////////////////////////////////////////////////
#if LIST_EMIT_BODY

LIST_DEF
void
list_init( LIST_FULL_TYPE * list, Pool_Allocator_Generic * nodes_allocator )
{
	tg_assert( list );
	tg_assert( nodes_allocator );
	{
		LIST_FULL_TYPE empty_one = {};
		tg_assert( memcmp( list, &empty_one, sizeof( LIST_FULL_TYPE ) ) == 0 );
	}

	list->nodes_allocator = nodes_allocator;
	tg_assert( nodes_allocator->element_size == sizeof( LIST_NODE_TYPE ) );
}

LIST_DEF
bool
list_init_malloc( LIST_FULL_TYPE * list, s64 capacity )
{
	bool ret = false;

	Pool_Allocator_Generic * nodes_allocator = (Pool_Allocator_Generic*)malloc( sizeof( *nodes_allocator ) );
	if ( !nodes_allocator )
	{
		return ret;
	}
	s64 element_size = sizeof( LIST_NODE_TYPE );
	void * mem = malloc( element_size * capacity );
	if ( !mem )
	{
		return ret;
	}
	pool_allocator_generic_init( nodes_allocator, mem, capacity, element_size );

	list_init( list, nodes_allocator );

	ret = true;

	return ret;
}

LIST_DEF
bool
list_push_first( LIST_FULL_TYPE * list, LIST_TYPE * element )
{
	bool ret = false;
	tg_assert( list );
	tg_assert( element );
	LIST_NODE_TYPE * n = (LIST_NODE_TYPE *) pool_allocator_generic_allocate( list->nodes_allocator );
	if ( !n )
	{
		return ret;
	}

	memcpy( &n->element, element, sizeof( LIST_TYPE ) );
	n->next = list->first;
	list->first = n;
	if ( !list->last )
	{
		list->last = n;
	}
	++list->length;
	ret = true;

	return ret;
}

LIST_DEF
bool
list_push_last( LIST_FULL_TYPE * list, LIST_TYPE * element )
{
	bool ret = false;
	tg_assert( list );
	tg_assert( element );
	LIST_NODE_TYPE * n = (LIST_NODE_TYPE *) pool_allocator_generic_allocate( list->nodes_allocator );
	if ( !n )
	{
		return ret;
	}

	memcpy( &n->element, element, sizeof( LIST_TYPE ) );
	n->next = NULL;
	if ( list->last )
	{
		list->last->next = n;
	} else
	{
		tg_assert( !list->first );
		list->first = n;
	}
	list->last = n;
	++list->length;
	ret = true;

	return ret;
}

LIST_DEF
bool
list_move_list_first( LIST_FULL_TYPE * list, LIST_FULL_TYPE * list_to_prepend )
{
	bool ret = false;
	tg_assert( list );
	tg_assert( list->nodes_allocator == list_to_prepend->nodes_allocator );

	if ( ! list_to_prepend->first )
	{
		return ret;
	}

	if ( !list->first )
	{
		list->first  = list_to_prepend->first;
		list->last   = list_to_prepend->last;
		list->length = list_to_prepend->length;
	} else
	{
		list_to_prepend->last->next = list->first;
		list->first      = list_to_prepend->first;
		list->length    += list_to_prepend->length;
	}
	list_to_prepend->first  = 0;
	list_to_prepend->last   = 0;
	list_to_prepend->length = 0;

	return ret;
}

LIST_DEF
bool
list_move_list_last( LIST_FULL_TYPE * list, LIST_FULL_TYPE * list_to_append )
{
	bool ret = false;
	tg_assert( list );
	tg_assert( list->nodes_allocator == list_to_append->nodes_allocator );

	if ( ! list_to_append->first )
	{
		return ret;
	}

	if ( !list->first )
	{
		list->first  = list_to_append->first;
		list->last   = list_to_append->last;
		list->length = list_to_append->length;
	} else
	{
		list->last->next = list_to_append->first;
		list->last       = list_to_append->last;
		list->length    += list_to_append->length;
	}
	list_to_append->first  = 0;
	list_to_append->last   = 0;
	list_to_append->length = 0;

	return ret;
}

LIST_DEF
bool
list_insert_after( LIST_FULL_TYPE * list, LIST_TYPE * element, LIST_NODE_TYPE * node )
{
	bool ret = false;
	tg_assert( list );
	tg_assert( node );
	tg_assert( element );
	LIST_NODE_TYPE * n = (LIST_NODE_TYPE *) pool_allocator_generic_allocate( list->nodes_allocator );
	if ( !n )
	{
		return ret;
	}

	memcpy( &n->element, element, sizeof( LIST_TYPE ) );
	if ( node == list->last )
	{
		list->last = n;
	}
	n->next = node->next;
	node->next = n;

	++list->length;
	ret = true;

	return ret;
}

LIST_DEF
bool
list_remove_next( LIST_FULL_TYPE * list, LIST_NODE_TYPE * node )
{
	bool ret = false;
	tg_assert( list );
	tg_assert( node );

	if ( !node->next )
	{
		return ret;
	}

	if ( list->last == node->next )
	{
		list->last = node;
	}

	--list->length;

	LIST_NODE_TYPE * to_deallocate = node->next;
	node->next = node->next->next;
	pool_allocator_generic_deallocate( list->nodes_allocator, to_deallocate );

	ret = true;

	return ret;
}

LIST_DEF
bool
list_remove_first( LIST_FULL_TYPE * list )
{
	bool ret = false;
	tg_assert( list );

	if ( !list->first )
	{
		tg_assert( !list->last );
		return ret;
	}

	if ( list->last == list->first )
	{
		list->last = NULL;
	}

	LIST_NODE_TYPE * to_del = list->first;
	list->first = to_del->next;
	--list->length;
	pool_allocator_generic_deallocate( list->nodes_allocator, to_del );

	ret = true;

	return ret;
}

LIST_DEF
void
list_remove_all( LIST_FULL_TYPE * list )
{
	tg_assert( list );

	while ( list_remove_first( list ) ){}
}

#endif // LIST_EMIT_BODY

#undef LIST_NODE_TYPE_ST
#undef LIST_NODE_SHORT_TYPE
#undef LIST_NODE_TYPE
#undef LIST_SHORT_TYPE
#undef LIST_FULL_TYPE
#undef LIST_DEF
#undef L_GLUE3M
#undef L_GLUE3
#undef L_GLUE2MM
#undef L_GLUE2M
#undef L_GLUE2
#undef LIST_TYPE
