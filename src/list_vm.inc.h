/*

Copyright (c) <2018> <Gianluca Alloisio>

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgement in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.

*/
/*
   NOTE(theGiallo): List - Mono-Directional List 2017-10-08
   NOTE(theGiallo): List_VM - Mono-Directional List with use of virtual memory 2018-04-09
 */

#include "pool_allocator_generic_vm.h"

#define L_GLUE2(a,b) a##b
#define L_GLUE2M(a,b) L_GLUE2(a,b)
#define L_GLUE2MM(a,b) L_GLUE2M(a,b)
#define L_GLUE3(a,b,c) a##b##c
#define L_GLUE3M(a,b,c) L_GLUE3(a,b,c)

#ifndef LIST_VM_TYPE
#error  You have to define LIST_VM_TYPE and optionally LIST_VM_SHORT_TYPE \
before including list_vm.inc.h
#define LIST_VM_TYPE int
#endif

#ifdef LIST_VM_STATIC
#define LIST_VM_DEF static
#else
#define LIST_VM_DEF extern
#endif

#ifndef LIST_VM_TYPE_SHORT
#define LIST_VM_TYPE_SHORT LIST_VM_TYPE
#endif

#define LIST_VM_FULL_TYPE L_GLUE2M(LIST_VM_TYPE,_List_VM)
#define LIST_VM_SHORT_TYPE L_GLUE2M(LIST_VM_TYPE_SHORT,_List_VM)
#define LIST_VM_NODE_TYPE L_GLUE2MM(LIST_VM_FULL_TYPE,_Node)
#define LIST_VM_NODE_SHORT_TYPE L_GLUE2MM(LIST_VM_SHORT_TYPE,_Node)
#define LIST_VM_NODE_TYPE_ST L_GLUE2MM(LIST_VM_FULL_TYPE,_Node_st)

#if ! LIST_VM_BODY_ONLY

typedef
struct
LIST_VM_NODE_TYPE_ST
{
	LIST_VM_TYPE           element;
	LIST_VM_NODE_TYPE_ST * next;
} LIST_VM_NODE_TYPE, LIST_VM_NODE_SHORT_TYPE;
typedef
struct
L_GLUE2M(LIST_VM_FULL_TYPE,_st)
{
	LIST_VM_NODE_TYPE * first;
	LIST_VM_NODE_TYPE * last;
	u64 length;
	Pool_Allocator_Generic_VM * nodes_allocator_p;
	Pool_Allocator_Generic_VM nodes_allocator;
} LIST_VM_FULL_TYPE, LIST_VM_SHORT_TYPE;

LIST_VM_DEF
void
list_vm_init( LIST_VM_FULL_TYPE * list, Pool_Allocator_Generic_VM * nodes_allocator_p );

LIST_VM_DEF
bool
list_vm_init( LIST_VM_FULL_TYPE * list, s64 max_capacity, s64 initial_capacity );

LIST_VM_DEF
bool
list_vm_push_first( LIST_VM_FULL_TYPE * list, LIST_VM_TYPE * element );

LIST_VM_DEF
bool
list_vm_push_last( LIST_VM_FULL_TYPE * list, LIST_VM_TYPE * element );

LIST_VM_DEF
bool
list_vm_move_list_first( LIST_VM_FULL_TYPE * list, LIST_VM_FULL_TYPE * list_vm_to_prepend );

LIST_VM_DEF
bool
list_vm_move_list_last( LIST_VM_FULL_TYPE * list, LIST_VM_FULL_TYPE * list_vm_to_append );

LIST_VM_DEF
bool
list_vm_insert_after( LIST_VM_FULL_TYPE * list, LIST_VM_TYPE * element, LIST_VM_NODE_TYPE * node );

LIST_VM_DEF
bool
list_vm_remove_next( LIST_VM_FULL_TYPE * list, LIST_VM_NODE_TYPE * node );

LIST_VM_DEF
bool
list_vm_remove_first( LIST_VM_FULL_TYPE * list );

LIST_VM_DEF
void
list_vm_remove_all( LIST_VM_FULL_TYPE * list );

#endif // LIST_VM_BODY_ONLY

////////////////////////////////////////////////////////////////////////////////
#if LIST_VM_EMIT_BODY

#define NODES_POOL_ALLOCATOR_VM_POINTER(l) ((l)->nodes_allocator_p?(l)->nodes_allocator_p:&(l)->nodes_allocator)

LIST_VM_DEF
void
list_vm_init( LIST_VM_FULL_TYPE * list, Pool_Allocator_Generic_VM * nodes_allocator_p )
{
	tg_assert( list );
	tg_assert( nodes_allocator_p );
	{
		LIST_VM_FULL_TYPE empty_one = {};
		tg_assert( memcmp( list, &empty_one, sizeof( LIST_VM_FULL_TYPE ) ) == 0 );
	}

	list->nodes_allocator_p = nodes_allocator_p;
	tg_assert( nodes_allocator_p->element_size == sizeof( LIST_VM_NODE_TYPE ) );
}

LIST_VM_DEF
bool
list_vm_init( LIST_VM_FULL_TYPE * list, s64 max_capacity, s64 initial_capacity )
{
	bool ret = false;

	tg_assert( list );
	{
		LIST_VM_FULL_TYPE empty_one = {};
		tg_assert( memcmp( list, &empty_one, sizeof( LIST_VM_FULL_TYPE ) ) == 0 );
	}

	list->nodes_allocator_p = NULL;

	u64 element_size = sizeof( LIST_VM_NODE_TYPE );
	pool_allocator_generic_vm_init( &list->nodes_allocator, element_size, max_capacity, initial_capacity );

	ret = list->nodes_allocator.vm_start;

	return ret;
}

LIST_VM_DEF
bool
list_vm_push_first( LIST_VM_FULL_TYPE * list, LIST_VM_TYPE * element )
{
	bool ret = false;
	tg_assert( list );
	tg_assert( element );
	LIST_VM_NODE_TYPE * n = (LIST_VM_NODE_TYPE *) pool_allocator_generic_vm_allocate( NODES_POOL_ALLOCATOR_VM_POINTER(list) );
	if ( !n )
	{
		return ret;
	}

	memcpy( &n->element, element, sizeof( LIST_VM_TYPE ) );
	n->next = list->first;
	list->first = n;
	if ( !list->last )
	{
		list->last = n;
	}
	++list->length;
	ret = true;

	return ret;
}

LIST_VM_DEF
bool
list_vm_push_last( LIST_VM_FULL_TYPE * list, LIST_VM_TYPE * element )
{
	bool ret = false;
	tg_assert( list );
	tg_assert( element );
	LIST_VM_NODE_TYPE * n = (LIST_VM_NODE_TYPE *) pool_allocator_generic_vm_allocate( NODES_POOL_ALLOCATOR_VM_POINTER(list) );
	if ( !n )
	{
		return ret;
	}

	memcpy( &n->element, element, sizeof( LIST_VM_TYPE ) );
	n->next = NULL;
	if ( list->last )
	{
		list->last->next = n;
	} else
	{
		tg_assert( !list->first );
		list->first = n;
	}
	list->last = n;
	++list->length;
	ret = true;

	return ret;
}

LIST_VM_DEF
bool
list_vm_move_list_first( LIST_VM_FULL_TYPE * list, LIST_VM_FULL_TYPE * list_vm_to_prepend )
{
	bool ret = false;
	tg_assert( list );
	tg_assert( NODES_POOL_ALLOCATOR_VM_POINTER(list) == NODES_POOL_ALLOCATOR_VM_POINTER(list_vm_to_prepend) );

	if ( ! list_vm_to_prepend->first )
	{
		return ret;
	}

	if ( !list->first )
	{
		list->first  = list_vm_to_prepend->first;
		list->last   = list_vm_to_prepend->last;
		list->length = list_vm_to_prepend->length;
	} else
	{
		list_vm_to_prepend->last->next = list->first;
		list->first      = list_vm_to_prepend->first;
		list->length    += list_vm_to_prepend->length;
	}
	list_vm_to_prepend->first  = 0;
	list_vm_to_prepend->last   = 0;
	list_vm_to_prepend->length = 0;

	return ret;
}

LIST_VM_DEF
bool
list_vm_move_list_last( LIST_VM_FULL_TYPE * list, LIST_VM_FULL_TYPE * list_vm_to_append )
{
	bool ret = false;
	tg_assert( list );
	tg_assert( NODES_POOL_ALLOCATOR_VM_POINTER(list) == NODES_POOL_ALLOCATOR_VM_POINTER(list_vm_to_append) );

	if ( ! list_vm_to_append->first )
	{
		return ret;
	}

	if ( !list->first )
	{
		list->first  = list_vm_to_append->first;
		list->last   = list_vm_to_append->last;
		list->length = list_vm_to_append->length;
	} else
	{
		list->last->next = list_vm_to_append->first;
		list->last       = list_vm_to_append->last;
		list->length    += list_vm_to_append->length;
	}
	list_vm_to_append->first  = 0;
	list_vm_to_append->last   = 0;
	list_vm_to_append->length = 0;

	return ret;
}

LIST_VM_DEF
bool
list_vm_insert_after( LIST_VM_FULL_TYPE * list, LIST_VM_TYPE * element, LIST_VM_NODE_TYPE * node )
{
	bool ret = false;
	tg_assert( list );
	tg_assert( node );
	tg_assert( element );
	LIST_VM_NODE_TYPE * n = (LIST_VM_NODE_TYPE *) pool_allocator_generic_vm_allocate( NODES_POOL_ALLOCATOR_VM_POINTER(list) );
	if ( !n )
	{
		return ret;
	}

	memcpy( &n->element, element, sizeof( LIST_VM_TYPE ) );
	if ( node == list->last )
	{
		list->last = n;
	}
	n->next = node->next;
	node->next = n;

	++list->length;
	ret = true;

	return ret;
}

LIST_VM_DEF
bool
list_vm_remove_next( LIST_VM_FULL_TYPE * list, LIST_VM_NODE_TYPE * node )
{
	bool ret = false;
	tg_assert( list );
	tg_assert( node );

	if ( !node->next )
	{
		return ret;
	}

	if ( list->last == node->next )
	{
		list->last = node;
	}

	--list->length;

	LIST_VM_NODE_TYPE * to_deallocate = node->next;
	node->next = node->next->next;
	pool_allocator_generic_vm_deallocate( NODES_POOL_ALLOCATOR_VM_POINTER(list), to_deallocate );

	ret = true;

	return ret;
}

LIST_VM_DEF
bool
list_vm_remove_first( LIST_VM_FULL_TYPE * list )
{
	bool ret = false;
	tg_assert( list );

	if ( !list->first )
	{
		tg_assert( !list->last );
		return ret;
	}

	if ( list->last == list->first )
	{
		list->last = NULL;
	}

	LIST_VM_NODE_TYPE * to_del = list->first;
	list->first = to_del->next;
	--list->length;
	pool_allocator_generic_vm_deallocate( NODES_POOL_ALLOCATOR_VM_POINTER(list), to_del );

	ret = true;

	return ret;
}

LIST_VM_DEF
void
list_vm_remove_all( LIST_VM_FULL_TYPE * list )
{
	tg_assert( list );

	while ( list_vm_remove_first( list ) ){}
}

#undef NODES_POOL_ALLOCATOR_VM_POINTER
#endif // LIST_VM_EMIT_BODY

#undef LIST_VM_NODE_TYPE_ST
#undef LIST_VM_NODE_SHORT_TYPE
#undef LIST_VM_NODE_TYPE
#undef LIST_VM_SHORT_TYPE
#undef LIST_VM_FULL_TYPE
#undef LIST_VM_DEF
#undef L_GLUE3M
#undef L_GLUE3
#undef L_GLUE2MM
#undef L_GLUE2M
#undef L_GLUE2
#undef LIST_VM_TYPE
#undef LIST_VM_TYPE_SHORT
