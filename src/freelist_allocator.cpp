#include "freelist_allocator.h"
#include "utility.h"
#include <string.h>

inline
void
memory_area_advance( Memory_Area * memory_area, s64 bytes_count )
{
	memory_area->memory =
	   (void *)( s64(memory_area->memory) + bytes_count );
	memory_area->memory_bytes_size -= bytes_count;
}

void
freelist_allocator_init( Freelist_Allocator * allocator, Memory_Area memory_area, s64 list_capacity /* = -1 */ )
{
	tg_assert( allocator );

	if ( list_capacity < 0 )
	{
		list_capacity = memory_area.memory_bytes_size / 32;
	}

	Pool_Allocator_Generic * pool_allocator =
	   ( Pool_Allocator_Generic * ) memory_to_next_64bits_alignment( memory_area.memory );
	memory_area_advance(
	   &memory_area,
	   (s64)pool_allocator - (s64)memory_area.memory
	 + size_round_to_64bits_alignment( sizeof( Pool_Allocator_Generic ) ) );
	tg_assert( memory_area.memory_bytes_size > 0 );
	pool_allocator_generic_init( pool_allocator,
	                             memory_area.memory,
	                             list_capacity,
	                             sizeof( Memory_Area_List_Node ) );

	memory_area_advance(
	   &memory_area,
	   size_round_to_64bits_alignment( list_capacity * sizeof( Memory_Area_List_Node ) ) );
	tg_assert( memory_area.memory_bytes_size > 0 );

	allocator->memory_area = memory_area;
	list_init( &allocator->freelist, pool_allocator );
	list_push_first( &allocator->freelist, &memory_area );
}

void *
freelist_allocator_allocate( Freelist_Allocator * allocator, s64 bytes_size )
{
	void * ret = NULL;

	bytes_size = size_round_to_64bits_alignment( bytes_size );
	s64 real_bytes_size = bytes_size + sizeof( s64 );

	if ( allocator->memory_area.memory_bytes_size - allocator->occupancy
	   < real_bytes_size )
	{
		return ret;
	}

	for ( Memory_Area_List_Node * node = allocator->freelist.first, * prev_node = NULL;
	      node;
	      prev_node = node, node = node->next )
	{
		Memory_Area * ma = &node->element;
		if ( ma->memory_bytes_size >= real_bytes_size )
		{
			allocator->occupancy += real_bytes_size;
			ma->memory_bytes_size -= real_bytes_size;
			ret = ma->memory;
			memory_area_advance( ma, real_bytes_size );
			*(s64*)ret = bytes_size;
			ret = ( void * ) ( (s64)ret + sizeof( s64 ) );
			if ( ! ma->memory_bytes_size )
			{
				if ( prev_node )
				{
					list_remove_next( &allocator->freelist, prev_node );
				} else
				{
					list_remove_first( &allocator->freelist );
				}
			}
			break;
		}
	}

	return ret;
}

void *
freelist_allocator_allocate_clean( Freelist_Allocator * allocator, s64 bytes_size )
{
	void * ret = freelist_allocator_allocate( allocator, bytes_size );

	if ( ret )
	{
		memset( ret, 0x0, size_round_to_64bits_alignment( bytes_size ) );
	}

	return ret;
}

void
freelist_allocator_deallocate( Freelist_Allocator * allocator, void * ptr )
{
	s64 bytes_size = *( (s64*)ptr - 1 );
	void * ptr_after = ( void * )( s64( ptr ) + bytes_size );
	Memory_Area this_ma = {};
	this_ma.memory_bytes_size = bytes_size + sizeof( s64 );
	this_ma.memory = (void*)( (s64)ptr - sizeof( s64 ) );

	for ( Memory_Area_List_Node * node = allocator->freelist.first, * prev_node = NULL;
	      node;
	      prev_node = node, node = node->next )
	{
		Memory_Area * ma = &node->element;

		if ( ptr_after < ma->memory )
		{
			if ( !prev_node )
			{
				list_push_first( &allocator->freelist, &this_ma );
			} else
			{
				list_insert_after( &allocator->freelist, &this_ma, prev_node );
			}
		} else
		if ( ptr_after == ma->memory )
		{
			node->element.memory_bytes_size += this_ma.memory_bytes_size;
			node->element.memory = this_ma.memory;
		} else
		{
			continue;
		}

		tg_assert( !prev_node
		        || (   (s64)prev_node->element.memory
		             + prev_node->element.memory_bytes_size ) <= (s64)this_ma.memory );

		if ( prev_node
		  && (   (s64)prev_node->element.memory
		       + prev_node->element.memory_bytes_size ) == (s64)this_ma.memory )
		{
			list_remove_next( &allocator->freelist, prev_node );
		}
		break;
	}
}

#define LIST_BODY_ONLY 1
#define LIST_EMIT_BODY 1
#define LIST_TYPE Memory_Area
#include "list.inc.h"
#undef LIST_BODY_ONLY
