#ifndef _STACK_GENERIC_VM_H_
#define _STACK_GENERIC_VM_H_ 1

#include "basic_types.h"

struct
Stack_Generic_VM
{
	void * mem;
	u64    element_size;
	u64    max_capacity;
	u64    current_capacity;
	u64    occupancy;
	// NOTE(theGiallo): new_size = adder + old_size * multiplier;
	f64    capacity_increment_multiplier;
	u64    capacity_increment_adder;
};

bool
stack_generic_vm_init( Stack_Generic_VM * stack, u64 element_size, u64 max_capacity, u64 initial_capacity = 0, f64 capacity_increment_multiplier = 1.5, u64 capacity_increment_adder = 0 );

bool
stack_generic_vm_reserve( Stack_Generic_VM * stack, u64 new_capacity_at_least );

bool
stack_generic_vm_push( Stack_Generic_VM * stack, void * element );

void *
stack_generic_vm_peek( Stack_Generic_VM * stack );

bool
stack_generic_vm_peek( Stack_Generic_VM * stack, void * out_copied_element );

void *
stack_generic_vm_pop( Stack_Generic_VM * stack );

bool
stack_generic_vm_pop( Stack_Generic_VM * stack, void * out_copied_element );

void
stack_generic_vm_destroy( Stack_Generic_VM * stack );

#endif /* ifndef _STACK_GENERIC_VM_H_ */
