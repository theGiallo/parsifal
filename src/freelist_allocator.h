#ifndef __FREELIST_ALLOCATOR_H__
#define __FREELIST_ALLOCATOR_H__ 1

#include "basic_types.h"

struct
Memory_Area
{
	void * memory;
	s64    memory_bytes_size;
};

#define LIST_TYPE Memory_Area
#include "list.inc.h"

struct
Freelist_Allocator
{
	Memory_Area      memory_area;
	Memory_Area_List freelist;
	s64 occupancy;
};

void
freelist_allocator_init( Freelist_Allocator * allocator, Memory_Area memory_area, s64 list_capacity = -1 );

// TODO(theGiallo, 2017-10-13): function to get size of additional memory needed
// by the freelist

void *
freelist_allocator_allocate( Freelist_Allocator * allocator, s64 bytes_size );

void *
freelist_allocator_allocate_clean( Freelist_Allocator * allocator, s64 bytes_size );

// TODO(theGiallo, 2017-10-13): allocate version that allocates in a place to
// minimize fragmentation

void
freelist_allocator_deallocate( Freelist_Allocator * allocator, void * ptr );

#endif /* ifndef __FREELIST_ALLOCATOR_H__ */
