#include "hash_table_generic_vm.h"
#include "utility.h"

#define LIST_VM_EMIT_BODY 1
#define LIST_VM_BODY_ONLY 1
#define LIST_VM_TYPE Hash_Table_Generic_VM_Element
#define LIST_VM_TYPE_SHORT HTGVM_Element
#include "list_vm.inc.h"
#undef LIST_VM_EMIT_BODY
#undef LIST_VM_BODY_ONLY


#define ELEMENT_MATCH_HASH_AND_KEY(element,the_hash,the_key,key_size) \
	( (element).hash == (the_hash) \
	  && ( (key_size) == -1 \
	     ? string_equal( (u8*)(element).key, (u8*)(the_key) ) \
	     : memcmp( (element).key, (the_key), (key_size) ) == 0 ) )

#define HASH_KEY(ht,key) \
	if ( (ht)->key_size == -1 ) \
	{ \
		hash = u64_FNV1a( (char*)(key) ); \
	} else \
	{ \
		hash = u64_FNV1a( (key), (ht)->key_size ); \
	}

bool
hash_table_generic_vm_init( Hash_Table_Generic_VM * ht,
                            s64 key_size,
                            u64 max_capacity,
                            u32 table_length,
                            HTGVM_Element_List_VM * table,
                            Pool_Allocator_Generic_VM * element_list_nodes_pool_p )
{
	tg_assert( ht );
	tg_assert( key_size );
	tg_assert( table_length );

	bool ret = false;

	ht->key_size = key_size;
	ht->table_length = table_length;

	ht->table = table;
	ht->element_list_nodes_pool_p = element_list_nodes_pool_p;

	u64 table_size_bytes = sizeof( HTGVM_Element_List_VM ) * table_length;
	ht->table_is_extern = table;
	if ( !table )
	{
		table = (HTGVM_Element_List_VM*)sys_mem_reserve( table_size_bytes );
		if ( table )
		{
			if ( !sys_mem_commit( table, table_size_bytes ) )
			{
				sys_mem_free( table, table_size_bytes );
				table = 0;
				return ret;
			}
		} else
		{
			return ret;
		}
		ht->table = table;
	}
	memset( ht->table, 0x0, table_size_bytes );

	Pool_Allocator_Generic_VM * ht_element_list_nodes_pool_p = element_list_nodes_pool_p;
	if ( !element_list_nodes_pool_p )
	{
		ht->element_list_nodes_pool = {};
		// NOTE(theGiallo): here max_capacity is correct because the lists are not resident
		pool_allocator_generic_vm_init( &ht->element_list_nodes_pool,
		                                sizeof(HTGVM_Element_List_VM_Node),
		                                max_capacity, -1 );

		ht_element_list_nodes_pool_p = &ht->element_list_nodes_pool;
	}
	ht->occupancy = 0;
	for ( u64 i = 0; i != table_length; ++i )
	{
		list_vm_init( ht->table + i, ht_element_list_nodes_pool_p );
	}
	ret = true;

	return ret;
}

void
hash_table_generic_vm_destroy( Hash_Table_Generic_VM * ht )
{
	tg_assert( ht );

	ht->occupancy = 0;

	if ( ht->table_is_extern )
	{
		sys_mem_free( ht->table, sizeof( HTGVM_Element_List_VM ) * ht->table_length );
	}

	if ( !ht->element_list_nodes_pool_p )
	{
		pool_allocator_generic_vm_destroy( &ht->element_list_nodes_pool );
	} else
	{
		for ( u32 i = 0; i != ht->table_length; ++i )
		{
			list_vm_remove_all( ht->table + i );
		}
	}
}

bool
hash_table_generic_vm_insert( Hash_Table_Generic_VM * ht, void * key, void * value )
{
	tg_assert( ht );
	tg_assert( key );
	tg_assert( !ht->should_enforce_uniqueness );

	bool ret;

	u64 hash;
	HASH_KEY( ht, key );

	Hash_Table_Generic_VM_Element e = { .hash = hash, .key = key, .value = value };

	u32 index = hash % ht->table_length;

	ret = list_vm_push_last( ht->table + index, &e );
	if ( ret )
	{
		++ht->occupancy;
	}

	return ret;
}

bool
hash_table_generic_vm_insert_unique_or_get_one( Hash_Table_Generic_VM * ht, void * key, void * value, void ** out_value )
{
	tg_assert( ht );
	tg_assert( key );

	bool ret = false;

	u64 hash;
	HASH_KEY( ht, key );

	Hash_Table_Generic_VM_Element e = { .hash = hash, .key = key, .value = value };

	u32 index = hash % ht->table_length;

	HTGVM_Element_List_VM * list = ht->table + index;
	bool already_present = false;
	for ( HTGVM_Element_List_VM_Node * node = list->first;
	      node;
	      node = node->next )
	{
		if ( ELEMENT_MATCH_HASH_AND_KEY( node->element, hash, key, ht->key_size ) )
		{
			already_present = true;
			*out_value = node->element.value;
			break;
		}
	}
	if ( !already_present )
	{
		// NOTE(theGiallo): we don't insert last to avoid a potential cache miss
		ret = list_vm_push_first( ht->table + index, &e );
		if ( ret )
		{
			++ht->occupancy;
		}
		*out_value = 0;
	}

	return ret;
}

bool
hash_table_generic_vm_insert_unique( Hash_Table_Generic_VM * ht, void * key, void * value )
{
	tg_assert( ht );
	tg_assert( key );

	bool ret = false;

	u64 hash;
	HASH_KEY( ht, key );

	Hash_Table_Generic_VM_Element e = { .hash = hash, .key = key, .value = value };

	u32 index = hash % ht->table_length;

	HTGVM_Element_List_VM * list = ht->table + index;
	bool already_present = false;
	for ( HTGVM_Element_List_VM_Node * node = list->first;
	      node;
	      node = node->next )
	{
		if ( ELEMENT_MATCH_HASH_AND_KEY( node->element, hash, key, ht->key_size ) )
		{
			already_present = true;
			break;
		}
	}
	if ( !already_present )
	{
		// NOTE(theGiallo): we don't insert last to avoid a potential cache miss
		ret = list_vm_push_first( ht->table + index, &e );
		if ( ret )
		{
			++ht->occupancy;
		}
	}

	return ret;
}

bool
hash_table_generic_vm_insert_enforce_uniqueness_if_necessary( Hash_Table_Generic_VM * ht, void * key, void * value )
{
	tg_assert( ht );
	tg_assert( key );

	bool ret;
	if ( ht->should_enforce_uniqueness )
	{
		ret = hash_table_generic_vm_insert_unique( ht, key, value );
	} else
	{
		ret = hash_table_generic_vm_insert( ht, key, value );
	}
	return ret;
}

bool
hash_table_generic_vm_get_one( Hash_Table_Generic_VM * ht, void * key, void ** out_value )
{
	tg_assert( ht );
	tg_assert( key );

	bool ret = false;

	u64 hash;
	HASH_KEY( ht, key );

	u32 index = hash % ht->table_length;

	HTGVM_Element_List_VM * list = ht->table + index;
	for ( HTGVM_Element_List_VM_Node * node = list->first;
	      node;
	      node = node->next )
	{
		if ( ELEMENT_MATCH_HASH_AND_KEY( node->element, hash, key, ht->key_size ) )
		{
			ret = true;
			*out_value = node->element.value;
			break;
		}
	}
	if ( !ret )
	{
		*out_value = {};
	}

	return ret;
}

bool
hash_table_generic_vm_remove_and_get_one( Hash_Table_Generic_VM * ht, void * key, void ** out_value )
{
	tg_assert( ht );
	tg_assert( key );

	bool ret = false;

	u64 hash;
	HASH_KEY( ht, key );

	u32 index = hash % ht->table_length;

	HTGVM_Element_List_VM * list = ht->table + index;
	for ( HTGVM_Element_List_VM_Node *      node = list->first,
	                                 * prev_node = 0;
	      node;
	      prev_node = node,
	      node = node->next )
	{
		if ( ELEMENT_MATCH_HASH_AND_KEY( node->element, hash, key, ht->key_size ) )
		{
			ret = true;
			*out_value = node->element.value;
			bool b_res;
			if ( prev_node )
			{
				b_res =
				list_vm_remove_next( list, prev_node );
			} else
			{
				b_res =
				list_vm_remove_first( list );
			}
			tg_assert( b_res );
			--ht->occupancy;
			break;
		}
	}
	if ( !ret )
	{
		*out_value = 0;
	}

	return ret;
}

bool
hash_table_generic_vm_remove_one( Hash_Table_Generic_VM * ht, void * key )
{
	tg_assert( ht );
	tg_assert( key );

	bool ret = false;

	u64 hash;
	HASH_KEY( ht, key );

	u32 index = hash % ht->table_length;

	HTGVM_Element_List_VM * list = ht->table + index;
	for ( HTGVM_Element_List_VM_Node *      node = list->first,
	                                 * prev_node = 0;
	      node;
	      prev_node = node,
	      node = node->next )
	{
		if ( ELEMENT_MATCH_HASH_AND_KEY( node->element, hash, key, ht->key_size ) )
		{
			ret = true;
			bool b_res;
			if ( prev_node )
			{
				b_res =
				list_vm_remove_next( list, prev_node );
			} else
			{
				b_res =
				list_vm_remove_first( list );
			}
			tg_assert( b_res );
			--ht->occupancy;
			break;
		}
	}

	return ret;
}

bool
hash_table_generic_vm_get_all( Hash_Table_Generic_VM * ht, void * key, HTGVM_Element_List_VM * out_values_list )
{
	tg_assert( ht );
	tg_assert( key );

	bool ret = false;

	u64 hash;
	HASH_KEY( ht, key );

	u32 index = hash % ht->table_length;

	HTGVM_Element_List_VM * list = ht->table + index;
	for ( HTGVM_Element_List_VM_Node * node = list->first;
	      node;
	      node = node->next )
	{
		if ( ELEMENT_MATCH_HASH_AND_KEY( node->element, hash, key, ht->key_size ) )
		{
			ret = true;
			list_vm_push_first( out_values_list, &node->element );
		}
	}

	return ret;
}

bool
hash_table_generic_vm_remove_and_get_all( Hash_Table_Generic_VM * ht, void * key, HTGVM_Element_List_VM * out_values_list )
{
	tg_assert( ht );
	tg_assert( key );

	bool ret = false;

	u64 hash;
	HASH_KEY( ht, key );

	u32 index = hash % ht->table_length;

	HTGVM_Element_List_VM * list = ht->table + index;
	for ( HTGVM_Element_List_VM_Node *      node = list->first,
	                                 * prev_node = 0;
	      node;
	      prev_node = node,
	      node = node ? node->next : list->first )
	{
		if ( ELEMENT_MATCH_HASH_AND_KEY( node->element, hash, key, ht->key_size ) )
		{
			ret = true;
			list_vm_push_first( out_values_list, &node->element );
			bool b_res;
			if ( prev_node )
			{
				b_res =
				list_vm_remove_next( list, prev_node );
			} else
			{
				b_res =
				list_vm_remove_first( list );
			}
			node = prev_node;
			tg_assert( b_res );
			--ht->occupancy;
		}
	}

	return ret;
}

bool
hash_table_generic_vm_remove_all( Hash_Table_Generic_VM * ht, void * key )
{
	tg_assert( ht );
	tg_assert( key );

	bool ret = false;

	u64 hash;
	HASH_KEY( ht, key );

	u32 index = hash % ht->table_length;

	HTGVM_Element_List_VM * list = ht->table + index;
	for ( HTGVM_Element_List_VM_Node *      node = list->first,
	                                 * prev_node = 0;
	      node;
	      prev_node = node,
	      node = node ? node->next : list->first )
	{
		if ( ELEMENT_MATCH_HASH_AND_KEY( node->element, hash, key, ht->key_size ) )
		{
			ret = true;
			bool b_res;
			if ( prev_node )
			{
				b_res =
				list_vm_remove_next( list, prev_node );
			} else
			{
				b_res =
				list_vm_remove_first( list );
			}
			node = prev_node;
			tg_assert( b_res );
			--ht->occupancy;
		}
	}

	return ret;
}

#undef ELEMENT_MATCH_HASH_AND_KEY
#undef HASH_KEY
