#!/bin/bash

BIN_NAME=bin/parsifal


ALL=false
DEBUG=false
RELEASE=false
PROFILE=false

if [ $# == 0 ]
then
	ALL=true
else

	while [[ $# > 0 ]]
	do
	key="$1"

	case $key in
		-d|--debug)
			DEBUG=true
			CMD="./"$BIN_NAME"_d"
		;;
		-dopt|--debug-opt)
			DEBUG=true
			CMD="./"$BIN_NAME"_dopt"
		;;
		-r|--release)
			RELEASE=true
			CMD="./"$BIN_NAME
		;;
		-p|--gprof)
			PROFILE=true
			CMD="./"$BIN_NAME"_prof"
		;;
		-P|--perf)
			PERF=true
			CMD="./"$BIN_NAME"_perf"
	;;
		*)
			echo "Unknown option "$1
		;;
	esac
	shift
	done;
fi

$CMD tests/test.txt
