#!/bin/bash

ALL=false
DEBUG=false
DEBUG_OPT=false
RELEASE=false
PROFILE=false
PERF=false

if [ $# == 0 ]
then
	ALL=true
else

	while [[ $# > 0 ]]
	do
	key="$1"

	case $key in
		-d|--debug)
			DEBUG=true
		;;
		-dopt|--debug-opt)
			DEBUG_OPT=true
		;;
		-r|--release)
			RELEASE=true
		;;
		-p|--gprof)
			PROFILE=true
		;;
		-P|--perf)
			PERF=true
		;;
		*)
			echo "Unknown option "$1
		;;
	esac
	shift
	done;
fi

unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     machine=Linux;;
    Darwin*)    machine=Mac;;
    CYGWIN*)    machine=Cygwin;;
    MINGW*)     machine=MinGw;;
    *)          machine="UNKNOWN:${unameOut}"
esac
echo ${machine}


if [ ! -d bin ]
then
	mkdir bin 2> /dev/null
fi
BIN_NAME=parsifal
BIN_PATH=bin/

TIMER='time -p '
#GPP="g++-5"
# GPP="g++ -fdiagnostics-color=auto"
if [[ ${machine} = "Linux" ]]
then
	GPP="clang++-3.9 -fuse-ld=gold"
fi
if [[ ${machine} = "Mac" ]]
then
	GPP="clang++"
fi
#GPP="clang++-3.9 -fuse-ld=gold"
#GPP="clang++-3.8"
#GPP="clang++-3.6"
#GPP="g++-6"
#GPP="g++-4.8"
GPP=$TIMER$GPP
CPP_STD="-std=c++11"

declare -a CPP_SOURCES
CPP_SOURCES[${#CPP_SOURCES[@]}]='src/main.cpp'
#CPP_SOURCES[${#CPP_SOURCES[@]}]=`find src -name \*.cpp | sed -r 's/(.*game_api.*|.*tgmath.cpp)//g' | xargs echo`
#CPP_SOURCES[${#CPP_SOURCES[@]}]=`find src -name \*.cpp | sed -r 's/(.*game_api.*|.*tgmath.cpp)//g'`

declare -a INCLUDE_DIRS
INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]="src"
#INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]="./libraries/kiss_fft130/"
#INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]="./libraries/lepre/"
#INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]="./libraries/lodepng/"
#INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]="./libraries/stb_truetype/"
#INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]="./libraries/stb_vorbis/"
#
# INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]="./libraries/SDL2_mixer/linux/include/"

declare -a INCLUDE_QUOTE_DIRS
#SDL_LIBS="$(./libraries/SDL2/linux/bin/sdl2-config --libs)"
declare -a LIBS_DIRS
# LIBS_DIRS[${#LIBS_DIRS[@]}]="./libraries/SDL2_ttf/linux/lib64/"
# LIBS_DIRS[${#LIBS_DIRS[@]}]="./ibraries/SDL2_mixer/linux/lib64/"

declare -a LIBS
# LIBS[${#LIBS[@]}]=SDL2
# LIBS[${#LIBS[@]}]=SDL2_ttf
# LIBS[${#LIBS[@]}]=SDL2_mixer
#LIBS[${#LIBS[@]}]=GL
#LIBS[${#LIBS[@]}]=GLEW
# LIBS[${#LIBS[@]}]=GLU
LIBS[${#LIBS[@]}]=m

declare -a DEFINES
# DEFINES[${#DEFINES[@]}]="PIPPO=1"
#DEFINES[${#DEFINES[@]}]="WE_LOAD_OUR_GL=1"
#DEFINES[${#DEFINES[@]}]="THE_MEMORY_MALLOCED=1"

declare -a CPP_FLAGS
CPP_FLAGS[${#CPP_FLAGS[@]}]='-Wall'
CPP_FLAGS[${#CPP_FLAGS[@]}]='-Wextra'
CPP_FLAGS[${#CPP_FLAGS[@]}]='-fno-exceptions'
# CPP_FLAGS[${#CPP_FLAGS[@]}]=''
CPP_FLAGS[${#CPP_FLAGS[@]}]='-Wno-missing-braces'
CPP_FLAGS[${#CPP_FLAGS[@]}]='-Wfatal-errors'
CPP_FLAGS[${#CPP_FLAGS[@]}]='-rdynamic'
#CPP_FLAGS[${#CPP_FLAGS[@]}]='-fsplit-stack'

#declare -a GAME_API_CPP_FLAGS
#GAME_API_CPP_FLAGS[${#GAME_API_CPP_FLAGS[@]}]='-fPIC'
#GAME_API_CPP_FLAGS[${#GAME_API_CPP_FLAGS[@]}]='-shared'
#GAME_API_CPP_FLAGS[${#GAME_API_CPP_FLAGS[@]}]='-fno-exceptions'
# CPP_FLAGS[${#CPP_FLAGS[@]}]=''

function \
include_quote_dirs()
{
	for p in ${INCLUDE_QUOTE_DIRS[@]}
	do
		printf " -iquote$p "
	done
}
function \
include_dirs()
{
	for p in ${INCLUDE_DIRS[@]}
	do
		printf " -I$p "
	done
}
function \
libs_dirs()
{
	for p in ${LIBS_DIRS[@]}
	do
		printf " -L$p -Wl,-rpath=$p "
	done
}
function \
link_libs()
{
	for l in ${LIBS[@]}
	do
		printf " -l$l "
	done
}
function \
defines()
{
	for d in ${DEFINES[@]}
	do
		printf " -D$d "
	done
}
function \
cpp_flags()
{
	for f in ${CPP_FLAGS[@]}
	do
		printf " $f "
	done
}
function \
game_api_cpp_flags()
{
	for f in ${GAME_API_CPP_FLAGS[@]}
	do
		printf " $f "
	done
}
function \
game_api_cpp_sources()
{
	for f in ${GAME_API_CPP_SOURCES[@]}
	do
		printf " $f "
	done
}
function \
main_flags()
{
	printf " $CPP_STD "
	cpp_flags
	defines
	include_quote_dirs
	include_dirs
	printf " $SDL_INCLUDE "
	libs_dirs
	link_libs
	printf " $SDL_LIBS "
}
function \
game_api_flags()
{
	printf " $CPP_STD "
	cpp_flags
	defines
	include_quote_dirs
	include_dirs
	libs_dirs
	link_libs
	printf " -shared -fPIC "
}


function \
delete_other_dls()
{
	ESC_BP=$(echo $BIN_PATH | sed -e 's/[\/&]/\\&/g')
	ESC_NAME=$( echo $GAME_API_SO_NAME | sed -e 's/[]\/$*.^|[]/\\&/g')
	ESC_SFX=$( echo $SUFFIX | sed -e 's/[]\/$*.^|[]/\\&/g')
	ESC_EXT=$( echo $GAME_API_SO_EXTENSION | sed -e 's/[]\/$*.^|[]/\\&/g')
	MATCH=$ESC_NAME$ESC_SFX'_[0-9]*'$ESC_EXT
	ls -1 $BIN_PATH | grep -e "$MATCH"| \
	sed -sr 's/('$MATCH')/'$ESC_BP'\1/g' | \
	tr '\n' ' ' | \
	xargs trash
}

if $ALL || $DEBUG
then
	echo "----------------------------------------"
	echo " Going to compile [debug]"
	echo

	SUFFIX="_d"
	#  -Wno-write-strings
	echo compiler: $GPP
	echo version options: -O0 -ggdb3 -fstack-protector-all -DDEBUG=1 -DDEBUG_GL=1 -DBIN_SUFFIX='"'$SUFFIX'"'
	echo flags: `main_flags`
	echo sources: $CPP_SOURCES
	echo output: $BIN_PATH$BIN_NAME$SUFFIX

	#$GPP -O0 -ggdb3 -fstack-protector-all -DDEBUG=1 -DDEBUG_GL=1 -DBIN_SUFFIX='"'$SUFFIX'"' \
	#     `main_flags` -Wno-unused-function \
	#     $CPP_SOURCES -o $BIN_PATH$BIN_NAME$SUFFIX
	command=" -O0 -ggdb3 -fstack-protector-all -DDEBUG=1 -DDEBUG_GL=1 -DBIN_SUFFIX=\"$SUFFIX\" "
	command=$command"`main_flags` -Wno-unused-function "
	command=$command"`echo $CPP_SOURCES | xargs echo` -o $BIN_PATH$BIN_NAME$SUFFIX"
	echo "command: $GPP $command"
	echo

	$GPP $command
	ret=$?

	command="clang++ $command"

	echo "["                                                           >  compile_commands.json
	for f in $CPP_SOURCES
	do
		echo "{"                                                       >> compile_commands.json
		echo "	\"directory\":\"`pwd`\","                              >> compile_commands.json
		echo "	\"command\": \"`echo $command | sed 's/"/\\\\"/g'`\"," >> compile_commands.json
		echo "	\"file\":\"$f\""                                       >> compile_commands.json
		echo "},"                                                      >> compile_commands.json
	done


	if [[ $ret != 0 ]]
	then
		echo "main debug compilation failed! Terminating build script."
		exit $ret
	else
		echo "main debug compilation succeeded!"
	fi
fi


if $ALL || $DEBUG_OPT
then
	echo "----------------------------------------"
	echo " Going to compile [debug opt]"
	echo

	SUFFIX="_dopt"
	#  -Wno-write-strings
	echo compiler: $GPP
	echo version options: -O3 -ggdb3 -DDEBUG=1 -DDEBUG_GL=1 -DBIN_SUFFIX='"'$SUFFIX'"'
	echo flags: `main_flags`
	echo sources: $CPP_SOURCES
	echo output: $BIN_PATH$BIN_NAME$SUFFIX

	$GPP -O3 -ggdb3 -DDEBUG=1 -DDEBUG_GL=1 -DBIN_SUFFIX='"'$SUFFIX'"' \
		`main_flags` -Wno-unused-function \
		$CPP_SOURCES -o $BIN_PATH$BIN_NAME$SUFFIX


	ret=$?
	if [[ $ret != 0 ]]
	then
		echo "main debug compilation failed! Terminating build script."
		exit $ret
	else
		echo "main debug compilation succeeded!"
	fi
fi

if $ALL || $RELEASE
then
	echo "----------------------------------------"
	echo " Going to compile [release]"
	echo

	unset SUFFIX

	echo compiler: $GPP
	echo version options: -O3 -DNDEBUG -DBIN_SUFFIX='"'$SUFFIX'"'
	echo flags: `main_flags`
	echo sources: $CPP_SOURCES
	echo output: $BIN_PATH$BIN_NAME$SUFFIX


	#  -Wno-write-strings
	$GPP -O3 -DNDEBUG -DBIN_SUFFIX='"'$SUFFIX'"' \
	     `main_flags`\
	     $CPP_SOURCES -o $BIN_PATH$BIN_NAME$SUFFIX

	ret=$?
	if [[ $ret != 0 ]]
	then
		echo "main release compilation failed! Terminating build script."
		exit $ret
	else
		echo "main release compilation succeeded!"
	fi
fi

if $ALL || $PROFILE
then
	echo "----------------------------------------"
	echo " Going to compile [profiling]"

	SUFFIX="_gprof"

	#  -Wno-write-strings
	$GPP $CPP_STD -pg `cpp_flags` -O0 -ggdb3 -DDEBUG=0 -DNDEBUG \
	 `defines` `include_quote_dirs` `include_dirs` `libs_dirs` `link_libs` \
	 $CPP_SOURCES -o $BIN_PATH$BIN_NAME$SUFFIX

	ret=$?
	if [[ $ret != 0 ]]
	then
		echo "main profiling compilation failed! Terminating build script."
		exit $ret
	else
		echo "main profiling compilation succeeded!"
	fi
fi


if $ALL || $PERF
then
	echo -e "----------------------------------------\n"\
	        " Going to compile [perf]\n"

	SUFFIX="_perf"

	#  -Wno-write-strings
	$GPP $CPP_STD -fno-omit-frame-pointer `cpp_flags` -O3 -DDEBUG=0 -DNDEBUG \
	 `defines` `include_quote_dirs` `include_dirs` `libs_dirs` `link_libs` \
	 $CPP_SOURCES -o $BIN_PATH$BIN_NAME$SUFFIX

	ret=$?
	if [[ $ret != 0 ]]
	then
		echo "main profiling compilation failed! Terminating build script."
		exit $ret
	else
		echo "main profiling compilation succeeded!"
	fi
fi
